import server
mod = server.problems.load_language('python', 'sl')

name = 'Hello world!'
slug = 'Hello world!'

description = '''\
<p>Napišite program, ki izpiše "Hello world!".</p>'''

printing = '''\
<p>V Pythonu izpisujemo s funkcijo <code>print</code>.</p>'''

syntax_error = '''\
<p>Primer programa, ki bi pravilno izpisal samo "Hello!": <code>print("Hello!")</code></p>'''

plan = ['''\
<p>Program izvedemo z uporabo funkcije <code>print</code>, ki naj izpiše "Hello world!".</p>
''',
        printing,
        syntax_error
        ]

hint = {
    'printing': printing,

    'syntax_error' : [
        mod.general_msg['error_head'],
        mod.general_msg['general_exception'],
        syntax_error
    ],


    'final_hint' : [
            '''\
<p>Odlično, program deluje!<br>
Pri tej nalogi si se naučil, kako enostavno je uporabiti funkcijo <code>print</code>.</p>
''']
}
