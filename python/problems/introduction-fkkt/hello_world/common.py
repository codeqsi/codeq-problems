from python.util import has_token_sequence, string_almost_equal, \
    string_contains_number, get_tokens, get_numbers, get_exception_desc
from server.hints import Hint
import re

id = 25000
number = 1
visible = True

solution = '''\
print("Hello world!")
'''

hint_type = {
    'syntax_error': Hint('syntax_error'),
    'printing': Hint('printing'),
    'final_hint': Hint('final_hint'),
}

def test(python, code, aux_code=''):
    # List of inputs: (expression to eval, stdin).
    test_in = [
        (None, '')
    ]
    test_out = [
        'Hello world!'
    ]

    # List of outputs: (expression result, stdout, stderr, exception).
    answers = python(code=aux_code+code, inputs=test_in, timeout=1.0)

    if answers[0][1].rstrip() == 'Hello world!':
        n_correct = 1
        passed = True
    else:
        n_correct = 0
        passed = False

    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_in)}}]
    if passed:
        hints.append({'id': 'final_hint'})
    return passed, hints

def hint(python, code, aux_code=''):
    tokens = get_tokens(code)

    # run one test first to see if there are any exceptions
    test_in = [(None, '')]
    answer = python(code=aux_code+code, inputs=test_in, timeout=1.0)
    exc = answer[0][3]
    exc_hint = get_exception_desc(answer[0][3])
    if exc:
        if 'SyntaxError' in exc:
            return [{'id':'syntax_error', 'args': {'message': exc}}]
        else:
            return exc_hint

    # student is not using print function
    if not has_token_sequence(tokens, ['print']):
        return [{'id' : 'printing'}]

    return None
