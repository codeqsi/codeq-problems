import server
mod = server.problems.load_language('python', 'sl')

name = 'Kako ti je ime?'
slug = 'Kako ti je ime?'

description = '''\
<p>Napišite program, ki uporabnika povpraša najprej po imenu in na zaslon izpiše dolžino tega imena.</p>
<p>Primer uporabe:</p>
<pre>
Vpiši ime: Janez 
Dolžina imena Janez je 5
</pre>'''

no_input_call = ['''\
<p>Uporabi funkcijo <code>input</code> in shrani ime.</p>''',
                 '''\
<p>Funkcija <code>input</code> sprejme niz (<em>angl.</em> string), ki se
prikaže uporabniku kot vprašanje in vrača, kar je uporabnik napisal. </p>''',
                 '''\
<p>Primer:</p>
<pre>
ime = input("Kako ti je ime?")
</pre>
<p>pokliče funkcijo <code>input</code>, ki povpraša uporabnika po imenu in
shrani uporabnikov odgovor v spremenljivko <code>ime</code>.</p>''']

printing = ['''\
<p>V Pythonu izpisujemo s funkcijo <code>print</code>.</p>''',
            '''\
</p>Če želimo izpisati več elementov, jih ločimo z vejico ali pa
sestavimo elemente v en niz.
Imejmo spremenljivko <code>ime</code>, ki vsebuje naše ime, potem:
<pre>
print("Ime mi je", ime, ".")
</pre>''']

plan = ['''\
<p>Program izvedemo v dveh korakih:</p>
<ol>
  <li>Vprašaj uporabnika po imenu.</li>
  <li>Izpiši potreben niz.</li>
</ol>''',
        no_input_call,
        printing
        ]

hint = {
    'no_input_call': no_input_call,

    'printing': printing,

    'name_error' : [
        mod.general_msg['error_head'],
        mod.general_msg['general_exception'],
        mod.general_msg['name_error'],
        '''\
<p>Verjetno uporabljaš spremenljivko. Ali pri izpisu morda poskušaš
izpisati napačno spremenljivko?</p>'''
    ],

    'final_hint' : [
            '''\
<p>Odlično, program deluje!<br>
Pri tej nalogi si se naučil uporabljati funkcijo <code>input</code>
in izpisati več elementov hkrati.
''']
}
