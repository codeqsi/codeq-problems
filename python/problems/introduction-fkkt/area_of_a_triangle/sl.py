import server
mod = server.problems.load_language('python', 'sl')

name = 'Ploščina pravokotnega trikotnika'
slug = 'Ploščina pravokotnega trikotnika'

description = '''\
<p>Program iz prejšnje naloge spremenite tako, da izračuna in izpiše ploščino pravokotnega trikotnika.<br/>
Primer uporabe:</p>
<pre>
Prva kateta [cm]: 3
Druga kateta [cm]: 4
Ploščina = 6.0 cm^2
</pre>
'''

no_input_call = ['''\
<p>Tako kot pri prejšnji nalogi za branje uporabimo funkcijo <code>input</code></p>''',
                 '''\
<p>Preberemo dve vrednosti in jih shranimo v dve spremenljivki:</p>
<pre>
 a = float(input("Prva kateta: "))
 b = float(input("Druga kateta: "))
</pre>''',
                '''\
<p> Imeni <code>a</code> in <code>b</code> sta spremenljivki (angl. variable).
Spremenljivke uporabljamo, kadar želimo kakšno vrednost shraniti, ki jo bomo
potrebovali kasneje v programu. Imena spremenljivk so lahko poljubno
 dolga, v našem primeru bi jim lahko rekli tudi <code>kateta_a</code> in <code>kateta_b</code>.
 Pri programiranju velja, da izbiramo taka imena spremenljivk,
 ki bodo naredila program berljiv. </p>
 '''
]

math_functions =  ['''\
<p>Ploščina pravokotnega trikotnika je polovica zmnožka obeh katet.</p>''',
                   '''\
<pre>
p = (a * b) / 2
</pre>'''
]

printing = ['''\
<p> V Pythonu izpisujemo s funkcijo <code>print</code>. </p>''',
            '''\
<p>Če želimo izpisati več elementov,
jih ločimo z vejico.  Recimo, da imamo spremenljivko <code>ime</code>,
ki vsebuje naše ime, potem lahko napišemo:</p>
<pre>
print("Ime mi je", ime, ".")
</pre>''']

plan = ['''\
<p>Program razdelimo na tri dele, kot pri prejšnji nalogi:</p>
<ol>
    <li>Preberi vrednosti katet (a,b = ?)</li>
    <li>Izračunaj ploščino trikotnika p (p = …)</li>
    <li>Izpis ploščine trikotnika (print … )</li>
</ol>
''',
        no_input_call,
        math_functions,
        printing]

hint = {
    'no_input_call': no_input_call,

    'math_functions': math_functions,

    'printing': printing,

    'name_error' : [mod.general_msg['error_head'], mod.general_msg['general_exception'],
    mod.general_msg['name_error'], '''
    <p>Verjetno uporabljaš spremenljivko, ki nima vrednosti. Ali v izrazu za izračun
     uporabljaš napačno spremenljivko? Ali pri izpisu morda poskušaš
     izpisati napačno spremenljivko?</p>'''],

    'unsupported_operand' : [mod.general_msg['error_head'], mod.general_msg['general_exception'],
    mod.general_msg['type_error'], '''
<p>Verjetni razlog: funkcija <code>input</code> vrača vrednost tipa niz,
ki jo moramo najprej pretvoriti v tip <code>float</code>, če želimo z njo računati:</p>
<pre>
v = float(input(" ...
</pre>
'''],

    'final_hint' : [
        '''\
<p>Program deluje pravilno! <br>
To pomeni, da znaš prebrati števila in na njih uporabljati matematične operacije.
</p>''']

}
