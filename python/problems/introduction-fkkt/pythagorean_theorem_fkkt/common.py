from python.util import has_token_sequence, string_almost_equal, \
    string_contains_number, get_tokens, get_numbers, get_exception_desc
from server.hints import Hint

id = 25002
number = 3
visible = True

solution = '''\
from math import *
a = float(input("Prva kateta: "))
b = float(input("Druga kateta: "))
c = sqrt(a ** 2 + b ** 2)
print("Hipotenuza trikotnika s stranicama", a, "in", b, "je", c)
'''

hint_type = {
    'plan': Hint('plan'),
    'name_error': Hint('name_error'),
    'unsupported_operand': Hint('unsupported_operand'),
    'no_input_call' : Hint('no_input_call'),
    'printing': Hint('printing'),
    'math_functions': Hint('math_functions'),
    'final_hint': Hint('final_hint')
}

def test(python, code, aux_code=''):
    # List of inputs: (expression to eval, stdin).
    test_in = [
        (None, '3\n4\n'),
        (None, '4\n3\n'),
        (None, '12\n5\n'),
        (None, '5\n12\n'),
        (None, '5\n0\n'),
        (None, '0\n5\n'),
    ]

    test_out = [
        "Hipotenuza = 5.0 cm",
        "Hipotenuza = 5.0 cm",
        "Hipotenuza = 13.0 cm",
        "Hipotenuza = 13.0 cm",
        "Hipotenuza = 5.0 cm",
        "Hipotenuza = 5.0 cm",
    ]

    # List of outputs: (expression result, stdout, stderr, exception).
    answers = python(code=aux_code+code, inputs=test_in, timeout=1.0)
    outputs = [ans[1] for ans in answers]

    n_correct = 0
    tin = None
    for i, (output, correct) in enumerate(zip(outputs, test_out)):
        if output.rstrip().endswith(correct):
        #if string_almost_equal(output, float(correct), prec=2):
            n_correct += 1
        else:
            tin = test_in[i][1]
            tout = correct

    passed = n_correct == len(test_in)
    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_in)}}]
    if tin:
        hints.append({'id': 'problematic_test_case', 'args': {'testin': str(tin), 'testout': str(tout)}})
    if passed:
        hints.append({'id': 'final_hint'})
    return passed, hints

def hint(python, code, aux_code=''):
    tokens = get_tokens(code)

    # run one test first to see if there are any exceptions
    test_in = [(None, '3\n4\n')]
    answer = python(code=aux_code+code, inputs=test_in, timeout=1.0)
    exc = answer[0][3]
    exc_hint = get_exception_desc(answer[0][3])
    if exc:
        if 'NameError' in exc:
            return [{'id':'name_error', 'args': {'message': exc}}]
        elif 'unsupported operand' in exc or 'TypeError' in exc:
            return [{'id':'unsupported_operand', 'args': {'message': exc}}]
        else:
            return exc_hint

    # if input is not present in code, student needs to learn about input
    if not has_token_sequence(tokens, ['input']) or \
       (not has_token_sequence(tokens, ['float']) and not has_token_sequence(tokens, ['int'])) or \
       not has_token_sequence(tokens, ['=']):
        return [{'id': 'no_input_call'}]

    # if tokens sqrt or ** are not in code, we have to teach them how to 
    # use math functions.
    if not has_token_sequence(tokens, ['**']):
        return [{'id' : 'math_functions'}]

    # student is not using print function
    if not has_token_sequence(tokens, ['print']):
        return [{'id' : 'printing'}]

    return None
