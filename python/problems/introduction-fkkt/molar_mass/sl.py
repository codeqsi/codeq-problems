import server
mod = server.problems.load_language('python', 'sl')

name = 'Molska masa'
slug = 'Molska masa'

description = '''\
<p>Molsko maso lahko izračunamo elementom in spojinam. V periodnem sistemu najdemo relativne atomske mase in iz njih izračunamo molske mase. Relativna atomska masa neona je A<sub>r</sub>(Ne) = 20.2. Iz tega sledi, da je molska masa neona M(Ne) = 20.2 g/mol. Pri izračunu molske mase večatomnih molekul (elementov in spojin) moramo upoštevati število posameznih atomov v molekuli. Tako je relativna atomska masa vodika A<sub>r</sub>(H) = 1.0, relativna atomska masa kisika pa A<sub>r</sub>(O) = 16.0. Iz tega sledi, da je relativna molekulska masa vode M<sub>r</sub>(H<sub>2</sub>O) = 2·1.0 + 16.0 = 18.0. Molska masa vode pa je M(H<sub>2</sub>O) = 18.0 g/mol.</p>
<p>Bakrov(II) sulfat je kemijska spojina s formulo CuSO<sub>4</sub>. Spojina ima pravzaprav več kemijskih formul, ki so odvisne od stopnje hidratacije. Tako je <a href="https://sl.wikipedia.org/wiki/Galica">modra galica</a> bakrov(II) sulfat pentahidrat s formulo CuSO<sub>4</sub>·5H<sub>2</sub>O. Izračunajte molsko maso na podlagi preglednice in vnosa uporabnika, ki vnese stopnjo hidracije oz. število molekul vode.</p>
<center id="yui_3_17_2_1_1507822990439_110">
<table>
<thead>
<tr>
<th scope="col" style="border: 1px solid black;">element</th>
<th scope="col" style="border: 1px solid black;">relativna atomska masa</th>
</tr>
</thead>
<tbody>
<tr>
<td style="border: 1px solid black;">Cu</td>
<td style="border: 1px solid black;">63.5</td>
</tr>
<tr>
<td style="border: 1px solid black;">S</td>
<td style="border: 1px solid black;">32.1</td>
</tr>
<tr>
<td style="border: 1px solid black;">O</td>
<td style="border: 1px solid black;">16.0</td>
</tr>
<tr>
<td style="border: 1px solid black;">H</td>
<td style="border: 1px solid black;">1.0</td>
</tr>
</tbody>
</table>
</center>
<p>Primer uporabe:</p>
<pre>Stopnja hidracije: 5 
Molska masa je 249.6 g/mol</pre>
'''

no_input_call = ['''\
<p>Tako kot pri prejšnji nalogi za branje uporabimo funkcijo <code>input</code></p>''',
                 '''\
<p>Preberemo vrednost in jo shranimo v spremenljivko:</p>
<pre>
 h = float(input("Stopnja hidracije: "))
</pre>''',
                '''\
<p> Ime <code>h</code> je spremenljivka (angl. variable).
Spremenljivke uporabljamo, kadar želimo kakšno vrednost shraniti, ki jo bomo
potrebovali kasneje v programu. Imena spremenljivk so lahko poljubno
 dolga, v našem primeru bi ji lahko rekli tudi <code>hidracija</code>.
 Pri programiranju velja, da izbiramo taka imena spremenljivk,
 ki bodo naredila program berljiv. </p>
 '''
]

printing = ['''\
<p> V Pythonu izpisujemo s funkcijo <code>print</code>. </p>''',
            '''\
<p>Če želimo izpisati več elementov,
jih ločimo z vejico.  Recimo, da imamo spremenljivko <code>ime</code>,
ki vsebuje naše ime, potem lahko napišemo:</p>
<pre>
print("Ime mi je", ime, ".")
</pre>''']

plan = ['''\
<p>Program razdelimo na tri dele, kot pri prejšnji nalogi:</p>
<ol>
    <li>Preberi vrednost hidracije (h = ?)i</li>
    <li>Izračunaj molsko maso mm (mm = …)</li>
    <li>Izpiši rezultat (print … )</li>
</ol>
''',
        no_input_call,
        printing]

hint = {
    'no_input_call': no_input_call,

    'printing': printing,

    'name_error' : [mod.general_msg['error_head'], mod.general_msg['general_exception'],
    mod.general_msg['name_error'], '''
    <p>Verjetno uporabljaš spremenljivko, ki nima vrednosti. Ali v izrazu za izračun
     uporabljaš napačno spremenljivko? Ali pri izpisu morda poskušaš
     izpisati napačno spremenljivko?</p>'''],

    'unsupported_operand' : [mod.general_msg['error_head'], mod.general_msg['general_exception'],
    mod.general_msg['type_error'], '''
<p>Verjetni razlog: funkcija <code>input</code> vrača vrednost tipa niz,
ki jo moramo najprej pretvoriti v tip <code>float</code>, če želimo z njo računati:</p>
<pre>
v = float(input(" ...
</pre>
'''],

    'final_hint' : [
        '''\
<p>Program deluje pravilno! 
</p>''']

}
