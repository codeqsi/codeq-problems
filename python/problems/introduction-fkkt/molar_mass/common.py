from python.util import has_token_sequence, string_almost_equal, \
    string_contains_number, get_tokens, get_numbers, get_exception_desc
from server.hints import Hint

id = 25004
number = 5
visible = True

solution = '''\
hidracija = int(input('Stopnja hidracije: '))

Cu = 63.5
S = 32.1
O = 16.0
H = 1.0
mm = Cu + S + 4 * O + hidracija * (2 * H + O)

print('Molska masa je', mm, 'g/mol.')
'''

hint_type = {
    'plan': Hint('plan'),
    'name_error': Hint('name_error'),
    'unsupported_operand': Hint('unsupported_operand'),
    'no_input_call' : Hint('no_input_call'),
    'printing': Hint('printing'),
    'final_hint': Hint('final_hint')
}

def test(python, code, aux_code=''):
    # List of inputs: (expression to eval, stdin).
    test_in = [
        (None, '1'),
        (None, '2'),
        (None, '5')
    ]

    test_out = [
        "Molska masa je 177.6 g/mol",
        "Molska masa je 195.6 g/mol",
        "Molska masa je 249.6 g/mol",
    ]

    # List of outputs: (expression result, stdout, stderr, exception).
    answers = python(code=aux_code+code, inputs=test_in, timeout=1.0)
    outputs = [ans[1] for ans in answers]

    n_correct = 0
    tin = None
    for i, (output, correct) in enumerate(zip(outputs, test_out)):
        #if string_almost_equal(output, float(correct), prec=2):
        if output.rstrip().endswith(correct):
            n_correct += 1
        else:
            tin = test_in[i][1]
            tout = correct

    passed = n_correct == len(test_in)
    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_in)}}]
    if tin:
        hints.append({'id': 'problematic_test_case', 'args': {'testin': str(tin), 'testout': str(tout)}})
    if passed:
        hints.append({'id': 'final_hint'})
    return passed, hints

def hint(python, code, aux_code=''):
    tokens = get_tokens(code)

    # run one test first to see if there are any exceptions
    test_in = [(None, '1\n')]
    answer = python(code=aux_code+code, inputs=test_in, timeout=1.0)
    exc = answer[0][3]
    exc_hint = get_exception_desc(answer[0][3])
    if exc:
        if 'NameError' in exc:
            return [{'id':'name_error', 'args': {'message': exc}}]
        elif 'unsupported operand' in exc or 'TypeError' in exc:
            return [{'id':'unsupported_operand', 'args': {'message': exc}}]
        else:
            return exc_hint

    # if input is not present in code, student needs to learn about input
    if not has_token_sequence(tokens, ['input']) or \
       (not has_token_sequence(tokens, ['float']) and not has_token_sequence(tokens, ['int'])) or \
       not has_token_sequence(tokens, ['=']):
        return [{'id': 'no_input_call'}]

    if not has_token_sequence(tokens, ['print']):
        return [{'id' : 'printing'}]

    return None
