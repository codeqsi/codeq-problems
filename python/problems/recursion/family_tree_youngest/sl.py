import server
mod = server.problems.load_language('python', 'sl')

id = 20803
name = 'Družinsko drevo: najmlajši'

description = '''\
<p>
Funkcija <code>youngest(name)</code> naj vrne starost in ime
najmlajšega člana določene rodbine.
Uporabite družinsko
drevo (<a target="_blank" href="[%@resource novak.png%]">slika</a> in
<a target="_blank" href="[%@resource slovar.html%]">slovar</a>). </p>
<pre>
>>> youngest('Hans')
(64, 'Hans')
>>> youngest('Daniel')
(5, 'Alenka')
</pre>
'''

plan = []

hint = {
    'final_hint': ['''\
<p>Program je pravilen! <br>
</p>
'''],
}
