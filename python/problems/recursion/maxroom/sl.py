import server
mod = server.problems.load_language('python', 'sl')

id = 20812
name = 'Največja številka'

description = '''\
<p> <img src="[%@resource map.png%]"> </p>
<p>
Napiši funkcijo <code>maxroom(map, room)</code>, ki vrne največjo številko,
v katero lahko pridemo, če se spustimo iz te sobe. </p>
<pre>
>>> maxroom(map, 0)
81
>>> maxroom(map, 3)
66
>>> maxroom(map, 12)
12
</pre>
'''

plan = []

hint = {
    'final_hint': ['''\
<p>Program je pravilen! <br>
</p>
''']
}
