import re
from python.util import has_token_sequence, string_almost_equal, \
    string_contains_number, get_tokens, get_numbers, get_exception_desc, \
    all_tokens, has_comprehension, has_loop, almost_equal, get_ast
from server.hints import Hint

id = 20812
number = 12
visible = True

solution = '''\
def maxroom(map, room):
    if room == None:
        return -1
    return max(room, maxroom(map, map[room][0]), maxroom(map, map[room][1]))
'''

hint_type = {
    'final_hint': Hint('final_hint')
}

map = {0: [6, 3], 6: [5, 81], 3: [8, 24], 5: [2, 18],
81: [None, None], 8: [42, None], 24: [63, 13], 2: [7, 27],
18: [None, 35], 42: [None, 66], 63: [61, None], 13: [4, 12],
7: [None, None], 27: [None, None], 35: [None, None], 66: [None, None],
61: [None, None], 4: [None, None], 12: [None, None]}

def test(python, code, aux_code=''):
    func_name = 'maxroom'
    tokens = get_tokens(code)
    if not has_token_sequence(tokens, ['def', func_name]):
        return False, [{'id' : 'no_func_name', 'args' : {'func_name' : func_name}}]

    in_out = [
        (0, 81),
        (3, 66),
        (63, 63),
        (5, 35),
    ]
    test_in = [('{}({}, {})'.format(func_name, str(map), l[0]), None)
               for l in in_out]
    test_out = [l[1] for l in in_out]

    answers = python(code=aux_code+code, inputs=test_in, timeout=1.0)
    n_correct = 0
    tin, tout = None, None
    for i, (ans, to) in enumerate(zip(answers, test_out)):
        res = ans[0]
        corr = res == to
        n_correct += corr
        if not corr:
            tin = test_in[i][0]
            tout = to

    passed = n_correct == len(test_in)
    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_in)}}]
    if tin:
        hints.append({'id': 'problematic_test_case', 'args': {'testin': str(tin), 'testout': str(tout)}})
    if passed:
        hints.append({'id': 'final_hint'})

    return passed, hints


def hint(python, code, aux_code=''):
    tokens = get_tokens(code)

    # run one test first to see if there are any exceptions
    answer = python(code=aux_code+code, inputs=[(None, None)], timeout=1.0)
    exc = get_exception_desc(answer[0][3])
    if exc: return exc

    return None
