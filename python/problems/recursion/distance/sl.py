import server
mod = server.problems.load_language('python', 'sl')

id = 20811
name = 'Razdalja'

description = '''\
<p> <img src="[%@resource map.png%]"> </p>
<p>
Nadaljujmo z meglenim gorovjem. Napiši funkcijo
<code>distance(map, start_room, ring_room)</code>,
ki prejme zemljevid, številko začetne sobe in številko sobe s prstanom,
kot rezultat pa vrne dolžino poti do nje. Če katerakoli od sob ne obstaja,
vrni <code>-1</code>.
Na gornjem zemljevidu je globina sobe 42 enaka 3. Zato:</p>
<pre>
>>> distance(map, 0, 42)
3
>>> distance(map, 0, 43)
-1
</pre>
'''

plan = []

hint = {
    'final_hint': ['''\
<p>Program je pravilen! <br>
</p>
''']
}
