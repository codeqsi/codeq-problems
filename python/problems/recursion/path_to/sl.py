import server
mod = server.problems.load_language('python', 'sl')

id = 20805
name = 'Družinsko drevo: pot do'

description = '''\
<p>
Reši podobno nalogo kot je prejšnja, le da naj funkcija ne vrne razdalje do
določene osebe, temveč pot do nje. Uporabite družinsko
drevo (<a target="_blank" href="[%@resource novak.png%]">slika</a> in
<a target="_blank" href="[%@resource slovar.html%]">slovar</a>). </p>
<pre>
>>> pathto('Daniel', 'Hans')
['Daniel', 'Hans']
>>> pathto('Adam', 'Hans')
['Adam', 'Daniel', 'Hans']
>>> pathto('Adam', 'Franc')
['Adam', 'Daniel', 'Elizabeta', 'Jurij', 'Franc']
</pre>
'''

plan = []

hint = {
    'final_hint': ['''\
<p>Program je pravilen! <br>
</p>
'''],
}
