import server
mod = server.problems.load_language('python', 'sl')

id = 20810
name = 'Kam?'

description = '''\
<p>
od Meglenim gorovjem je, kot nekateri vedo že dolgo,
nekateri pa so morali čakati na film, cel labirint votlin.
Bilbu bi bilo veliko lažje, če bi imel zemljevid. Mi ga imamo.

Zemljevid lahko shranimo tudi v obliki slovarja,
v katerem so ključi številke sob, vrednosti pa sobe,
v katere pridemo po levi in po desni poti.
Kadar kakšne poti ni, je ustrezni element enak None.</p>

<img src="[%@resource map.png%]">

<pre>
map = {0: [6, 3], 6: [5, 81], 3: [8, 24], 5: [2, 18],
81: [None, None], 8: [42, None], 24: [63, 13], 2: [7, 27],
18: [None, 35], 42: [None, 66], 63: [61, None], 13: [4, 12],
7: [None, None], 27: [None, None], 35: [None, None], 66: [None, None],
61: [None, None], 4: [None, None], 12: [None, None]}</pre>

<p>Napiši funkcijo <code>where_to(map, room, path)</code>,
ki kot argument prejme zemljevid (npr. zgornji slovar),
začetno sobo (ta bo vedno 0, a to naj te ne moti) in pot (<code>path</code>),
podano kot zaporedje znakov <code>L</code> in <code>R</code> (levo in desno).
Kot rezultat mora vrniti sobo, v katero pelje podana pot.</p>

<pre>
>>> where_to(map, 0, "LLR")
18
>>> where_to(map, 0, "RRRR")
12
>>> where_to(map, 0, "L")
6
>>> where_to(map, 0, "")
0
</pre>

<p> Predpostaviti smeš, da je pot pravilna in se nikoli ne odpravi v hodnik,
ki ga ni.</p>
'''

plan = []

hint = {
    'final_hint': ['''\
<p>Program je pravilen! <br>
</p>
''']
}
