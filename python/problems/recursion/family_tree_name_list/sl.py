import server
mod = server.problems.load_language('python', 'sl')

id = 20802
name = 'Družinsko drevo: seznam imen'

description = '''\
<p>
Podobno kot pri prejšnji nalogi napišite funkcijo <code>names(name)</code>, ki
tokrat vrne vse imena v seznamu. Uporabite družinsko
drevo (<a target="_blank" href="[%@resource novak.png%]">slika</a> in
<a target="_blank" href="[%@resource slovar.html%]">slovar</a>). </p>
<pre>
>>> names('Hans')
['Hans']
>>> names('Daniel')
['Daniel', 'Elizabeta', 'Ludvik', 'Jurij', 'Franc', 'Jožef', 'Alenka', 'Aleksander', 'Petra', 'Barbara', 'Herman', 'Margareta', 'Mihael', 'Hans']
</pre>
'''

plan = []

hint = {
    'final_hint': ['''\
<p>Program je pravilen! <br>
</p>
'''],
}
