import re
from python.util import has_token_sequence, string_almost_equal, \
    string_contains_number, get_tokens, get_numbers, get_exception_desc, \
    all_tokens, has_comprehension, has_loop, almost_equal, get_ast
from server.hints import Hint

id = 20808
number = 8
visible = True

solution = '''\
def binary(n):
    if n <= 1:
        return str(n)
    return binary(n // 2) + str(n % 2)
'''

hint_type = {
    'final_hint': Hint('final_hint'),
    'has_loop': Hint('has_loop')
}

def test(python, code, aux_code=''):
    func_name = 'binary'
    tokens = get_tokens(code)
    ast = get_ast(code)
    if not has_token_sequence(tokens, ['def', func_name]):
        return False, [{'id' : 'no_func_name', 'args' : {'func_name' : func_name}}]

    in_out = [
        (3, '11'),
        (2, '10'),
        (1, '1'),
        (0, '0'),
        (42, '101010')
    ]

    test_in = [('{0}({1})'.format(func_name, str(l[0])), None)
               for l in in_out]
    test_out = [l[1] for l in in_out]

    answers = python(code=aux_code+code, inputs=test_in, timeout=1.0)
    n_correct = 0
    tin, tout = None, None
    for i, (ans, to) in enumerate(zip(answers, test_out)):
        res = ans[0]
        corr = res == to
        n_correct += corr
        if not corr:
            tin = test_in[i][0]
            tout = to

    passed = n_correct == len(test_in)
    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_in)}}]
    if tin:
        hints.append({'id': 'problematic_test_case', 'args': {'testin': str(tin), 'testout': str(tout)}})
    if passed:
        if has_loop(ast):
            hints.append({'id': 'has_loop'})
        else:
            hints.append({'id': 'final_hint'})


    return passed, hints


def hint(python, code, aux_code=''):
    tokens = get_tokens(code)

    # run one test first to see if there are any exceptions
    answer = python(code=aux_code+code, inputs=[(None, None)], timeout=1.0)
    exc = get_exception_desc(answer[0][3])
    if exc: return exc

    return None
