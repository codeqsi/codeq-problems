import server
mod = server.problems.load_language('python', 'sl')

id = 20808
name = 'Binarno'

description = '''\
<p>
Napiši rekurzivno funkcijo <code>binary(n)</code>, ki kot argument prejme število
<code>n</code> in kot rezultat vrne niz s tem številom v dvojiškem zapisu. </p>

<pre>
>>> binary(42)
'101010'
</pre>
'''

plan = []

hint = {
    'final_hint': ['''\
<p>Program je pravilen! <br>
</p>
'''],

    'has_loop': ['''\
<p>Program sicer deluje pravilno, vendar vsebuje zanko.
</p>
'''],
}
