import server
mod = server.problems.load_language('python', 'sl')

id = 20807
name = 'Vsota seznama'

description = '''\
<p>
Napiši funkcijo <code>sum(xs)</code>, ki sešteje elemente vgnezdenega seznama xs.
Pomagajte si s funkcijo isinstance.</p>
<pre>
>>> isinstance([1, 2, 3], list)
True
>>> isinstance(1, list)
False
>>> sum([])
0
>>> sum([1, 2, 3, 4, 5])
15
>>> sum([1, [], [2, 3, [4]], 5])
15
</pre>
'''

plan = []

hint = {
    'final_hint': ['''\
<p>Program je pravilen! <br>
</p>
'''],
}
