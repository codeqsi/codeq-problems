import server
mod = server.problems.load_language('python', 'sl')

id = 20806
name = 'Fakulteta'

description = '''\
<p>
Napiši rekurzivno funkcijo <code>factorial(n)</code>, ki izračuna n!.
Funkcija je definirana z enačbama <code>0! = 1</code> in
<code>n! = n * (n - 1)!</code>.
Rešitev ne sme vsebovati zanke <code>for</code> ali zanke <code>while</code>.</p>
<pre>
>>> factorial(0)
1
>>> factorial(1)
1
>>> factorial(5)
120
>>> factorial(20)
2432902008176640000
</pre>
'''

plan = []

hint = {
    'final_hint': ['''\
<p>Program je pravilen! <br>
</p>
'''],

    'has_loop': ['''\
<p>Program sicer deluje pravilno, vendar vsebuje zanko.
</p>
'''],
}
