import server
mod = server.problems.load_language('python', 'sl')

id = 20804
name = 'Družinsko drevo: globina do'

description = '''\
<p>
Napišite funkcijo <code>depthto(fr, to)</code>, ki pove kako globoko v rodbini
osebe <code>fr</code> je oseba z imenom <code>to</code>.
Uporabite družinsko
drevo (<a target="_blank" href="[%@resource novak.png%]">slika</a> in
<a target="_blank" href="[%@resource slovar.html%]">slovar</a>). </p>
<pre>
>>> depthto('Daniel', 'Hans')
1
>>> depthto('Adam', 'Hans')
2
>>> depthto('Adam', 'Franc')
4
</pre>
'''

plan = []

hint = {
    'final_hint': ['''\
<p>Program je pravilen! <br>
</p>
'''],
}
