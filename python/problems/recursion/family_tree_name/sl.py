import server
mod = server.problems.load_language('python', 'sl')

id = 20801
name = 'Družinsko drevo: izpis imen'

description = '''\
<p>
Napišite funkcijo <code>printnames(name)</code>, ki izpiše vsa imena v določeni rodbini.
Uporabite družinsko
drevo (<a target="_blank" href="[%@resource novak.png%]">slika</a> in
<a target="_blank" href="[%@resource slovar.html%]">slovar</a>),
ki ste ga spoznali na predavanjih. </p>
<pre>
>>> printnames('Hans')
Hans
>>> printnames('Daniel')
Daniel
Elizabeta
Ludvik
Jurij
Franc
Jožef
Alenka
Aleksander
Petra
Barbara
Herman
Margareta
Mihael
Hans
</pre>
'''

plan = []

hint = {
    'final_hint': ['''\
<p>Program je pravilen! <br>
</p>
'''],
}
