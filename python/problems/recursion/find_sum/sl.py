import server
mod = server.problems.load_language('python', 'sl')

id = 20809
name = 'Išči vsoto'

description = '''\
<p>
Napiši funkcijo <code>find_sum(xs, gs)</code>, ki kot argument prejme seznam
pozitivnih števil in število, ki predstavlja ciljno vsoto. Funkcija vrne
<code>True</code>, kadar lahko sestavimo ciljno vsoto iz števil v seznamu. </p>

<pre>
>>> find_sum([2,7,3,1,4], 10)
True
>>> find_sum([2,3,2,4], 10)
False
</pre>
'''

plan = []

hint = {
    'final_hint': ['''\
<p>Program je pravilen! <br>
</p>
''']
}
