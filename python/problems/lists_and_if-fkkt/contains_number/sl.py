import server
mod = server.problems.load_language('python', 'sl')


name = 'Števila v seznamu'
slug = 'Števila v seznamu'


description = '''\
<p>Napišite program, ki ugotovi, ali je število, ki ga vpiše uporabnik, v seznamu. Seznam definirajte na vrhu programa: <code>seznam = [3, 7, 9, 10, 12, 17, 21, 33]</code><br>
Primer uporabe:</p>

<pre><code>Vpišite število: <span style="color: rgb(239, 69, 64);">9</span>
Seznam vsebuje število 9

Vpišite število: <span style="color: rgb(239, 69, 64);">4</span>
Seznam ne vsebuje števila 4
</code></pre>

<p>Seveda mora program delati za poljubne sezname in ne samo za seznam iz primera.</p>
'''

if_clause = ['''\
<p>Preveri, ali imamo dano število v seznamu?</p>''',
             '''\
<p>Uporabi pogojni stavek <code>if</code>!</p>''',
             '''\
<pre>
if x in seznam:
</pre>''']


plan = []

hint = {
    'no_xs': ['''\
<p>Program mora imeti na začetku definiran seznam <code>seznam</code>.</p>'''],

    'no_input_call': '''<p>Za branje uporabimo funkcijo <code>input</code></p>''',

    'if_clause': if_clause,

    'printing': ['''\
<p>Izpiši rezultat.</p>'''],

    'final_hint': ['''\
<p>Program deluje pravilno!</p>'''],

}
