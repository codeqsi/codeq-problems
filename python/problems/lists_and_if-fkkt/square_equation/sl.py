import server
mod = server.problems.load_language('python', 'sl')


name = 'Kvadratna enačba'
slug = 'Kvadratna enačba'


description = '''\
<p>Napišite program <code>kvadratna_enacba.py</code>, ki izračuna vse realne rešitve <a href="https://sl.wikipedia.org/wiki/Kvadratna_ena%C4%8Dba">kvadratne enačbe</a> <code>ax^2 + bx + c = 0</code>, 
podane z argumenti a, b in c. Vaš program naj se obnaša kot je prikazano spodaj.</p>

<pre><code class="brush: python">Vpiši a: <span style="color: rgb(239, 69, 64);">1</span>
Vpiši b: <span style="color: rgb(239, 69, 64);">2</span>
Vpiši c: <span style="color: rgb(239, 69, 64);">1</span>
Enačba ima eno realno rešitev: -1.0 

Vpiši a: <span style="color: rgb(239, 69, 64);">1</span> 
Vpiši b: <span style="color: rgb(239, 69, 64);">2</span> 
Vpiši c: <span style="color: rgb(239, 69, 64);">0</span> 
Enačba ima dve realni rešitvi: 0.0 in -2.0 

Vpiši a: <span style="color: rgb(239, 69, 64);">1</span> 
Vpiši b: <span style="color: rgb(239, 69, 64);">2</span> 
Vpiši c: <span style="color: rgb(239, 69, 64);">2</span> 
Enačba nima realnih rešitev.
</code></pre>
'''

if_clause = ['''\
<p>Preveri, koliko rešitev ima enačba?</p>''',
             '''\
<p>Uporabi pogojni stavek <code>if</code>!</p>''',
             '''\
<pre>
if d == 0:
</pre>''']


plan = []

hint = {
    'no_input_call': '''<p>Za branje uporabimo funkcijo <code>input</code></p>''',

    'if_clause': if_clause,

    'printing': ['''\
<p>Izpiši rezultat.</p>'''],

    'final_hint': ['''\
<p>Program deluje pravilno!</p>'''],

}
