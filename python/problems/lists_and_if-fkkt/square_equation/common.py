import re
from python.util import has_token_sequence, string_almost_equal, \
    string_contains_number, get_tokens, get_numbers, get_exception_desc
from server.hints import Hint

id = 25012
number = 8
visible = True

solution = '''\
a = int(input('Vpiši a: '))
b = int(input('Vpiši b: '))
c = int(input('Vpiši c: '))

d = b**2 - 4 * a * c
if d < 0:
    print('Enačba nima realnih rešitev.')
elif d == 0:
    x = -b / (2 * a)
    print('Enačba ima eno realno rešitev:', x)
else:
    x1 = (-b + pow(d, 1/2)) / (2 * a)
    x2 = (-b - pow(d, 1/2)) / (2 * a)
    print('Enačba ima dve realni rešitvi:', x1, 'in', x2)
'''

hint_type = {
    'if_clause': Hint('if_clause'),
    'printing': Hint('printing'),
    'no_input_call' : Hint('no_input_call'),
    'final_hint': Hint('final_hint')
}

def test(python, code, aux_code=''):
    tokens = get_tokens(code)

    test_in = [
        (None, '1\n2\n1\n'),
        (None, '1\n2\n0\n'),
        (None, '1\n2\n2\n')
    ]
    test_out = [
        'Enačba ima eno realno rešitev: -1.0',
        'Enačba ima dve realni rešitvi: 0.0 in -2.0',
        'Enačba nima realnih rešitev.'
    ]

    # List of outputs: (expression result, stdout, stderr, exception).
    answers = python(code=aux_code+code, inputs=test_in, timeout=1.0)
    outputs = [ans[1] for ans in answers]

    n_correct = 0
    tin = None
    for i, (output, correct) in enumerate(zip(outputs, test_out)):
        if output.rstrip().endswith(correct):
            n_correct += 1
        else:
            tin = test_in[i][1]
            tout = correct

    passed = n_correct == len(test_in)
    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_in)}}]
    if tin != None:
        hints.append({'id': 'problematic_test_case', 'args': {'testin': str(tin), 'testout': str(tout)}})
    else:
        hints.append({'id' : 'final_hint'})
    return passed, hints

def hint(python, code, aux_code=''):
    # run one test first to see if there are any exceptions
    test_in = [(None, '1\n2\n1\n')]
    answer = python(code=aux_code+code, inputs=test_in, timeout=1.0)
    exc = get_exception_desc(answer[0][3])
    if exc: return exc

    tokens = get_tokens(code)

    # if input is not present in code, student needs to learn about input
    if not has_token_sequence(tokens, ['input']):
        return [{'id': 'no_input_call'}]

    if not has_token_sequence(tokens, ['if']):
        return [{'id' : 'if_clause'}]

    # student is not using print function
    if not has_token_sequence(tokens, ['print']):
        return [{'id' : 'printing'}]

    return None
