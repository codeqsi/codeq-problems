import re
from python.util import has_token_sequence, string_almost_equal, \
    string_contains_number, get_tokens, get_numbers, get_exception_desc
from server.hints import Hint

id = 25005
number = 1
visible = True

solution = '''\
seznam = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
seznam_lihi = seznam[::2]
seznam_sodi = seznam[1::2]
print(seznam_lihi)
print(seznam_sodi)
'''

hint_type = {
    'no_xs': Hint('no_xs'),
    'printing': Hint('printing'),
    'final_hint': Hint('final_hint')
}

def test(python, code, aux_code=''):
    tokens = get_tokens(code)

    test_xs = [[1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
               [1, 2, 3, 4],
			   [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]
			  ]
    test_out = [
        '[1, 3, 5, 7, 9]\n[2, 4, 6, 8, 10]',
        '[1, 3]\n[2, 4]',
        '[1, 3, 5, 7, 9, 11, 13]\n[2, 4, 6, 8, 10, 12, 14]'
    ]

    n_correct = 0
    tin = None
    for xs_i, xs in enumerate(test_xs):
        # change code to contain new xs instead of the one
        # given by user
        tcode = re.sub(r'^seznam\s*=\s*\[.*?\]',
                       'seznam = ' + str(xs),
                       code,
                       flags = re.DOTALL | re.MULTILINE)

        # use python session to call tcode
        answers = python(code=aux_code+tcode, inputs=[(None, None)], timeout=1.0)
        output = answers[0][1]

        if re.sub("\s", "", test_out[xs_i]) in re.sub("\s", "", output):
            n_correct += 1
        else:
            tin = test_xs[xs_i]
            tout = test_out[xs_i]

    passed = n_correct == len(test_xs)
    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_xs)}}]
    if tin != None:
        hints.append({'id': 'problematic_test_case', 'args': {'testin': str(tin), 'testout': str(tout)}})
    else:
        hints.append({'id' : 'final_hint'})
    return passed, hints

def hint(python, code, aux_code=''):
    # run one test first to see if there are any exceptions
    answer = python(code=aux_code+code, inputs=[(None, None)], timeout=1.0)
    exc = get_exception_desc(answer[0][3])
    if exc: return exc

    tokens = get_tokens(code)

    # if has no xs, tell him to ask for values
    if not has_token_sequence(tokens, ['seznam', '=', '[']):
        return [{'id' : 'no_xs'}]


    # student is not using print function
    if not has_token_sequence(tokens, ['print']):
        return [{'id' : 'printing'}]

    return None
