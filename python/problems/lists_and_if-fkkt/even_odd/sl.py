import server
mod = server.problems.load_language('python', 'sl')

name = 'Liha in soda števila'
slug = 'Liha in soda števila'

description = '''\
<p>V spremenljivki&nbsp;<code>seznam</code>&nbsp;imamo podan seznam vseh naravnih števil na intervalu [1, n].<br>Z uporabo pravil naslavljanja in rezanja seznamov ustvarite dva nova podseznama in jih izpišite:</p><ul><li><code>seznam_lihi</code>, ki vsebuje vsa liha števila iz seznama&nbsp;<code>seznam</code></li><li><code>seznam_sodi</code>, ki vsebuje vsa soda števila iz seznama&nbsp;<code>seznam</code></li>
</ul>
<p>Seznam definiraj na vrhu programa: <code>seznam = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]</code><br>
Izpis:</p>

<pre><code>[1, 3, 5, 7, 9]
[2, 4, 6, 8, 10] 
</code></pre>
'''

plan = ['''\
<p>Z uporabo pravil naslavljanja in rezanja seznamov ustvarite dva nova podseznama in jih izpišite.</p>
''']

hint = {
    'no_xs': ['''\
<p>Program mora imeti na začetku definiran seznam <code>seznam</code>.</p>'''],

    'printing': ['''\
<p>Izpiši rezultat.</p>'''],

    'final_hint': ['''\
<p>Program deluje pravilno!</p>'''],

    'problematic_test_case': ['''\
<p>Program ne dela pravilno!<br>
Poskusi seznam = [%=testin%] <br>
pravilen rezultat: [%=testout%]</p>
'''],
}
