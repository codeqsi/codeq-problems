import server
mod = server.problems.load_language('python', 'sl')


name = 'Palindrom'
slug = 'Palindrom'


description = '''\
<p>Napišite program, ki uporabnika vpraša po nizu, nato pa izpiše, ali je niz <a href="https://sl.wikipedia.org/wiki/Palindrom">palindrom</a>.<br>
Primer uporabe:</p>

<pre><code>Vpišite niz: <span style="color: rgb(239, 69, 64);">kisik</span>
True

Vpišite niz: <span style="color: rgb(239, 69, 64);">vnos</span>
False
</code></pre>
'''


plan = []

hint = {
    'no_input_call': '''<p>Za branje uporabimo funkcijo <code>input</code></p>''',

    'printing': ['''\
<p>Izpiši rezultat.</p>'''],

    'final_hint': ['''\
<p>Program deluje pravilno!</p>'''],

}
