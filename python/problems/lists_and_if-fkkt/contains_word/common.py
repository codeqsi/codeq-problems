import re
from python.util import has_token_sequence, string_almost_equal, \
    string_contains_number, get_tokens, get_numbers, get_exception_desc
from server.hints import Hint

id = 25007
number = 2
visible = True

solution = '''\
seznam = ["beseda", "spremenljivka", "niz", "zanka", "stavek", "slovar"]
niz = input("Vpišite niz: ")
if niz in seznam:
    print("Seznam vsebuje niz", niz)
else:
    print("Seznam ne vsebuje niza", niz)
'''

hint_type = {
    'no_xs': Hint('no_xs'),
    'if_clause': Hint('if_clause'),
    'printing': Hint('printing'),
    'no_input_call' : Hint('no_input_call'),
    'final_hint': Hint('final_hint')
}

def test(python, code, aux_code=''):
    tokens = get_tokens(code)

    test_in = [
        (None, 'zanka\n'),
        (None, 'vrednost\n'),
        (None, 'slovar\n'),
        (None, 'število\n')
    ]
    test_out = [
        'Seznam vsebuje niz zanka',
        'Seznam ne vsebuje niza vrednost',
        'Seznam vsebuje niz slovar',
        'Seznam ne vsebuje niza število'
    ]

    # List of outputs: (expression result, stdout, stderr, exception).
    answers = python(code=aux_code+code, inputs=test_in, timeout=1.0)
    outputs = [ans[1] for ans in answers]

    n_correct = 0
    tin = None
    for i, (output, correct) in enumerate(zip(outputs, test_out)):
        if output.rstrip().endswith(correct):
            n_correct += 1
        else:
            tin = test_in[i][1]
            tout = correct

    passed = n_correct == len(test_in)
    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_in)}}]
    if tin != None:
        hints.append({'id': 'problematic_test_case', 'args': {'testin': str(tin), 'testout': str(tout)}})
    else:
        hints.append({'id' : 'final_hint'})
    return passed, hints

def hint(python, code, aux_code=''):
    # run one test first to see if there are any exceptions
    test_in = [(None, '1\n')]
    answer = python(code=aux_code+code, inputs=test_in, timeout=1.0)
    exc = get_exception_desc(answer[0][3])
    if exc: return exc

    tokens = get_tokens(code)

    # if has no xs, tell him to ask for values
    if not has_token_sequence(tokens, ['seznam', '=', '[']):
        return [{'id' : 'no_xs'}]

    # if input is not present in code, student needs to learn about input
    if not has_token_sequence(tokens, ['input']):
        return [{'id': 'no_input_call'}]

    if not has_token_sequence(tokens, ['if']):
        return [{'id' : 'if_clause'}]

    # student is not using print function
    if not has_token_sequence(tokens, ['print']):
        return [{'id' : 'printing'}]

    return None
