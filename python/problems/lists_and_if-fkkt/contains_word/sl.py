import server
mod = server.problems.load_language('python', 'sl')


name = 'Niz v seznamu'
slug = 'Niz v seznamu'


description = '''\
<p>Napišite program, ki ugotovi, ali je niz, ki ga vpiše uporabnik, v seznamu. Seznam definirajte na vrhu programa: <code>seznam = ["beseda", "spremenljivka", "niz", "zanka", "stavek", "slovar"]</code><br>
Primer uporabe:</p>

<pre><code>Vpišite niz: <span style="color: rgb(239, 69, 64);">zanka</span>
Seznam vsebuje niz zanka

Vpišite niz: <span style="color: rgb(239, 69, 64);">vrednost</span>
Seznam ne vsebuje niza vrednost
</code></pre>

<p>Seveda mora program delati za poljubne sezname in ne samo za seznam iz primera.</p>
'''

if_clause = ['''\
<p>Preveri, ali imamo dan niz v seznamu?</p>''',
             '''\
<p>Uporabi pogojni stavek <code>if</code>!</p>''',
             '''\
<pre>
if x in seznam:
</pre>''']


plan = []

hint = {
    'no_xs': ['''\
<p>Program mora imeti na začetku definiran seznam <code>seznam</code>.</p>'''],

    'no_input_call': '''<p>Za branje uporabimo funkcijo <code>input</code></p>''',

    'if_clause': if_clause,

    'printing': ['''\
<p>Izpiši rezultat.</p>'''],

    'final_hint': ['''\
<p>Program deluje pravilno!</p>'''],

}
