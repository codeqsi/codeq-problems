import re
from python.util import has_token_sequence, string_almost_equal, \
    string_contains_number, get_tokens, get_numbers, get_exception_desc
from server.hints import Hint

id = 25009
number = 5
visible = True

solution = '''\
st1 = int(input("Vpišite 1. število: "))
st2 = int(input("Vpišite 2. število: "))
st3 = int(input("Vpišite 3. število: "))
max = st1
min = st1
if st2 > max:
    max = st2
if st3 > max:
    max = st3
if st2 < min:
    min = st2
if st3 < min:
    min = st3
print("Minimum: ", min, ", Maksimum: ", max)
'''

hint_type = {
    'if_clause': Hint('if_clause'),
    'printing': Hint('printing'),
    'no_input_call' : Hint('no_input_call'),
    'no_min_max': Hint('no_min_max'),
    'final_hint': Hint('final_hint')
}

def test(python, code, aux_code=''):
    tokens = get_tokens(code)

    if has_token_sequence(tokens, ['min', '(']) or has_token_sequence(tokens, ['max', '(']):
        return False, [{'id': 'no_min_max'}]

    test_in = [
        (None, '5\n8\n7\n'),
        (None, '4\n3\n2\n'),
        (None, '1\n0\n2\n'),
        (None, '1\n1\n1\n')
    ]
    test_out = [
        'Minimum: 5 , Maksimum: 8',
        'Minimum: 2 , Maksimum: 4',
        'Minimum: 0 , Maksimum: 2',
        'Minimum: 1 , Maksimum: 1'
    ]

    # List of outputs: (expression result, stdout, stderr, exception).
    answers = python(code=aux_code+code, inputs=test_in, timeout=1.0)
    outputs = [ans[1] for ans in answers]

    n_correct = 0
    tin = None
    for i, (output, correct) in enumerate(zip(outputs, test_out)):
        if re.sub('\s', '', output).endswith(re.sub('\s', '', correct)) and \
           not re.sub('\s', '', output).endswith(re.sub('\s', '', correct)*2):
            n_correct += 1
        else:
            tin = test_in[i][1]
            tout = correct

    passed = n_correct == len(test_in)
    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_in)}}]
    if tin != None:
        hints.append({'id': 'problematic_test_case', 'args': {'testin': str(tin), 'testout': str(tout)}})
    else:
        hints.append({'id' : 'final_hint'})
    return passed, hints

def hint(python, code, aux_code=''):
    # run one test first to see if there are any exceptions
    test_in = [(None, '5\n8\n7\n')]
    answer = python(code=aux_code+code, inputs=test_in, timeout=1.0)
    exc = get_exception_desc(answer[0][3])
    if exc: return exc

    tokens = get_tokens(code)

    # if input is not present in code, student needs to learn about input
    if not has_token_sequence(tokens, ['input']):
        return [{'id': 'no_input_call'}]

    if has_token_sequence(tokens, ['min', '(']) or has_token_sequence(tokens, ['max', '(']):
        return [{'id': 'no_min_max'}]

    if not has_token_sequence(tokens, ['if']):
        return [{'id' : 'if_clause'}]

    # student is not using print function
    if not has_token_sequence(tokens, ['print']):
        return [{'id' : 'printing'}]

    return None
