import server
mod = server.problems.load_language('python', 'sl')


name = 'Največje in najmanjše število'
slug = 'Največje in najmanjše število'


description = '''\
<p>Izpišite največje in najmanjše izmed treh števil, ki jih vnese uporabnik. Ne uporabite funkcij <code>min()</code> in <code>max()</code>.<br>
Primer uporabe:</p>

<pre><code><span style="color: rgb(12, 23, 255);">Vpišite 1. število:</span> <span style="color: rgb(239, 69, 64);">5</span>
<span style="color: rgb(12, 23, 255);">Vpišite 2. število:</span> <span style="color: rgb(239, 69, 64);">8</span>
<span style="color: rgb(12, 23, 255);">Vpišite 3. število:</span> <span style="color: rgb(239, 69, 64);">7</span>
<span style="color: rgb(12, 23, 255);">Minimum: 5 , Maksimum: 8</span>
</code></pre>
'''

if_clause = ['''\
<p>Uporabi pogojni stavek <code>if</code>!</p>''',
             '''\
<pre>
if x > y:
</pre>''']


plan = []

hint = {
    'no_input_call': '''<p>Za branje uporabimo funkcijo <code>input</code></p>''',

    'no_min_max': '''Funkciji <code>min()</code> in <code>max()</code> nista dovoljeni pri tej nalogi!''',

    'if_clause': if_clause,

    'printing': ['''\
<p>Izpiši rezultat.</p>'''],

    'final_hint': ['''\
<p>Program deluje pravilno!</p>'''],

}
