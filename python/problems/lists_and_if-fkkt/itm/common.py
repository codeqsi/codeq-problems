import re
from python.util import has_token_sequence, string_almost_equal, \
    string_contains_number, get_tokens, get_numbers, get_exception_desc
from server.hints import Hint

id = 25011
number = 7
visible = True

solution = '''\
visina = float(input("Telesna višina [cm]: "))
teza = float(input("Teža [kg]: "))
indeks = teza / (visina/100) ** 2
print("Vaš indeks telesne mase je: ", round(indeks,2))
if indeks > 25:
    print("Potrebno se bo več gibati in jesti bolj zdravo!")
elif indeks < 18.5:
    print("Pojejte kakšen kos torte več! ;)")
else:
    print("Super, nadaljujte s svojim življenjskim stilom!")
'''

hint_type = {
    'if_clause': Hint('if_clause'),
    'printing': Hint('printing'),
    'no_input_call' : Hint('no_input_call'),
    'final_hint': Hint('final_hint')
}

def test(python, code, aux_code=''):
    tokens = get_tokens(code)

    test_in = [
        (None, '189\n70\n'),
        (None, '165\n70\n'),
        (None, '170\n53\n')
   
    ]
    test_out = [
        'Vaš indeks telesne mase je: 19.6\nSuper, nadaljujte s svojim življenjskim stilom!',
        'Vaš indeks telesne mase je: 25.71\nPotrebno se bo več gibati in jesti bolj zdravo!',
        'Vaš indeks telesne mase je: 18.34\nPojejte kakšen kos torte več! ;)'
    ]

    # List of outputs: (expression result, stdout, stderr, exception).
    answers = python(code=aux_code+code, inputs=test_in, timeout=1.0)
    outputs = [ans[1] for ans in answers]

    n_correct = 0
    tin = None
    for i, (output, correct) in enumerate(zip(outputs, test_out)):
        if re.sub('\s', '', output).endswith(re.sub('\s', '', correct)):
            n_correct += 1
        else:
            tin = test_in[i][1]
            tout = correct

    passed = n_correct == len(test_in)
    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_in)}}]
    if tin != None:
        hints.append({'id': 'problematic_test_case', 'args': {'testin': str(tin), 'testout': str(tout)}})
    else:
        hints.append({'id' : 'final_hint'})
    return passed, hints

def hint(python, code, aux_code=''):
    # run one test first to see if there are any exceptions
    test_in = [(None, '189\n70\n')]
    answer = python(code=aux_code+code, inputs=test_in, timeout=1.0)
    exc = get_exception_desc(answer[0][3])
    if exc: return exc

    tokens = get_tokens(code)

    # if input is not present in code, student needs to learn about input
    if not has_token_sequence(tokens, ['input']):
        return [{'id': 'no_input_call'}]

    if not has_token_sequence(tokens, ['if']):
        return [{'id' : 'if_clause'}]

    # student is not using print function
    if not has_token_sequence(tokens, ['print']):
        return [{'id' : 'printing'}]

    return None
