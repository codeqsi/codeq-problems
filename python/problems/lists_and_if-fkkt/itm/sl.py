import server
mod = server.problems.load_language('python', 'sl')


name = 'Indeks telesne mase'
slug = 'Indeks telesne mase'


description = '''\
<p>Napišite program, ki uporabnika vpraša po telesni višini in teži ter izračuna indeks telesne mase. Če je indeks manjši od 18.5, uporabniku sporočite, da je presuh in mu priporočite, da naj poje kakšen kos torte. Če je indeks telesne mase večji od 25, spodbudite uporabnika, da se začne več gibati in jesti bolj zdravo. Drugače pa uporabniku sporočite, da naj kar nadaljuje s svojim življenjskim stilom.<br>
Primer uporabe:</p>

<pre><code>Telesna višina [cm]: <span style="color: rgb(239, 69, 64);">189</span>
Teža [kg]: <span style="color: rgb(239, 69, 64);">70</span>
Vaš indeks telesne mase je: 19.6
Super, nadaljujte s svojim življenjskim stilom!

Telesna višina [cm]: <span style="color: rgb(239, 69, 64);">170</span>
Teža [kg]: <span style="color: rgb(239, 69, 64);">53</span>
Vaš indeks telesne mase je: 18.34
Pojejte kakšen kos torte več! ;)

Telesna višina [cm]: <span style="color: rgb(239, 69, 64);">165</span>
Teža [kg]: <span style="color: rgb(239, 69, 64);">70</span>
Vaš indeks telesne mase je: 25.71
Potrebno se bo več gibati in jesti bolj zdravo!
</code></pre>
'''

if_clause = ['''\
<p>Preveri, če ITM presega določeno mejo?</p>''',
             '''\
<p>Uporabi pogojni stavek <code>if</code>!</p>''',
             '''\
<pre>
if x in seznam:
</pre>''']


plan = []

hint = {
    'no_input_call': '''<p>Za branje uporabimo funkcijo <code>input</code></p>''',

    'if_clause': if_clause,

    'printing': ['''\
<p>Izpiši rezultat.</p>'''],

    'final_hint': ['''\
<p>Program deluje pravilno!</p>'''],

}
