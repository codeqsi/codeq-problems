import server
mod = server.problems.load_language('python', 'sl')


name = 'Pretvarjanje temperatur'
slug = 'Pretvarjanje temperatur'


description = '''\
<p>Napišite program, ki uporabnika vpraša po stopinjah Celzija in v katero enoto naj jih spremeni (Fahrenheit ali Kelvin). Program izpiše pretvorjeno vrednost. Če uporabnik vnese napačno vrednost, mu to tudi izpiše.<br>
Iz Celzijev v Kelvine pretvarjamo tako, da stopinjam prištejemo 273,15. Iz Celzijev v Fahrenheite pa pretvarjamo tako, da stopinje pomnožimo z 9/5 in prištejemo 32.<br>
Primer uporabe:</p>

<pre><code>Vpiši temperaturo [°C]: <span style="color: rgb(239, 69, 64);">32</span>
Želite pretvoriti v Kelvine (vpiši K) ali Fahrenheite (vpiši F)? <span style="color: rgb(239, 69, 64);">K</span>
32.0 °C je enako 305.15 K

Vpiši temperaturo [°C]: <span style="color: rgb(239, 69, 64);">23</span>
Želite pretvoriti v Kelvine (vpiši K) ali Fahrenheite (vpiši F)? <span style="color: rgb(239, 69, 64);">F</span>
23.0 °C je enako 73.4 °F

Vpiši temperaturo [°C]: <span style="color: rgb(239, 69, 64);">13</span>
Želite pretvoriti v Kelvine (vpiši K) ali Fahrenheite (vpiši F)? <span style="color: rgb(239, 69, 64);">C</span>
Vnesli ste napačno enoto!
</code></pre>
'''

if_clause = ['''\
<p>Preveri, v katere enote želiš pretvoriti stopinje?</p>''',
             '''\
<p>Uporabi pogojni stavek <code>if</code>!</p>''',
             '''\
<pre>
if pretvorba == 'K':
</pre>''']


plan = []

hint = {
    'no_input_call': '''<p>Za branje uporabimo funkcijo <code>input</code></p>''',

    'if_clause': if_clause,

    'printing': ['''\
<p>Izpiši rezultat.</p>'''],

    'final_hint': ['''\
<p>Program deluje pravilno!</p>'''],

}
