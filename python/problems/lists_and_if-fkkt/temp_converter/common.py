import re
from python.util import has_token_sequence, string_almost_equal, \
    string_contains_number, get_tokens, get_numbers, get_exception_desc
from server.hints import Hint

id = 25010
number = 6
visible = True

solution = '''\
temp_C = float(input("Vpiši temperaturo [°C]: "))
pretvorba = input("Želite pretvoriti v Kelvine (vpiši K) ali Fahrenheite (vpiši F)? ")
if pretvorba == "K":
    temp_K = temp_C + 273.15
    print(str(temp_C) + " °C je enako " + str(temp_K) + " K")
elif pretvorba == "F":
    temp_F = temp_C * 9/5 + 32
    print(str(temp_C) + " °C je enako " + str(temp_F) + " °F")
else:
    print("Vnesli ste napačno enoto!")
'''

hint_type = {
    'if_clause': Hint('if_clause'),
    'printing': Hint('printing'),
    'no_input_call' : Hint('no_input_call'),
    'final_hint': Hint('final_hint')
}

def test(python, code, aux_code=''):
    tokens = get_tokens(code)

    test_in = [
        (None, '32\nK\n'),
        (None, '23\nF\n'),
        (None, '3\n1\n'),
        (None, '13\nC\n')
    ]
    test_out = [
        '32.0 °C je enako 305.15 K',
        '23.0 °C je enako 73.4 °F',
        'Vnesli ste napačno enoto!',
        'Vnesli ste napačno enoto!'
    ]

    # List of outputs: (expression result, stdout, stderr, exception).
    answers = python(code=aux_code+code, inputs=test_in, timeout=1.0)
    outputs = [ans[1] for ans in answers]

    n_correct = 0
    tin = None
    for i, (output, correct) in enumerate(zip(outputs, test_out)):
        if output.rstrip().endswith(correct):
            n_correct += 1
        else:
            tin = test_in[i][1]
            tout = correct

    passed = n_correct == len(test_in)
    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_in)}}]
    if tin != None:
        hints.append({'id': 'problematic_test_case', 'args': {'testin': str(tin), 'testout': str(tout)}})
    else:
        hints.append({'id' : 'final_hint'})
    return passed, hints

def hint(python, code, aux_code=''):
    # run one test first to see if there are any exceptions
    test_in = [(None, '32\nK\n')]
    answer = python(code=aux_code+code, inputs=test_in, timeout=1.0)
    exc = get_exception_desc(answer[0][3])
    if exc: return exc

    tokens = get_tokens(code)

    # if input is not present in code, student needs to learn about input
    if not has_token_sequence(tokens, ['input']):
        return [{'id': 'no_input_call'}]

    if not has_token_sequence(tokens, ['if']):
        return [{'id' : 'if_clause'}]

    # student is not using print function
    if not has_token_sequence(tokens, ['print']):
        return [{'id' : 'printing'}]

    return None
