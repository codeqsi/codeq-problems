import server
mod = server.problems.load_language('python', 'sl')


name = 'Delitelji'
slug = 'Delitelji'


description = '''\
<p>Napiši program, ki izpiše vse delitelje števila, ki ga vnese uporabnik..'''


input_clause = ['''\
Uporabi funkcijo <code>input</code>.
''']

range_function = ['''\
Uporabi funkcijo <code>range(a,b)</code>
''',
                  '''\
<p>Funkcija <code>range(a, b)</code> pripravi seznam števil od a do števila b-1. Poskusi!</p>''',
                  '''\
<p>Z zanko <code>for</code> se sprehodi čez elemente seznama oz. v našem primeru
čez vsa števila od <code>a</code> do <code>b-1</code>.</p>
''']

divisor = ['''\
<p>Operator % vrne ostanek pri deljenju.</p>''',
           '''\
<pre>
>>> 5%3
2
>>> 17%8
1
</pre>''',
           '''\
<p>Če je ostanek pri deljenju števila <code>a</code> s številom <code>b</code> enak 0, potem <code>b</code>
deli število <code>a</code>. </p>''',
           '''\
<pre>
if a%b == 0:
    # b je delitelj števila a
</pre>
''']

plan = ['''\
<p>Preglej vsa števila od <code>1</code> do <code>n-1</code> in izpiši tista, ki delijo <code>n</code>.</p>
''',
        '''\
<p>Plan: </p>
<pre>
Uporabnik vnese število n
Za vsak element v seznamu [1, 2, ..., n-1]
    Preveri, če to število deli n
        Izpiši število
</pre>
''',
        range_function,
        divisor]


hint = {
    'input_clause': input_clause,

    'range_function': range_function,

    'printing': ['''\
<p>Izpiši rezultat.</p>'''],

    'divisor': divisor,

    'last_number': ['''\
<p>Število deli samega sebe!</p>'''],

    'final_hint': ['''\
 <p>Naloga rešena!</p>

 <p>Dejansko ne potrebujemo pregledati vseh števil med <code>1</code> in <code>n-1</code>,
 dovolj bo če gremo do kvadratnega korena števila <code>n</code>:</p>
 <pre>
from math import *

n = int(input('Vnesi število: '))
for i in range(1, int(sqrt(n)+1)):
    if n % i == 0:
        print(i, n/i)
</pre>'''],

    'zero_division': [mod.general_msg['error_head'],
                      mod.general_msg['general_exception'],
                      '''\
<p>Deljenje z nič ni dovoljeno!</p>''',
                      '''\
<p>Računanje ostanka z operatorjem % je tudi deljenje.''']

}
