import server
mod = server.problems.load_language('python', 'sl')


id = 223
name = 'Podnizi'
slug = 'Podnizi'


description = '''\
<p>
Program naj sestavi seznam vseh podnizov podane besede.
<pre>
Vpiši besedo: tema
['', 't', 'e', 'm', 'a', 'te', 'em', 'ma', 'tem', 'ema', 'tema']
</pre>
Vrstni red naj bo točno tak kot ga prikazuje primer. Začnemo s praznim nizom,
nato pridejo na vrsto podnizi dolžine ena, dva, tri, ...
</p>'''


substring = ['''\
<p>Del niza oz. del seznama dobimo z rezanjem</p>
''',
             '''\
<p>Za rezanje uporabi dvopičje</p>''',
             '''\
<pre>
>>> s = "Rezanje"
>>> s[:2]
'Re'
>>> s[2:]
'zanje'
>>> s[2:5]
'zan'
</pre>''']

plan = ['''\
<p>Potrebujemo dvojno zanko.  </p>
''',
        '''\
<p>Prva zanka gre po dolžini podniza, druga po začetku podniza. </p>''',
        '''\
<pre>
naredi seznam s praznim podnizom
Z zanko pojdi od 0 do dolžine niza:
    z zanko pojdi od 0 do dolžine podniza
        v seznam dodaj ustrezen podniz 
</pre>''',

        '''\
<p>Nov prazen seznam:</p>
<pre>
xs = []
</pre>
''',
        '''\
<p>Zanka od <code>0</code> do <code>n</code>:</p>
<pre>
for i in range(n):
</pre>''',
        substring]

hint = {
    'final_hint': ['''\
<p>Program je pravilen! <br>
</p>
'''],
}
