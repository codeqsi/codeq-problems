import server
mod = server.problems.load_language('python', 'sl')


id = 222
name = 'Deli'
slug = 'Deli'


description = '''\
<p>
Program naj sestavi sezam vseh delitev podane besede.
<pre>
Vpiši besedo: gozd
[('', 'gozd'), ('g', 'ozd'), ('go', 'zd'), ('goz', 'd'), ('gozd', '')]
</pre>
</p>'''

substring = ['''\
<p>Del niza oz. del seznama dobimo z rezanjem</p>
''',
             '''\
<p>Za rezanje uporabi dvopičje</p>''',
             '''\
<pre>
>>> s = "Rezanje"
>>> s[:2]
'Re'
>>> s[2:]
'zanje'
>>> s[2:5]
'zan'
</pre>''']

plan = ['''\
<p>Ta naloga je skorajda identična prejšnji (predpone), le da tu dodajamo dva podniza.  </p>
''',
        '''\
<pre>
naredi prazen seznam
Z zanko pojdi od 0 do dolžine niza:
    v seznam dodaj oba podniza
</pre>''',

        '''\
<p>Nov prazen seznam:</p>
<pre>
xs = []
</pre>
''',
        '''\
<p>Zanka od <code>0</code> do <code>n</code>:</p>
<pre>
for i in range(n):
</pre>''',
        substring]
hint = {
    'final_hint': ['''\
<p>Program je pravilen! <br>
</p>
'''],
}
