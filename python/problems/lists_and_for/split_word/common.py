import re
from python.util import has_token_sequence, string_almost_equal, \
    string_contains_number, get_tokens, get_numbers, get_exception_desc
from server.hints import Hint

id = 222
number = 8
visible = True

solution = '''\
s = input('Vpiši besedo: ')

xs = []
for i in range(len(s) + 1):
    xs.append((s[:i], s[i:]))
print(xs)
'''

hint_type = {
    'final_hint': Hint('final_hint')
}

def test(python, code, aux_code=''):
    in_out = [
        ('a', [('', 'a'), ('a', '')]),
        ('ab', [('', 'ab'), ('a', 'b'), ('ab', '')]),
        ('abc', [('', 'abc'), ('a', 'bc'), ('ab', 'c'), ('abc', '')]),
        ('gozd', [('', 'gozd'), ('g', 'ozd'), ('go', 'zd'), ('goz', 'd'), ('gozd', '')]),
    ]

    test_in = [(None, s[0]+'\n') for s in in_out]
    test_out = [str(s[1]) for s in in_out]
    # List of outputs: (expression result, stdout, stderr, exception).
    answers = python(code=aux_code+code, inputs=test_in, timeout=1.0)
    outputs = [ans[1] for ans in answers]

    n_correct = 0
    tin = None
    for i, (output, correct) in enumerate(zip(outputs, test_out)):
        if correct in output:
            n_correct += 1
        else:
            tin = test_in[i][1]
            tout = correct

    passed = n_correct == len(test_in)

    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_in)}}]
    if tin:
        hints.append({'id': 'problematic_test_case', 'args': {'testin': str(tin), 'testout': str(tout)}})
    if passed:
        hints.append({'id': 'final_hint'})
    return passed, hints

def hint(python, code, aux_code=''):
    tokens = get_tokens(code)

    # run one test first to see if there are any exceptions
    answer = python(code=aux_code+code, inputs=[(None, None)], timeout=1.0)
    exc = get_exception_desc(answer[0][3])
    if exc: return exc

    return None
