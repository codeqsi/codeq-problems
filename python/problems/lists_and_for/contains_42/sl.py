import server
mod = server.problems.load_language('python', 'sl')


name = 'Vsebuje'
slug = 'Vsebuje'


description = '''\
<p>Napiši program, ki ugotovi ali seznam števil vsebuje število 42.
Seznam z imenom <code>xs</code> definiraj na vrhu programa. Primer:</p>
<pre>
xs = [42, 5, 4, -7, 2, 12, -3, -4, 11, 42, 2]
</pre>
<p>Program naj izpiše le <code>True</code> ali <code>False</code>.
Seveda mora program delati za poljubne sezname in ne samo za seznam iz primera.</p>
'''

for_loop = ['''\
<p>Pregledati bo treba vse elemente v seznamu <code>xs</code>''',
            '''\
<p>Najlažje bo s <code>for</code> zanko.
''',
             '''\
<p>Poskusi naslednji dve vrstici:</p>
<pre>
for x in xs:
    print (x)
</pre>''',
             '''\
<p>V zgornjem primeru z zanko <code>for</code> Pythonu naročimo naj se sprehodi čez seznam <code>xs</code>
in na vsakem koraku trenutni element seznama shrani v spremenljivko <code>x</code>.
Kaj naj Python naredi s to spremenljivko, je zapisano v zamaknjenih vrsticah.
Tokrat vrednost le izpišemo.</p>''']

if_clause = ['''\
<p>Preveri, ali imamo število 42?</p>''',
             '''\
<p>Uporabi pogojni stavek <code>if</code>!</p>''',
             '''\
<pre>
if x == 42:
</pre>''']

seen_42 = ['''\
<p>Zapomni si, da si našel 42!</p>''',
           '''\
<p>Uporabi novo spremenljivko!</p>
''',
              '''\
<p>Spremenljivko na začetku nastavimo na False:</p>
<pre>
videl42 = False
</pre>
<p>in jo tekom zanke ustrezno spremenimo.''']

plan = ['''\
<p>Kako bi se tega lotil ročno? Nekako takole: </p>
<pre>
Za vsak element v seznamu
    Poglej, ali je 42?
        Če je, si zapomni, da si videl 42.
Izpiši, ali si videl 42 ali ne.
</pre>
<p>Zdaj pa je potrebno le še slovenščino prevesti v Python.</p>
''',
        for_loop,
        if_clause,
        seen_42]

hint = {
    'no_xs': ['''\
<p>Program mora imeti na začetku definiran seznam <code>xs</code>.</p>'''],

    'for_loop': for_loop,

    'if_clause': if_clause,

    'printing': ['''\
<p>Izpiši rezultat.</p>'''],

    'print_out_for': ['''\
<p>Pazi, da izpišeš rezultat izven zanke!</p>'''],

    'seen_42': seen_42,

    'final_hint': ['''\
<p>Program deluje pravilno!</p>'''],

    'final_hint_nobreak': ['''\
<p>Program deluje pravilno! <br>
Namig za bolj učinkovit program: ni vedno potrebno, da se program pregleda vse elemente. Če najdemo vrednost 42, nam ni
potrebno več naprej iskati - zanko lahko prekinemo z ukazom <code>break</code> </p>'''],

    'problematic_test_case': ['''\
<p>Program ne dela pravilno!<br>
Poskusi xs = [%=testin%] <br>
pravilen rezultat: [%=testout%]</p>
'''],
}
