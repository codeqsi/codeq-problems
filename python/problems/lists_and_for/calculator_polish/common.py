import re
from python.util import has_token_sequence, string_almost_equal, \
    string_contains_number, get_tokens, get_numbers, get_exception_desc
from server.hints import Hint

id = 224
number = 10
visible = True

solution = '''\
operacije = ["*", "+", "-", "/", "%"]

while True:
    izraz = input("Izraz: ").split()
    if not izraz:
        break

    sklad = []
    for e in izraz:
        sklad.append(e)
        if sklad[-1] in operacije:
            operacija = sklad.pop()
            o2 = int(sklad.pop())
            o1 = int(sklad.pop())
            if operacija == "+":
                rezultat = o1 + o2
            elif operacija == "-":
                rezultat = o1 - o2
            elif operacija == "*":
                rezultat = o1 * o2
            elif operacija == "/":
                rezultat = o1 // o2
            elif operacija == "%":
                rezultat = o1 % o2
            sklad.append(rezultat)

    print("Rezultat:", sklad[0])
    print()
'''

hint_type = {
    'final_hint': Hint('final_hint')
}

def test(python, code, aux_code=''):
    in_out = [
        (['6 3 +'], [9]),
        (['6 3 -'], [3]),
        (['6 3 /'], [2]),
        (['7 3 /'], [2]),
        (['6 3 *'], [18]),
        (['6 3 %'], [0]),
        (['1 2 +', '2 3 +', '1 2 3 4 5 * * * *'], [3, 5, 120]),
        (['1 2 + 3 +'], [6]),
        (['1 2 3 + +'], [6]),
        (['1 2 + 3 4 + *'], [21]),
        (['1 2 3 * 4 + +'], [11]),
        (['3 5 + 2 * 10 2 1 - * -'], [6]),
        (['11 22 33 * * 7 + 2 / 100 % 1 2 - -'], [97]),
    ]

    test_in = [(None, '\n'.join(s[0])+'\n') for s in in_out]
    test_out = [s[1] for s in in_out]
    # List of outputs: (expression result, stdout, stderr, exception).
    answers = python(code=aux_code+code, inputs=test_in, timeout=1.0)
    outputs = [ans[1] for ans in answers]

    n_correct = 0
    tin = None
    for i, (output, correct) in enumerate(zip(outputs, test_out)):
        if all(string_almost_equal(output, c, prec=2) for c in correct):
            n_correct += 1
        else:
            if not tin:
                tin = test_in[i][1]
                tout = correct

    passed = n_correct == len(test_in)

    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_in)}}]
    if tin:
        hints.append({'id': 'problematic_test_case', 'args': {'testin': str(tin), 'testout': str(tout)}})
    if passed:
        hints.append({'id': 'final_hint'})
    return passed, hints

def hint(python, code, aux_code=''):
    tokens = get_tokens(code)

    # run one test first to see if there are any exceptions
    answer = python(code=aux_code+code, inputs=[(None, None)], timeout=1.0)
    exc = get_exception_desc(answer[0][3])
    if exc: return exc

    return None
