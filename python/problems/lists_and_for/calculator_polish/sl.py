import server
mod = server.problems.load_language('python', 'sl')


id = 224
name = 'Kalkulator v inverzni poljski notaciji'
slug = 'Kalkulator v inverzni poljski notaciji'


description = '''\
<p>
V <a href="http://en.wikipedia.org/wiki/Reverse_Polish_notation">inverzni poljski ali postfiksni notaciji</a> ne potrebujemo oklepajev:
računska operacija sledi številoma in se vedno izvede takoj ter zamenja predhodni števili in operacijo z rezultatom operacije.

Poglejmo si računanje <code>(3 + 5) * 2 - 10 * (2 - 1)</code> ali postfiksno <code>3 5 + 2 * 10 2 1 - * -</code>:

<ul>
<li> Beremo vhod do prve operacije. </li>
<li> <code>3 5 + </code>; izvede se seštevanje, tri elemente zamenja rezultat 8, beremo dalje. </li>
<li> <code>8 2 * </code>; rezultat je 16, beremo dalje.</li>
<li> <code>16 10 2 1 -</code>; rezultat 1, beremo dalje.</li>
<li> <code>16 10 1 *</code>; rezultat 10, beremo dalje.</li>
<li> <code>16 10 -</code>; rezultat 6, beremo dalje.</li>
<li> <code>6</code>; vhoda je konec zato se se računanje ustavi.</li>
</ul>
Napišite kalkulator s celoštevilskimi operacijami seštevanja, odštevanja, množenja, deljenja in izračunom ostanka,
ki prejme vhod v inverzni poljski notaciji. Ko uporabnik ne poda ničesar (pritisne le enter), naj se program zaključi.
Števila ter operande ločujte s presledkom.
<pre>
Izraz: 3 5 +
Rezultat: 8

Izraz: 16 5 %
Rezultat: 1

Izraz: 16 5 /
Rezultat: 3

Izraz: 3 3 3 + +
Rezultat: 9

Izraz: 3 5 + 2 * 10 2 1 - * -
Rezultat: 6

Izraz:
</pre>
</p>'''

stack = ['''\
<p>
Uporabi sklad, saj prebrano operacijo vedno izvajamo na zadnjih dveh številih. 
</p>''', 
        '''\
<p>
Sklad je seznam, kjer se elementi vedno dodajajo in brišejo na koncu seznama. 
V Pythonovih seznamih imamo za ta namen metodi <code>append</code> in 
<code>pop</code>.
</p>'''
]

split = ['''\
<p> 
Niz razbijemo na besede z metodo <code>split</code>
</p>''',
        '''\
<pre>
>>> izraz = '3 5 + 2 * 10 2'
>>> izraz.split()
['3', '5', '+', '2', '*', '10', '2']
</pre>''',
        '''\
<p>Zanimivost, ki je ne potrebujete za to nalogo: 
če splitu damo znak v argument, bo razbil niz po tem znaku:</p>
<pre>
>>> b = 'brina,burja,miha;maja,maks'
>>> b.split(",")
>>> ['brina', 'burja', 'miha;maja', 'maks']
</pre>''']

plan = [stack,
        '''\
<pre>
for zanka čez vse elemente (števila, operacije):
    če je element operacija:
        Iz sklada preberi zadnji dve vrednosti, izračunaj in postavi nazaj na sklad
    drugače postavi število na sklad
</pre>
''',
        split]

hint = {
    'final_hint': ['''\
<p>Program je pravilen! <br>
</p>
'''],
}
