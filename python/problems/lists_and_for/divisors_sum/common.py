import re
from python.util import has_token_sequence, string_almost_equal, \
    string_contains_number, get_tokens, get_numbers, get_exception_desc
from server.hints import Hint

id = 216
number = 21
visible = True

solution = '''\
n = int(input('Vnesi število: '))
vsota = 0
for i in range(1, n):
    if n % i == 0:
        vsota += i
print(vsota)
'''

hint_type = {
    'input_clause': Hint('input_clause'),
    'range_function': Hint('range_function'),
    'printing': Hint('printing'),
    'divisor': Hint('divisor'),
    'final_hint': Hint('final_hint'),
    'zero_division': Hint('zero_division'),
    'last_number': Hint('last_number'),
    'summing': Hint('summing'),
}

def test(python, code, aux_code=''):
    test_in = [
        (None, '8\n'),
        (None, '6\n'),
        (None, '5\n'),
        (None, '2\n'),
        (None, '15\n'),
        (None, '20\n'),
    ]
    test_out = [
            7,
            6,
            1,
            1,
            9,
            22]

    # List of outputs: (expression result, stdout, stderr, exception).
    answers = python(code=aux_code+code, inputs=test_in, timeout=1.0)
    outputs = [ans[1] for ans in answers]

    n_correct, tin = 0, None
    for i, (output, result) in enumerate(zip(outputs, test_out)):
        if result in get_numbers(output):
            n_correct += 1
        else:
            tin = test_in[i][1]
            tout = result

    passed = n_correct == len(test_in)
    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_in)}}]
    if tin:
        hints.append({'id': 'problematic_test_case', 'args': {'testin': str(tin), 'testout': str(tout)}})
    if passed:
        hints.append({'id': 'final_hint'})
    return passed, hints

def hint(python, code, aux_code=''):
    tokens = get_tokens(code)
    # run one test first to see if there are any exceptions
    answer = python(code=aux_code+code, inputs=[(None, '8\n')], timeout=1.0)
    exc = get_exception_desc(answer[0][3])
    if exc:
        if 'ZeroDivisionError' in answer[0][3]:
            return [{'id' : 'zero_division', 'args': {'message': answer[0][3]}}]
        else:
            return exc

    # if has no input, tell him how to ask questions
    if not has_token_sequence(tokens, ['input']):
        return [{'id' : 'input_clause'}]

    # if it has no range, explain how we can create a list (generator) from a number
    if not has_token_sequence(tokens, ['range']):
        return [{'id' : 'range_function'}]

    # if it has no print function, tell him to use print
    if not has_token_sequence(tokens, ['print']):
        return [{'id' : 'printing'}]

    # if it has no divisoin-by-modulo operator, explain that operator
    if not has_token_sequence(tokens, ['%']):
        return [{'id' : 'divisor'}]

    # if the program prints 15 instead of 7,
    # it means that is adds also itself
    nums = get_numbers(answer[0][1])
    if 15 in nums and 7 not in nums:
        return [{'id' : 'last_number'}]

    # if program prints only the last number or 
    # only the first number, explain how to sum
    # elements in iterations
    if 7 not in nums and \
       (1 in nums or 4 in nums or 8 in nums):
        return [{'id' : 'summing'}]


    return None
