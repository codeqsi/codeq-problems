import server
mod = server.problems.load_language('python', 'sl')


id = 216
name = 'Vsota deliteljev'
slug = 'Vsota deliteljev'


description = '''\
<p> Napiši program, ki izpiše vsoto vseh deliteljev števila, ki ga vnese uporabnik. 
Med delitelji izpustite vnešeno število. Primer: </p>
<pre>
>>> Vpiši število: 6
Vsota deliteljev: 6
</pre>'''

input_clause = ['''\
Uporabi funkcijo <code>input</code>.
''']

range_function = ['''\
Uporabi funkcijo <code>range(a,b)</code>
''',
                  '''\
<p>Funkcija <code>range(a, b)</code> pripravi seznam števil od a do števila b-1. Poskusi!</p>''',
                  '''\
<p>Z zanko <code>for</code> se sprehodi čez elemente seznama oz. v našem primeru
čez vsa števila od <code>a</code> do <code>b-1</code>.</p>
''']

divisor = ['''\
<p>Operator % vrne ostanek pri deljenju.</p>''',
           '''\
<pre>
>>> 5%3
2
>>> 17%8
1
</pre>''',
           '''\
<p>Če je ostanek pri deljenju števila <code>a</code> s številom <code>b</code> enak 0, potem <code>b</code>
deli število <code>a</code>. </p>''',
           '''\
<pre>
if a%b == 0:
    # b je delitelj števila a
</pre>
''']

summing = ['''\
<p>Če hočemo delitelje prišteti vsoti, moramo imeti vsoto nekje shranjeno. 
Torej potrebujemo spremenljivko.</p>''',
           '''\
<p>Nekako tako: </p>
<pre>
vsota = 0
for ....
    if ...
        vsota += d # d deli izbrano število
</pre>''']

plan = ['''\
<p>Plan bo podoben kot v prejšnji naloge, kjer smo delitelje izpisali. 
Tokrat delitelja ne izpišemo, ampak ga prištejemo vsoti. </p>
''',
        summing]


hint = {
    'input_clause': input_clause,

    'range_function': range_function,

    'printing': ['''\
<p>Izpiši rezultat.</p>'''],

    'divisor': divisor,

    'last_number': ['''\
<p>Tokrat med delitelji ne upoštevamo vnešenega števila!</p>'''],

    'final_hint': ['''\
<p>Naloga rešena! <br>
Nakoč bomo znali to napisati krajše: </p>
<pre>
n = int(input("Vpiši število: "))
print(sum(i for i in range(1, n) if n % i == 0))
</pre>'''],

    'summing': summing,

    'zero_division': [mod.general_msg['error_head'],
                      mod.general_msg['general_exception'],
                      '''\
<p>Deljenje z nič ni dovoljeno!</p>''',
                      '''\
<p>Računanje ostanka z operatorjem % je tudi deljenje.''']

}
