import re
from python.util import has_token_sequence, string_almost_equal, \
    string_contains_number, get_tokens, get_numbers, get_exception_desc
from server.hints import Hint

id = 220
number = 6
visible = True

solution = '''\
xs = [42, 5, 4, -7, 2, 12, -3, -4, 11, 42, 2]

ys = []
i = 2
while i < len(xs):
    ys.append(xs[i])
    i += 3
print(ys)
'''

hint_type = {
    'final_hint': Hint('final_hint')
}

def test(python, code, aux_code=''):
    in_out = [
        ([], []),
        ([0], []),
        ([0, 1], []),
        ([0, 1, 2], [2]),
        ([1, 2, 3, 4, 5, 5, 4, 3, 2, 1], [3, 5, 2]),
        ([42, 5, 4, -7, 2, 12, -3, -4, 11, 42, 2], [4, 12, 11]),
        (list(range(15)), [2, 5, 8, 11, 14])
    ]

    test_in = [t[0] for t in in_out]
    test_out = [t[1] for t in in_out]

    n_correct = 0
    tin = None
    for xs_i, xs in enumerate(test_in):
        # change code to contain new xs instead of the one
        # given by user
        tcode = re.sub(r'^xs\s*=\s*\[.*?\]',
                       'xs = ' + str(xs),
                       code,
                       flags = re.DOTALL | re.MULTILINE)

        # use python session to call tcode
        answers = python(code=aux_code+tcode, inputs=[(None, None)], timeout=1.0)
        output = answers[0][1]

        if str(test_out[xs_i]) in output:
            n_correct += 1
        else:
            tin = test_in[xs_i]
            tout = test_out[xs_i]
    passed = n_correct == len(test_in)

    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_in)}}]
    if tin != None:
        hints.append({'id': 'problematic_test_case', 'args': {'testin': str(tin), 'testout': str(tout)}})
    if passed:
        hints.append({'id': 'final_hint'})
    return passed, hints

def hint(python, code, aux_code=''):
    tokens = get_tokens(code)

    # run one test first to see if there are any exceptions
    answer = python(code=aux_code+code, inputs=[(None, None)], timeout=1.0)
    exc = get_exception_desc(answer[0][3])
    if exc: return exc

    return None
