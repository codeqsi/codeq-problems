import server
mod = server.problems.load_language('python', 'sl')


id = 220
name = 'Vsak tretji'
slug = 'Vsak tretji'


description = '''\
<p>
Napiši program, ki iz podanega seznama sestavi nov seznam, ki vsebuje le vsak tretji element podanega seznama.
Kot dosedaj, naj se program začne z
<pre>
xs = [42, 5, 4, -7, 2, 12, -3, -4, 11, 42, 2]
</pre>
in (v tem primeru) izpiše
<pre>
[4, 12, 11]
</pre>
</p>'''

empty_list = ['''\
<p>Nov, prazen seznam ustvarimo s stavkom: </p>
<pre>
s = []
</pre>
''']

enumerate_function = ['''\
<p> Uporabi funkcijo <code>enumerate</code>.
</p>
''',
    '''\
<pre>
for index, val in enumerate(xs):
    print index, v
</pre>''',
    '''\
<p>V zgornji <code>for</code>zanki se v spremenljivko <code>val</code> zapiše
trenutni element v seznamu <code>xs</code>, v spremenljivki <code>indeks</code>
pa je zapisano mesto oz. indeks trenutnega elementa. </p>''']

append_method = [
        '''\
<p>V seznam dodamo nov element z metodo <code>append</code>.</p>''',
        '''\
<p>Poskusi: </p>
<pre>
s = []
s.append(3)
s.append(5)
print (s)
</pre>''']

plan = ['''\
<p>S <code>for</code> zanko pojdi čez seznam. V novi seznam dodaj vsak tretji element. </p>
''',
        '''\
<p>
<pre>
Ustvari prazen seznam mest
Za vsak element v seznamu
    Ali je (mesto elementa + 1) deljivo s 3?
        Če je, dodaj trenutno mesto v seznam mest
Izpiši seznam mest
</pre>
</p>''',
        empty_list,
        enumerate_function,
        mod.general_msg["modulo"],
        append_method]

hint = {
    'final_hint': ['''\
<p>Program je pravilen! <br>
</p>
'''],
}
