import server
mod = server.problems.load_language('python', 'sl')


id = 217
name = 'Popolna števila'
slug = 'Popolna števila'


description = '''\
<p> Napiši program, ki izpiše "je popolno", če je podano število popolno, oz. 
"ni popolno", če ni. Popolna števila so števila, ki so enaka vsoti svojih 
deliteljev (brez samega sebe). Primer popolnega števila je 28, 
saj so njegovi delitelji 1, 2, 4, 7, 14, njihova vsota, 
1+2+4+7+14 pa je spet enaka 28. </p>'''

input_clause = ['''\
Uporabi funkcijo <code>input</code>.
''']

range_function = ['''\
Uporabi funkcijo <code>range(a,b)</code>
''',
                  '''\
<p>Funkcija <code>range(a, b)</code> pripravi seznam števil od a do števila b-1. Poskusi!</p>''',
                  '''\
<p>Z zanko <code>for</code> se sprehodi čez elemente seznama oz. v našem primeru
čez vsa števila od <code>a</code> do <code>b-1</code>.</p>
''']

divisor = ['''\
<p>Operator % vrne ostanek pri deljenju.</p>''',
           '''\
<pre>
>>> 5%3
2
>>> 17%8
1
</pre>''',
           '''\
<p>Če je ostanek pri deljenju števila <code>a</code> s številom <code>b</code> enak 0, potem <code>b</code>
deli število <code>a</code>. </p>''',
           '''\
<pre>
if a%b == 0:
    # b je delitelj števila a
</pre>
''']

summing = ['''\
<p>Če hočemo delitelje prišteti vsoti, moramo imeti vsoto nekje shranjeno. 
Torej potrebujemo spremenljivko.</p>''',
           '''\
<p>Nekako tako: </p>
<pre>
vsota = 0
for ....
    if ...
        vsota += d # d deli izbrano število
</pre>''']

plan = ['''\
<p>Gre za praktično enako nalogo, kot je bila prejšnja. Le na koncu 
moramo preveriti, če je vsota deliteljev enaka vnešenemu številu. </p>
''']


hint = {
    'input_clause': input_clause,

    'range_function': range_function,

    'printing': ['''\
<p>Izpiši rezultat.</p>'''],

    'divisor': divisor,

    'final_hint': ['''\
<p>Naloga rešena! <br>
Podobno kot prejšnjo, lahko rešitev napišemo krajše: </p>
<pre>
n = int(input("Vpiši število: "))
print(n == sum(i for i in range(1, n) if n % i == 0))
</pre>'''],

    'summing': summing,

    'zero_division': [mod.general_msg['error_head'],
                      mod.general_msg['general_exception'],
                      '''\
<p>Deljenje z nič ni dovoljeno!</p>''',
                      '''\
<p>Računanje ostanka z operatorjem % je tudi deljenje.''']

}
