import server
mod = server.problems.load_language('python', 'sl')


name = 'Štej'
slug = 'Štej'


description = '''\
<p>Napiši program, ki v podanem seznamu prešteje število ponovitev števila 42. 
Seznam <code>xs</code> definiraj na vrhu programa, kot v prejšnjih nalogah,
izpis pa naj bo takšen: "Število 42 se v seznamu pojavi 2 krat."'''

for_loop = ['''\
<p>Pregledati bo treba vse elemente v seznamu <code>xs</code>''',
            '''\
<p>Najlažje s <code>for</code> zanko.
''',
             '''\
<p>Poskusi naslednji dve vrstici:</p>
<pre>
for x in xs:
    print (x)
</pre>''',
             '''\
<p>V zgornjem primeru z zanko <code>for</code> Pythonu naročimo naj se sprehodi čez seznam <code>xs</code>
in na vsakem koraku trenutni element seznama shrani v spremenljivko <code>x</code>.
Kaj naj Python naredi s to spremenljivko, je zapisano v zamaknjenih vrsticah.
V danem primeru vrednost le izpišemo.</p>''']

if_clause = ['''\
<p>Preveri, ali imamo število 42?</p>''',
         '''\
<p>Uporabi pogojni stavek <code>if</code>!</p>''',
         '''\
<pre>
if x == 42:
</pre>''']

count_42 = ['''\
<p>Zapomni si, da si našel 42! Povečaj števec!</p>''',
            '''\
<p>Števec na začetku nastavimo na 0</p>
<pre>
stevec = 0
</pre>
<p>in ga tekom zanke ustrezno spreminjamo.''']

plan = ['''\
<p><b>Plan</b>: Preglej vse elemente v seznamu in vsakič, ko srečaš 42, povečaj 
števec za 1.</p>
''',
        '''\
<p>Bolj podroben plan: </p>
<pre>
Za vsak element v seznamu
    Poglej, ali je 42?
        Če je, povečaj števec.
Izpiši števec.
</pre>
<p>Zdaj pa je potrebno le še slovenščino prevesti v Python.</p>
''',
        for_loop,
        if_clause,
        count_42]

hint = {
    'no_xs': ['''\
<p>Program mora imeti na začetku definiran seznam <code>xs</code>.</p>'''],

    'for_loop': for_loop,

    'if_clause': if_clause,

    'printing': ['''\
<p>Izpiši rezultat.</p>'''],

    'print_out_for': ['''\
<p>Pazi, da izpišeš rezultat izven zanke!</p>'''],

    'final_hint': ['''\
<p>Program je pravilen! <br>
Nalogo lahko rešiš hitreje, če poznaš metodo <code>count</code></p>
 <pre>
xs = [42, 5, 4, -7, 2, 12, -3, -4, 11, 42, 2]
print('Število 42 se v seznamu pojavi', xs.count(42), 'krat.')
</pre>'''],

    'problematic_test_case': ['''\
<p>Program ne dela pravilno!<br>
Poskusi xs = [%=testin%] <br>
pravilen rezultat: [%=testout%]</p>
'''],

}
