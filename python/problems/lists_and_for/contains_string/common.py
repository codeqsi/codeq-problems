import re
from python.util import has_token_sequence, string_almost_equal, \
    string_contains_number, get_tokens, get_numbers, get_exception_desc
from server.hints import Hint

id = 194
number = 2
visible = True

solution = '''\
xs = ['foo', 'bar', 'baz', 'Waldo', 'foobar']

vsebuje = False
for x in xs:
    if x == 'Waldo':
        vsebuje = True
print(vsebuje)
'''

hint_type = {
    'no_xs': Hint('no_xs'),
    'for_loop': Hint('for_loop'),
    'if_clause': Hint('if_clause'),
    'printing': Hint('printing'),
    'print_out_for': Hint('print_out_for'),
    'final_hint': Hint('final_hint')
}

def test(python, code, aux_code=''):
    test_xs = [['waldo', 'foo', 'bar', 'baz'],
               ['foo', 'bar', 'baz', 'Waldo', 'foobar'],
               ['foo', 'bar', 'baz', 'Waldo'],
               ['Waldo', 'foo', 'bar', 'baz'],
               [],
               ['Waldo']]
    test_out = [
        False,
        True,
        True,
        True,
        False,
        True
    ]

    n_correct = 0
    tin = None
    for xs_i, xs in enumerate(test_xs):
        # change code to contain new xs instead of the one
        # given by user
        tcode = re.sub(r'^xs\s*=\s*\[.*?\]',
                       'xs = ' + str(xs),
                       code,
                       flags = re.DOTALL | re.MULTILINE)

        # use python session to call tcode
        answers = python(code=aux_code+tcode, inputs=[(None, None)], timeout=1.0)
        output = answers[0][1]

        if str(test_out[xs_i]) in output and \
           str(not test_out[xs_i]) not in output:
            n_correct += 1
        else:
            tin = test_xs[xs_i]
            tout = test_out[xs_i]

    passed = n_correct == len(test_xs)
    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_xs)}}]
    if tin != None:
        hints.append({'id': 'problematic_test_case', 'args': {'testin': str(tin), 'testout': str(tout)}})
    if passed:
        hints.append({'id': 'final_hint'})

    return passed, hints

def hint(python, code, aux_code=''):
    # run one test first to see if there are any exceptions
    answer = python(code=aux_code+code, inputs=[(None, None)], timeout=1.0)
    exc = get_exception_desc(answer[0][3])
    if exc: return exc

    tokens = get_tokens(code)

    # if has no xs, tell him to ask for values
    if not has_token_sequence(tokens, ['xs', '=', '[']):
        return [{'id' : 'no_xs'}]

    # student does not have while or for: instruct him on loops
    if not has_token_sequence(tokens, ['while']) and \
       not has_token_sequence(tokens, ['for']):
        return [{'id' : 'for_loop'}]

    if not has_token_sequence(tokens, ['if']):
        return [{'id' : 'if_clause'}]

    # student is not using print function
    if not has_token_sequence(tokens, ['print']):
        return [{'id' : 'printing'}]

    # student is not using print function
    if not has_token_sequence(tokens, ['\n', 'print']):
        return [{'id' : 'print_out_for'}]


    return None
