import server
mod = server.problems.load_language('python', 'sl')


name = 'Vsebuje niz'
slug = 'Vsebuje niz'


description = '''\
<p>Enako kot naloga vsebuje, le da bomo tokrat v seznamu nizov iskali niz 'Waldo'.
Program naj se začne z </p>
<pre>
xs = ['foo', 'bar', 'baz', 'Waldo', 'foobar']
</pre>
<p> in izpiše <code>True</code> ali <code>False</code>.</p>
'''

for_loop = ['''\
<p>Pregledati bo treba vse elemente v seznamu <code>xs</code>''',
            '''\
<p>Najlažje bo s <code>for</code> zanko.
''',
             '''\
<p>Poskusii naslednji dve vrstici:</p>
<pre>
for x in xs:
    print (x)
</pre>''',
             '''\
<p>V zgornjem primeru z zanko <code>for</code> Pythonu naročimo naj se sprehodi čez seznam <code>xs</code>
in na vsakem koraku trenutni element seznama shrani v spremenljivko <code>x</code>.
Kaj naj Python naredi s to spremenljivko, je zapisano v zamaknjenih vrsticah.
Tokrat vrednost le izpišemo.</p>''']

if_clause = ['''\
<p><code>Poglej, ali je element Waldo?</code> Uporabite pogojni stavek <b>if</b>!</p>''',
             '''\
<pre>
if x == 'Waldo':
</pre>''']


plan = ['''\
<p>Plan je enak kot pri prvi nalogi:
<pre>
Za vsak element v seznamu
    Poglej, ali je element enak 'Waldo'?
        Če je, si to zapomni.
Izpiši, ali si videl 'Waldo' ali ne.
</pre>
''',
        for_loop,
        if_clause]

hint = {
    'no_xs': ['''\
<p>Program mora imeti na začetku definiran seznam <code>xs</code>.</p>'''],

    'for_loop': for_loop,

    'if_clause': if_clause,

    'printing': ['''\
<p>Izpiši rezultat!</p>'''],

    'print_out_for': ['''\
<p>Pazi, da izpišeš rezultat izven zanke!</p>'''],

    'problematic_test_case': ['''\
<p>Program ne dela pravilno!<br>
Poskusi xs = [%=testin%] <br>
pravilen rezultat: [%=testout%]</p>
'''],

    'final_hint': ['''\
<p>Program deluje pravilno! <br>
Nalogo lahko rešiš hitreje z operatorjem <code>in</code></p>
<pre>
xs = ['foo', 'bar', 'baz', 'Waldo', 'foobar']
print('Waldo' in xs)
</pre>'''],

}
