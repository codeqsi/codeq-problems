import server
mod = server.problems.load_language('python', 'sl')


id = 218
name = 'Mesta'
slug = 'Mesta'


description = '''\
<p>
Program naj sestavi seznam vseh mest, na katerih se v podanem seznamu pojavi število 42.
Za seznam iz prve naloge
<pre>
xs = [42, 5, 4, -7, 2, 12, -3, -4, 11, 42, 2]
</pre>
naj vaš program izpiše [0, 9], saj se število 42 pojavi na ničtem in devetem mestu (če začnemo šteti z 0).
</p>'''

empty_list = ['''\
<p>Nov, prazen seznam ustvarimo s stavkom: </p>
<pre>
s = []
</pre>
''']

enumerate_function = ['''\
<p> Uporabi funkcijo <code>enumerate</code>.
</p>
''',
    '''\
<pre>
for index, val in enumerate(xs):
    print index, v
</pre>''',
    '''\
<p>V zgornji <code>for</code>zanki se v spremenljivko <code>val</code> zapiše 
trenutni element v seznamu <code>xs</code>, v spremenljivki <code>indeks</code>
pa je zapisano mesto oz. indeks trenutnega elementa. </p>''']

append_method = [
        '''\
<p>V seznam dodamo nov element z metodo <code>append</code>.</p>''',
        '''\
<p>Poskusi: </p>
<pre>
s = []
s.append(3)
s.append(5)
print (s)
</pre>''']

plan = ['''\
<p>S <code>for</code> zanko pojdi čez seznam. Ko naletiš na 42, dodaj mesto trenutnega elementa
v nov seznam. </p>
''',
        '''\
<p>
<pre>
Ustvari prazen seznam mest
Za vsak element v seznamu
    Ali je 42?
        Če je, dodaj trenutno mesto v seznam mest
Izpiši seznam mest
</pre>
</p>''',
        empty_list,
        enumerate_function,
        append_method]

hint = {
    'final_hint': ['''\
<p>Program je pravilen! <br>
</p>
'''],
}




