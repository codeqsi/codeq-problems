import server
mod = server.problems.load_language('python', 'sl')


id = 219
name = 'Vsebuje večkratnik'
slug = 'Vsebuje večkratnik'


description = '''\
<p>
Napiši program, ki izpiše <code>True</code>, če se v seznamu števil pojavi večkratnik števila
<code>42</code>, sicer izpiše <code>False</code>.
Seznam <code>xs</code> definiraj na vrhu programa.
</p>'''



plan = ['''\
<p>Popolnoma enaka naloga kot naloga "Vsebuje", le pri pogoju poglej, ali je število deljivo z 42.</p>
''',
        mod.general_msg["modulo"]]

hint = {
    'final_hint': ['''\
<p>Program je pravilen! <br>
</p>
'''],
}
