import server
mod = server.problems.load_language('python', 'sl')


id = 221
name = 'Predpone'
slug = 'Predpone'


description = '''\
<p>
Uporabnika prosite, da vam zaupa besedo, nato pa sestavite in izpišite seznam vseh predpon te besede.
<pre>
Vpiši besedo: drevo
['', 'd', 'dr', 'dre', 'drev', 'drevo']
</pre>
</p>'''

substring = ['''\
<p>Del niza oz. del seznama dobimo z rezanjem</p>
''',
             '''\
<p>Za rezanje uporabi dvopičje</p>''',
             '''\
<pre>
>>> s = "Rezanje"
>>> s[:2]
'Re'
>>> s[2:]
'zanje'
>>> s[2:5]
'zan'
</pre>''']

plan = ['''\
<p>Z zanko pojdi čez vse črke v besedi. Vsakič v seznam dodaj podniz od prvega znaka do trenutnega.  </p>
''',
        '''\
<pre>
naredi prazen seznam
Z zanko pojdi od 0 do dolžine niza:
    v seznam dodaj podniz (od prvega znaka do trenutnega)
</pre>''',

        '''\
<p>Nov prazen seznam:</p>
<pre>
xs = []
</pre>
''',
        '''\
<p>Zanka od <code>0</code> do <code>n</code>:</p>
<pre>
for i in range(n):
</pre>''',
        substring]


hint = {
    'final_hint': ['''\
<p>Program je pravilen! <br>
</p>
'''],
}
