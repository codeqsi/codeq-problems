import server
mod = server.problems.load_language('python', 'sl')

name = 'Blagajna "top shop"'
slug = 'Blagajna "top shop"'


description = '''\
<p>Tretja trgovina se je odločila, da bo konkurirala drugi tako, da bo imela na blagajnah krajše vrste kot
druga, pri kateri se plačevanje odvija počasi zato, ker morajo blagajniki prešteti izdelke preden lahko
začnejo vnašati njihove cene. Popravi program tako, da ne vpraša po številu izdelkov, temveč sprašuje
po cenah toliko časa, dokler mu blagajnik ne vnese ničle. Program naj na koncu izpiše tudi povprečno
ceno.</p>
<pre>
Cena artikla: 2
Cena artikla: 4
Cena artikla: 1
Cena artikla: 0
Vsota: 7
Poprečna cena: 2.33333333333
</pre>
'''

main_plan = ['''\
<p>Plan:</p>
<pre>
1. Ponavljaj dokler je cena večja od 0:
    2. Preberi ceno.
    3. Prištej vsoti.
4. Izpiši vsoto in povprečje.
</pre>''']

while_condition = ['''\
<p>Koliko korakov naj naredi zanka?</p>''',
                   '''\
<p>Ne vemo, koliko korakov naj naredi zanka. Vemo pa,
kdaj se bo zanka ustavila: ko bo cena enaka 0!</p>'''
                   '''\
<pre>
while cena != 0:
    ...
</pre>'''
]

plan = [main_plan,
        while_condition]

while_clause = ['''\
<p>Uporabi zanko <while</p>''',
         '''\
<pre>
while Pogoj:
    stavek 1
    stavek 2
    ...
stavek n # stavek izven while.
</pre>''',
         '''\
<p>Stavki znotraj while (zamaknjeni) se izvajajo toliko časa, dokler velja <code>Pogoj</code>.
Ko pogoj ne velja več, Python preskoči vrstice, ki so del while-a in nadaljuje s stavki, ki sledijo – v
našem primeru s stavkom n.</p>'''
        ]

hint = {
    'while_clause': while_clause,

    'while_condition': while_condition,

    'average': ['''\
<p>Formula za povprečje: povp = vsota / št.elementov</p>''',
                '''\
<p>Vsoto že znamo izračunati, za št. elementov pa potrebujemo števec.'''],

    'printing': ['''\
<p>Izpiši rezultat!</p>''',
                 '''\
<p> V Pythonu izpisujemo s funkcijo <code>print</code>. <p>''',
'''<p>Pazi, da stavek s <code>print</code> ne bo zamaknjen, saj bo v takem
primeru del while-a in se bo večkrat izpisal. </p>'''],

    'nonumber': ['''<p>Izpiši vsoto<p>'''],

    'name_error' : [mod.general_msg['error_head'],
                    mod.general_msg['general_exception'],
                    mod.general_msg['name_error'],
                    '''\
<p>Verjetno v pogoju uporabljaš nedefinirano spremenljivko.''',
                    '''\
<pre>
cena = 1
while cena != 0:
    ...'''],

    'final_hint': ['''\
<p>Odlično, program je pravilen! <br>
Kaj bi pa moral narediti, da bi število -1 pomenilo konec?</p>''',
                   '''\
<p>Spremeniti pogoj in paziti,
da se vrednost -1 ne prišteje vsoti!</p>'''],

    'problematic_test_case': ['''\
<p>Zaporedje cen, kjer program ne dela prav: [%=testin%]<br>
Pravilna vsota [%=sum%], pravilno povprečje: [%=avg%]</p>''']

}
