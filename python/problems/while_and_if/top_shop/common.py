from python.util import has_token_sequence, string_almost_equal, \
    string_contains_number, get_tokens, get_numbers, get_exception_desc
from server.hints import Hint

id = 198
number = 3
visible = True

solution = '''\
cena = 1
stevec, vsota = 0, 0
while cena != 0:
  cena = int(input("Cena: "))
  vsota += cena
  stevec += 1
print('Vsota:', vsota)
print('Povprečje: ', vsota / stevec)
'''

hint_type = {
    'printing': Hint('printing'),
    'while_clause': Hint('while_clause'),
    'nonumber': Hint('nonumber'),
    'while_condition': Hint('while_condition'),
    'average': Hint('while_condition'),
    'final_hint': Hint('final_hint'),
    'minimax': Hint('minimax')
}

def test(python, code, aux_code=''):
    # List of inputs: (expression to eval, stdin).
    test_in = [
        (None, '2\n4\n1\n0\n'),
        (None, '1\n1\n1\n1\n1\n0\n'),
        (None, '1\n2\n0\n'),
        (None, '5\n4\n3\n11\n7\n0\n'),
        (None, '0\n'),
    ]

    test_out = [
        (7, 2.333),
        (5, 1),
        (3, 1.5),
        (30, 6),
        (0, 0),
    ]

    # List of outputs: (expression result, stdout, stderr, exception).
    answers = python(code=aux_code+code, inputs=test_in, timeout=1.0)
    outputs = [ans[1] for ans in answers]

    n_correct = 0
    tin = None
    for i, (output, correct) in enumerate(zip(outputs, test_out)):
        if all(string_almost_equal(output, correct[i], prec=2) for i in range(2)):
            n_correct += 1
        else:
            tin = test_in[i][1]
            tout = correct

    passed = n_correct == len(test_in)
    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_in)}}]
    if tin:
        hints.append({'id': 'problematic_test_case', 'args': {'testin': str(tin),
                                                              'sum': str(tout[0]),
                                                              'avg': str(tout[1]),
                                                              }})
    if n_correct == len(test_in): # add an iteresting hint
        tokens = get_tokens(code)
        if has_token_sequence(tokens, ['!=', '0.', ':']):
            hints.append({'id': 'final_hint'})
    return passed, hints

def hint(python, code, aux_code=''):
    tokens = get_tokens(code)

    # run one test first to see if there are any exceptions
    test_in = [(None, '1\n1\n1\n1\n1\n0\n')]
    answer = python(code=aux_code+code, inputs=test_in, timeout=1.0)
    exc = get_exception_desc(answer[0][3])
    if exc:
        if 'NameError' in answer[0][3]:
            return [{'id':'name_error', 'args': {'message': answer[0][3]}}]
        else:
            return exc

    # student does not have while or for: instruct him on loops
    if not has_token_sequence(tokens, ['while']) and \
       not has_token_sequence(tokens, ['for']):
        return [{'id' : 'while_clause'}]

    # student is not using division, therefore computing no average
    if not has_token_sequence(tokens, ['/']):
        return [{'id' : 'average'}]

    # student is not using print function
    if not has_token_sequence(tokens, ['print']):
        return [{'id' : 'printing'}]

    # student does not print any values
    if not get_numbers(answer[0][1]):
        return [{'id' : 'nonumber'}]

    # student's answer is not correct
    if not string_almost_equal(answer[0][1], 5):
        return [{'id' : 'while_condition'}]

    return None
