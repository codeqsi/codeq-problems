from python.util import has_token_sequence, string_almost_equal, \
    string_contains_number, get_tokens, get_numbers, get_exception_desc
from server.hints import Hint

id = 200
number = 5
visible = True

solution = '''\
stanje = 0
while stanje > -100:
    stanje += int(input('Sprememba '))
    print('Stanje', stanje)
print('Bankrot')
'''

hint_type = {
    'while_clause': Hint('while_clause'),
    'while_condition': Hint('while_condition'),
}

def test(python, code, aux_code=''):
    # List of inputs: (expression to eval, stdin).
    test_in = [
        (None, '23\n15\n-30\n10\n100\n-200\n-50\n'),
        (None, '10\n-100\n1000\n-10000\n'),
        (None, '1\n2\n'),
        (None, '-1000\n'),
        (None, '0\n'),
    ]

    test_out = [
        (-132, True),
        (-9090, True),
        (3, False),
        (-1000, True),
        (0, False),
    ]

    # List of outputs: (expression result, stdout, stderr, exception).
    answers = python(code=aux_code+code, inputs=test_in, timeout=1.0)
    outputs = [ans[1] for ans in answers]

    n_correct = 0
    tin = None
    for i, (output, correct) in enumerate(zip(outputs, test_out)):
        if string_almost_equal(output, correct[0]) and \
           ('Bankrot' in output) == correct[1]:
            n_correct += 1
        else:
            tin = test_in[i][1]
            tout = correct

    passed = n_correct == len(test_in)
    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_in)}}]
    if tin:
        hints.append({'id': 'problematic_test_case', 'args': {'testin': str(tin),
                                                              'sum': str(tout[0]),
                                                              'bancrupt': str(tout[1])}})
    return passed, hints

def hint(python, code, aux_code=''):
    tokens = get_tokens(code)

    # run one test first to see if there are any exceptions
    test_in = [(None, '23\n15\n-30\n8\n10\n100\n-200\n-50\n')]
    answer = python(code=aux_code+code, inputs=test_in, timeout=1.0)
    exc = get_exception_desc(answer[0][3])
    if exc:
        return exc

    # student does not have while or for: instruct him on loops
    if not has_token_sequence(tokens, ['while']) and \
       not has_token_sequence(tokens, ['for']):
        return [{'id' : 'while_clause'}]

    # student's answer is not correct
    if not string_almost_equal(answer[0][1], -132):
        return [{'id' : 'while_condition'}]

    return None
