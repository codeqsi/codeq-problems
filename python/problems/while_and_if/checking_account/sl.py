import server
mod = server.problems.load_language('python', 'sl')

name = 'Tekoči račun'
slug = 'Tekoči račun'


description = '''\
<p>Državna agencija za varstvo potrošnikov je razpisala projekt za izdelavo programa, s katerimi bodo
lahko potrošniki nadzorovali svoje tekoče račune. V program uporabniki vtipkavajo prejemke in
izdatke (kot pozitivne in negativne zneske) na svojem tekočem računu. Program jim sproti izpisuje
stanje in se ustavi, ko je uporabnik v minusu za 100 evrov ali več. Takrat naj program izpiše "Bankrot". </p>
<pre>
Sprememba 23
Stanje 23
Sprememba 15
Stanje 38
Sprememba -30
Stanje 8
Sprememba 10
Stanje 18
Sprememba 100
Stanje 118
Sprememba -200
Stanje -82
Sprememba -50
Stanje -132
Bankrot
</pre>
'''

main_plan = ['''\
<p>Plan je enak kot pri prejšnjih nalogah, le vsebina je drugačna:
<pre>
1.Ponavljaj dokler ni bankrot
    2.Preberi ceno
    3.Posodobi vsoto
4. Izpiši bankrot
</pre>
''']

while_condition = ['''\
<p>Koliko korakov naj naredi zanka?</p>''',
                   '''\
<p>Dokler velja, da je stanje višje od 100</p>'''
                   '''\
<pre>
while stanje > -100:
    ...
</pre>'''
]

plan = [main_plan,
        while_condition]

while_clause = ['''\
<p>Uporabi zanko <while</p>''',
         '''\
<pre>
while Pogoj:
    stavek 1
    stavek 2
    ...
stavek n # stavek izven while.
</pre>''',
         '''\
<p>Stavki znotraj while (zamaknjeni) se izvajajo toliko časa, dokler velja <code>Pogoj</code>.
Ko pogoj ne velja več, Python preskoči vrstice, ki so del while-a in nadaljuje s stavki, ki sledijo – v
našem primeru s stavkom n.</p>'''
        ]

hint = {
    'while_clause': while_clause,

    'while_condition': while_condition,

    'problematic_test_case': ['''\
<p>Zaporedje cen, kjer program ne dela prav: [%=testin%]<br>
Bankrot: [%=bancrupt%], Končno stanje: [%=sum%]''']

}
