from python.util import has_token_sequence, string_almost_equal, \
    string_contains_number, get_tokens, get_numbers, get_exception_desc
from server.hints import Hint

id = 201
number = 6
visible = True

solution = '''\
artiklov = 0
vsota = 0
cena = 1
while cena != 0 and vsota < 100 and artiklov < 10:
    cena = int(input('Cena: '))
    vsota += cena
    artiklov += 1

if cena == 0:
    artiklov -= 1

print('Porabili boste', vsota, 'evrov za', artiklov, 'stvari.')
'''

hint_type = {
    'printing': Hint('printing'),
    'while_clause': Hint('while_clause'),
    'nonumber': Hint('nonumber'),
    'while_condition': Hint('while_condition'),
    'final_hint': Hint('final_hint'),
    'summation': Hint('summation'),
    'counting': Hint('counting'),
    'counting_with_0': Hint('counting_with_0'),
    'problematic_test_case': Hint('problematic_test_case'),
}

def test(python, code, aux_code=''):
    # List of inputs: (expression to eval, stdin).
    test_in = [
        (None, '10\n5\n0\n'),
        (None, '10\n5\n90\n'),
        (None, '10\n5\n90\n1\n1\n0\n'),
        (None, '1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n'),
        (None, '1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1\n'),
        (None, '1\n1\n1\n1\n1\n1\n1\n1\n1\n0\n'),
        (None, '0\n'),
    ]

    test_out = [
        (15, 2),
        (105, 3),
        (105, 3),
        (10, 10),
        (10, 10),
        (9, 9),
        (0, 0)
    ]

    # List of outputs: (expression result, stdout, stderr, exception).
    answers = python(code=aux_code+code, inputs=test_in, timeout=1.0)
    outputs = [ans[1] for ans in answers]

    n_correct = 0
    tin = None
    for i, (output, correct) in enumerate(zip(outputs, test_out)):
        if all(string_almost_equal(output, correct[i]) for i in range(2)):
            n_correct += 1
        else:
            tin = test_in[i][1]
            tout = correct

    passed = n_correct == len(test_in)
    hints = [{'id': 'test_results', 'args': {'passed': str(n_correct), 'total': str(len(test_in))}}]
    if tin:
        hints.append({'id': 'problematic_test_case', 'args': {'testin': str(tin),
                                                              'sum': str(tout[0]),
                                                              'count': str(tout[1]),
                                                              }})
    if passed:
        hints.append({'id': 'final_hint'})
    return passed, hints

def hint(python, code, aux_code=''):
    tokens = get_tokens(code)

    # run one test first to see if there are any exceptions
    test_in = [(None, '10\n5\n0\n'),
               (None, '10\n5\n90\n'),
               (None, '2\n2\n2\n2\n2\n2\n2\n2\n2\n2\n')]

    answer = python(code=aux_code+code, inputs=test_in, timeout=1.0)
    exc = get_exception_desc(answer[0][3])
    if exc:
        if 'NameError' in answer[0][3]:
            return [{'id':'name_error', 'args': {'message': answer[0][3]}}]
        if 'EOFError' in answer[0][3]:
            exc.append({'id' : 'while_condition'})
            return exc
        else:
            return exc

    # student does not have while or for: instruct him on loops
    if not has_token_sequence(tokens, ['while']) and \
       not has_token_sequence(tokens, ['for']):
        return [{'id' : 'while_clause'}]

    # student is not using print function
    if not has_token_sequence(tokens, ['print']):
        return [{'id' : 'printing'}]

    # student does not print any values
    if len(get_numbers(answer[0][1])) < 2:
        return [{'id' : 'nonumber'}]

    # student's answer is not correct (three possibilities)
    if not string_almost_equal(answer[0][1], 15) or \
       not string_almost_equal(answer[1][1], 105) or \
       not string_almost_equal(answer[2][1], 20):
        return [{'id' : 'summation'}]

    # student's counting of items, when last price iz zero is incorrect
    if not string_almost_equal(answer[0][1], 2) and \
       string_almost_equal(answer[0][1], 3):
        return [{'id' : 'counting_with_0'}]

    # counting in general is incorrect
    if not string_almost_equal(answer[1][1], 3) or \
       not string_almost_equal(answer[2][1], 10):
        return [{'id' : 'counting'}]

    return None
