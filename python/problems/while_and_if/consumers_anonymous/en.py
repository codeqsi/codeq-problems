import server
mod = server.problems.load_language('python', 'sl')

id = 201
name = 'Consumers anonymous'
slug = 'Consumers anonymous'

description = '''\
<p>(translation missing)</p>'''


main_plan = ['''\
''']

while_condition = ['''\
''']

while_clause = ['''\
'''
]

plan = [main_plan,
        while_condition]

hint = {
    'while_clause': while_clause,

    'while_condition': while_condition,

    'printing': ['''\
'''],

    'nonumber': ['''<p><p>'''],

    'summation': ['''<p><p>'''],

    'counting': ['''<p><p>'''],

    'name_error' : [mod.general_msg['error_head'],
                    mod.general_msg['general_exception'],
                    mod.general_msg['name_error'],
                    '''\
'''],

    'problematic_test_case': ['''\
'''],

    'final_hint': ['''\
'''],

    'eof_error':[mod.general_msg['eof_error'],
                 '''\
''']
}

