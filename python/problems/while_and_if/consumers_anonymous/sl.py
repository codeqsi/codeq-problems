import server
mod = server.problems.load_language('python', 'sl')

name = 'Klub anonimnih potrošnikov'
slug = 'Klub anonimnih potrošnikov'


description = '''\
<p>Razpas trgovin je pripeljal do zasvojenosti z nakupovanjem. Ena od metod zdravljenja
temelji na inteligentnih košaricah, ki sprejmejo največ deset artiklov; po tem se zaklenejo
in jih lahko kupec le še odnese na blagajno. Prav tako se zaklenejo,
če cena artiklov doseže (ali preseže) 100 evrov.</p>

<p>Napiši program, ki mu uporabnik vnaša cene in ki se neha izvajati,
ko uporabnik vnese 0 (ne bo več kupoval),
ko je vnešenih deset števil ali ko vsota cen doseže ali preseže 100 evrov.</p>

<pre>
Cena: 10
Cena: 5
Cena: 0
Porabili boste 15 evrov za 2 stvari.
</pre>
<p>Pazite, uporabnik je kupil dve stvari, čeprav je vnesel tri cene!</p>

<pre>
Cena: 10
Cena: 5
Cena: 90
Porabili boste 105 evrov za 3 stvari.

Cena: 1
Cena: 1
Cena: 1
Cena: 1
Cena: 1
Cena: 1
Cena: 1
Cena: 1
Cena: 1
Cena: 1
Porabili boste 10 evrov za 10 stvari.
</pre>
'''

main_plan = ['''\
<p>Plan je enak kot pri prejšnjih nalogah: while zanka + izpis števca in vsote. </p>
''']

while_condition = ['''\
<p>Pogoj v zanki <code>while</code> bo sestavljen iz več pogojev.</p>''',
                   '''\
<p>V Pythonu združujemo pogoje z logičnimi operatoriji <code>and</code>,
<code>or</code> in <code>not</code></p>'''
                   '''\
<p>Zanka se ustavi, če presežemo 100 EUR, če smo vpisali 0 ali kupili 10 stvari.
Vendar pazite: napisati moramo pogoj, kdaj se zanka nadaljuje!</p>''',
                   '''\
<pre>
cena != 0 and vsota < 100 and artiklov < 10
</pre>
''']

plan = [main_plan,
        while_condition]

while_clause = ['''\
<p>Uporabi zanko <while</p>''',
         '''\
<pre>
while Pogoj:
    stavek 1
    stavek 2
    ...
stavek n # stavek izven while.
</pre>''',
         '''\
<p>Stavki znotraj while (zamaknjeni) se izvajajo toliko časa, dokler velja <code>Pogoj</code>.
Ko pogoj ne velja več, Python preskoči vrstice, ki so del while-a in nadaljuje s stavki, ki sledijo – v
našem primeru s stavkom n.</p>'''
        ]

hint = {
    'while_clause': while_clause,

    'while_condition': while_condition,

    'printing': ['''\
<p>Izpiši rezultat!</p>'''],

    'nonumber': ['''<p>Izpiši vsoto in število produktov.<p>'''],

    'summation': ['''<p>Seštevanje cen ne deluje pravilno.<p>'''],

    'counting': ['''<p>Napaka pri štetju stvari.<p>'''],

    'counting_with_0': ['''<p>Napaka pri štetju, kadar je zadnja cena 0.<p>''',
                        '''<p>Števec povečaj, le če je cena > 0</p>'''],

    'name_error' : [mod.general_msg['error_head'],
                    mod.general_msg['general_exception'],
                    mod.general_msg['name_error'],
                    '''\
<p>Verjetno v pogoju uporabljaš nedefinirano spremenljivko.'''],

    'problematic_test_case': ['''\
<p>Zaporedje cen, kjer program ne dela prav: [%=testin%]<br>
Pravilna vsota [%=sum%], pravilno število stvari: [%=count%]'''],

    'final_hint': ['''\
<p>Odlično! Naloga rešena.<br>
Še zanimivost: v while zanki smo dobili ustavitveni pogoj tako, da smo negirali pogoj iz teksta (ali vnesemo 0 ali je
vnešenih deset števil ali ko vsota cen doseže ali preseže 100 evrov). Pri tem smo ali (or) spremenili v in (and):
<code>cena > 0 and stevec < 10 and vsota < 100</code>. Temu pravimo De Morganov zakon.</p>
'''],

    'eof_error':[mod.general_msg['eof_error'],
                 '''\
<p>Verjetno se zanka izvede prevečkrat. Preveri pogoj!</p>''']

}
