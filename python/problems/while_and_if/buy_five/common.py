from python.util import has_token_sequence, string_almost_equal, \
    string_contains_number, get_tokens, get_numbers, get_exception_desc
from server.hints import Hint

id = 185
number = 1
visible = True

solution = '''\
vsota = 0
i = 0
while i < 5:
    vsota += int(input('Cena artikla: '))
    i += 1
print('Vsota:', vsota)
'''

hint_type = {
    'printing': Hint('printing'),
    'while_clause': Hint('while_clause'),
    'reading_while': Hint('reading_while'),
    'summation': Hint('summation'),
    'nonumber': Hint('nonumber'),
}

def test(python, code, aux_code=''):
    # List of inputs: (expression to eval, stdin).
    test_in = [
        (None, '1\n1\n1\n1\n1\n'),
        (None, '1\n2\n3\n4\n5\n'),
        (None, '1\n2\n0\n3\n-1\n'),
        (None, '5\n4\n3\n11\n7\n'),
        (None, '0\n0\n0\n0\n0\n'),
    ]

    test_out = [
        5,
        15,
        5,
        30,
        0,
    ]

    # List of outputs: (expression result, stdout, stderr, exception).
    answers = python(code=aux_code+code, inputs=test_in, timeout=1.0)
    outputs = [ans[1] for ans in answers]

    n_correct = 0
    tin = None
    for i, (output, correct) in enumerate(zip(outputs, test_out)):
        if string_almost_equal(output, correct):
            n_correct += 1
        else:
            tin = test_in[i][1]
            tout = correct

    passed = n_correct == len(test_in)
    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_in)}}]
    if tin:
        hints.append({'id': 'problematic_test_case', 'args': {'testin': str(tin), 'testout': str(tout)}})
    return passed, hints

def hint(python, code, aux_code=''):
    # run one test first to see if there are any exceptions
    test_in = [(None, '1\n1\n1\n1\n1\n')]
    answer = python(code=aux_code+code, inputs=test_in, timeout=1.0)
    exc = get_exception_desc(answer[0][3])
    if exc: return exc

    tokens = get_tokens(code)

    # student does not have while or for: instruct him on loops
    if not has_token_sequence(tokens, ['while']) and \
       not has_token_sequence(tokens, ['for']):
        return [{'id' : 'while_clause'}]

    # student does not know how to read several values in while
    if not has_token_sequence(tokens, ['input']):
        return [{'id' : 'reading_while'}]

    # student is not using print function
    if not has_token_sequence(tokens, ['print']):
        return [{'id' : 'printing'}]

    # student does not know how to accumulate values (print contains 0 or 1)
    if string_contains_number(answer[0][1], 0) or \
       string_contains_number(answer[0][1], 1):
        return [{'id' : 'summation'}]

    # student does not print any values
    if not get_numbers(answer[0][1]):
        return [{'id' : 'nonumber'}]

    return None
