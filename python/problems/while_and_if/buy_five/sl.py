import server
mod = server.problems.load_language('python', 'sl')

name = 'Blagajna "vse po pet"'
slug = 'Blagajna "vse po pet"'


description = '''\
<p>V trgovini "vse po pet" morajo stranke vedno kupiti natanko pet artiklov. Za blagajne zato potrebujejo
programsko opremo, ki uporabnika (blagajnika) vpraša po petih cenah; ko jih le-ta vnese, program
izpiše vsoto.</p>
<pre>
Cena artikla: 2
Cena artikla: 4
Cena artikla: 1
Cena artikla: 6
Cena artikla: 3
Vsota: 16
</pre>
'''

bad_solution = ['''\
<p>Primer, kako NE reševati</h3>
<pre>
cena1 = float(input('Cena artikla: '))
cena2 = float(input('Cena artikla: '))
cena3 = float(input('Cena artikla: '))
cena4 = float(input('Cena artikla: '))
cena5 = float(input('Cena artikla: '))
vsota = cena1+cena2+cena3+cena4+cena5
print ("Vsota: " + vsota)
</pre>''',
        '''\
<p>Rešitev je slaba, saj imamo pet enakih vrstic! Uporabite zanko! </p>''']

main_plan = ['''\
<p>Plan:</p>
<pre>
1. Ponavljaj 5x:
    2. Preberi ceno.
    3. Prištej vsoti.
4. Izpiši vsoto.
</pre>''']

while_clause = ['''\
<p>Uporabi zanko <code>while</code>.</p>''',
         '''\
<p>Zanka <code>while</code> ima naslednjo sintakso:</p>
<pre>
while Pogoj:
    stavek 1
    stavek 2
    ...
stavek n # stavek izven while.
</pre>''',
         '''\
<p>Stavki znotraj while (ki so zamaknjeni) se izvajajo toliko časa, dokler velja <code>Pogoj</code> v glavi stavka while.
Ko pogoj ne velja več, Python preskoči vrstice, ki so del while-a in nadaljuje s stavki, ki sledijo – v
našem primeru s stavkom n.</p>'''
        ]

reading_while =  ['''\
<p>Za večkratno branje želimo uporabiti zanko</p>''',

'''\
<pre>
while ____:
    cena = float(input('Cena artikla: '))
</pre>
<p>Kakšen je pogoj?</p>''',

'''\
Izmisliti si moramo pogoj, ki bo resničen 5 korakov, potem ne
več. Običajni trik je uporaba števca. ''',

'''\
Števec je spremenljivka, ki se v zanki vsakič poveča za ena - torej šteje''',

'''\
<pre>
stevec = 0
while _________:
    cena = float(input('Cena artikla: '))
    stevec += 1
</pre>
<p>Imaš zdaj morda idejo, kakšen naj bo pogoj?</p>''',

'''\
<pre>
while stevec < 5:
</pre>
''']

summation = ['''\
<p>Računaj vsoto sproti v zanki. <p>''',

'''\
<p>Izmisli si spremenljivko, ki bo predstavljala vsoto, jo na
začetku (pred zanko) nastavi na 0 in ji v vsakem koraku prištej trenutno ceno.
Podobno kot števec. </p>'''
]

plan = [bad_solution,
            main_plan,
            while_clause,
            reading_while,
            summation]

hint = {
    'while_clause': while_clause,

    'reading_while': reading_while,

    'summation': summation,

    'printing': ['''\
<p>Izpiši vsoto</p>''',
                 '''\
<p> V Pythonu izpisujemo s funkcijo <code>print</code><p>.''',
'''<p>Pazi, da stavek s <code>print</code> ne bo zamaknjen, saj bo v takem
primeru del while-a in se bo večkrat izpisal. </p>'''],

    'nonumber': ['''<p>Izpiši vsoto<p>'''],

    'name_error' : [mod.general_msg['error_head'],
                    mod.general_msg['general_exception'],
                    mod.general_msg['name_error'],
                    '''\
<p>Verjetno uporabljaš spremenljivko, ki nima nastavljene vrednosti. Morda v pogoju?'''],
}
