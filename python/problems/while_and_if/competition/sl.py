import server
mod = server.problems.load_language('python', 'sl')


name = 'Blagajna "konkurenca"'
slug = 'Blagajna "konkurenca"'


description = '''\
<p>Konkurenčna trgovina za vogalom se je odločila za posebno ponudbo: kupec lahko kupi toliko
izdelkov, kolikor želi. Popravi gornji program tako, da blagajnika najprej vpraša, koliko izdelkov je v
košarici, nato vpraša po cenah teh izdelkov in na koncu spet izpiše vsoto.<p>
<pre>
Število izdelkov: 3
Cena artikla: 2
Cena artikla: 4
Cena artikla: 1
Vsota: 7
</pre>
'''
main_plan = ['''\
<p>Plan bo enak kot pri prejšnji nalogi,
le število ponavljanj se spremeni. </p>''',
             '''\
<pre>
1. Preberi število produktov N.
2. Ponavljaj N-krat:
    3. Preberi ceno.
    4. Prištej vsoti.
5. Izpiši vsoto.
</pre>''']
plan = [main_plan]


while_clause = ['''\
<p>Kako bi prebral N cen z uporabo zanke while?</p>''',
         '''\
<p>Zanka while ima naslednjo sintakso:</p>
<pre>
while Pogoj:
    stavek 1
    stavek 2
    ...
stavek n # stavek izven while.
</pre>''',
         '''\
<p>Stavki znotraj while (ki so zamaknjeni) se izvajajo toliko časa, dokler velja <code>Pogoj</code> v glavi stavka while.
Ko pogoj ne velja več, Python preskoči vrstice, ki so del while-a in nadaljuje s stavki, ki sledijo – v
našem primeru s stavkom n.</p>'''
        ]

summation = ['''\
<p>Računaj vsoto sproti v zanki. <p>''',

'''\
<p>Izmisli si spremenljivko, ki bo predstavljala vsoto, jo na
začetku (pred zanko) nastavi na 0 in ji v vsakem koraku prištej trenutno ceno.
Podobno kot števec. </p>'''
]


hint = {
    'while_clause': while_clause,

    'read_before_while': ['''\
<p>Najprej je potrebno vprašati, koliko izdelkov bo kupil.</p>'''],

    'summation': summation,

    'printing': ['''\
<p>Izpiši vsoto</p>''',
                 '''\
<p> V Pythonu izpisujemo s funkcijo <code>print</code><p>.''',
'''<p>Pazi, da stavek s <code>print</code> ne bo zamaknjen, saj bo v takem
primeru del while-a in se bo večkrat izpisal. </p>'''],

    'nonumber': ['''<p>Izpiši vsoto<p>'''],

    'name_error' : [mod.general_msg['error_head'],
                    mod.general_msg['general_exception'],
                    mod.general_msg['name_error'],
                    '''\
<p>Verjetno uporabljaš spremenljivko, ki nima nastavljene vrednosti. Morda v pogoju?'''],

    'eof_error':[mod.general_msg['eof_error'],
                 '''\
<p>Verjetno se zanka izvede prevečkrat. Preveri pogoj!</p>''']
}
