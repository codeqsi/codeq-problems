import server
mod = server.problems.load_language('python', 'sl')

name = 'Blagajna "minimax"'
slug = 'Blagajna "minimax"'


description = '''\
<p>Popravite prejšnji program tako, da poleg vsote in povprečne cene izpiše še najnižjo in najvišjo ceno.</p>
<pre>
Cena artikla: 2
Cena artikla: 4
Cena artikla: 1
Cena artikla: 0
Vsota: 7
Poprečna cena: 2.33333333333
Najnižja cena: 1
Najvišja cena: 4
</pre>
'''

main_plan = ['''\
<p>Plan:</p>
<pre>
1. Ponavljaj dokler je cena večja od 0:
    2. Preberi ceno.
    3. Prištej vsoti.
    4. Posodobi najmanjšo in največjo ceno.
4. Izpiši vsoto, povprečje, najmanjšo in največjo vrednost.
</pre>''']

while_condition = ['''\
<p>Koliko korakov naj naredi zanka?</p>''',
                   '''\
<p>Ne vemo, koliko korakov naj naredi zanka. Vemo pa,
kdaj se bo zanka ustavila: ko bo cena enaka 0!</p>'''
                   '''\
<pre>
while cena != 0:
    ...
</pre>'''
]

minimax = ['''\
<p>V vsaki iteraciji posodobi največjo in najmanjšo ceno</p>''',
           '''\
<p>Če je trenutna cena manjša od najmanjše, potem je to nova najmanjša ...</p>''',
           '''\
<pre>
if cena < min_cena:
    min_cena = cena
</pre>'''
]

plan = [main_plan,
        while_condition,
        minimax]

while_clause = ['''\
<p>Uporabi zanko <while</p>''',
         '''\
<pre>
while Pogoj:
    stavek 1
    stavek 2
    ...
stavek n # stavek izven while.
</pre>''',
         '''\
<p>Stavki znotraj while (zamaknjeni) se izvajajo toliko časa, dokler velja <code>Pogoj</code>.
Ko pogoj ne velja več, Python preskoči vrstice, ki so del while-a in nadaljuje s stavki, ki sledijo – v
našem primeru s stavkom n.</p>'''
        ]

hint = {
    'while_clause': while_clause,

    'while_condition': while_condition,

    'average': ['''\
<p>Formula za povprečje: povp = vsota / št.elementov</p>''',
                '''\
<p>Vsoto že znamo izračunati, za št. elementov pa potrebujemo števec.'''],

    'printing': ['''\
<p>Izpiši rezultat!</p>''',
                 '''\
<p> V Pythonu izpisujemo s funkcijo <code>print</code>. <p>''',
'''<p>Pazi, da stavek s <code>print</code> ne bo zamaknjen, saj bo v takem
primeru del while-a in se bo večkrat izpisal. </p>'''],

    'nonumber': ['''<p>Izpiši vsoto<p>'''],

    'minimax': minimax,

    'name_error' : [mod.general_msg['error_head'],
                    mod.general_msg['general_exception'],
                    mod.general_msg['name_error'],
                    '''\
<p>Verjetno v pogoju uporabljaš nedefinirano spremenljivko.''',
                    '''\
<pre>
cena = 1
while cena != 0:
    ...'''],

    'problematic_test_case': ['''\
<p>Zaporedje cen, kjer program ne dela prav: [%=testin%]<br>
Pravilna vsota [%=sum%], pravilno povprečje: [%=avg%], min: [%=min%], max: [%=max%]</p>''']

}
