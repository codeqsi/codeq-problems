name = 'Pogojni stavki in zanke'
description = '''<a target="_blank" href="[%@resource ifclause_sl.html%]">Pogojni stavek if-else</a>,
<a target="_blank" href="[%@resource conditions_sl.html%]">logični izrazi</a>
<a target="_blank" href="[%@resource loops_sl.html%]">zanka <code>while</code></a>'''
