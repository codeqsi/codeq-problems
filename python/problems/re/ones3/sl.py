import server
mod = server.problems.load_language('python', 'sl')

id = 20902
name = 'Ponovitve znaka 3x'

description = '''\
<p>
Napišite funkcijo <code>ones(s)</code>, ki vrne seznam vseh podnizov, ki 
vsebujejo tri ali več ponovitev znaka 1. </p>
<pre>
>>> ones('12211222111')
['111']
</pre>
'''

plan = []

hint = {
    'final_hint': ['''\
<p>Program je pravilen! <br>
</p>
'''],
}
