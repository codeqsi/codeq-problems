import server
mod = server.problems.load_language('python', 'sl')

id = 20906
name = 'Oklepaji'

description = '''\
<p>
Napišite funkcijo <code>parantheses(s)</code>, ki vrne vse nize, ki se pojavijo
v oklepajih.</p> 
<pre>
>>> parentheses('(a a) bb (cc)')
['(a a)', '(cc)']
</pre>
'''

plan = []

hint = {
    'final_hint': ['''\
<p>Program je pravilen! <br>
</p>
'''],
}
