import server
mod = server.problems.load_language('python', 'sl')

id = 20905
name = 'Besede'

description = '''\
<p>
Napišite funkcijo <code>words(s)</code>, ki vrne vse besede v nizu. Beseda
je sestavljena iz malih in velikih črk. Pazite, da ne vračate ločil.
<pre>
>>> words('Vse besede. Brez locil!!!')
['Vse', 'besede', 'Brez', 'locil']
</pre>
'''

plan = []

hint = {
    'final_hint': ['''\
<p>Program je pravilen! <br>
</p>
'''],
}
