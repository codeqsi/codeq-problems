import server
mod = server.problems.load_language('python', 'sl')

id = 20907
name = 'Dvojni oklepaji'

description = '''\
<p>
Napišite funkcijo <code>double_parantheses(s)</code>, ki vrne vse nize, ki se 
pojavijo v dvojnih oklepajih.</p> 
<pre>
>>> double_parentheses('((aa (aa) aa)) bb ((cc)) (dd)')
['((aa (aa) aa))', '((cc))']
</pre>
'''

plan = []

hint = {
    'final_hint': ['''\
<p>Program je pravilen! <br>
</p>
'''],
}
