import server
mod = server.problems.load_language('python', 'sl')

id = 20904
name = 'Predznačeno poljubno število'

description = '''\
<p>
Napišite funkcijo <code>num(s)</code>, ki vrne prvo poljubno predznačeno število
(lahko ima tudi decimalko). Število naj bo tipa <code>float</code>. Če števila v nizu ni, 
naj funkcija vrne <code>None</code>.</p>
<pre>
>>> num('Večna pot 113')
113
>>> num('a-20.5=21.5')
-20.5
>>> num('Pregledali smo 10e+5 elementov.')
10e+5
</pre>
'''

plan = []

hint = {
    'final_hint': ['''\
<p>Program je pravilen! <br>
</p>
'''],
}
