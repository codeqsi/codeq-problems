import server
mod = server.problems.load_language('python', 'sl')

id = 20901
name = 'Ponovitve znaka'

description = '''\
<p>
Napišite funkcijo <code>ones(s)</code>, ki vrne seznam vseh podnizov, ki 
vsebujejo eno ali več ponovitev znaka 1. </p>
<pre>
>>> ones('12211222111')
['1', '11', '111']
>>> ones('')
[]
</pre>
'''

plan = []

hint = {
    'final_hint': ['''\
<p>Program je pravilen! <br>
</p>
'''],
}
