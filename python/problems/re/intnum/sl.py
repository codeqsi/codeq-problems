import server
mod = server.problems.load_language('python', 'sl')

id = 20903
name = 'Predznačeno celo število'

description = '''\
<p>
Napišite funkcijo <code>num(s)</code>, ki vrne prvo celo predznačeno število 
v nizu. Število naj bo tipa int. Če števila v nizu ni, naj funkcija vrne
<code>None</code>.</p>
<pre>
>>> num('Večna pot 113')
113
>>> num('a-20=22')
-20
</pre>
'''

plan = []

hint = {
    'final_hint': ['''\
<p>Program je pravilen! <br>
</p>
'''],
}
