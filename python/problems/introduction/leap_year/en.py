name = '*Leap year'

description = '''\
<p>Write a program that reads a number representing a year and determines whether
this is a leap year. A leap year is divisible by 4, but not divisible by 100,
except if it is divisible also by 400. Thence years 2004, 2008, and 2000 are
leap years, while 2005 and 2100 are not.</p>'''

hint = {
    'plan': '''\
<p>(translation missing)</p>''',

    'no_input_call': '''\
<p>(translation missing)</p>''',
}
