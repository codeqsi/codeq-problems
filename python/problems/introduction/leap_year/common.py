from python.util import has_token_sequence, string_almost_equal, \
    string_contains_number, get_tokens, get_numbers, get_exception_desc
from server.hints import Hint
import re

id = 20107
number = 7
visible = True

solution = '''\
leto = int(input("Vnesi leto: "))
if leto % 4 == 0 and (leto % 100 != 0 or leto % 400 == 0):
    print("Je prestopno")
else:
    print("Ni prestopno")
'''

hint_type = {
    'mod_expression': Hint('mod_expression'),
    'printing': Hint('printing'),
    'name_error': Hint('name_error'),
    'unsupported_operand': Hint('unsupported_operand'),
    'final_hint': Hint('final_hint'),
}

def test(python, code, aux_code=''):
    # List of inputs: (expression to eval, stdin).
    test_in = [
        (None, '1900\n'),
        (None, '1950\n'),
        (None, '2000\n'),
        (None, '2016\n'),
        (None, '2100\n'),
    ]
    test_out = [
        "Ni prestopno",
        "Ni prestopno",
        "Je prestopno",
        "Je prestopno",
        "Ni prestopno"
    ]

    # List of outputs: (expression result, stdout, stderr, exception).
    answers = python(code=aux_code+code, inputs=test_in, timeout=1.0)
    outputs = [ans[1] for ans in answers]

    n_correct = 0
    tin = None
    for i, (output, correct) in enumerate(zip(outputs, test_out)):
        if correct in output:
            n_correct += 1
        else:
            tin = test_in[i][1]
            tout = correct

    passed = n_correct == len(test_in)
    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_in)}}]
    if tin:
        hints.append({'id': 'problematic_test_case', 'args': {'testin': str(tin), 'testout': str(tout)}})
    if passed:
        hints.append({'id': 'final_hint'})
    return passed, hints

def hint(python, code, aux_code=''):
    tokens = get_tokens(code)

    # run one test first to see if there are any exceptions
    test_in = [(None, '212\n')]
    answer = python(code=aux_code+code, inputs=test_in, timeout=1.0)
    exc = answer[0][3]
    exc_hint = get_exception_desc(answer[0][3])
    if exc:
        if 'NameError' in exc:
            return [{'id':'name_error', 'args': {'message': exc}}]
        elif 'not callable' in exc:
            return [{'id':'not_callable', 'args': {'message': exc}}]
        elif 'unsupported operand' in exc:
            return [{'id':'unsupported_operand', 'args': {'message': exc}}]
        elif 'TypeError' in exc:
            return [{'id':'unsupported_operand', 'args': {'message': exc}}]
        else:
            return exc_hint

    # if token % not in code, we have to teach them how to
    # evaluate expressions.
    if (not has_token_sequence(tokens, ['%'])):
        return [{'id' : 'mod_expression'}]

    # student is not using print function
    if not has_token_sequence(tokens, ['print']):
        return [{'id' : 'printing'}]

    return None
