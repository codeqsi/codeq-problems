import server
mod = server.problems.load_language('python', 'sl')

name = '*Prestopno leto'

description = '''\
<p> Napiši program, ki preveri ali je leto prestopno. Leto je prestopno,
 če je deljivo s 4, razen v primeru, ko je deljivo s 100,
 takrat leto ni prestopno, razen takrat, ko je deljivo s 400,
 takrat je leto prestopno. Tako so leta 2004, 2008 in 2000 prestopna,
 leti 2005 in 2100 pa ne. </p>

 <p> Če je leto prestopno, naj izpiše "Je prestopno", če ni, "Ni prestopno". '''

printing = ['''\
<p>V Pythonu izpisujemo s funkcijo <code>print</code>.</p>''',
            '''\
</p>Če želimo izpisati več elementov, jih ločimo z vejico.
Imejmo spremenljivko <code>ime</code>, ki vsebuje naše ime, potem:
<pre>
print("Ime mi je", ime, ".")
</pre>''']

mod_expression = ['''\
<p>Ostanek pri deljenju dobiš z operatorjem %.''']

plan = ['''\
<p>1. Preberi vrednost 2.izračunaj 3.izpiši.</p>''',
        mod_expression,
        printing

]
hint = {

    'mod_expression': mod_expression,

    'printing': printing,

    'name_error' : [
        mod.general_msg['error_head'],
        mod.general_msg['general_exception'],
        mod.general_msg['name_error'],
        '''\
<p>Verjetno uporabljaš spremenljivko, ki nima vrednosti. Ali v izrazu za
izračun uporabljaš napačno spremenljivko? Ali pri izpisu morda poskušaš
izpisati napačno spremenljivko?</p>'''
    ],

    'unsupported_operand' : [
        mod.general_msg['error_head'],
        mod.general_msg['general_exception'],
        mod.general_msg['type_error'],
        '''\
<p>Verjetni razlog: funkcija <code>input</code> vrača vrednost tipa niz, ki jo
moramo najprej pretvoriti v tip <code>int</code>, če želimo z njo
računati:</p>
<pre>
v = int(input(" ...
</pre>''',],

    'final_hint' : [
            '''\
<p>Odlično, program deluje!<br>
''']
}
