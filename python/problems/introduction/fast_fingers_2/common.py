from python.util import has_token_sequence, string_almost_equal, \
    string_contains_number, get_tokens, get_numbers, get_exception_desc
from server.hints import Hint

id = 191
number = 6
visible = True

solution = '''\
import random
import time
x = random.randint(1, 10)
y = random.randint(1, 10)
t = time.time()
print('Koliko je ', x, ' krat ', y, '? ')
z = float(input())
if x * y == z:
    print(True)
else:
    print(False)
print('Za razmišljanje ste porabili', time.time() ­ t, 's.')
'''

random_code = '''\
import random
random.randint = lambda x, y: {}
'''

hint_type = {
    'random': Hint('random'),
    'if_clause': Hint('if_clause'),
    'final_hint': Hint('final_hint'),
    'final_hint_noif': Hint('final_hint_noif')
}

def test(python, code, aux_code=''):
    # List of inputs: (expression to eval, stdin).
    test = [(5,'25\n'),
            (6, '29\n'),
            (1, '1\n'),
            (7, '65\n'),
            (9, '81\n')]

    test_out = [
        True,
        False,
        True,
        False,
        True
    ]

   # List of outputs: (expression result, stdout, stderr, exception).

    n_correct = 0
    tin = None
    for ti, t in enumerate(test):
        # change code, so that it replaces random values
        # hook randint
        tcode = random_code.format(t[0]) + code

        answers = python(code=aux_code+tcode, inputs=[(None, t[1])], timeout=1.0)
        output = answers[0][1]

        if str(test_out[ti]) in output and str(not test_out[ti]) not in output:
            n_correct += 1
        else:
            tin = '{t[1]}'.format(t=t)
            tout = test_out[ti]
            mult = '{t[0]}*{t[0]}'.format(t=t)

    passed = n_correct == len(test)
    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test)}}]
    if tin:
        hints.append({'id': 'problematic_test_case', 'args': {'mult' : mult, 'testin': str(tin), 'testout': str(tout)}})
    else:
        tokens = get_tokens(code)
        if has_token_sequence(tokens, ['if']):
            hints.append({'id': 'final_hint'})
        else:
            hints.append({'id': 'final_hint_noif'})
            
    return passed, hints

def hint(python, code, aux_code=''):
    tokens = get_tokens(code)

    # run one test first to see if there are any exceptions
    test_in = [(None, '16\n')]
    answer = python(code=aux_code+code, inputs=test_in, timeout=1.0)
    exc = answer[0][3]
    exc_hint = get_exception_desc(answer[0][3])
    # if have an exception!
    if exc_hint:
        return exc_hint

    # First: if student does not import random, tell him about that module
    if not has_token_sequence(tokens, ['random']) or \
       not has_token_sequence(tokens, ['randint']):
        return [{'id': 'random'}]


    # Student will have to test whether result is correct or not
    if not has_token_sequence(tokens, ['if']) and \
       not has_token_sequence(tokens, ['==']):
        return [{'id' : 'if_clause'}]

    return None
