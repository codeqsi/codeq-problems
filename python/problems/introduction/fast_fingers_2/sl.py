import server
mod = server.problems.load_language('python', 'sl')

name = 'Hitri prsti 2'
slug = 'Hitri prsti 2'

description = '''\
<p>Napiši program, podoben prejšnjemu, vendar naj ne vpraša vedno, koliko je 6 krat 7, temveč naj si
izmišlja naključna vprašanja iz poštevanke. Program bo torej izžrebal dve števili med 1 in 10 in izpisal
račun s tema dvema številoma namesto s 6 in 7. Tokrat naj program izpiše <code>True</code>, če je uporabnik
pravilno izračunal produkt, drugače naj izpiše <code>False</code>. </p>
<pre>
Koliko je 6 krat 3? UPORABNIK VTIPKA 18
True
Za razmišljanje ste porabili 2.1922357082366943 s.
</pre>
'''

random = ['''\
<p>V modulu <code>random</code> imate funkcijo <code>randint</code>,
ki vrača naključno celo število. </p>''',
          '''\
<p>Če napišemo na začetek programa:</p>
<pre>
from random import *
</pre>
<p>dobimo (med drugim) dostop do funkcije <code>randint(x,y)</code>, ki vrne naključno
celo število med <code>x</code> in <code>y</code>:</p>
<pre>
st = randint(1, 10)
</pre>''']

if_clause = ['''\
<p>Če izračunamo pravilno, izpišemo True, drugače
False. To omogoča pogojni stavek <code>if</code>. </p>''',
             '''\
<p> Primer pogojnega stavka <code>if</code>:
<pre>
if a*b == c: # dvopičje na koncu pogoja!
    print(True)
else:
    print(False)
</pre>'''
    ]

plan = ['''\
<p>Razširimo plan iz prejšnje naloge:</p>
<ol>
    <li>Izmisli si dve naključni števili. </li>
    <li>Izmeri trenutni čas. </li>
    <li>Vprašaj za rezultat produkta. </li>
    <li>Izmeri trenutni čas. </li>
    <li>Ali je rezultat množenja pravilen? </li>
    <li>Izračunaj porabljen čas. </li>
    <li>Izpiši. </li>
</ol>
''',
        random,
        if_clause]

hint = {
    'random': random,

    'if_clause': if_clause,

    'final_hint': ['''\
<p>Odlično, program je pravilen! <br>
Za konec pa še zanimivost. Pri tej nalogi stavka <code>if</code> niti ne potrebujemo, saj bi lahko napisali le:</p>
<pre>
print(a*b == c)
</pre>
<p>kar bi izpisalo rezultat tega izraza. Poskusi!</p>'''],

    'final_hint_noif': ['''\
<p>Odlično, program je pravilen! </p>'''],

    'problematic_test_case': [
        '''\
<p>Program ne deluje pravilno! <br>
Primer množenja: [%=mult%] <br>
Če vnesemo: [%=testin%], <br>
bi moral izpisati: [%=testout%].</p>''']
}
