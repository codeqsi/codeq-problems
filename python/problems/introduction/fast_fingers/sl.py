import server
mod = server.problems.load_language('python', 'sl')

name = 'Hitri prsti'
slug = 'Hitri prsti'

description = '''\
<p>Napiši program, ki uporabnika vpraša, koliko je 6 krat 7. Uporabnik bo premislil in vpisal odgovor.
Program naj se ne ukvarja s tem, ali je odgovor pravilen ali ne, temveč naj ga le izpiše. Poleg tega naj izpiše, koliko
sekund je človek potreboval za razmišljanje.<p>
<pre>
Koliko je 6 krat 7? UPORABNIK VTIPKA 42
Vpisal si 42. Za razmišljanje si porabil 2.503019332885742 s.
</pre>
'''

time = ['''\
<p>V modulu <code>time</code> imate funkcijo <code>time</code>''',
        '''\
<p>Funkcija <code>time</code> vrača čas v sekundah od 1.januarja 1970
(ta datum označujemo tudi kot epoch oz. začete časa)
do trenutka, ko smo to funkcijo klicali. Poskusite:</p>
<pre>
from time import *
trenutno = time()
print("Od začetka časa je minilo že", trenutno, "sekund.")
</pre>''']

time_diff = ['''\
<p>Porabljen čas lahko izračunamo tako, da od časa po vprašanju odštejemo
izmerjen čas pred vprašanjem.</p>''',
            '''\
<pre>
zacetek = time()
...
konec = time()
cas = konec – zacetek
</pre>
''']

plan = ['''\
<p>Pri tej nalogi moramo poznati čas pred klicem funkcije <code>input</code> in
po klicu funkcije <code>input</code>:</p>
<ol>
<li>Izmeri trenutni čas.</li>
<li>Vprašaj za rezultat.</li>
<li>Izmeri trenutni čas.</li>
<li>Izračunaj porabljen čas.</li>
<li>Izpiši rezultat in porabljen čas.</li>
</ol>
''',
        time,
        time_diff]


hint = {
    'time': time,

    'time_diff': time_diff,

    'printing': '''<p>Izpiši rezultat!</p>''',

    'name_error' : [mod.general_msg['error_head'], mod.general_msg['general_exception'],
    mod.general_msg['name_error'], '''
<p>Verjetno uporabljaš spremenljivko, ki nima vrednosti. Ali v izrazu za izračun
uporabljaš napačno spremenljivko? Ali pri izpisu morda poskušaš
izpisati napačno spremenljivko?</p>'''],

    'type_error' : [mod.general_msg['error_head'], mod.general_msg['general_exception'],
    mod.general_msg['type_error'], '''
<p>Verjetni razlog: funkcija <code>input</code> vrača vrednost tipa niz,
ki jo moramo najprej pretvoriti v tip <code>float</code>, če želimo z njo računati:</p>
<pre>
v = float(input(" ...
</pre>
'''],
}
