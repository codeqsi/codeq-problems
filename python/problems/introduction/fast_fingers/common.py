from python.util import has_token_sequence, string_almost_equal, \
    string_contains_number, get_tokens, get_numbers, get_exception_desc
from server.hints import Hint

id = 190
number = 5
visible = True

solution = '''\
from time import time
zacetek = time()
rezultat = float(input("Koliko je 7*6? "))
konec = time()
cas = konec­zacetek
print ("Vpisal si ", rezultat, ". Za razmišljanje si porabil ", cas, " s.")
'''

hint_type = {
    'name_error': Hint('name_error'),
    'type_error': Hint('type_error'),
    'error': Hint('error'),
    'time': Hint('time'),
    'time_diff': Hint('time_diff'),
    'printing': Hint('printing'),
}

def test(python, code, aux_code=''):
    # List of inputs: (expression to eval, stdin).
    test_in = [
        (None, '1\n'),
        (None, '2\n'),
        (None, '42\n'),
        (None, '-42\n'),
        (None, '0\n'),
    ]
    test_out = [
        1,
        2,
        42,
        -42,
        0
    ]

   # List of outputs: (expression result, stdout, stderr, exception).
    answers = python(code=aux_code+code, inputs=test_in, timeout=1.0)
    outputs = [ans[1] for ans in answers]

    n_correct = 0
    tin = None
    for i, (output, correct) in enumerate(zip(outputs, test_out)):
        if string_contains_number(output, correct):
            n_correct += 1
        else:
            tin = test_in[i][1]
            tout = correct

    passed = n_correct == len(test_in)
    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_in)}}]
    if tin:
        hints.append({'id': 'problematic_test_case', 'args': {'testin': str(tin), 'testout': str(tout)}})
    return passed, hints

def hint(python, code, aux_code=''):
    tokens = get_tokens(code)

    # run one test first to see if there are any exceptions
    test_in = [(None, '212\n')]
    answer = python(code=aux_code+code, inputs=test_in, timeout=1.0)
    exc = answer[0][3]
    exc_hint = get_exception_desc(answer[0][3])
    # if have an exception!
    if exc:
        if 'NameError' in exc:
            return [{'id':'name_error', 'args': {'message': exc}}]
        elif 'TypeError' in exc:
            return [{'id':'type_error', 'args': {'message': exc}}]
        else:
            return exc_hint

    # First: if student does not import time, tell him about that module
    if not has_token_sequence(tokens, ['time']):
        return [{'id': 'time'}]

    # Student will have to compute time difference and they will need
    # the substraction (-) operator to do this. Check for the minus operator.
    if not has_token_sequence(tokens, ['-']):
        return [{'id' : 'time_diff'}]

    if not string_contains_number(answer[0][1], 212):
        return [{'id' : 'printing'}]

    return None
