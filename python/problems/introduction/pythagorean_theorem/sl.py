import server
mod = server.problems.load_language('python', 'sl')

name = 'Pitagorov izrek'
slug = 'Pitagorov izrek'

description = '''\
<p>Napiši program, ki uporabnika vpraša po dolžinah katet pravokotnega trikotnika in
izpiše dolžino hipotenuze. '''

no_input_call = ['''\
<p>Tako kot pri prejšnji nalogi za branje uporabimo funkcijo <code>input</code></p>''',
                 '''\
<p>Preberemo dve vrednosti in jih shranimo v dve spremenljivki:</p>
<pre>
 a = float(input("Prva kateta: "))
 b = float(input("Druga kateta: "))
</pre>''',
                '''\
<p> Imeni <code>a</code> in <code>b</code> sta spremenljivki (angl. variable).
Spremenljivke uporabljamo, kadar želimo kakšno vrednost shraniti, ki jo bomo
potrebovali kasneje v programu. Imena spremenljivk so lahko poljubno
 dolga, v našem primeru bi jim lahko rekli tudi <code>kateta_a</code> in <code>kateta_b</code>.
 Pri programiranju velja, da izbiramo taka imena spremenljivk,
 ki bodo naredila program berljiv. </p>
 '''
]

math_functions =  ['''\
<p>Dolžina hipotenuze je kvadratni koren vsote kvadratov katet.</p>''',
                   '''\
<pre>
c = sqrt(a**2 + b**2)
</pre>
<p>Dvojni znak za množenje ** je potenciranje. Funkcija <code>sqrt</code>
izračuna kvadratni koren. ''',
                   '''\
Funkcija <code>sqrt</code> ni vgrajena v Python,
temveč se nahaja v ločenem matematičnem modulu oz. zbirki  matematičnih funkcij.
Da bi lahko dostopali do teh funkcij, moramo ta modul najprej uvoziti:</p>
<pre>
from math import *
</pre>
<p>Poleg funkcije <code>sqrt</code> matematični modul vsebuje še vrsto
uporabnih matematičnih funkcij, kot so:   log, exp, trigonometrične funkcije, itd.</p>
''']

printing = ['''\
<p> V Pythonu izpisujemo s funkcijo <code>print</code>. </p>''',
            '''\
<p>Če želimo izpisati več elementov,
jih ločimo z vejico.  Recimo, da imamo spremenljivko <code>ime</code>,
ki vsebuje naše ime, potem lahko napišemo:</p>
<pre>
print("Ime mi je", ime, ".")
</pre>''']

plan = ['''\
<p>Program razdelimo na tri dele kot pri Fahrenheitih:</p>
<ol>
    <li>Preberi vrednosti katet (a,b = ?)</li>
    <li>Izračunaj dolžino hipotenuze c (c = …)</li>
    <li>Izpis dolžine hipotenuze (print … )</li>
</ol>
''',
        no_input_call,
        math_functions,
        printing]

hint = {
    'no_input_call': no_input_call,

    'math_functions': math_functions,

    'printing': printing,

    'name_error' : [mod.general_msg['error_head'], mod.general_msg['general_exception'],
    mod.general_msg['name_error'], '''
    <p>Verjetno uporabljaš spremenljivko, ki nima vrednosti. Ali v izrazu za izračun
     uporabljaš napačno spremenljivko? Ali pri izpisu morda poskušaš
     izpisati napačno spremenljivko?</p>'''],

    'unsupported_operand' : [mod.general_msg['error_head'], mod.general_msg['general_exception'],
    mod.general_msg['type_error'], '''
<p>Verjetni razlog: funkcija <code>input</code> vrača vrednost tipa niz,
ki jo moramo najprej pretvoriti v tip <code>float</code>, če želimo z njo računati:</p>
<pre>
v = float(input(" ...
</pre>
'''],

    'final_hint' : [
        '''\
<p>Program deluje pravilno! <br>
To pomeni, da znaš uporabljati matematične funkcije, ki so v modulu 
<a href="https://docs.python.org/3.5/library/math.html"><code>math</code></a>. 
Tekom tega sklopa si bomo pogledali še modula za delo z nakjučnimi števili in za delo s časom. </p>''']

}
