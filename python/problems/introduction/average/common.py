from python.util import has_token_sequence, string_almost_equal, \
    string_contains_number, get_tokens, get_numbers, get_exception_desc
from server.hints import Hint


id = 189
number = 4
visible = True

solution = '''\
a = float(input('Ocena [Ana]? '))
b = float(input('Ocena [Benjamin]? '))
c = float(input('Ocena [Cilka]? '))
print('Povprečje:', (a + b + c) / 3)
print('Srednja vrednost:', a + b + c - min(a, b, c) - max( a, b, c))
'''

hint_type = {
    'plan': Hint('plan'),
    'eval_expression': Hint('eval_expression'),
    'name_error': Hint('name_error'),
    'unsupported_operand': Hint('unsupported_operand'),
    'error': Hint('error'),
    'radians': Hint('radians'),
    'printing': Hint('printing'),
    'average': Hint('average'),
    'median': Hint('median'),
    'final_hint': Hint('final_hint')
}



def test(python, code, aux_code=''):
    # List of inputs: (expression to eval, stdin).
    test_in = [
        (None, '2\n4\n5\n'),
        (None, '1\n1\n4\n'),
        (None, '2\n5\n1\n'),
        (None, '3\n1\n1\n'),
        (None, '5\n1\n5\n'),
        (None, '1\n1\n1\n')
    ]

    test_out = [
        [3.67, 4],
        [2, 1],
        [2.67, 2],
        [1.67, 1],
        [3.67, 5],
        [1, 1]
    ]

    # List of outputs: (expression result, stdout, stderr, exception).
    answers = python(code=aux_code+code, inputs=test_in, timeout=1.0)
    outputs = [ans[1] for ans in answers]

    n_correct = 0
    tin = None
    for i, (output, correct) in enumerate(zip(outputs, test_out)):
        if string_almost_equal(output, correct[0], prec=2) and \
           string_almost_equal(output, correct[1], prec=2):
            n_correct += 1
        else:
            tin = test_in[i][1].replace('\n', ' ')
            tout = correct

    passed = n_correct == len(test_in)
    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_in)}}]
    if tin:
        hints.append({'id': 'problematic_test_case', 'args': {'testin': str(tin), 'testout': str(tout)}})
    if passed:
        hints.append({'id': 'final_hint'})
    return passed, hints


def hint(python, code, aux_code=''):
    tokens = get_tokens(code)

    # run one test first to see if there are any exceptions
    test_in = [(None, '1\n1\n4\n')]
    answer = python(code=aux_code+code, inputs=test_in, timeout=1.0)
    exc = answer[0][3]
    exc_hint = get_exception_desc(answer[0][3])
    if exc:
        if 'NameError' in exc:
            return [{'id':'name_error', 'args': {'message': exc}}]
        elif 'TypeError' in exc:
            return [{'id':'type_error', 'args': {'message': exc}}]
        else:
            return exc_hint

    # Help to compute average (if /3 is not program)
    if not has_token_sequence(tokens, ['/', '3']):
        return [{'id': 'average'}]

    # If student is not using functions min or max, then he is
    # not computing median in the right way
    if not has_token_sequence(tokens, ['min']) or \
       not has_token_sequence(tokens, ['max']):
        return [{'id': 'median'}]

    # program is working correctly for mean but not for median
    if '2' in answer[0][1] and '1' not in answer[0][1]:
        return [{'id': 'median'}]

    # student is not using print function
    if not has_token_sequence(tokens, ['print']):
        return [{'id' : 'printing'}]

    return None
