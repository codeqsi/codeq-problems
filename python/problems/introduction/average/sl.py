import server
mod = server.problems.load_language('python', 'sl')

name = 'Povprečna ocena'
slug = 'Povprečna ocena'

description = '''\
<p>Napiši program, ki mu uporabnik vpiše oceno, ki so jo pri matematiki dobili Ana, Benjamin in Cilka.</p>
<p>Program naj izračuna in izpiše povprečno oceno ter srednjo vrednost. Sprogramiraj slednjo brez
uporabe pogojnih stavkov ali česa podobno "naprednega". Konkretno, uporabljaj le funkcije <code>input</code>,
<code>print</code>, <code>min</code> in <code>max</code>.</p>
<p>Namig: <code>min</code> in <code>max</code> lahko prejmeta poljubno število argumentov. Pomisli tudi na
to, da imaš samo tri osebe; pri štirih ta trik ne bi vžgal. </p>
<p>Primer izvajanja programa:</p>
<pre>
Ocena [Ana]? 2
Ocena [Benjamin]? 4
Ocena [Cilka]? 5
Povprečje: 3.6666666666666665
Srednja vrednost: 4.0
</pre>
'''

average =  ['''\
<p>Povprečje izračunamo kot vsoto vseh elementov, ki jo delimo s številom elementov. </p>''',
            '''\
<p>Konkretno v
našem primeru:</p>
<pre>
p = (a + b + c) / 3
</pre>
''']

median = ['''\
<p>Če imamo 3 vrednosti in odstranimo najmanjšo ter največjo vrednost, nam ostane srednja
vrednost.</p>''',
          '''\
<p>Vrednosti seštej in vsoti odštej najmanjšo in največjo vrednost.</p>
''']

plan = [ '''\
<p>Ista strategija kot pri predhodnih nalogah:</p>
<ol>
  <li>preberi ocene Ane, Benjamina in Cilke,</li>
  <li>izračunaj povprečno vrednost in srednjo vrednost ter</li>
  <li>izpiši.</li>
</ol>
''',
         average,
         median]

hint = {
    'average': average,

    'median': median,
    
    'printing': '''
<p> V Pythonu izpisujemo s funkcijo <code>print</code>.</p>''',

    'name_error' : [mod.general_msg['error_head'], mod.general_msg['general_exception'],
    mod.general_msg['name_error'], '''
    <p>Verjetno uporabljaš spremenljivko, ki nima vrednosti. Ali v izrazu za izračun
     uporabljaš napačno spremenljivko? Ali pri izpisu morda poskušaš
     izpisati napačno spremenljivko?</p>'''],

    'type_error' : [mod.general_msg['error_head'], mod.general_msg['general_exception'],
    mod.general_msg['type_error'], '''
<p>Verjetni razlog: funkcija <code>input</code> vrača vrednost tipa niz,
ki jo moramo najprej pretvoriti v tip <code>float</code>, če želimo z njo računati:</p>
<pre>
v = float(input(" ...
</pre>
'''],
    'final_hint': [
        '''\
<p>Odlično, program deluje pravilno! <br>
Pa še to: za funkciji <code>min</code> in <code>max</code> nismo rabili uvoziti nobene
knjižnice (<code>import …</code>), ker sta
<a href="https://docs.python.org/3/library/functions.html">vgrajeni funkciji</a>.''']

}
