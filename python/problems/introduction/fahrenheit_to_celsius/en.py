name = 'Converting Fahrenheit to Celsius'
slug = 'Converting Fahrenheit to Celsius'

description = '''\
<p>Write a program that reads a temperature in degrees Fahrenheit from the
user, and prints the same temperature converted to degrees Celsius. The
conversion formula is C = 5/9 (F – 32).</p>'''

hint = {
    'plan': '''\
<p>(translation missing)</p>''',

    'no_input_call': '''\
<p>(translation missing)</p>''',
}
