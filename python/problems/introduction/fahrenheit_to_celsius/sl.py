import server
mod = server.problems.load_language('python', 'sl')

name = 'Pretvarjanje iz Fahrenheitov v Celzije'
slug = 'Pretvarjanje iz Fahrenheitov v Celzije'

description = '''\
<p>Napiši program, ki mu uporabnik vpiše temperaturo v Fahrenheitovih
stopinjah, program pa jo izpiše v Celzijevih. Med temperaturama pretvarjamo po
formuli C = 5/9 (F – 32).</p>'''

no_input_call = ['''\
<p>Uporabi funkcijo <code>input</code> in shrani rezultat.</p>''',
                 '''\
<p>Funkcija <code>input</code> sprejme niz (<em>angl.</em> string), ki se
prikaže uporabniku kot vprašanje in vrača, kar je uporabnik napisal. </p>''',
                 '''\
<p>Primer:</p>
<pre>
ime = input("Kako ti je ime?")
</pre>
<p>pokliče funkcijo <code>input</code>, ki povpraša uporabnika po imenu in
shrani uporabnikov odgovor v spremenljivko <code>ime</code>.</p>''']

expressions_python = ['''\
<p>Kot kaže, imaš temperaturo v Fahrenheitih že prebrano. 
Zdaj pa izračunaj temperaturo v Celzijevih stopinjah. </p>''',
                      '''\
<p>Python izraze izračuna. Če napišemo</p>
<pre>
3 + 6 * 5
</pre>
<p>bo izračunal <code>3 + 6 * 5</code>.</p>''',
                      '''\
<p>Kadar želimo rezultat shraniti, za to uporabimo <em>prireditveni stavek</em>, kjer na levo
napišemo ime spremenljivke, na desno pa izraz:</p>
<pre>
c = 2 * a * (3 + b)
</pre>
''']

printing = ['''\
<p>V Pythonu izpisujemo s funkcijo <code>print</code>.</p>''',
            '''\
</p>Če želimo izpisati več elementov, jih ločimo z vejico.
Imejmo spremenljivko <code>ime</code>, ki vsebuje naše ime, potem:
<pre>
print("Ime mi je", ime, ".")
</pre>''']

plan = ['''\
<p>Program izvedemo v treh korakih:</p>
<ol>
  <li>Vprašaj uporabnika po temperaturi v Fahrenheitih (F = ?).</li>
  <li>Izračun temperature v Celzijih: C = 5/9 (F – 32)</li>
  <li>Izpis temperature v Celzijih (izpiši C).</li>
</ol>''',
        no_input_call,
        expressions_python,
        printing
        ]

hint = {
    'no_input_call': no_input_call,

    'expressions_python': expressions_python,

    'printing': printing,

    'name_error' : [
        mod.general_msg['error_head'],
        mod.general_msg['general_exception'],
        mod.general_msg['name_error'],
        '''\
<p>Verjetno uporabljaš spremenljivko, ki nima vrednosti. Ali v izrazu za
izračun uporabljaš napačno spremenljivko? Ali pri izpisu morda poskušaš
izpisati napačno spremenljivko?</p>'''
    ],

    'unsupported_operand' : [
        mod.general_msg['error_head'],
        mod.general_msg['general_exception'],
        mod.general_msg['type_error'],
        '''\
<p>Verjetni razlog: funkcija <code>input</code> vrača vrednost tipa niz, ki jo
moramo najprej pretvoriti v tip <code>float</code>, če želimo z njo
računati:</p>
<pre>
v = float(input(" ...
</pre>''',
        '''\
<p>Na primeru pretvarjanja temperatur:</p>
<pre>
fniz = input("Temperatura [F]: ")
f = float(fniz)
</pre>''',
        '''\
<p>Krajše, združeno v eno vrstico:</p>
<pre>
f = float(input("Temperatura [F]: "))
</pre>
<p>Funkcijo <code>float</code> lahko kličemo le, če je v nizu zapisano število,
npr. "10". Če imamo v nizu tudi kakšno črko, bo Python javil napako.</p>'''
    ],

    'not_callable' : [
        mod.general_msg['error_head'],
        mod.general_msg['general_exception'],
        mod.general_msg['type_error'],
        '''\
<p>V programu poskušaš uporabiti število kot funkcijo, a to ne gre.</p>''',
        '''\
<p>Verjetni razlog: Ali si v izrazu pozabil na znak *? Pri pretvorbi temperatur bi namesto:</p>
<pre>
C = 5/9(F – 32)
</pre>
<p>moral napisati: </p>
<pre>
C = 5/9 * (F – 32).
</pre>
<p>Python ne zna izpuščati znaka za množenje, kot to delamo pri matematiki.</p>'''
    ],

    'final_hint' : [
            '''\
<p>Odlično, program deluje!<br>
Pri tej nalogi si se naučil:</p>
<ul>
  <li>uporabljati funkcijo <code>input</code>,</li>
  <li>kako spremeniti niz v število,</li>
  <li>računati v Pythonu in</li>
  <li>izpisati rezultat!</li>
</ul>''']
}
