from python.util import has_token_sequence, string_almost_equal, \
    string_contains_number, get_tokens, get_numbers, get_exception_desc
from server.hints import Hint

id = 187
number = 3
visible = True

solution = '''\
from math import *
g = 9.8
kot = float(input("Vnesi kot (v stopinjah): "))
v = float(input("Vnesi hitrost (v m/s): "))
kot_rad = kot * 2 * pi / 360
razdalja = v ** 2 * sin(2 * kot_rad) / g
print("Kroglo bo odneslo", razdalja, "metrov.")
'''

hint_type = {
    'plan': Hint('plan'),
    'eval_expression': Hint('eval_expression'),
    'sin_error': Hint('sin_error'),
    'name_error': Hint('name_error'),
    'unsupported_operand': Hint('unsupported_operand'),
    'error': Hint('error'),
    'radians': Hint('radians'),
    'printing': Hint('printing'),
    'betterg': Hint('betterg'),
    'final_hint': Hint('final_hint')
}

def contains_negative(s):
    nums = get_numbers(s)
    for n in nums:
        if n < 0:
            return True
    return False

def test(python, code, aux_code=''):
    # List of inputs: (expression to eval, stdin).
    test_in = [
        (None, '45\n100\n'),
        (None, '44\n100\n'),
        (None, '46\n100\n'),
        (None, '0\n100\n'),
        (None, '90\n100\n'),
        (None, '32\n747\n'),
        (None, '45\n10\n'),
        (None, '40\n10\n'),
        (None, '10\n10\n'),
        (None, '80\n10\n'),
        (None, '80\n20\n'),
        (None, '80\n0\n')
    ]

    test_out = [
        '1020.4',
        '1019.79',
        '1019.79',
        '0.0',
        '0.0',
        '51177.06',
        '10.20',
        '10.05',
        '3.49',
        '3.49',
        '13.96',
        '0'
    ]

    # List of outputs: (expression result, stdout, stderr, exception).
    answers = python(code=aux_code+code, inputs=test_in, timeout=1.0)
    outputs = [ans[1] for ans in answers]

    n_correct = 0
    tin = None
    for i, (output, correct) in enumerate(zip(outputs, test_out)):
        if string_almost_equal(output, float(correct), prec=2):
            n_correct += 1
        else:
            tin = test_in[i][1].replace('\n', ' ')
            tout = correct

    passed = n_correct == len(test_in)
    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_in)}}]
    if tin:
        hints.append({'id': 'problematic_test_case', 'args': {'testin': str(tin), 'testout': str(tout)}})
    if passed:
        hints.append({'id': 'final_hint'})
    return passed, hints

def hint(python, code, aux_code=''):
    tokens = get_tokens(code)

    # run one test first to see if there are any exceptions
    test_in = [(None, '5\n10\n')]
    answer = python(code=aux_code+code, inputs=test_in, timeout=1.0)
    exc = answer[0][3]
    exc_hint = get_exception_desc(answer[0][3])
    if exc:
        if 'sin' in exc and 'NameError' in exc:
            return [{'id':'sin_error', 'args': {'message': exc}}]
        elif 'NameError' in exc:
            return [{'id':'name_error', 'args': {'message': exc}}]
        elif 'unsupported operand' in exc:
            return [{'id':'unsupported_operand', 'args': {'message': exc}}]
        elif 'TypeError' in exc:
            return [{'id':'type_error', 'args': {'message': exc}}]
        else:
            return exc_hint

    # if sinus is not in code, we need to teach students where they can get it
    # use math functions.
    if  (not has_token_sequence(tokens, ['sin'])):
        return [{'id' : 'eval_expression'}]

    # student is not using print function
    if not has_token_sequence(tokens, ['print']):
        return [{'id' : 'printing'}]

    # if result is negative, student did not translate to radians
    if contains_negative(answer[0][1]):
        return [{'id': 'radians'}]

    # using g=10
    if string_almost_equal(answer[0][1], 1.736, prec=2):
        return [{'id': 'betterg'}]

    return None
