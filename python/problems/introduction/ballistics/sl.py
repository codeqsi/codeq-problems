import server
mod = server.problems.load_language('python', 'sl')

name = 'Topologija'
slug = 'Topologija'

description = '''\
<p>Napiši program za izračun dolžine strela s topom (ki brez trenja izstreljuje 
točkaste krogle v brezzračnem prostoru, a pustimo trivio). Program od uporabnika 
ali uporabnice zahteva, da vnese kot, pod katerim je izstreljen, in hitrost izstrelka. Program naj izračuna in izpiše, kako daleč
bo letela krogla.</p>
<p>Pomoč za fizično nebogljene: \( s=v^2 \\sin(2f)/g \), kjer je \(s\) razdalja,
\(v\) hitrost izstrelka, \(f\) kot, \(g\) pa gravitacijska konstanta (9.8).</p>
<p>Preveri tole: krogla leti najdalj, če jo izstrelimo pod kotom 45 stopinj. 
Poskusi, kako daleč gre pod kotom 45 in kako daleč pod 46 stopinj -- po 45 mora leteti dlje. 
Preveri tudi kot 50 stopinj: če pod tem kotom leti nazaj (razdalja je negativna), 
si ga gotovo nekje polomil.
<p><i>Opomba: za pravilno testiranje moraš v programu najprej prebrati kot in potem hitrost.</i></p>
'''

eval_expression = ['''\
<p>Dolžino strela dobiš po formuli: \( s=v^2 \\sin(2f)/g \).</p>''',
                   '''\
<p>Funkcijo <code>sin</code> dobiš v modulu <code>math</code>, kot smo tam
dobili funkcijo <code>sqrt</code>. Na začetku napiši:</p>
<pre>
from math import *
</pre>''',
                   '''\
<p>Če <code>sin</code> vrača čudne vrednosti, si
poglej <a href="https://docs.python.org/3/library/math.html">dokumentacijo</a>.
Še posebno pozorno preberi razlago v oklepajih.
Do dokumentacije lahko dostopaš tudi z ukazom <code>help</code>:</p>
<pre>
help(sin)
</pre>
'''
]

printing = ['''\
<p> V Pythonu izpisujemo s funkcijo <code>print</code>.</p>''']

plan = ['''\
<p>Plan sledi že znani strategiji: preberi vrednosti – izračunaj – izpiši.</p>''',
        eval_expression,
        printing

]

hint = {

    'eval_expression': eval_expression,

    'printing': printing,

    'radians': ['''\
<p>Poskusi <code>sin(30)</code>. Zakaj je rezultat negativen?</p>''',
                '''\
<p>Vse triginometrične funkcije sprejemajo kot v radianih in ne v stopinjah.</p>''',
''' <p>V stopinjah ima cel krog 360°, v radianih pa \( 2 \\pi \). </p>''',
''' <p>Formula za pretvorbo med stopinjami in radiani je: </p>
<pre>
kot_rad = kot * 2 * pi / 360
</pre>
'''],

    'betterg': '''Konstanta \(g\) naj ima vrednost 9.8''',

    'name_error' : [mod.general_msg['error_head'], mod.general_msg['general_exception'],
    mod.general_msg['name_error'], '''
    <p>Verjetno uporabljaš spremenljivko, ki nima vrednosti. Morda v računu ali pri izpisu?</p>'''],

    'sin_error' : [mod.general_msg['error_head'], mod.general_msg['general_exception'],
    mod.general_msg['name_error'], '''
    <p>Za uporabo funkcije <code>sin</code> je potrebno prej uvoziti modul <code>math</code>:
    <code>from math import *</code> ali <code>import math</code>. '''],

    'unsupported_operand' : [mod.general_msg['error_head'], mod.general_msg['general_exception'],
    mod.general_msg['type_error'], '''
<p>Verjetni razlog: funkcija <code>input</code> vrača vrednost tipa niz,
ki jo moramo najprej pretvoriti v tip <code>float</code>:</p>
<pre>
v = float(input(" ...
</pre>
'''],
    'type_error' : [mod.general_msg['error_head'], mod.general_msg['general_exception'],
    mod.general_msg['type_error'], '''
<p>Verjetni razlog: funkcija <code>input</code> vrača vrednost tipa niz,
ki jo moramo najprej pretvoriti v tip <code>float</code>:</p>
<pre>
v = float(input(" ...
</pre>
'''],
    'final_hint' : [
        '''\
<p>Odlično, program deluje pravilno!<br>
Če želiš izvedeti še kaj več o <a target="_blank" href="https://en.wikipedia.org/wiki/Topology">topologiji</a>….</p>
''']

}
