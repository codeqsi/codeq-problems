import server
mod = server.problems.load_language('python', 'sl')


id = 240
name = 'Domine'
slug = 'Domine'


description = '''\
<p>
Vrsta domin je podana s seznamom parov (terk), na primer
<code>[(3, 6), (6, 6), (6, 1), (1, 0)] ali [(3, 6), (6, 6), (2, 3)]</code>. Napišite funkcijo <code>dominoes(xs)</code>,
ki prejme takšen seznam in pove, ali so domine pravilno zložene. Za prvi seznam mora vrniti <code>True</code> in za drugega <code>False</code>.
</p>'''

plan = []

hint = {
    'final_hint': ['''\
<p>Program je pravilen! <br>
</p>
'''],
}
