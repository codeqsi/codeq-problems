import re
from python.util import has_token_sequence, string_almost_equal, \
    string_contains_number, get_tokens, get_numbers, get_exception_desc
from server.hints import Hint

id = 240
number = 6
visible = True

solution = '''\
def dominoes(domine):
    for i in range(len(domine) - 1):
        if domine[i][1] != domine[i + 1][0]:
            return False
    return True
'''

hint_type = {
    'final_hint': Hint('final_hint')
}

def test(python, code, aux_code=''):
    func_name = 'dominoes'
    tokens = get_tokens(code)
    if not has_token_sequence(tokens, ['def', func_name]):
        return False, [{'id' : 'no_func_name', 'args' : {'func_name' : func_name}}]

    in_out = [
        ([], True),
        ([(2, 4), (4, 4)], True),
        ([(2, 4), (4, 4), (4, 2)], True),
        ([(2, 4), (4, 4), (4, 2), (2, 9), (9, 1)], True),
        ([(2, 4), (4, 3), (4, 2), (2, 9), (9, 1)], False),
        ([(3, 6), (6, 6), (6, 1), (1, 0)], True),
        ([(3, 6), (6, 6), (2, 3)], False),
    ]

    test_in = [(func_name+'(%s)'%str(l[0]), None) for l in in_out]
    test_out = [l[1] for l in in_out]

    answers = python(code=aux_code+code, inputs=test_in, timeout=1.0)
    n_correct = 0
    tin, tout = None, None
    for i, (ans, to) in enumerate(zip(answers, test_out)):
        n_correct += ans[0] == to
        if ans[0] != to:
            tin = test_in[i][0]
            tout = to

    passed = n_correct == len(test_in)
    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_in)}}]
    if tin:
        hints.append({'id': 'problematic_test_case', 'args': {'testin': str(tin), 'testout': str(tout)}})
    if passed:
        hints.append({'id': 'final_hint'})
    return passed, hints


def hint(python, code, aux_code=''):
    tokens = get_tokens(code)

    # run one test first to see if there are any exceptions
    answer = python(code=aux_code+code, inputs=[(None, None)], timeout=1.0)
    exc = get_exception_desc(answer[0][3])
    if exc: return exc

    return None
