import server
mod = server.problems.load_language('python', 'sl')


id = 235
name = 'Najdaljša beseda'
slug = 'Najdaljša beseda'


description = '''\
<p>
Napiši funkcijo <code>longest(s)</code>, ki vrne najdaljšo besedo v nizu <code>s</code>.
<pre>
>>> longest('an ban pet podgan')
'podgan'
</p>'''

split = ['''\
<p>
Uporabi metodo <code>split</code>, ki razdeli niz na podnize.</p>''',
        '''\
<pre>
>>> st = 'an ban pet podgan'
>>> st.split()
['an', 'ban', 'pet, 'podgan']
</pre>
''']

len_func = [
        '''\
<p>Funkcija <code>len</code> vrne dolžino niza (pa tudi seznama, terke, itd.)</p>''']




plan = [split,
'''\
<pre>
def longest(s):
    z zanko čez vse besede
        če je beseda daljša od trenutno najdaljše besede
            ustrezno spremeni najdaljšo besedo
    vrni rezultat
</pre>
''',
     len_func]

hint = {
    'final_hint': ['''\
<p>Program je pravilen! <br>
</p>
'''],
}
