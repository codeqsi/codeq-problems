import server
mod = server.problems.load_language('python', 'sl')


id = 238
name = 'Vsi'
slug = 'Vsi'


description = '''\
<p>
Napišite funkcijo <code>all</code>, ki sprejme seznam <code>xs</code> in vrne <code>True</code>,
če so vse vrednosti v seznamu resnične. Elementi seznama <code>xs</code> so lahko poljubnega tipa, ne le <code>bool</code>.
<pre>
>>> all([True, True, False])
False
>>> all([True, True])
True
>>> all([1, 2, 3, 0])
False
>>> all(['foo', 42, True])
True
>>> all([])
True
</pre>
</p>'''

plan = []

hint = {
    'final_hint': ['''\
<p>Program je pravilen! <br>
</p>
'''],
}
