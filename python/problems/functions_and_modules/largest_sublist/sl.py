import server
mod = server.problems.load_language('python', 'sl')


id = 242
name = 'Največji podseznam'
slug = 'Največji podseznam'


description = '''\
<p>
Napiši funkcijo <code>largest_sublist</code>, ki vrne podseznam z največjo vsoto elementov.
<pre>
>>> largest_sublist([[1, 1, 1], [1, 1]])
[1, 1, 1]
>>> largest_sublist([[2, 4, 1], [3, 1], [], [8, 2], [1, 1, 1, 1]])
[8, 2]
</pre>
</p>'''

plan = []

hint = {
    'final_hint': ['''\
<p>Program je pravilen! <br>
</p>
'''],
}
