import server
mod = server.problems.load_language('python', 'sl')


id = 243
name = 'Cezarjeva šifra'
slug = 'Cezarjeva šifra'


description = '''\
<p>
Napišite funkcijo <code>caesar(s)</code>, ki podan niz zašifrira z uporabo <a href="http://sl.wikipedia.org/wiki/Cezarjeva_%C5%A1ifra">cezarjeve šifre</a>.
Preden se lotite naloge, se je morda vredno pozanimati kaj počneta funkciji
<a href="https://docs.python.org/3.5/library/functions.html#ord">ord</a> in
<a href="https://docs.python.org/3.5/library/functions.html#chr">chr</a>.
Predpostavite lahko, da niz <code>s</code> vsebuje le male črke angleške besede in presledke.
<pre>
>>> caesar('the quick brown fox jumps over the lazy dog')
'wkh txlfn eurzq ira mxpsv ryhu wkh odcb grj'
</pre>
</p>'''

plan = []

hint = {
    'final_hint': ['''\
<p>Program je pravilen! <br>
</p>
'''],
}
