import server
mod = server.problems.load_language('python', 'sl')


id = 237
name = 'Sumljive besede'
slug = 'Sumljive besede'


description = '''\
<p>
Napiši funkcijo <code>suspicious(s)</code>, ki vrne seznam vseh sumljivih besed v danem nizu.
Beseda je sumljiva, če vsebuje tako črko u kot črko a.
<pre>
>>> susupicious('Muha pa je rekla: "Tale juha se je pa res prilegla, najlepša huala," in odletela.')
['Muha', 'juha', 'huala,"']
</pre>
</p>'''

plan = []

hint = {
    'final_hint': ['''\
<p>Program je pravilen! <br>
</p>
'''],
}
