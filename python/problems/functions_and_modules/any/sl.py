import server
mod = server.problems.load_language('python', 'sl')


id = 239
name = 'Vsaj eden'
slug = 'Vsaj eden'


description = '''\
<p>
Napišite funkcijo <code>any(xs)</code>, ki deluje podobno kot <code>all</code>, le da vrne <code>True</code>,
če je vsaj ena vrednost v seznamu resnična.
<pre>
>>> any([2, 3, 0])
True
>>> any([])
False
</pre>
</p>'''

plan = []

hint = {
    'final_hint': ['''\
<p>Program je pravilen! <br>
</p>
'''],
}
