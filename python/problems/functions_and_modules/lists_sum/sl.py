import server
mod = server.problems.load_language('python', 'sl')


id = 241
name = 'Vsota seznamov'
slug = 'Vsota seznamov'


description = '''\
<p>
Podan je seznam seznamov, npr. <code>[[2, 4, 1], [3, 1], [], [8, 2], [1, 1, 1, 1]]</code>.
Napiši funkcijo <code>lists_sum(xxs)</code>, ki v seznamu vrne vsote vseh elementov v posameznih podseznamih.
Za gornji seznam naj funkcija vrne seznam <code>[7, 4, 0, 10, 4]</code>, saj je, na primer, <code>2 + 4 + 1 = 7</code>.
<pre>
>>> lists_sum([[1, 1, 1], [1, 1]])
[3, 2]
>>> lists_sum([[2, 4, 1], [3, 1], [], [8, 2], [1, 1, 1, 1]])
[7, 4, 0, 10, 4]
</pre>
</p>'''

plan = []

hint = {
    'final_hint': ['''\
<p>Program je pravilen! <br>
</p>
'''],
}
