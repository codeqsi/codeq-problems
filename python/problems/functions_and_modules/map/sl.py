import server
mod = server.problems.load_language('python', 'sl')


id = 245
name = 'Slikaj'
slug = 'Slikaj'


description = '''\
<p>
Napišite funkcijo <code>map(f, xs)</code>, ki sprejme funkcijo <code>f</code> in seznam <code>[x_1, x_2, ..., x_n]</code>
in vrne nov seznam <code>[f(x_1), f(x_2), ..., f(x_n)]</code>.
<pre>
>>> map(abs, [-5, 8, -3, -1, 3])
[5, 8, 3, 1, 3]
>>> map(len, "Daydream delusion limousine eyelash".split())
[8, 8, 9, 7]
</pre>
</p>'''

plan = []

hint = {
    'final_hint': ['''\
<p>Program je pravilen! <br>
</p>
'''],
}
