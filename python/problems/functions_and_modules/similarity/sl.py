import server
mod = server.problems.load_language('python', 'sl')


id = 236
name = 'Podobnost'
slug = 'Podobnost'


description = '''\
<p>
Napišite funkcijo <code>similarity(s1, s2)</code>, ki izračuna podobnost med dvema nizoma.
Podobnost definirajmo kot število mest v katerih se niza ujemata.
<pre>
sobota
robot
------
011110 -> 4

>>> podobnost('sobota', 'robot')
4
>>> podobnost('robot', 'sobota')
4
</pre>
</p>'''

range_for = ['''\
<p>S <code>for</code> prehodi vse indekse in na vsakem koraku primerjaj ali sta 
črki (pri trenutnem indeksu) enaki.</p>''',
        '''\
<p>Zanka s <code>for</code>:</p>
<pre>
for i in range(min(len(s1), len(s2))):
</pre>''']

plan = ['''\
<p>Naloge se lahko lotiš na dva načina: 1) z <code>zip</code> ustvariš pare 
soležnih elementov, ali 2) uporabiš funkcijo <code>range</code> in primerjaš po indeksih. </p>''',
        '''\
<p>Zanka z <code>zip</code>: </p>
<pre>
for v1, v2 in zip(s1, s2):
</pre>''',
        range_for]


hint = {
    'final_hint': ['''\
<p>Program je pravilen! <br>
</p>
'''],
}
