import server
mod = server.problems.load_language('python', 'sl')


id = 244
name = 'Multiplikativni range'
slug = 'Multiplikativni range'


description = '''\
<p>
Napišite funkcijo <code>mrange(start, factor, length)</code>, ki vrne seznam, kjer je vsako naslednje število za
<code>factor</code> večje od prejšnjega. Npr., v seznamu <code>[1, 2, 4, 8, 16]</code> je vsako naslednje število 2-krat večje od prejšnjega.
<pre>
>>> print(mrange(7, 4, 7))
[7, 28, 112, 448, 1792, 7168, 28672]
</pre>
</p>'''

plan = []

hint = {
    'final_hint': ['''\
<p>Program je pravilen! <br>
</p>
'''],
}
