import server
mod = server.problems.load_language('python', 'sl')


id = 229
name = 'Palindromska števila'
slug = 'Palindromska števila'


description = '''\
<p>
Največje palindromsko število, ki ga lahko dobimo kot produkt dveh dvomestnih števil je <code>9009 = 91 * 99</code>.
Napišite funkcijo <code>palindromic_number()</code>, ki poišče in vrne največje palindromsko število,
ki ga lahko dobimo kot produkt dveh tromestnih števil.

Vir: Project Euler, <a href="http://projecteuler.net/index.php?section=problems&id=4">Problem 4</a>.
</p>'''

num_to_str = [
        '''\
<p>Ali je niz palindrom, že znamo ugotoviti. To smo se naučili pri prejšnji nalogi.</p>''',
        '''\
<p>Kako bi spremenil število v niz?</p>''',
        '''\
<code>
niz = str(stevilo)
</code>''']

double_loop = [
        '''\
<p>Preizkusiti moraš produkte vseh kombinacij tromestnih števil.</p>''',
        '''\
<p>Potrebna bo dvojna zanka...</p>''',
        '''\
<pre>
for st1 in range(100, 1000):
    for st2 in range(100, 1000):
</pre>''']

plan = [num_to_str,
        double_loop]

hint = {
    'final_hint': ['''\
<p>Program je pravilen! <br>
</p>
'''],
}
