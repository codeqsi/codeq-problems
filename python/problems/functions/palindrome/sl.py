import server
mod = server.problems.load_language('python', 'sl')


id = 228
name = 'Palindrom'
slug = 'Palindrom'


description = '''\
<p>
Napišite funkcijo <code>palindrome(s)</code>, ki preveri, ali je niz s palindrom.
<pre>
>>> palindrome('pericarezeracirep')
True
>>> palindrome('perica')
False
</pre>
</p>'''

plan = ['''\
<p>Niz je palindrom, če je enak obrnjenemu nizu. Kako bi obrnil niz?</p>
''',
        '''\
<p>s[::-1]</p>''']

hint = {
    'final_hint': ['''\
<p>Program je pravilen! <br>
</p>
'''],
}
