import server
mod = server.problems.load_language('python', 'sl')


name = 'Največji negativec'
slug = 'Največji negativec'


description = '''\
<p>Napiši funkcijo <code>max_neg(xs)</code>, ki vrne največje negativno število
v seznamu <code>xs</code>. Če seznam nima negativnih števil, naj funkcija vrne <code>None</code>.</p>
<pre>
>>> max_neg([5, 1, -6, -7, 2])
-6
</pre>
'''

function = ['''\
<p>Napiši definicijo funkcije <code>max_neg(xs)</code>.</p>''',
            '''\
<p>Definicijo funkcije začnemo z <code>def</code>, temu sledi ime, potem oklepaji,
v katerih naštejemo argumente funkcije, nato zaklepaj in na koncu dvopičje</p>''',
            '''\
<pre>
def max_neg(xs):
    # poišči največje negativno število v xs

s = [5, 1, -6, -7, 2]
print (max_neg(s))
</pre>''']

main_plan = ['''\
<p>Plan: po vrsti pogledamo vse elemente in sproti hranimo največje negativno
število.</p>''',
             '''\
<p>Na začetku ne moremo največjemu nastaviti vrednosti prvega elementa v seznamu,
saj je lahko prvi element pozitiven.</p>''',
             '''\
</p>Nastavite ga na <code>None</code></p>''',
             ]

return_clause = ['''\
<p>Namesto, da izpišemo rezultat, ga vračamo s stavkom <code>return</code>.</p>''',
                 '''\
<p>Pazi, da vrneš rezultat, šele ko se zanka izteče.</p>''']

plan = [function,
        main_plan,
        return_clause]

for_loop = ['''\
<p>Preglej elemente z zanko</p>.
''',
             '''\
<pre>
for x in xs:
</pre>''']


if_clause = ['''\
<p>Preveri, če je trenutni element negativen in večji od trenutno največjega negativca.</p>''',]


hint = {
    'no_def': function,

    'no_return': return_clause,

    'for_loop': for_loop,

    'if_clause': if_clause,

    'not_int': '''\
<p>Funkcija ne vrača števila</p>''',

    'return_first': '''\
<p>Funkcija vrača prvi element v seznamu''',

    'return_last': '''\
<p>Funkcija vrača zadnji element v seznamu.''',

    'return_greatest': '''\
<p>Funkcija vrača največji pozitivni element v seznamu (ne pa največji negativni).''',

    'return_positive': '''\
<p>Funkcija napačno vrača pozitivno vrednost največjega negativnega števila.''',

    'return_absolute': '''\
<p>Funkcija napačno vrača element z največjo absolutno vrednostjo.</p>''',

    'return_indent': '''\
<p>Ali imaš stavek <code>return</code> znotraj zanke?
V tem primeru se lahko zgodi, da se zanka ne izteče do konca.</p>'''

}

