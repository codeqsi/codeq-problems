import server
mod = server.problems.load_language('python', 'sl')


id = 225
name = 'Oštevilči'
slug = 'Oštevilči'


description = '''\
<p>
Napiši funkcijo <code>numbers(xs)</code>, ki vrne seznam oblike <code>[(0, xs[0]), (1, xs[1]), ..., (n, xs[n])]</code>.
Število <code>n</code> je enako dolžini seznama <code>xs</code> minus ena.
<pre>
>>> numbers([4, 4, 4])
[(0, 4), (1, 4), (2, 4)]
>>> numbers([5, 1, 4, 2, 3])
[(0, 5), (1, 1), (2, 4), (3, 2), (4, 3)]
</pre>
</p>'''

range_function = [
        '''\
<p>
Uporabi funkcijo <code>range</code>.
</p>''',
        '''\
<p>Poskusi, kaj naredi naslednji program: </p>
<pre>
xs = [2,3,4,5]
for i in range(len(xs)):
    print (i, xs[i])
</pre>''']

tuples = [
        '''\
<p>
Več elementov skupaj v navadnih oklepajih imenujemo terka oz. <code>tuple</code>.
</p>''',
        '''\
<p>
V seznam lahko dodajam poljubne objekte. Lahko dodamo tudi terko, poskusi: 
</p>
<pre>
xs = [1,2]
xs.append((1,2))
print (xs)
</pre>'''
]

plan = [range_function,
        tuples]

hint = {
    'final_hint': ['''\
<p>Program je pravilen! <br>
</p>
'''],
}
