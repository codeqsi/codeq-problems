import server
mod = server.problems.load_language('python', 'sl')


id = 234
name = 'Praštevila'
slug = 'Praštevila'


description = '''\
<p>
Napišite funkcijo <code>prime(n)</code>, ki izpiše vsa praštevila manjša od <code>n</code>.
</p>'''

plan = []

hint = {
    'final_hint': ['''\
<p>Program je pravilen! <br>
</p>
'''],
}
