import server
mod = server.problems.load_language('python', 'sl')


name = 'Največji'
slug = 'Največji'


description = '''\
<p>Napišite funkcijo <code>max_val(xs)</code>, ki vrne največje število v seznamu <code>xs</code>.
<pre>
>>> max_val([5, 1, -6, -7, 2])
5
</pre></p>
'''

function = ['''\
<p>Napišite definicijo funckcije <code>max_val(xs)</code></p>''',
            '''\
<p>Definicijo funkcije začnemo z <code>def</code>, temu sledi ime, potem oklepaji,
v katerih naštejemo argumente funkcije, nato zaklepaj in na koncu dvopičje</p>''',
            '''\
<pre>
def max_val(xs):
    # program, ki poišče največjega v xs
</pre>''']

main_plan = ['''\
<p>Ideja: po vrsti pogledamo vse elemente in sproti hranimo največjega.</p>''',
             '''\
<ol>
<li>Začnemo s prvim elementom in si ga zapomnimo kot največjega.</li>
<li>Potem pogledamo drugi element, če je večji od največjega, ta postane največji.</li>
<li>Pogledamo tretji element, če je večji od največjega, postane največji. </li>
<li>Pogledamo četrti element, ... </li>
<li>...</li>
<li>Pogledamo zadnji element, ...
</ol>''',
             '''\
<p>Potrebovali bomo zanko!</p>
<pre>
najvecji = prvi element od xs
for x in xs:
    ce je x > najvecji:
        najvecji postane x
izpisi najvecjega
</pre>''']

return_clause = ['''\
<p>Namesto, da izpišemo rezultat, ga vračamo s stavkom <code>return</code>.</p>''']

plan = [['''\
<p>Najprej poskusi napisati program (brez funkcije), ki v <code>xs</code> poišče največji element</p>''',
         '''\
<pre>
xs = [5, 1, -6, -7, 2]
# sledi program, ki izpiše največjega v xs
</pre>'''],
        main_plan,
        ['''\
<p>Napišite funkcijo tako, da vsebuje napisani program. </p>''',
        '''\
<p>Funkcijo testirajte!</p>
<pre>
# najprej definicija funkcije
def max_val...

s1 = [5, 1, -6, -7, 2]
naj_elt = max_val(s1)
print(naj_elt)

s2 = [-5, -1, 6, 7, -2]
naj_elt = max_val(s2)
print(naj_elt)
</pre>'''],
        function,
        return_clause]

for_loop = ['''\
<p>Preglejte elemente z zanko</p>.
''',
             '''\
<pre>
for x in xs:
    print (x)
</pre>''']


if_clause = ['''\
<p>Preverite, če je trenutni element večji od največjega</p>''',
             '''\
<pre>
if x > najvecji:
    najvecji = x
</pre>''']


hint = {
    'no_def': function,

    'no_return': return_clause,

    'for_loop': for_loop,

    'if_clause': if_clause,

    'final_hint': '''\
<p>Odlično, naloga rešena! Še zanimivost: Python ima funkcijo <code>max</code> že vgrajeno:</p>
<pre>
def max_val(xs):
    return max(xs)
</pre>''',

    'not_int': '''\
<p>Funkcija ne vrača števila</p>''',

    'return_first': '''\
<p>Funkcija vrača prvi element v seznamu''',

    'return_last': '''\
<p>Funkcija vrača zadnji element v seznamu.''',

    'return_indent': '''\
<p>Ali imaš stavek <code>return</code> znotraj zanke?
V tem primeru se lahko zgodi, da se zanka ne izteče do konca.</p>'''
}
