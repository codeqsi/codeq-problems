import server
mod = server.problems.load_language('python', 'sl')


id = 232
name = 'Popolna števila'
slug = 'Popolna števila'


description = '''\
<p>
Napiši funkcijo <code>perfect(n)</code>, ki vrne <code>True</code>, če je podano število popolno, oz. <code>False</code>, če ni.
Popolna števila so števila, ki so enaka vsoti svojih deliteljev (brez samega sebe).
Primer popolnega števila je 28, saj so njegovi delitelji 1, 2, 4, 7, 14, njihova vsota, 1+2+4+7+14 pa je spet enaka 28.
</p>
<p> Uporabite funkcijo za vsoto deliteljev!</p>'''

plan = []

hint = {
    'final_hint': ['''\
<p>Program je pravilen! <br>
</p>
'''],
}
