import re
from python.util import has_token_sequence, string_almost_equal, \
    string_contains_number, get_tokens, get_numbers, get_exception_desc, almost_equal
from server.hints import Hint

id = 227
number = 6
visible = True

solution = '''\
def bmi2(names, weights, heights):
    ys = []
    for name, weight, height in zip(names, weights, heights):
        ys.append((name, weight / (height / 100)**2))
    return ys
'''

hint_type = {
    'final_hint': Hint('final_hint')
}

def test(python, code, aux_code=''):
    func_name = 'bmi2'
    tokens = get_tokens(code)
    if not has_token_sequence(tokens, ['def', func_name]):
        return False, [{'id' : 'no_func_name', 'args' : {'func_name' : func_name}}]

    in_out = [
        (([], [], []), []),
        ((['Ana', 'Berta'], [55, 60], [165, 153]),
            [('Ana', 20.202020202020204), ('Berta', 25.63116749967961)]),
        ((['Ana', 'Berta', 'Cilka'], [55, 60, 70], [165, 153, 183]),
            [('Ana', 20.202020202020204), ('Berta', 25.63116749967961), ('Cilka', 20.902385858042937)]),
    ]

    test_in = [(func_name+'(*%s)'%str(l[0]), None) for l in in_out]
    test_out = [l[1] for l in in_out]

    answers = python(code=aux_code+code, inputs=test_in, timeout=1.0)
    n_correct = 0
    tin, tout = None, None
    for i, (ans, to) in enumerate(zip(answers, test_out)):
        corr = 0
        if isinstance(ans[0], list) and len(ans[0]) == len(to):
            corr = 1
            for v1, v2 in zip(ans[0], to):
                if not isinstance(v1, tuple) or len(v1) != len(v2):
                    corr = 0
                    break
                if v1[0] != v2[0] or not almost_equal(v1[1], v2[1], 2):
                    corr = 0
                    break

        n_correct += corr
        if not corr:
            tin = test_in[i][0]
            tout = to

    passed = n_correct == len(test_in)
    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_in)}}]
    if tin:
        hints.append({'id': 'problematic_test_case', 'args': {'testin': str(tin), 'testout': str(tout)}})
    if passed:
        hints.append({'id': 'final_hint'})
    return passed, hints

def hint(python, code, aux_code=''):
    tokens = get_tokens(code)

    # run one test first to see if there are any exceptions
    answer = python(code=aux_code+code, inputs=[(None, None)], timeout=1.0)
    exc = get_exception_desc(answer[0][3])
    if exc: return exc

    return None
