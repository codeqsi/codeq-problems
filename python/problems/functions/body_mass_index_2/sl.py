import server
mod = server.problems.load_language('python', 'sl')

name = 'Indeks telesne teže 2'
slug = 'Indeks telesne teže 2'

description = '''\
<p>
Naloga je podobna prejšnji, le da imamo tokrat podatke v drugačni obliki:
<pre>
>>> imena = ['Ana', 'Berta']
>>> teze = [55, 60]
>>> visine = [165, 153]
>>> bmi2(imena, teze, visine)
[('Ana', 20.202020202020204), ('Berta', 25.63116749967961)]
</pre>
</p>'''

zip_function = ['''\
<p>Uporabi <code>zip</code> in rešitev bo zelo podobna prejšnji. </p>''',
        '''
<pre>
for n, w, h in zip(names, weights, heights):
</pre>''']


plan = [zip_function]

hint = {
    'final_hint': ['''\
<p>Program je pravilen! <br>
</p>
'''],
}
