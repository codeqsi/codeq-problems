import server
mod = server.problems.load_language('python', 'sl')

id = 226
name = 'Indeks telesne teže'
slug = 'Indeks telesne teže'


description = '''\
<p>
Podan imamo seznam trojk: ime osebe, teža, višina, na primer:
<pre>
>>> osebe = [('Ana', 55, 165), ('Berta', 60, 153)]
</pre>
Napišite funkcijo <code>bmi(osebe)</code>, ki na podlagi podanega seznama osebe, sestavi seznam dvojk: ime osebe, indeks telesne teže:
<pre>
>>> bmi(osebe)
[('Ana', 20.202020202020204), ('Berta', 25.63116749967961)]
</pre>
</p>'''

for_multiple = [
        '''\
<p>Kadar imamo v seznamu strukture z enako elementi, lahko v <code>for</code> 
zanko napišemo več spremenljivk:</p>
<pre>
for ime, teza, visina in osebe:
</pre>''']

bmi = [
        '''\
<p><a href="//sl.wikipedia.org/wiki/Indeks_telesne_mase">Link</a>''']


plan = [bmi, for_multiple]

hint = {
    'final_hint': ['''\
<p>Program je pravilen! <br>
</p>
'''],
}
