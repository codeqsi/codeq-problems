import server
mod = server.problems.load_language('python', 'sl')


id = 233
name = 'Prijateljska števila'
slug = 'Prijateljska števila'


description = '''\
<p>
220 in 284 sta prijateljski števili. Delitelji 220 so 1, 2, 4, 5, 10, 11, 20, 22, 44, 55 in 110.
Če jih seštejemo, dobimo 284. Delitelji 284 pa so 1, 2, 4, 71 in 142. Vsota teh števil pa je 220.
Napiši funkcijo <code>amicable_number(n)</code>, ki vrne prijateljsko število številu <code>n</code>, če ga ima, oz.
vrne <code>None</code>, če ga nima. Primer:
<pre>
>>> amicable_number(220)
284
>>> amicable_number(222)
None
</pre>
</p>
<p> Uporabite funkcijo za vsoto deliteljev!</p>'''

plan = []

hint = {
    'final_hint': ['''\
<p>Program je pravilen! <br>
</p>
'''],
}
