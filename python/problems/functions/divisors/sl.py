import server
mod = server.problems.load_language('python', 'sl')

id = 230
name = 'Delitelji'
slug = 'Delitelji'


description = '''\
<p>
Napiši funkcijo <code>divisors(n)</code>, ki izpiše vse delitelje števila (brez samega sebe), ki ga vnese uporabnik.</p>'''

div_modulo = [
        '''\
<p>Število <code>a</code> deli število <code>b</code>, če je ostanek
pri deljenju števil enak 0.</p>''',
        '''\
<pre>
if b % a == 0: 
</pre>''']

plan = [
        '''\
<p>Za vsa cela števila, ki so manjša od <code>n</code>, preveri, 
če delijo <code>n</code>. Če delijo, jih izpiši. </p>''',
        div_modulo]

hint = {
    'final_hint': ['''\
<p>Program je pravilen! <br>
</p>
'''],
}
