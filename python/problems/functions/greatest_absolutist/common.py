import re
from python.util import has_token_sequence, string_almost_equal, \
    string_contains_number, get_tokens, get_numbers, get_exception_desc
from server.hints import Hint

id = 196
number = 2
visible = True

solution = '''\
def max_abs(xs):
    ma = xs[0]
    for x in xs:
        if abs(x) > abs(ma):
            ma = x
    return ma
'''

hint_type = {
    'no_def': Hint('no_def'),
    'no_return': Hint('no_return'),
    'return_indent': Hint('return_indent'),
    'for_loop': Hint('for_loop'),
    'not_int': Hint('not_int'),
    'return_first': Hint('return_first'),
    'return_last': Hint('return_last'),
    'return_greatest': Hint('return_greatest'),
    'return_positive': Hint('return_positive'),
    'return_indent': Hint('return_indent'),
    'final_hint': Hint('final_hint')
}


def test(python, code, aux_code=''):
    tokens = get_tokens(code)
    if not has_token_sequence(tokens, ['def', 'max_abs']):
        return False, [{'id' : 'no_func_name', 'args' : {'func_name' : 'max_abs'}}]

    test_lists = [[6, 4, 2, 0],
               [4, 6, 2, 0],
               [4, 2, 6, 0],
               [4, 2, 0, 6],
               [6, -8, 2, 0],
               [-8, 6, 2, 0],
               [8, -6, -2, 0],
               [-5, -6, -2, -1],
               [-8, -1, -6, -2],
               [-1, -8, -6, -2],
               [42],
               [-42]]
    test_in = [('max_abs(%s)'%str(l), None) for l in test_lists]
    test_out = [6, 6, 6, 6, -8, -8, 8, -6, -8, -8, 42, -42]

    answers = python(code=aux_code+code, inputs=test_in, timeout=1.0)
    n_correct = 0
    tin, tout = None, None
    for i, (ans, to) in enumerate(zip(answers, test_out)):
        n_correct += ans[0] == to
        if ans[0] != to:
            tin = test_lists[i]
            tout = to
    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_in)}}]
    if tin:
        hints.append({'id': 'problematic_test_case', 'args': {'testin': str(tin), 'testout': str(tout)}})
    if n_correct == len(test_in): # add an iteresting hint
        tokens = get_tokens(code)
        if not has_token_sequence(tokens, ['lambda']):
            hints.append({'id': 'final_hint'})
    return n_correct == len(test_in), hints


def hint(python, code, aux_code=''):
    tokens = get_tokens(code)
    # having function max_val is necessary!
    if not has_token_sequence(tokens, ['max_abs']):
        return [{'id' : 'no_func_name', 'args' : {'func_name' : 'max_abs'}}]

    # run one test first to see if there are any exceptions
    answer = python(code=aux_code+code, inputs=[('max_abs([1, 3, 3, -6, 4, 3, 2])', None)], timeout=1.0)
    exc = get_exception_desc(answer[0][3])
    if exc: return exc

    # if has no def, tell him to define the function
    if not has_token_sequence(tokens, ['def']):
        return [{'id' : 'no_def'}]

    # if has no return, tell him to return the value
    if not has_token_sequence(tokens, ['return']):
        return [{'id' : 'no_return'}]

    # if has no loop, tell him to use it
    if not has_token_sequence(tokens, ['while']) and \
       not has_token_sequence(tokens, ['for']):
        return [{'id' : 'for_loop'}]

    # if has no condition if, tell him to use it
    if not has_token_sequence(tokens, ['if']):
        return [{'id' : 'if_clause'}]

    # test for wrong answers
    res = answer[0][0]
    if not isinstance(res, int):
        return [{'id' : 'not_int'}]
    cases = {
        1: [{'id' : 'return_first'}],
        2: [{'id' : 'return_last'}],
        4: [{'id' : 'return_greatest'}],
        6: [{'id' : 'return_positive'}]
    }
    if res in cases:
        return cases[res]

    # if has no return at the beginning of block,
    # tell not to return from for
    if not has_token_sequence(tokens, ['\n', 'return']):
        return [{'id' : 'return_indent'}]

    return None
