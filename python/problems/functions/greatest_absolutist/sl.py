import server
mod = server.problems.load_language('python', 'sl')


name = 'Največji absolutist'
slug = 'Največji absolutist'


description = '''\
<p>Napiši funkcijo <code>max_abs(xs)</code>, ki vrne največje število po
absolutni vrednosti v seznamu <code>xs</code>.</p>
<pre>
>>> max_abs([5, 1, -6, -7, 2])
-7
</pre>
'''

function = ['''\
<p>Napiši definicijo funkcije <code>max_abs(xs)</code>.</p>''',
            '''\
<p>Definicijo funkcije začnemo z <code>def</code>, temu sledi ime, potem oklepaji,
v katerih naštejemo argumente funkcije, nato zaklepaj in na koncu dvopičje</p>''',
            '''\
<pre>
def max_abs(xs):
    # poišči največje absolutno število v xs

s = [5, 1, -6, -7, 2]
print (max_abs(s))
</pre>''']

main_plan = ['''\
<p>Plan: po vrsti pogledamo vse elemente in sproti hranimo največjega
(glede na absolutno vrednost).</p>''',
             '''\
<pre>
najvecji = prvi element od xs
for x in xs:
    če je x večji od najvecji po absolutni vrednosti:
        najvecji postane x
vrni najvecji
</pre>''']

return_clause = ['''\
<p>Namesto, da izpišemo rezultat, ga vračamo s stavkom <code>return</code>.</p>''']

plan = [function,
        main_plan,
        return_clause]

for_loop = ['''\
<p>Preglej elemente z zanko</p>.
''',
             '''\
<pre>
for x in xs:
</pre>''']


if_clause = ['''\
<p>Preveri, če je trenutni element večji od največjega (po absolutni vrednosti)</p>''',
             '''\
<pre>
if abs(x) > abs(najvecji):
    najvecji = x
</pre>''']


hint = {
    'no_def': function,

    'no_return': return_clause,

    'for_loop': for_loop,

    'if_clause': if_clause,

    'not_int': '''\
<p>Funkcija ne vrača števila</p>''',

    'return_first': '''\
<p>Funkcija vrača prvi element v seznamu''',

    'return_last': '''\
<p>Funkcija vrača zadnji element v seznamu.''',

    'return_greatest': '''\
<p>Funkcija vrača največji pozitivni element v seznamu (ne pa največji po absolutni vrednosti).''',

    'return_positive': '''\
<p>Funkcija napačno vrača absolutno vrednost največjega števila.''',

    'return_indent': '''\
<p>Ali imate stavek <code>return</code> znotraj zanke?
V tem primeru se lahko zgodi, da se zanka ne izteče do konca.</p>''',

    'final_hint': '''\
<p>Odlično, naloga rešena! Še zanimivost:
Tudi to nalogo lahko rešimo s funkcijo <code>max</code>:</p>
<pre>
def max_abs(xs):
    return max(xs, key = abs(x))
</pre>''',

}

