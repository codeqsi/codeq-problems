import server
mod = server.problems.load_language('python', 'sl')


id = 231
name = 'Vsota deliteljev'
slug = 'Vsota deliteljev'


description = '''\
<p>
Napiši funkcijo <code>divisors_sum(n)</code>, ki vrne vsoto vseh deliteljev števila, ki ga vnese uporabnik.
</p>'''

plan = ['''\
<p>Ideja je ista kot pri izpisu vseh deliteljev. Tu števil ne
izpisujemo, temveč jih prištevamo vsoti in vsoto na koncu vrnemo.</p>''']

hint = {
    'final_hint': ['''\
<p>Program je pravilen! <br>
</p>
'''],
}
