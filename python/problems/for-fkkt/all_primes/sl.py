import server
mod = server.problems.load_language('python', 'sl')


name = 'Praštevila'
slug = 'Praštevila'


description = '''\
<p>Napišite program, ki izpiše, koliko praštevil je manjših od števila n.
<br>
Primer uporabe:</p>

<pre><code>Vpiši število: <span style="color: rgb(239, 69, 64);">12</span>
5
</code></pre>


<p>Če si bo program za število 50000 vzel preveč časa (beri: se ne bo izvedel dovolj hitro), poskusi optimizirati rešitev. Za še večja števila (npr. 1000000) se pa poskusi poigrati z <a href="https://sl.wikipedia.org/wiki/Eratostenovo_sito">Eratostenovim sitom</a>.</p>
'''


plan = []

hint = {
    'no_input_call': '''<p>Za branje uporabi funkcijo <code>input</code></p>''',

    'printing': ['''\
<p>Izpiši rezultat.</p>'''],

    'final_hint': ['''\
<p>Program deluje pravilno!</p>'''],

}
