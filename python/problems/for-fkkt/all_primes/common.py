import re
from python.util import has_token_sequence, string_almost_equal, \
    string_contains_number, get_tokens, get_numbers, get_exception_desc
from server.hints import Hint

id = 25021
number = 9
visible = True

solution = '''\
n = int(input("Vpiši število: "))
stevec = 0
for i in range(2, n + 1):
    prastevilo = True
    for j in range(2, int(i**(1/2))+1):
        if i % j == 0:
            prastevilo = False
            break
    if prastevilo:
        stevec += 1
print(stevec)

'''

hint_type = {
    'no_input_call' : Hint('no_input_call'),
    'printing': Hint('printing'),
    'final_hint': Hint('final_hint')
}

def test(python, code, aux_code=''):
    tokens = get_tokens(code)

    test_in = [
        (None, '2\n'),
        (None, '3\n'),
        (None, '7\n'),
        (None, '12\n')
    ]
    test_out = [
        '0',
        '1',
        '3',
        '5'
    ]

    # List of outputs: (expression result, stdout, stderr, exception).
    answers = python(code=aux_code+code, inputs=test_in, timeout=1.0)
    outputs = [ans[1] for ans in answers]

    n_correct = 0
    tin = None
    for i, (output, correct) in enumerate(zip(outputs, test_out)):
        if output.rstrip().endswith(correct):
            n_correct += 1
        else:
            tin = test_in[i][1]
            tout = correct

    passed = n_correct == len(test_in)
    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_in)}}]
    if tin != None:
        hints.append({'id': 'problematic_test_case', 'args': {'testin': str(tin), 'testout': str(tout)}})
    else:
        hints.append({'id' : 'final_hint'})
    return passed, hints

def hint(python, code, aux_code=''):
    # run one test first to see if there are any exceptions
    answer = python(code=aux_code+code, inputs=[(None, '2\n')], timeout=1.0)
    exc = get_exception_desc(answer[0][3])
    if exc: return exc

    tokens = get_tokens(code)

    if not has_token_sequence(tokens, ['input']):
        return [{'id': 'no_input_call'}]

    # student is not using print function
    if not has_token_sequence(tokens, ['print']):
        return [{'id' : 'printing'}]

    return None
