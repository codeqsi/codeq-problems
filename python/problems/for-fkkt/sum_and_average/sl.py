import server
mod = server.problems.load_language('python', 'sl')


name = 'Vsota in povprečje'
slug = 'Vsota in povprečje'


description = '''\
<p>Napišite program, ki za podani seznam izračuna vsoto in povprečje elementov.<br>
Seznam števil naj vpiše uporabnik. Za pretvorbo vhodnega niza v seznam uporabite funkcijo <code>eval()</code>.<br>Primer uporabe:</p>

<pre><code>Vpišite seznam števil: <span style="color: rgb(239, 69, 64);">[23, 42, 87, 34, 1, -3, 2]</span>
186
26.571428571428573
</code></pre>
'''


plan = []

hint = {
    'no_input_call': '''<p>Za branje uporabi funkcijo <code>input</code></p>''',

    'no_eval_call': '''<p>Za pretvorbo niza v seznam uporabi funkcijo <code>eval</code></p>''',

    'printing': ['''\
<p>Izpiši rezultat.</p>'''],

    'final_hint': ['''\
<p>Program deluje pravilno!</p>'''],

}
