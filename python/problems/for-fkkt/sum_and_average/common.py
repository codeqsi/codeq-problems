import re
from python.util import has_token_sequence, string_almost_equal, \
    string_contains_number, get_tokens, get_numbers, get_exception_desc
from server.hints import Hint

id = 25014
number = 2
visible = True

solution = '''\
seznam = eval(input("Vpišite seznam števil: "))
vsota = 0
for elt in seznam:
    vsota += elt
print(vsota)
if len(seznam) != 0:
    print(vsota/len(seznam))
else:
    print(0)
'''

hint_type = {
    'no_input_call' : Hint('no_input_call'),
    'no_eval_call' : Hint('no_eval_call'),
    'printing': Hint('printing'),
    'final_hint': Hint('final_hint')
}

def test(python, code, aux_code=''):
    tokens = get_tokens(code)

    test_in = [
        (None, '[]\n'),
        (None, '[-1, -2, -3]\n'),
        (None, '[2, 4, 6, 8, 10]\n'),
        (None, '[23, 42, 87, 34, 1, -3, 2]\n')
    ]
    test_out = [
        '0\n0',
        '-6\n-2.0',
        '30\n6.0',
        '186\n26.571428571428573'
    ]

    # List of outputs: (expression result, stdout, stderr, exception).
    answers = python(code=aux_code+code, inputs=test_in, timeout=1.0)
    outputs = [ans[1] for ans in answers]

    n_correct = 0
    tin = None
    for i, (output, correct) in enumerate(zip(outputs, test_out)):
        if output.rstrip().endswith(correct):
            n_correct += 1
        else:
            tin = test_in[i][1]
            tout = correct

    passed = n_correct == len(test_in)
    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_in)}}]
    if tin != None:
        hints.append({'id': 'problematic_test_case', 'args': {'testin': str(tin), 'testout': str(tout)}})
    else:
        hints.append({'id' : 'final_hint'})
    return passed, hints

def hint(python, code, aux_code=''):
    # run one test first to see if there are any exceptions
    answer = python(code=aux_code+code, inputs=[(None, '[-1, -2, -3]\n')], timeout=1.0)
    exc = get_exception_desc(answer[0][3])
    if exc: return exc

    tokens = get_tokens(code)

    if not has_token_sequence(tokens, ['input']):
        return [{'id': 'no_input_call'}]

    if not has_token_sequence(tokens, ['eval']):
        return [{'id': 'no_eval_call'}]

    # student is not using print function
    if not has_token_sequence(tokens, ['print']):
        return [{'id' : 'printing'}]

    return None
