import server
mod = server.problems.load_language('python', 'sl')


name = 'Praštevilo'
slug = 'Praštevilo'


description = '''\
<p>Napišite program, ki za vnešeno število preveri, ali je praštevilo.
<br>
1. primer uporabe:</p>
<pre><code>Vpiši število: <span style="color: rgb(239, 69, 64);">13</span>
True
</code></pre>

<p>2. primer uporabe:</p>
<pre><code>Vpiši število: <span style="color: rgb(239, 69, 64);">22</span>
False
</code></pre>
'''


plan = []

hint = {
    'no_input_call': '''<p>Za branje uporabi funkcijo <code>input</code></p>''',

    'printing': ['''\
<p>Izpiši rezultat.</p>'''],

    'final_hint': ['''\
<p>Program deluje pravilno!</p>'''],

}
