import re
from python.util import has_token_sequence, string_almost_equal, \
    string_contains_number, get_tokens, get_numbers, get_exception_desc
from server.hints import Hint

id = 25013
number = 1
visible = True

solution = '''\
seznam = [23, 42, 87, 34, 1, -3, 2]
minimum = seznam[0]
for elt in seznam:
    if elt < minimum:
        minimum = elt
print(minimum)
'''

hint_type = {
    'no_xs': Hint('no_xs'),
    'for_loop': Hint('for_loop'),
    'if_clause': Hint('if_clause'),
    'printing': Hint('printing'),
    'no_min': Hint('no_min'),
    'final_hint': Hint('final_hint')
}

def test(python, code, aux_code=''):
    tokens = get_tokens(code)

    if has_token_sequence(tokens, ['min', '(']):
        return False, [{'id': 'no_min'}]

    test_xs = [[5, 4, -7, 2, 12, -3, -4, 11, 2],
               [0],
               [3, 1, 2, 8],
               [1, 2, 3, 42],
               [23, 42, 87, 34, 1, -3, 2]]
    test_out = [
        '-7',
        '0',
        '1',
        '1',
        '-3'
    ]

    n_correct = 0
    tin = None
    for xs_i, xs in enumerate(test_xs):
        # change code to contain new xs instead of the one
        # given by user
        tcode = re.sub(r'^seznam\s*=\s*\[.*?\]',
                       'seznam = ' + str(xs),
                       code,
                       flags = re.DOTALL | re.MULTILINE)

        # use python session to call tcode
        answers = python(code=aux_code+tcode, inputs=[(None, None)], timeout=1.0)
        output = answers[0][1]

        if output.rstrip().endswith(test_out[xs_i]):
            n_correct += 1
        else:
            tin = test_xs[xs_i]
            tout = test_out[xs_i]

    passed = n_correct == len(test_xs)
    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_xs)}}]
    if tin != None:
        hints.append({'id': 'problematic_test_case', 'args': {'testin': str(tin), 'testout': str(tout)}})
    else:
        hints.append({'id' : 'final_hint'})
    return passed, hints

def hint(python, code, aux_code=''):
    # run one test first to see if there are any exceptions
    answer = python(code=aux_code+code, inputs=[(None, None)], timeout=1.0)
    exc = get_exception_desc(answer[0][3])
    if exc: return exc

    tokens = get_tokens(code)

    # if has no xs, tell him to ask for values
    if not has_token_sequence(tokens, ['seznam', '=', '[']):
        return [{'id' : 'no_xs'}]

    # student does not have for: instruct him on loops
    if not has_token_sequence(tokens, ['for']):
        return [{'id' : 'for_loop'}]

    if has_token_sequence(tokens, ['min', '(']):
        return [{'id': 'no_min'}]

    if not has_token_sequence(tokens, ['if']):
        return [{'id' : 'if_clause'}]

    # student is not using print function
    if not has_token_sequence(tokens, ['print']):
        return [{'id' : 'printing'}]

    return None
