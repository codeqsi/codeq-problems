import server
mod = server.problems.load_language('python', 'sl')


name = 'Najmanjše število v seznamu'
slug = 'Najmanjše število v seznamu'


description = '''\
<p>Napišite program, ki poišče in izpiše najmanjši element v seznamu brez uporabe funkcije <code>min()</code>!<br>
Seznam definirajte na vrhu programa <code>seznam = [23, 42, 87, 34, 1, -3, 2]</code><br>
Izpis:</p>

<pre><code>-3</code></pre>

<p>Seveda mora program delovati tudi za vse druge sezname!</p>
'''

if_clause = ['''\
<p>Uporabi pogojni stavek <code>if</code>!</p>''',
             '''\
<pre>
if elt < minimum:
</pre>''']

for_loop = ['''\
<p>Pregledati bo treba vse elemente v seznamu <code>seznam</code>''',
            '''\
<p>Najlažje bo s <code>for</code> zanko.
''',
             '''\
<p>Poskusi naslednji dve vrstici:</p>
<pre>
for elt in seznam:
    print (elt)
</pre>''',
             '''\
<p>V zgornjem primeru z zanko <code>for</code> Pythonu naročimo naj se sprehodi čez seznam <code>seznam</code>
in na vsakem koraku trenutni element seznama shrani v spremenljivko <code>elt</code>.
Kaj naj Python naredi s to spremenljivko, je zapisano v zamaknjenih vrsticah.
Tokrat vrednost le izpišemo.</p>''']


plan = []

hint = {
    'no_xs': ['''\
<p>Program mora imeti na začetku definiran seznam <code>seznam</code>.</p>'''],

    'no_min': '''Funkcija <code>min()</code> ni dovoljena pri tej nalogi!''',

    'for_loop': for_loop,

    'if_clause': if_clause,

    'printing': ['''\
<p>Izpiši rezultat.</p>'''],

    'final_hint': ['''\
<p>Program deluje pravilno!</p>'''],

}
