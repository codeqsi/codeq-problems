import server
mod = server.problems.load_language('python', 'sl')


name = 'Izris smrekice'
slug = 'Izris smrekice'


description = '''\
<p>Napišite program, ki bo namesto trikotnikov izrisoval smrekice.
<br>
Primer uporabe:</p>

<pre><code>Vpiši višino: <span style="color: rgb(239, 69, 64);">4</span>
   *
  ***
 *****
*******
</code></pre>
'''


plan = []

hint = {
    'no_input_call': '''<p>Za branje uporabi funkcijo <code>input</code></p>''',

    'printing': ['''\
<p>Izpiši rezultat.</p>'''],

    'final_hint': ['''\
<p>Program deluje pravilno!</p>'''],

}
