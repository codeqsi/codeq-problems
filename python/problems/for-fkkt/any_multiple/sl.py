import server
mod = server.problems.load_language('python', 'sl')


name = 'Iskanje večkratnikov'
slug = 'Iskanje večkratnikov'


description = '''\
<p>Napišite program, ki preveri, če seznam vsebuje vsaj en večkratnik števila, ki ga vnese uporabnik. Poskusite uporabiti tudi <code>break</code>.<br>
Seznam števil naj vpiše uporabnik. Za pretvorbo vhodnega niza v seznam uporabite funkcijo <code>eval()</code>.<br>
1. primer uporabe:</p>

<pre><code>Vpišite seznam števil: <span style="color: rgb(239, 69, 64);">[23, 42, 87, 34, 1, -3, 2]</span>
Vnesite število: <span style="color: rgb(239, 69, 64);">3</span>
Vsebuje.</code></pre>

<p>2. primer uporabe:</p>
<pre><code>Vpišite seznam števil: <span style="color: rgb(239, 69, 64);">[23, 42, 87, 34, 1, -3, 2]</span>
Vnesite število: <span style="color: rgb(239, 69, 64);">8</span>
Ne vsebuje.
</code></pre>
'''


plan = []

hint = {
    'no_input_call': '''<p>Za branje uporabi funkcijo <code>input</code></p>''',

    'no_eval_call': '''<p>Za pretvorbo niza v seznam uporabi funkcijo <code>eval</code></p>''',

    'printing': ['''\
<p>Izpiši rezultat.</p>'''],

    'final_hint': ['''\
<p>Program deluje pravilno!</p>'''],

}
