import server
mod = server.problems.load_language('python', 'sl')


name = 'Vsota n naravnih števil'
slug = 'Vsota n naravnih števil'


description = '''\
<p>Napišite program, ki izračuna vsoto prvih <code>n</code> naravnih števil. Število <code>n</code> poda uporabnik. Če uporabnik vpiše <code>7</code>, mora program izpisati <code>28</code>, saj je <code>1 + 2 + 3 + 4 + 5 + 6 + 7 = 28</code>.
<br>
Primer uporabe:</p>

<pre><code>Vpiši število: <span style="color: rgb(239, 69, 64);">7</span>
28
</code></pre>
'''


plan = []

hint = {
    'no_input_call': '''<p>Za branje uporabi funkcijo <code>input</code></p>''',

    'printing': ['''\
<p>Izpiši rezultat.</p>'''],

    'final_hint': ['''\
<p>Program deluje pravilno!</p>'''],

}
