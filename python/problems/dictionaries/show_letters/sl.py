import server
mod = server.problems.load_language('python', 'sl')


id = 20602
name = 'Pokaži črke'

description = '''\
<p>
Napiši funkcijo <code>show_letters(word, letter)</code>, ki kot argument sprejme
besedo in množico (<code>set</code>) črk. Funkcija mora vrniti besedo, v
kateri so vse črke, ki ne nastopajo v množici crke, spremenjene v pike. Lahko
predpostaviš, da bodo vse besede sestavljene le iz velikih črk. </p>
<pre>
>>> show_letters("PONUDNIK", set(["O", "N"]))
'.ON..N..'
>>> show_letters("PONUDNIK", set(["O", "I", "K"]))
'.O....IK'
>>> show_letters("PONUDNIK", set())
'........'
>>> show_letters("PONUDNIK", set(["P", "O", "N", "I", "K", "U"]))
'PONU.NIK'
</pre>
'''

plan = []

hint = {
    'final_hint': ['''\
<p>Program je pravilen! <br>
</p>
'''],
}
