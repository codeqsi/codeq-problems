import re
from python.util import has_token_sequence, string_almost_equal, \
    string_contains_number, get_tokens, get_numbers, get_exception_desc
from server.hints import Hint

id = 20602
number = 1
visible = True

solution = '''\
def show_letters(word, letters):
    bes = ""
    for c in word:
        bes += c if c in letters else "."
    return bes
'''

hint_type = {
    'final_hint': Hint('final_hint')
}

def test(python, code, aux_code=''):
    func_name = 'show_letters'
    tokens = get_tokens(code)
    if not has_token_sequence(tokens, ['def', func_name]):
        return False, [{'id' : 'no_func_name', 'args' : {'func_name' : func_name}}]

    in_out = [
        (('PONUDNIK', set(['O', 'N'])), '.ON..N..'),
        (('PONUDNIK', set(['O', 'I', 'K'])), '.O....IK'),
        (('PONUDNIK', set()), '........'),
        (('PONUDNIK', set(['P', 'O', 'N', 'I', 'K', 'U'])), 'PONU.NIK'),
        (('PONUDNIK', set(['P', 'O', 'N', 'I', 'K', 'U', 'D', 'Z'])), 'PONUDNIK'),
        (('PONUDNIK', set(['A', 'B', 'J', 'L'])), '........'),
        (('', set(['A', 'B', 'J', 'L'])), '')
    ]

    test_in = [('{0}{1}'.format(func_name, str(l[0])), None) for l in in_out]
    test_out = [l[1] for l in in_out]

    answers = python(code=aux_code+code, inputs=test_in, timeout=1.0)
    n_correct = 0
    tin, tout = None, None
    for i, (ans, to) in enumerate(zip(answers, test_out)):
        n_correct += ans[0] == to
        if ans[0] != to:
            tin = test_in[i][0]
            tout = to

    passed = n_correct == len(test_in)
    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_in)}}]
    if tin:
        hints.append({'id': 'problematic_test_case', 'args': {'testin': str(tin), 'testout': str(tout)}})
    if passed:
        hints.append({'id': 'final_hint'})
    return passed, hints


def hint(python, code, aux_code=''):
    tokens = get_tokens(code)

    # run one test first to see if there are any exceptions
    answer = python(code=aux_code+code, inputs=[(None, None)], timeout=1.0)
    exc = get_exception_desc(answer[0][3])
    if exc: return exc

    return None
