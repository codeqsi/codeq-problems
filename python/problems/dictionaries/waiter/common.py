import re
from python.util import has_token_sequence, string_almost_equal, \
    string_contains_number, get_tokens, get_numbers, get_exception_desc
from server.hints import Hint

id = 20606
number = 5
visible = True

solution = '''\
import collections
def orders(s):
    cust = collections.defaultdict(list)
    for pers, snack in s:
        if snack[0] == "-":
            if snack[1:] in cust[pers]:
                cust[pers].remove(snack[1:])
        else:
            cust[pers].append(snack)
    return dict(cust)
'''

hint_type = {
    'final_hint': Hint('final_hint')
}

def comp(solution, test):
    if not any(test.values()) and dict(solution) == {}:
        return True
    return dict(solution) == test

def test(python, code, aux_code=''):
    func_name = 'orders'
    tokens = get_tokens(code)
    if not has_token_sequence(tokens, ['def', func_name]):
        return False, [{'id' : 'no_func_name', 'args' : {'func_name' : func_name}}]

    in_out = [
        ([("Tone", "pivo")], {"Tone": ["pivo"]}),
        ([("Tone", "pizza"), ("Tone", "sok")], {"Tone": ["pizza", "sok"]}),
        ([('Ana', 'torta'), ('Berta', 'krof'), ('Cilka', 'kava'),
             ('Ana', 'kava'), ('Berta', '-krof'), ('Cilka', '-torta'),
             ('Berta', 'torta')],
         {'Cilka': ['kava'], 'Berta': ['torta'], 'Ana': ['torta', 'kava']}),
    ]
    in_out_empty = [
        ([('Ana', 'torta'), ('Ana', '-torta')], {'Ana': []}),
        ([('Ana', '-torta')], {'Ana': []})
    ]

    test_in = [('{0}({1})'.format(func_name, str(l[0])), None)
               for l in in_out+in_out_empty]
    test_out = [l[1] for l in in_out+in_out_empty]

    answers = python(code=aux_code+code, inputs=test_in, timeout=1.0)
    n_correct = 0
    tin, tout = None, None
    for i, (ans, to) in enumerate(zip(answers, test_out)):
        n_correct += comp(ans[0], to)
        if not comp(ans[0], to):
            tin = test_in[i][0]
            tout = to

    passed = n_correct == len(test_in)
    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_in)}}]
    if tin:
        hints.append({'id': 'problematic_test_case', 'args': {'testin': str(tin), 'testout': str(tout)}})
    if passed:
        hints.append({'id': 'final_hint'})
    return passed, hints


def hint(python, code, aux_code=''):
    tokens = get_tokens(code)

    # run one test first to see if there are any exceptions
    answer = python(code=aux_code+code, inputs=[(None, None)], timeout=1.0)
    exc = get_exception_desc(answer[0][3])
    if exc: return exc

    return None
