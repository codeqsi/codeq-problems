import server
mod = server.problems.load_language('python', 'sl')


id = 20606
name = 'Natakar'

description = '''\
<p>
Ko je prišel natakar, so naročile:
<ul>
<li>Ana je naročila torto,</li>
<li>Berta je naročila krof,</li>
<li>Cilka je naročila kavo,</li>
<li>Ana je naročila še kavo,</li>
<li>Berta je rekla, da ne bo krofa,</li>
<li>Cilka je rekla, da ne bo torte (no, saj je niti ni naročila;
to natakar mirno ignorira),</li>
<li>Berta je naročila torto.</li></ul>
<p>
Vse skupaj zapišemo takole: <code>[("Ana", "torta"), ("Berta", "krof"),
("Cilka", "kava"), ("Ana", "kava"), ("Berta", "-krof"), ("Cilka", "-torta"),
("Berta", "torta")]</code>. Seznam torej vsebuje pare nizov (oseba, jed),
pri čemer se jed včasih začne z "-", kar pomeni, da stranka prekliče naročilo te
jedi oz. pijače.</p>

<p>Napiši funkcijo <code>orders(s)</code>, ki prejme takšen seznam in vrne
slovar, katerega ključi so imena strank, vrednost pri vsakem ključu pa je
seznam vsega, kar mora stranka na koncu dobiti.</p>
<p>Primer
<pre>
>>> orders([('Ana', 'torta'), ('Berta', 'krof'), ('Cilka', 'kava'), ('Ana', 'kava'),
('Berta', '-krof'), ('Cilka', '-torta'), ('Berta', 'torta')])
{'Cilka': ['kava'], 'Berta': ['torta'], 'Ana': ['torta', 'kava']}
>>> orders([('Ana', 'torta'), ('Ana', '-torta')])
{'Ana': []}
>>> orders([('Ana', '-torta')])
{'Ana': []} # Tu sme funkcija vrniti tudi prazen slovar, {}
</pre>
</p>
'''

plan = []

hint = {
    'final_hint': ['''\
<p>Program je pravilen! <br>
</p>
'''],
}
