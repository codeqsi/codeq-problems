import server
mod = server.problems.load_language('python', 'sl')


id = 20603
name = 'Število znakov'

description = '''\
<p>
Napiši funkcijo <code>maxchars(words)</code>, ki kot argument prejme seznam nizov
in kot rezultat vrne niz, ki ima največ različnih znakov. Male in velike črke
upoštevaj kot iste znake - beseda <code>"MamA"</code> ima samo dva različna znaka.
Če obstaja več besed z enakim številom različnih znakov, naj vrne prvo med njimi.
Če je seznam prazen, naj funkcija vrne <code>None</code>.
</p>
<pre>
>>> besede = ["RABarbara", "izpit", "zmagA"]
>>> maxchars(besede)
'izpit'
'''

plan = []

hint = {
    'final_hint': ['''\
<p>Program je pravilen! <br>
</p>
'''],
}
