import server
mod = server.problems.load_language('python', 'sl')


id = 20609
name = 'Nasledniki (družinsko drevo 4)'

description = '''\
<p>
Napišite funkcijo <code>successors(tree, name)</code>,
ki vrne seznam vseh naslednikov osebe.
</p>

<p>
Družinsko drevo <code>tree</code> je slovar, kjer ključi predstavljajo imena
staršev, vrednosti pa so njihovi otroci. Primer:
<pre>
>>> tree = {'renee': ['rob', 'bob'], 'ken': ['suzan'], 'rob': ['jim'],
            'sid': ['rob', 'bob'], ... , 'bob': ['mary', 'tom', 'judy']}
>>> successors(tree, 'tom')
['ken', 'suzan']
>>> successors(tree, 'sid')
['rob', 'bob', 'jim', 'mary', 'tom', 'judy', 'ken', 'suzan']
</pre>
</p>
'''

plan = []

hint = {
    'final_hint': ['''\
<p>Program je pravilen! <br>
</p>
'''],
}
