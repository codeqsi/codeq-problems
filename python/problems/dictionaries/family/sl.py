import server
mod = server.problems.load_language('python', 'sl')


id = 20601
name = 'Družinsko drevo'

description = '''\
<p>
V seznamu imamo spravljeno družinsko drevo. Primer:
<pre>
family = [('bob', 'mary'), ('bob', 'tom'),
          ('bob', 'judy'), ('alice', 'mary'),
          ('alice', 'tom'), ('alice', 'judy'),
          ('renee', 'rob'), ('renee', 'bob'),
          ('sid', 'rob'), ('sid', 'bob'),
          ('tom', 'ken'), ('ken', 'suzan'),
          ('rob', 'jim')]
</pre>
</p>
<p>
V vsaki terki sta zapisani dve imeni: ime starša in ime otroka. Terka
<code>('bob', 'mary')</code> nam pove, da je Bob Maryjin oče, terka
<code>('bob', 'tom')</code> pa, da je Bob Tomov oče, itd.
Za lažje razumevanje si relacije predstavimo s sliko:
<figure>
  <a href="[%@resource family.png%]" target="_blank">
    <img src="[%@resource family.png%]" />
  </a>
</figure>
</p>
<h3>Naloga</h3>
<p>
Napišite funkcijo <code>family_tree(family)</code>, ki sprejeme seznam
v katerem je spravljeno družinsko drevo in vrne slovar v katerem je za vsakega
starša spravljen seznam vseh njegovih otrok. Vrstni red otrok naj bo
enak vrstnem redu otrok v začetnem seznamu. </p>
<pre>
>>> family_tree(family)
{'renee': ['rob', 'bob'],
'ken': ['suzan'],
'rob': ['jim'],
'sid': ['rob', 'bob'],
... ,
'bob': ['mary', 'tom', 'judy']}
</pre>
</p>'''

plan = []

hint = {
    'final_hint': ['''\
<p>Program je pravilen! <br>
</p>
'''],
}
