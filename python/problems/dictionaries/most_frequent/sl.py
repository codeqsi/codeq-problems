import server
mod = server.problems.load_language('python', 'sl')


id = 20604
name = 'Najpogostejše'

description = '''\
<p>
Napišite funkcijo <code>most_frequent(s)</code>, ki vrne besedo in znak,
ki se v podanem nizu največkrat pojavita. V nizu
<code>'in to in ono in to smo mi'</code> se največkrat pojavita beseda
<code>'in'</code> in znak <code>' '</code> (presledek).
<pre>
>>> most_frequent('aa bb aa')
('aa', 'a')
>>> most_frequent('in to in ono in to smo mi')
('in', ' ')
</pre>
</p>
'''

plan = []

hint = {
    'final_hint': ['''\
<p>Program je pravilen! <br>
</p>
'''],
}
