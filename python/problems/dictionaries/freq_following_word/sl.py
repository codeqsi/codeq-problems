import server
mod = server.problems.load_language('python', 'sl')


id = 20611
name = 'Najpogostejša sledeča beseda'

description = '''\
<p>
Napišite funkcijo <code>freq_following_word(txt)</code>, ki za vsako besedo <code>b</code>
v nizu <code>txt</code> zapiše v slovarju najpogostejšo besedo, ki
sledi besedi <code>b</code>. Če je več takih besed, vzami prvo po abecedi.
Nekaj primerov:
<pre>
>>> freq_following_word('in in in in')
{'in': 'in'}
>>> freq_following_word('in to in ono in to smo mi')
{'smo': 'mi', 'to': 'in' , 'ono': 'in', 'in': 'to']}
</pre>
</p>
'''

plan = []

hint = {
    'final_hint': ['''\
<p>Program je pravilen! <br>
</p>
'''],
}
