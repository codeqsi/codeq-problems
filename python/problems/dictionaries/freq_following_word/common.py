import re
from python.util import has_token_sequence, string_almost_equal, \
    string_contains_number, get_tokens, get_numbers, get_exception_desc
from server.hints import Hint

id = 20611
number = 12
visible = True

solution = '''\
import collections

def following_words(txt):
    words = txt.split()
    freq = collections.defaultdict(list)
    for word, next_word in zip(words, words[1:]):
        freq[word].append(next_word)
    return freq

def freq_following_word(txt):
    following = following_words(txt)
    for f in following:
        vals = collections.Counter(following[f])
        s = sorted(vals.most_common(), key = lambda x: (-x[1], x[0]))
        following[f] = s[0][0]
    return following
'''

hint_type = {
    'final_hint': Hint('final_hint')
}

def test(python, code, aux_code=''):
    func_name = 'freq_following_word'
    tokens = get_tokens(code)
    if not has_token_sequence(tokens, ['def', func_name]):
        return False, [{'id' : 'no_func_name', 'args' : {'func_name' : func_name}}]

    in_out = [
        ('in in in in', {'in': 'in'}),
        ('in to in ono in to smo mi', {'smo': 'mi', 'to': 'in',
                                       'ono': 'in', 'in': 'to'}),
        ('danes je lep dan danes sije sonce',
         {'lep': 'dan', 'je': 'lep', 'dan': 'danes',
          'danes': 'je', 'sije': 'sonce'}),
        ('danes je lep dan danes sije sonce danes sije dan ki je sonce',
         {'lep': 'dan', 'je': 'lep', 'dan': 'danes',
          'danes': 'sije', 'sije': 'dan', 'ki': 'je', 'sonce':'danes'}),
    ]

    test_in = [(func_name+'("%s")'%str(l[0]), None) for l in in_out]
    test_out = [l[1] for l in in_out]

    answers = python(code=aux_code+code, inputs=test_in, timeout=1.0)
    n_correct = 0
    tin, tout = None, None
    for i, (ans, to) in enumerate(zip(answers, test_out)):
        corr = dict(ans[0]) == to
        n_correct += corr
        if not corr:
            tin = test_in[i][0]
            tout = to

    passed = n_correct == len(test_in)
    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_in)}}]
    if tin:
        hints.append({'id': 'problematic_test_case', 'args': {'testin': str(tin), 'testout': str(tout)}})
    if passed:
        hints.append({'id': 'final_hint'})
    return passed, hints


def hint(python, code, aux_code=''):
    tokens = get_tokens(code)

    # run one test first to see if there are any exceptions
    answer = python(code=aux_code+code, inputs=[(None, None)], timeout=1.0)
    exc = get_exception_desc(answer[0][3])
    if exc: return exc

    return None
