import server
mod = server.problems.load_language('python', 'sl')


id = 20610
name = 'Sledeče besede'

description = '''\
<p>
Napišite funkcijo <code>following_words(txt)</code>, ki za vsako besedo <code>b</code>
v nizu <code>txt</code> zgradi seznam besed, ki v tekstu nastopijo za
besedo <code>b</code>. Nekaj primerov:
<pre>
>>> following_words('in in in in')
{'in': ['in', 'in', 'in']}
</pre>
V besedilu se pojavi samo beseda <code>in</code>; beseda <code>in</code> se pojavi
na treh mestih za <code>in</code>.
<pre>
>>> following_words('in to in ono in to smo mi')
{'smo': ['mi'], 'to': ['in', 'smo'], 'ono': ['in'], 'in': ['to', 'ono', 'to']}
</pre>
Za besedo <code>smo</code> se pojavi samo beseda <code>mi</code>, medtem ko se
recimo za besedo <code>to</code> pojavita tako beseda <code>smo</code> kot
tudi beseda <code>in</code>.
</p>
'''

plan = []

hint = {
    'final_hint': ['''\
<p>Program je pravilen! <br>
</p>
'''],
}
