import server
mod = server.problems.load_language('python', 'sl')


id = 20608
name = 'Vnuki (družinsko drevo 3)'

description = '''\
<p>
Napišite funkcijo <code>grandchildren(tree, name)</code>, ki v družinskem drevesu <code>tree</code>
vrne seznam vseh vnukov in vnukinj osebe. V primeru, da oseba nima vnukov, vrnite prazen seznam.
</p>

<p>
Družinsko drevo <code>tree</code> je slovar, kjer ključi predstavljajo imena
staršev, vrednosti pa so njihovi otroci. Primer iz prejšnje naloge:
<pre>
>>> tree = {'renee': ['rob', 'bob'], 'ken': ['suzan'], 'rob': ['jim'],
            'sid': ['rob', 'bob'], ... , 'bob': ['mary', 'tom', 'judy']}
>>> grandchildren(tree, 'renee')
['jim', 'mary', 'tom', 'judy']
</pre>
</p>
'''

plan = []

hint = {
    'final_hint': ['''\
<p>Program je pravilen! <br>
</p>
'''],
}
