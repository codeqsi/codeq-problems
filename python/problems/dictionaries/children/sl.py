import server
mod = server.problems.load_language('python', 'sl')


id = 20607
name = 'Otroci (družinsko drevo 2)'

description = '''\
<p>
Napišite funkcijo <code>children(tree, name)</code>, ki v družinskem drevesu <code>tree</code>
vrne seznam vseh otrok osebe. V primeru, da oseba nima otrok, vrnite prazen seznam.
</p>

<p>
Družinsko drevo <code>tree</code> je slovar, kjer ključi predstavljajo imena
staršev, vrednosti pa so njihovi otroci. Primer iz prejšnje naloge:
<pre>
>>> tree = {'renee': ['rob', 'bob'], 'ken': ['suzan'], 'rob': ['jim'],
            'sid': ['rob', 'bob'], ... , 'bob': ['mary', 'tom', 'judy']}
>>> children(tree, 'alice')
['mary', 'tom', 'judy']
>>> children(tree, 'mary')
[]
</pre>
</p>
'''

plan = []

hint = {
    'final_hint': ['''\
<p>Program je pravilen! <br>
</p>
'''],
}
