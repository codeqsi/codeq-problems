import re
from python.util import has_token_sequence, string_almost_equal, \
    string_contains_number, get_tokens, get_numbers, get_exception_desc
from server.hints import Hint

id = 20605
number = 4
visible = True

solution = '''\
def transactions(start, trans):
    state = dict(start)
    for fr, to, amount in trans:
        state[fr] -= amount
        state[to] += amount
    most = None # could use -1, however all can be in debt
    for who, amount in state.items():
        if most is None or amount > most:
            most, richest = amount, who
    return richest
'''

hint_type = {
    'final_hint': Hint('final_hint')
}

def test(python, code, aux_code=''):
    func_name = 'transactions'
    tokens = get_tokens(code)
    if not has_token_sequence(tokens, ['def', func_name]):
        return False, [{'id' : 'no_func_name', 'args' : {'func_name' : func_name}}]

    in_out = [
        (([("Ana", 0), ("Berta", 1)], [("Berta", "Ana", 1)]), "Ana"),
        (([("Ana", 0), ("Berta", 1)], []), "Berta"),
        (([("Ana", 3), ("Berta", 2)], [("Berta", "Berta", 3)]), "Ana"),
        (([("Ana", 4), ("Berta", 8), ("Cilka", 10)],
          [("Cilka", "Ana", 3), ("Cilka", "Ana", 2), ("Ana", "Berta", 2)]),
         "Berta"),
    ]

    test_in = [('{0}{1}'.format(func_name, str(l[0])), None) for l in in_out]
    test_out = [l[1] for l in in_out]

    answers = python(code=aux_code+code, inputs=test_in, timeout=1.0)
    n_correct = 0
    tin, tout = None, None
    for i, (ans, to) in enumerate(zip(answers, test_out)):
        n_correct += ans[0] == to
        if ans[0] != to:
            tin = test_in[i][0]
            tout = to

    passed = n_correct == len(test_in)
    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_in)}}]
    if tin:
        hints.append({'id': 'problematic_test_case', 'args': {'testin': str(tin), 'testout': str(tout)}})
    if passed:
        hints.append({'id': 'final_hint'})
    return passed, hints


def hint(python, code, aux_code=''):
    tokens = get_tokens(code)

    # run one test first to see if there are any exceptions
    answer = python(code=aux_code+code, inputs=[(None, None)], timeout=1.0)
    exc = get_exception_desc(answer[0][3])
    if exc: return exc

    return None
