import server
mod = server.problems.load_language('python', 'sl')


id = 20605
name = 'Transakcije'

description = '''\
<p>
V začetku je imela Ana štiri zlatnike, Berta 8 in CIlka 10, torej
<code>[('Ana', 4), ('Berta', 8), ('Cilka', 10)]</code>. Nato je dala Cilka
Ani 3 zlatnike; potem je dala Cilka Ani še 2; na koncu je dala Ana Berti 2,
kar zapišemo <code>[('Cilka', 'Ana', 3), ('Cilka', 'Ana', 2), ('Ana', 'Berta', 2)]</code>.
Kdo	ima na koncu največ?
</p>

<p>
Napišite funkcijo <code>transactions(start, tr)</code>, ki dobi gornja seznama in
vrne ime najbogatejše osebe po koncu transakcij. Če je na koncu več enako
bogatih najbogatejših oseb, naj vrne poljubno izmed njih. Namig: delo
si boste poenostavili, če bo funkcija takoj pretvorila sezmam v
primernejšo	podatkovno strukturo.
</p>
<p>
Primer
<pre>
>>> transakcije([('Ana', 4), ('Berta', 8), ('Cilka', 10)], [('Cilka', 'Ana', 3),
('Cilka', 'Ana', 2), ('Ana', 'Berta', 2)])
Berta
</pre>
</p>
'''

plan = []

hint = {
    'final_hint': ['''\
<p>Program je pravilen! <br>
</p>
'''],
}
