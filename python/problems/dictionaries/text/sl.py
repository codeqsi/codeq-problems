import server
mod = server.problems.load_language('python', 'sl')


id = 20612
name = 'Generirano besedilo'

description = '''\
<p>
Napisati želimo program, ki bo generiral tipičen stavek. Seveda ni dobro,
da si samo naključno izbiramo besede in jih lepimo skupaj, saj bi tako dobili
nekaj povsem neberljivega. Naloge se bomo lotili malo pametneje.
Recimo, da ima program na voljo nek tekst, npr. <code>'in to in ono smo mi'</code>,
iz katerega se lahko uči. Naš tekst bomo začeli z izbrano besedo.
Nadaljujemo tako, da se vprašamo katera beseda se v učnem tekstu pojavi
najpogosteje za izbrano besedo. Če začnemo z besedo <code>to</code>, potem
bo naslednja beseda <code>in</code>. Postopek nato ponovimo z besedo <code>in</code>.
</p>

<p>
Napišite funkcijo <code>text(word, full_text, num)</code>, ki sprejme začetno
besedo <code>word</code>, celotno besedilo <code>full_text</code>,
ter generira besedilo dolgo <code>num</code> besed.
</p>

<p> Da bodo generirani stavki bolj zanimivi, lahko program testiraš na
kakšnem romanu, npr. Orwellovi noveli 1984. Vendar pa tega ne boš mogel
izvajati v CodeQ, saj nima dostopa do mreže. Poženi iz kakšnega drugega programa,
npr. iz pyCharma ali kar iz ukazne vrstice.
<pre>
>>> import urllib.request
>>> txt = urllib.request.urlopen('http://squeeb1134.tripod.com/1984.txt').read().decode('utf8')
>>> text('Big', txt, 15)
'Big Brother is not be a few minutes at the Party member of the Party'
</pre>
'''

plan = []

hint = {
    'final_hint': ['''\
<p>Program je pravilen! <br>
</p>
'''],
}
