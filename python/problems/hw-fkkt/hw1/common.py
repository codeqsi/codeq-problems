from python.util import has_token_sequence, string_almost_equal, \
    string_contains_number, get_tokens, get_numbers, get_exception_desc
from server.hints import Hint
import re

id = 25050
number = 1
visible = True

solution = '''\
'''

hint_type = {
    'final_hint': Hint('final_hint'),
}

def test(python, code, aux_code=''):
    # List of inputs: (expression to eval, stdin).
    test_in = [
        (None, '298\n1.4\n0.028964'),
		(None, '0\n1.4\n0.028964'),
		(None, '500\n1.66\n0.03995'),
		(None, '407\n1.33\n0.018'),
    ]
    test_out = [
		'c = 346.07 m/s',
        'c = 0.0 m/s',
		'c = 415.62 m/s',
		'c = 500.04 m/s',
    ]

    # List of outputs: (expression result, stdout, stderr, exception).
    answers = python(code=aux_code+code, inputs=test_in, timeout=1.0)
    outputs = [ans[1] for ans in answers]

    n_correct = 0
    tin = None
    for i, (output, correct) in enumerate(zip(outputs, test_out)):
        if output.rstrip().endswith(correct) and \
				"Vnesite temperaturo [K]:" in output and \
				"Vnesite adiabatni koeficient:" in output and \
				"Vnesite molsko maso [kg/mol]:" in output:
            n_correct += 1
        else:
            tin = test_in[i][1]
            tout = correct

    passed = n_correct == len(test_in)
    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_in)}}]
    if tin:
        hints.append({'id': 'problematic_test_case', 'args': {'testin': str(tin), 'testout': str(tout)}})
    if passed:
        hints.append({'id': 'final_hint'})
    return passed, hints

def hint(python, code, aux_code=''):
	#return [{'id' : 'no_hints'}]
    return None
