import server
mod = server.problems.load_language('python', 'sl')

name = 'Druga domača naloga'
slug = 'Druga domača naloga'

description = '''\
<p>Navodila so objavljena na <a href="https://ucilnica.fri.uni-lj.si/course/view.php?id=239" target="_blank">spletni učilnici</a>.</p>'''


plan = []

hint = {
    'no_hints' : [
            "<p>Domače naloge ne vsebujejo namigov!</p>"
    ],


    'final_hint' : [
            '''\
<p>Odlično, program deluje na vseh testih!</p>
''']
}
