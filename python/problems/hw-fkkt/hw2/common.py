from python.util import has_token_sequence, string_almost_equal, \
    string_contains_number, get_tokens, get_numbers, get_exception_desc
from server.hints import Hint
import re

id = 25051
number = 2
visible = True

solution = '''\
'''

hint_type = {
    'final_hint': Hint('final_hint'),
}

def test(python, code, aux_code=''):
    # List of inputs: (expression to eval, stdin).
    test_in = [
        (None, '50.1'),
		(None, '100'),
		(None, '32\n37.2'),
		(None, '32\n36.9\nverjetno'),
        (None, '32\n36\nne'),
        (None, '32\n36\nda\nmogoče'),
        (None, '32\n36\nda\nda'),
        (None, '32\n36\nda\nne\nne vem'),
        (None, '32\n36\nda\nne\nne'),
        (None, '32\n36\nda\nne\nda'),
    ]
    test_out = [
		'V1',
        'V1',
		'V2',
		'Neveljaven vnos',
        'N1',
		'Neveljaven vnos',
        'V3',
		'Neveljaven vnos',
        'N2',
        'V4',
    ]

    # List of outputs: (expression result, stdout, stderr, exception).
    answers = python(code=aux_code+code, inputs=test_in, timeout=1.0)
    #outputs = [ans[1] for ans in answers]

    n_correct = 0
    tin = None
    error_hint = None
    for i, (answer, correct) in enumerate(zip(answers, test_out)):
        output = answer[1]
        error = answer[3]
        exc = get_exception_desc(error)
        if exc:
            tin = test_in[i][1]
            tout = correct
            error_hint = exc
        elif correct.lower() in output.rstrip().split('\n')[-1].lower():
            n_correct += 1
        else:
            if not error_hint:
                tin = test_in[i][1]
                tout = correct

    passed = n_correct == len(test_in)
    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_in)}}]
    if error_hint:
        hints += error_hint
    if tin:
        hints.append({'id': 'problematic_test_case', 'args': {'testin': str(tin), 'testout': str(tout)}})
    if passed:
        hints.append({'id': 'final_hint'})
    return passed, hints

def hint(python, code, aux_code=''):
	#return [{'id' : 'no_hints'}]
    return None
