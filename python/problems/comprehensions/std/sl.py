import server
mod = server.problems.load_language('python', 'sl')


id = 20704
name = 'Standardni odklon'

description = '''\
<p>
Napišite funkcijo <code>std(xs)</code>, ki izračuna
standardni odklon populacije <code>xs</code>. Formulo za standardno deviacijo je
\[ \sigma = \sqrt{ \\frac{1}{N}\sum_{i=1}^{N} \left(x_i - \mu \\right)^2} \],
Vrednost \( \mu \) je povprečna vrednost populacije.
</p>
<pre>
>>> xs = [183, 168, 175, 176, 192, 180]
>>> std(xs)
7.43863786814
</pre>
'''

plan = []

hint = {
    'final_hint': ['''\
<p>Program je pravilen! <br>
</p>
'''],

    'no_comprehension': ['''\
<p>Program je sicer pravilen, vendar ne vsebuje izpeljanih seznamov ali
generatorjev...
</p>
'''],

    'has_loop': ['''\
<p>Program je sicer pravilen, vendar vsebuje zanko.
</p>
'''],

}
