import server
mod = server.problems.load_language('python', 'sl')


id = 20707
name = 'ISBN'

description = '''\
<p>
Napišite funkcijo <code>valid(isbn)</code>, ki preveri ali je podan ISBN
<a href="http://en.wikipedia.org/wiki/Check_digit#ISBN_10">veljaven</a>.</p>
<pre>
>>> valid('0306406152'), valid('0553382578'), valid('0553293370'), valid('912115628X') # primer veljavnih ...
(True, True, True, True)
>>> valid('03064061522'), valid('1553382578'), valid('91211562811') # ... in neveljavnih ISBN.
(False, False, False)
</pre>
'''

plan = []

hint = {
    'final_hint': ['''\
<p>Program je pravilen! <br>
</p>
'''],

    'no_comprehension': ['''\
<p>Program je sicer pravilen, vendar ne vsebuje izpeljanih seznamov ali
generatorjev...
</p>
'''],

    'has_loop': ['''\
<p>Program je sicer pravilen, vendar vsebuje zanko.
</p>
'''],

}
