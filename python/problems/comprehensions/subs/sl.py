import server
mod = server.problems.load_language('python', 'sl')


id = 20703
name = 'Zamenjava črk'

description = '''\
<p>
Napišite funkcijo <code>subs(s, pos)</code>, ki premeče črke v nizu
<code>s</code> glede na podane nove položaje.
Niz ne bo imel več kot 10 znakov. </p>
<pre>
>>> subs("abc", "0002")
'aaac'
>>> subs("komar", "23401")
'marko'
</pre>
'''

plan = []

hint = {
    'final_hint': ['''\
<p>Program je pravilen! <br>
</p>
'''],

    'no_comprehension': ['''\
<p>Program je sicer pravilen, vendar ne vsebuje izpeljanih seznamov ali
generatorjev...
</p>
'''],

    'has_loop': ['''\
<p>Program je sicer pravilen, vendar vsebuje zanko.
</p>
'''],

}
