import server
mod = server.problems.load_language('python', 'sl')


id = 20701
name = 'Vsota kvadratov'

description = '''\
<p>
Napišite funkcijo <code>sumsquares(n)</code>, ki izračuna in vrne vsoto: </p>
<p>1<sup>2</sup> + 2<sup>2</sup> + ... + n<sup>2</sup> = ?</p>
<p> Nalogo rešite brez zank. </p>
'''

plan = []

hint = {
    'final_hint': ['''\
<p>Program je pravilen! <br>
</p>
'''],

    'no_comprehension': ['''\
<p>Program je sicer pravilen, vendar ne vsebuje izpeljanih seznamov ali
generatorjev...
</p>
'''],

    'has_loop': ['''\
<p>Program je sicer pravilen, vendar vsebuje zanko.
</p>
'''],

}
