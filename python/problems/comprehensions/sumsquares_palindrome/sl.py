import server
mod = server.problems.load_language('python', 'sl')


id = 20702
name = 'Vsota kvadratov palindromskih števil'

description = '''\
<p>
Napišite funkcijo <code>ss_palindrome(n)</code>, ki izračuna in vrne vsoto vseh
palindromskih števil manjših do števila <code>n</code>. Če je <code>n=1000</code>,
potem: </p>
<p>1<sup>2</sup> + 2<sup>2</sup> + ... +
323<sup>2</sup> + 333<sup>2</sup> + 343<sup>2</sup> + ... + 999<sup>2</sup> = ?</p>
'''

plan = []

hint = {
    'final_hint': ['''\
<p>Program je pravilen! <br>
</p>
'''],

    'no_comprehension': ['''\
<p>Program je sicer pravilen, vendar ne vsebuje izpeljanih seznamov ali
generatorjev...
</p>
'''],

    'has_loop': ['''\
<p>Program je sicer pravilen, vendar vsebuje zanko.
</p>
'''],

}
