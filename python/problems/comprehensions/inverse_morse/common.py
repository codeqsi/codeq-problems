import re
from python.util import has_token_sequence, string_almost_equal, \
    string_contains_number, get_tokens, get_numbers, get_exception_desc, \
    all_tokens, has_comprehension, has_loop, almost_equal, get_ast
from server.hints import Hint

id = 20706
number = 6
visible = True

solution = '''\
morse = {
    'A': '.-',
    'B': '-...',
    'C': '-.-.',
    'D': '-..',
    'E': '.',
    'F': '..-.',
    'G': '--.',
    'H': '....',
    'I': '..',
    'J': '.---',
    'K': '-.-',
    'L': '.-..',
    'M': '--',
    'N': '-.',
    'O': '---',
    'P': '.--.',
    'Q': '--.-',
    'R': '.-.',
    'S': '...',
    'T': '-',
    'U': '..-',
    'V': '...-',
    'W': '.--',
    'X': '-..-',
    'Y': '-.--',
    'Z': '--..',
    '1': '.----',
    '2': '..---',
    '3': '...--',
    '4': '....-',
    '5': '.....',
    '6': '-....',
    '7': '--...',
    '8': '---..',
    '9': '----.',
    '0': '-----',
    ' ': ''
}

morse_r = {v: k for k, v in morse.items()}
def morse2txt(s):
    return ''.join(morse_r[c] for c in s.split(' '))
'''

hint_type = {
    'final_hint': Hint('final_hint'),
    'no_comprehension': Hint('no_comprehension'),
    'has_loop': Hint('has_loop')
}

def test(python, code, aux_code=''):
    func_name = 'morse2txt'
    tokens = get_tokens(code)
    ast = get_ast(code)
    if not has_token_sequence(tokens, ['def', func_name]):
        return False, [{'id' : 'no_func_name', 'args' : {'func_name' : func_name}}]

    in_out = [
        ('- .  .-', 'TE A'),
        ('.... . .-.. .-.. ---  .-- --- .-. .-.. -..', 'HELLO WORLD'),
        ('- .... .  --.- ..- .. -.-. -.-  -... .-. --- .-- -.  ..-. --- -..-  .--- ..- -- .--. . -..  --- ...- . .-.  - .... .  .-.. .- --.. -.--  -.. --- --. ...  -... .- -.-. -.-  .---- ..--- ...-- ....- ..... -.... --... ---.. ----. -----',
         'THE QUICK BROWN FOX JUMPED OVER THE LAZY DOGS BACK 1234567890'),
    ]

    test_in = [('{0}("{1}")'.format(func_name, str(l[0])), None)
               for l in in_out]
    test_out = [l[1] for l in in_out]

    answers = python(code=aux_code+code, inputs=test_in, timeout=1.0)
    n_correct = 0
    tin, tout = None, None
    for i, (ans, to) in enumerate(zip(answers, test_out)):
        corr = ans[0] == to
        n_correct += corr
        if not corr:
            tin = test_in[i][0]
            tout = to

    passed = n_correct == len(test_in)
    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_in)}}]
    if tin:
        hints.append({'id': 'problematic_test_case', 'args': {'testin': str(tin), 'testout': str(tout)}})
    if passed:
        if not has_comprehension(ast):
            hints.append({'id': 'no_comprehension'})
        elif has_loop(ast):
            hints.append({'id': 'has_loop'})
        else:
            hints.append({'id': 'final_hint'})

    return passed, hints


def hint(python, code, aux_code=''):
    tokens = get_tokens(code)

    # run one test first to see if there are any exceptions
    answer = python(code=aux_code+code, inputs=[(None, None)], timeout=1.0)
    exc = get_exception_desc(answer[0][3])
    if exc: return exc

    return None
