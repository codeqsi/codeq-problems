import server
mod = server.problems.load_language('python', 'sl')


id = 20706
name = 'Inverzna Morsejeva abeceda'

description = '''\
<p>
Napišite funkcijo <code>morse2txt(code)</code>, ki iz Morsejeve kode razbere
sporočilo. </p>
<pre>
>>> morse2txt('.... . .-.. .-.. ---  .-- --- .-. .-.. -..')
'HELLO WORLD'
</pre>
</p>
'''

plan = []

hint = {
    'final_hint': ['''\
<p>Program je pravilen! <br>
</p>
'''],

    'no_comprehension': ['''\
<p>Program je sicer pravilen, vendar ne vsebuje izpeljanih seznamov ali
generatorjev...
</p>
'''],

    'has_loop': ['''\
<p>Program je sicer pravilen, vendar vsebuje zanko.
</p>
'''],

}
