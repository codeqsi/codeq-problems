import server
mod = server.problems.load_language('python', 'sl')


id = 20705
name = 'Morsejeva abeceda'

description = '''\
<p>
Napišite funkcijo <code>txt2morse</code>, ki pretvori sporočilo v
Morsejevo abecedo. </p>
<p>
Morsejeva abeceda izgleda takole: </p>
<pre>
'A': '.-',
'B': '-...',
'C': '-.-.',
'D': '-..',
'E': '.',
'F': '..-.',
'G': '--.',
'H': '....',
'I': '..',
'J': '.---',
'K': '-.-',
'L': '.-..',
'M': '--',
'N': '-.',
'O': '---',
'P': '.--.',
'Q': '--.-',
'R': '.-.',
'S': '...',
'T': '-',
'U': '..-',
'V': '...-',
'W': '.--',
'X': '-..-',
'Y': '-.--',
'Z': '--..',
'1': '.----',
'2': '..---',
'3': '...--',
'4': '....-',
'5': '.....',
'6': '-....',
'7': '--...',
'8': '---..',
'9': '----.',
'0': '-----',
</pre>

<p>Primer:
<pre>
>>> txt2morse('TE A')
'- .  .-'
>>> txt2morse('HELLO WORLD')
'.... . .-.. .-.. ---  .-- --- .-. .-.. -..'
</pre>
</p>
'''

plan = []

hint = {
    'final_hint': ['''\
<p>Program je pravilen! <br>
</p>
'''],

    'no_comprehension': ['''\
<p>Program je sicer pravilen, vendar ne vsebuje izpeljanih seznamov ali
generatorjev...
</p>
'''],

    'has_loop': ['''\
<p>Program je sicer pravilen, vendar vsebuje zanko.
</p>
'''],

}
