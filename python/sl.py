name = 'Python'
description = '''<a href="#group-9">Skoči na naloge za FKKT.</a>'''

general_msg = {
    'error_head' : '''\
<p>Napaka:</p>
<pre>
[%=message%]
</pre>
''',

    'general_exception': '''\
<p>Glej lokacijo napake (line ...) in za kakšno napako gre (zadnja vrstica).</p>
''',

    'name_error' : '''\
<p>Napaka <code>NameError</code> pomeni, da uporabljaš nedefinirano vrednost:
ali vrednost spremenljivke ni določena ali funkcija ni definirana oz. uvožena.</p>
''',

    'type_error': '''\
<p>TypeError napaka pomeni, da želiš izvesti operacijo na neprimernih tipih.
Npr., sešteti niz in število ali klicati funkcijo, čeprav tisto ni funkcija, itd.</p>
''',

    'eof_error':'''\
<p>Poskušaš prebrati preveč vrednosti.</p>''',

    'timed_out':'''\
<p>Program se izvaja predolgo.</p>''',

    'sandbox_violation': '''\
<p>Program je porabil preveč sistemskih virov ali pa je izvedel nedovoljeno
operacijo.</p>''',

    'modulo':'''\
<p>Ostanek pri deljenju dobimo z operatorjem %.</p>''',

}


hint = {
    'no_hint': ['''\
<p>Namig ne obstaja.</p>
'''],

'program_already_correct': '''\
<p>Tvoj program je pravilen!</p>
''',

    'system_error': ['''\
<p><span style="color: red;">Sistemska napaka:</span> [%=message%].</p>
'''],

    'test_results': ['''\
<p>Program je opravil [%=passed%] / [%=total%] testov.</p>
'''],

    'problematic_test_case': ['''\
<p>Program ne dela pravilno!<br>
Poskusi vnesti: [%=testin%]. <br>
Pravilen rezultat: [%=testout%].</p>
'''],

    'no_func_name': ['''\
<p>Funkcija z imenom <code>[%=func_name%]</code> ni definirana. </p>
'''],

    'syntax_error': ['''\
<p>Program vsebuje sintaktično napako v vrstici [%=lineno%]:</p>
<pre>
[%=message%]
</pre>
'''],

    'name_error' : [general_msg['error_head'],
                    general_msg['general_exception'],
                    general_msg['name_error']],

    'type_error' : [general_msg['error_head'],
                    general_msg['general_exception'],
                    general_msg['type_error']],

    'error' : [general_msg['error_head'], general_msg['general_exception']],

    'eof_error' : [general_msg['eof_error']],

    'timed_out' : [general_msg['timed_out']],

    'sandbox_violation' : [general_msg['sandbox_violation']]
}
