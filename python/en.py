name = 'Python'
description = 'Introductory Python course.'

general_msg = {
    'error_head' : '''\
''',

    'general_exception': '''\
''',

    'name_error' : '''\
''',

    'type_error': '''\
''',

    'eof_error':'''\
''',

    'timed_out':'''\
''',

    'sandbox_violation': '''\
'''
}

hint = {
    'no_hint': '''\
<p>No hint available, sorry!</p>
''',

    'test_results': '''\
<p>Your code passed [%=passed%] / [%=total%] tests.</p>
''',

    'syntax_error': '''\
<p>Your code contains a syntax error in line [%=lineno%]:</p>
<pre>
[%=message%]
</pre>
''',

    'system_error': ['''\
'''],

    'problematic_test_case': ['''\
'''],

    'no_func_name': ['''\
'''],

    'name_error' : [general_msg['error_head'],
                    general_msg['general_exception'],
                    general_msg['name_error']],

    'type_error' : [general_msg['error_head'],
                    general_msg['general_exception'],
                    general_msg['type_error']],

    'error' : [general_msg['error_head'], general_msg['general_exception']],

    'eof_error' : [general_msg['eof_error']],

    'timed_out' : [general_msg['timed_out']],

    'sandbox_violation': [general_msg['sandbox_violation']]
}
