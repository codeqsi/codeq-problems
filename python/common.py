import ast
from server.hints import Hint

id = 2

hint_type = {
    'no_hint': Hint('no_hint'),
    'program_already_correct': Hint('program_already_correct'),
    'system_error': Hint('system_error'),
    'test_results': Hint('test_results'),
    'syntax_error': Hint('syntax_error'),
    'name_error': Hint('name_error'),
    'type_error': Hint('name'),
    'error': Hint('error'),
    'eof_error': Hint('eof_error'),
    'timed_out': Hint('timed_out'),
    'sandbox_violation': Hint('sandbox_violation'),
    'problematic_test_case': Hint('problematic_test_case'),
    'no_func_name': Hint('no_func_name')
}

def hint(python, program, aux_code=''):
    # Check program for syntax errors.
    try:
        tree = ast.parse(program, filename='user')
    except SyntaxError as ex:
        error_msg = '{}{}^\n{}'.format(ex.text, ' '*(ex.offset-1), ex.msg)
        return [{'id': 'syntax_error', 'args': {'lineno': ex.lineno, 'message': error_msg}}]
    return []


def exception(exc_string):
    if exc_string:
        if 'EOFError' in exc_string:
            return [{'id':'eof_error'}]
        if 'timed out' in exc:
            return [{'id':'timed_out'}]
        if 'NameError' in exc:
            return [{'id':'name_error', 'args': {'message': exc}}]
        elif 'TypeError' in exc:
            return [{'id':'type_error', 'args': {'message': exc}}]
        else:
            return [{'id':'error', 'args': {'message': exc}}]
