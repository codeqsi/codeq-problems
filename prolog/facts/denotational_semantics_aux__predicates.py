id = 2

name = 'denotational semantics aux. predicates'

facts = '''\
findblank(R, Bx) :-
  findblank(R, 1, Bx).
findblank([0|_], Bx, Bx) :- !.
findblank([_|T], Cx, Bx) :-
  Cx1 is Cx + 1,
  findblank(T, Cx1, Bx).

swap([H1,H2|T],2,[H2,H1|T]).
swap([H|T],C0,[H|NewT]):-
  C0 > 2,
  C is C0 - 1,
  swap(T,C,NewT).

swap(R, I1, I2, NewR) :-
  (I1 < I2, !,
   Ix1 = I1, Ix2 = I2
   ;
   Ix1 = I2, Ix2 = I1),
  Cut1 is Ix1 - 1,
  Cut2 is Ix2 - Ix1 - 1,
  length(LBef, Cut1),
  length(LMid, Cut2),
  lists:append(LBef, [E1|RRest], R),
  lists:append(LMid, [E2|LAft], RRest),
  lists:append(LBef, [E2|RIntermediate], NewR),
  lists:append(LMid, [E1|LAft], RIntermediate).
'''
