name = 'Prolog'
description = '''\
Introductory Prolog course.
<a target="_blank" href="[%@resource intro_en.html%]"><span class="codeq">CodeQ</span> instructions.</a>
'''

hint = {
    'no_hint': '''\
<p>No hint available, sorry!</p>
''',

    'test_results': '''\
<p>Your code passed [%=passed%] / [%=total%] tests.</p>
''',

    'syntax_error': '''\
<p>Your code contains syntax errors! The Prolog compiler emitted the following messages:</p>
<pre>
[%=messages%]
</pre>
''',

    'monkey_main': '''\
<p>I was able to correct your program. Check out the editor for hints.</p>
''',

    'monkey_change': '''\
<p>Check the highlighted part.</p>
''',

    'monkey_insert': '''\
<p>Add another goal or clause.</p>
''',

    'monkey_remove': '''\
<p>Remove the highlighted part.</p>
''',

    'monkey_highlight': None,  # used to highlight erroneous tokens

    'monkey_buggy_literal': [
'''\
<p>Consider the highlighted values in the following program fragment:</p>
<ul class="code" style="list-style: square inside; padding-left: 2em;">[%=fragments%]</ul>
''',
'''\
<p>Do all values have the correct type (number/list/structure/…)? Do the highlighted arguments appear in correct relations with other objects in the program?</p>
'''
    ],

    'monkey_buggy_variable': [
'''\
<p>The variable <code>[%=variable%]</code> is used incorrectly. Pay particular attention to the following program fragments:</p>
<ul class="code" style="list-style: square inside; padding-left: 2em;">[%=fragments%]</ul>
''',
'''\
<p>Are all goals referencing the variable <code>[%=variable%]</code> correct?
Check whether you are using the right predicate or operator, and that the highlighted arguments make sense.</p>
<p>Do all occurrences of <code>[%=variable%]</code> denote the same value?
In Prolog rules, each variable can only refer to a single value (e.g. person or number).</p>
'''
    ],

    'monkey_singleton': [
'''\
<p>The highlighted variable <code>[%=variable%]</code> appears only once in a rule:</p>
<ul class="code" style="list-style: square inside; padding-left: 2em;">[%=fragments%]</ul>
''',
'''\
<p>This is usually an error. Should the value denoted by the variable <code>[%=variable%]</code> appear anywhere else in the rule? Also check your program for typos.</p>
'''
    ],

    'monkey_missing': '''\
<p>The program is partially correct, but not complete yet. Perhaps a goal is missing in one of the clauses, or another rule is needed.</p>
''',

    'monkey_unknown': [
'''\
<p>Consider the highlighted program fragments:</p>
<ul class="code" style="list-style: square inside; padding-left: 2em;">[%=fragments%]</ul>
''',
'''\
<p>This pattern does not occur in known solutions to this problem. This can indicate a mistake in the program, or that you are implementing a novel solution.</p>
'''
    ],
}
