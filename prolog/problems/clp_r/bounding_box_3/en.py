name = 'bounding_box/3'
slug = 'find the smallest bounding box'

description = '''\
<p><code>bounding_box(Points, X1/Y1, X2/Y2)</code>: <code>X1/Y1</code> and <code>X2/Y2</code> are the bottom-left and top-right points defining the smallest bounding box containing all points in the list <code>Points</code>.
<pre>
?- bounding_box([4.5/2.3, 3.6/1.2, 6.7/0.1], X1/Y1, X2/Y2).
  X1 = 3.6, Y1 = 0.1, X2 = 6.7, Y2 = 2.3.
</pre>'''

hint = {}
