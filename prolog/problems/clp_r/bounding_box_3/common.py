from operator import itemgetter
import socket

import prolog.engine
import prolog.util
from server.hints import Hint, HintPopup

id = 157
number = 50
visible = True
facts = None

solution = '''\
bounding_box([], Xa/Ya, Xb/Yb) :-
    minimize(Xb - Xa),
    minimize(Yb - Ya).
bounding_box([X/Y|L], Xa/Ya, Xb/Yb) :-
    { Xa =< X, X =< Xb,
      Ya =< Y, Y =< Yb },
    bounding_box(L, Xa/Ya, Xb/Yb).'''

test_cases = [
    ('bounding_box([2.1/5.0, -2.1/ -3.2, 1.4/8.9], X1/Y1, X2/Y2), \
      abs(X1 - (-2.1)) =< 1e-8, abs(Y1 - (-3.2)) =< 1e-8, abs(X2 - 2.1) =< 1e-8, abs(Y2 - 8.9) =< 1e-8',
        [{}]),
    ('bounding_box([0.1/0.6], X1/Y1, X2/Y2), \
      abs(X1 - 0.1) =< 1e-8, abs(Y1 - 0.6) =< 1e-8, abs(X2 - 0.1) =< 1e-8, abs(Y2 - 0.6) =< 1e-8',
        [{}]),
    ('bounding_box([7.3/7.3, 3.3/0.0, 0.6/1.2, 4.1/6.9], X1/Y1, X2/Y2), \
      abs(X1 - 0.6) =< 1e-8, abs(Y1 - 0.0) =< 1e-8, abs(X2 - 7.3) =< 1e-8, abs(Y2 - 7.3) =< 1e-8',
        [{}]),
]

def test(code, aux_code):
    n_correct = 0
    engine_id = None
    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)
        if engine_id is not None and 'error' not in map(itemgetter(0), output):
            # Engine successfully created, and no syntax error in program.
            for query, answers in test_cases:
                if prolog.engine.check_answers(engine_id, query=query, answers=answers, timeout=1.0):
                    n_correct += 1
    except socket.timeout:
        pass
    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_cases)}}]
    return n_correct, len(test_cases), hints

