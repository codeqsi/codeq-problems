name = 'megabytes/2'
slug = 'prevedi vrednosti v mebibajtih v megabajte'

description = '''\
<p><em>Mega</em>bajt je SI enota, ki pomeni 10<sup>6</sup> bajtov, medtem, ko je <em>mebi</em>bajt IEC enota, ki pomeni 2<sup>20</sup> bajtov. Napiši predikat <code>megabytes(SI, IEC)</code>, ki prevaja med tema enotama z uporabo omejitev.</p>
<pre>
?- megabytes(2, IEC).
  IEC = 1.9073486328125.
?- megabytes(SI, 2).
  SI = 2.097152.
</pre>'''

hint = {}
