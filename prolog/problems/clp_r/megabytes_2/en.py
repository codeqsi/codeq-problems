name = 'megabytes/2'
slug = 'convert mebibytes to megabytes'

description = '''\
<p>A <em>mega</em>byte is the SI unit meaning 10<sup>6</sup> bytes, while a <em>mebi</em>byte is the IEC unit meaning 2<sup>20</sup> bytes. Write the predicate <code>megabytes(SI, IEC)</code> that converts between the two using constraints.</p>
<pre>
?- megabytes(2, IEC).
  IEC = 1.9073486328125.
?- megabytes(SI, 2).
  SI = 2.097152.
</pre>'''

hint = {}
