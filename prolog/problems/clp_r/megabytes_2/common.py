from operator import itemgetter
import socket

import prolog.engine
import prolog.util
from server.hints import Hint, HintPopup

id = 160
number = 10
visible = True
facts = None

solution = '''\
megabytes(SI, IEC) :-
    { SI * 2^20 = IEC * 10^6 }.'''

test_cases = [
    ('megabytes(12, X), abs(X - 11.444091796875) =< 1e-8',
        [{}]),
    ('megabytes(65, X), abs(X - 61.98883056640625) =< 1e-8',
        [{}]),
    ('megabytes(X, 17), abs(X - 17.825792) =< 1e-8',
        [{}]),
    ('megabytes(X, 47), abs(X - 49.283072) =< 1e-8',
        [{}]),
]

def test(code, aux_code):
    n_correct = 0
    engine_id = None
    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)
        if engine_id is not None and 'error' not in map(itemgetter(0), output):
            # Engine successfully created, and no syntax error in program.
            for query, answers in test_cases:
                if prolog.engine.check_answers(engine_id, query=query, answers=answers, timeout=1.0):
                    n_correct += 1
    except socket.timeout:
        pass
    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_cases)}}]
    return n_correct, len(test_cases), hints

