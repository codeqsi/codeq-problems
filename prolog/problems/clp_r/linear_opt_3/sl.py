name = 'linear_opt/3'
slug = 'linearna optimizacija'

description = '''\
<p>Množica točk v ravnini je podana z naslednjimi neenačbami:</p>
<ul>
  <li>0 ≤ <code>X</code> ≤ 5,</li>
  <li><code>Y</code> ≥ 0,</li>
  <li><code>X</code> + <code>Y</code> ≤ 7,</li>
  <li><code>X</code> + 2*<code>Y</code> ≥ 4 ter</li>
  <li><code>Y</code> ≤ <code>X</code> + 5.</li>
</ul>
<p>Predikat <code>linear_opt(X, Y, MaxE)</code> naj vrne točko (<code>X</code>, <code>Y</code>) v kateri ima izraz <code>E = -0.4*X + 3.2*Y</code> največjo vrednost.</p>'''

hint = {}
