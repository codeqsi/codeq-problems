name = 'linear_opt/3'
slug = 'linear optimization'

description = '''\
<p>A set of points in the plane is defined by the inequalities</p>
<ul>
  <li>0 ≤ <code>X</code> ≤ 5,</li>
  <li><code>Y</code> ≥ 0,</li>
  <li><code>X</code> + <code>Y</code> ≤ 7,</li>
  <li><code>X</code> + 2*<code>Y</code> ≥ 4 and</li>
  <li><code>Y</code> ≤ <code>X</code> + 5.</li>
</ul>
<p>The predicate <code>linear_opt(X, Y, MaxE)</code> should return the point (<code>X</code>, <code>Y</code>) where the expression <code>E = -0.4*X + 3.2*Y</code> has the largest value.</p>'''

hint = {}
