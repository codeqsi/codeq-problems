from operator import itemgetter
import socket

import prolog.engine
import prolog.util
from server.hints import Hint, HintPopup

id = 159
number = 20
visible = True
facts = None

solution = '''\
linear_opt(X, Y, MaxE) :-
    { X >= 0, Y >= 0, X =< 5,
      X+Y =< 7, X+2*Y >= 4, Y =< X+5,
      MaxE = -0.4*X+3.2*Y },
     maximize(MaxE).'''

test_cases = [
    ('linear_opt(X, Y, MaxE), abs(X - 1.0) =< 1e-8, abs(Y - 6.0) =< 1e-8, abs(MaxE - 18.8) =< 1e-8',
        [{}]),
    ('setof(X/Y/MaxE, linear_opt(X, Y, MaxE), L), L = [X/Y/MaxE], abs(X - 1.0) =< 1e-8, abs(Y - 6.0) =< 1e-8, abs(MaxE - 18.8) =< 1e-8',
        [{}]),
]

def test(code, aux_code):
    n_correct = 0
    engine_id = None
    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)
        if engine_id is not None and 'error' not in map(itemgetter(0), output):
            # Engine successfully created, and no syntax error in program.
            for query, answers in test_cases:
                if prolog.engine.check_answers(engine_id, query=query, answers=answers, timeout=1.0):
                    n_correct += 1
    except socket.timeout:
        pass
    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_cases)}}]
    return n_correct, len(test_cases), hints
