name = 'center/3'
slug = 'najdi središča'

description = '''\
<p><code>center(Points, R, X/Y)</code>: <code>X/Y</code> je točka v seznamu točk <code>Points</code>, ki je za največ <code>R</code> oddaljena od vseh ostalih točk v seznamu.</p>
<pre>
?- center([1.0/1.1, 2.0/2.1, 3.0/3.1, 4.0/4.1], 4.0, X/Y).
  X = 2.0, Y = 2.1 ;
  X = 3.0, Y = 3.1.
</pre>'''

hint = {}
