from operator import itemgetter
import socket

import prolog.engine
import prolog.util
from server.hints import Hint, HintPopup

id = 158
number = 60
visible = True
facts = None

solution = '''\
memb158(X, [X|_]).
memb158(X, [_|T]) :-
    memb158(X, T).

check158([], _, _).
check158([X/Y | T], R, Xc/Yc) :-
    { (X-Xc)*(X-Xc) + (Y-Yc)*(Y-Yc) =< R*R },
    check158(T, R, Xc/Yc).

center(L, R, Xc/Yc) :-
    memb158(Xc/Yc, L),
    check158(L, R, Xc/Yc).
'''

test_cases = [
    ('setof(P, center([7.3/7.3, 3.3/0.0, 0.6/1.2, 4.1/6.9], 9.0, P), L), L = [X1/Y1, X2/Y2], \
      abs(X1 - 3.3) =< 1e-8, abs(Y1 - 0.0) =< 1e-8, abs(X2 - 4.1) =< 1e-8, abs(Y2 - 6.9) =< 1e-8',
        [{}]),
    ('setof(P, center([4.7/3.2, 1.0/ -4.4, 0.2/4.1, 0.0/6.5, 2.1/4.1, 2.2/1.5, 5.5/ -2.7], 7.0, P), L), L = [X1/Y1], \
      abs(X1 - 2.2) =< 1e-8, abs(Y1 - 1.5) =< 1e-8',
        [{}]),
    ('setof(P, center([1.0/0.0, 2.0/0.0, 3.0/0.0, 4.0/0.0, 3.5/0.0], 2.3, P), L), L = [X1/Y1, X2/Y2], \
      abs(X1 - 2.0) =< 1e-8, abs(Y1 - 0.0) =< 1e-8, abs(X2 - 3.0) =< 1e-8, abs(Y2 - 0.0) =< 1e-8',
        [{}]),
]

def test(code, aux_code):
    n_correct = 0
    engine_id = None
    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)
        if engine_id is not None and 'error' not in map(itemgetter(0), output):
            # Engine successfully created, and no syntax error in program.
            for query, answers in test_cases:
                if prolog.engine.check_answers(engine_id, query=query, answers=answers, timeout=1.0):
                    n_correct += 1
    except socket.timeout:
        pass
    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_cases)}}]
    return n_correct, len(test_cases), hints

