name = 'center/3'
slug = 'find central points'

description = '''\
<p><code>center(Points, R, X/Y)</code>: <code>X/Y</code> is a point in the list <code>Points</code> that is at most <code>R</code> away from all other points.</p>
<pre>
?- center([1.0/1.1, 2.0/2.1, 3.0/3.1, 4.0/4.1], 4.0, X/Y).
  X = 2.0, Y = 2.1 ;
  X = 3.0, Y = 3.1.
</pre>'''

hint = {}
