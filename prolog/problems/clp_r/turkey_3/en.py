name = 'turkey/3'
slug = 'turkey feed'

description = '''\
<p>The Holiday Meal Turkey Ranch is considering buying two different brands of turkey feed and blending them to provide a good, low-cost diet for its turkeys. Each brand of feed contains, in varying proportions, some or all of the three nutritional ingredients essential for fattening turkeys. Each kilogram of brand 1 contains 5 grams of ingredient <code>A</code>, 4 grams of ingredient <code>B</code> and 0.5 grams of ingredient <code>C</code>. Each kilogram of brand 2 contains 10 grams of ingredient <code>A</code>, 3 grams of ingredient <code>B</code>, but nothing of ingredient <code>C</code>. The brand 1 feed costs 0.20 € a kilogram, while the brand 2 feed costs 0.30 € a kilogram.</p>
<p>The minimum monthly requirement per turkey is: 90 grams of ingredient <code>A</code>; 48 grams of ingredient <code>B</code> and 1.5 grams of ingredient <code>C</code>.</p>
<p>Formulate an LP model to help the rancher decide how to mix the two brands of turkey feed so that the minimum monthly intake requirement for each nutritional ingredient is met at minimum cost. Write the predicate <code>turkey(Brand1, Brand2, Cost)</code> that returns the amount (in kg) of brands 1 and 2 per turkey per month, and the total cost (in €).</p>'''

hint = {}
