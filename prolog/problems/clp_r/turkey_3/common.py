from operator import itemgetter
import socket

import prolog.engine
import prolog.util
from server.hints import Hint, HintPopup

id = 161
number = 40
visible = True
facts = None

solution = '''\
turkey(Brand1, Brand2, Cost) :-
    {Cost = Brand1*0.20 + Brand2*0.30,
      A = Brand1*5 + Brand2*10,
      B = Brand1*4 + Brand2*3,
      C = Brand1*0.5,
      A >= 90, B >= 48, C >= 1.5},
    minimize(Cost).
'''

test_cases = [
    ('turkey(A, B, Cost), abs(A - 8.4) =< 1e-8, abs(B - 4.8) =< 1e-8, abs(Cost - 3.12) =< 1e-8',
        [{}]),
    ('setof(A/B/Cost, turkey(A, B, Cost), L), L = [A/B/Cost], abs(A - 8.4) =< 1e-8, abs(B - 4.8) =< 1e-8, abs(Cost - 3.12) =< 1e-8',
        [{}]),
]

def test(code, aux_code):
    n_correct = 0
    engine_id = None
    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)
        if engine_id is not None and 'error' not in map(itemgetter(0), output):
            # Engine successfully created, and no syntax error in program.
            for query, answers in test_cases:
                if prolog.engine.check_answers(engine_id, query=query, answers=answers, timeout=1.0):
                    n_correct += 1
    except socket.timeout:
        pass
    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_cases)}}]
    return n_correct, len(test_cases), hints

