name = 'turkey/3'
slug = 'puranja hrana'

description = '''\
<p>Podjetje Praznični puran d.o.o. razmišlja o nakupu dveh različnih znamk puranje hrane za kvalitetno in poceni dieto svojih puranov. Vsaka izmed znamk hrane vsebuje, v različnih razmerjih, nekatere (ali tudi vse) od treh nujno potrebnih sestavin hrane za debeljenje puranov. Vsak kilogram prve znamke vsebuje 5 gramov sestavine <code>A</code>, 4 grame sestavine <code>B</code> in 0,5 grama sestavine <code>C</code>. Vsak kilogram druge znamke vsebuje 10 gramov sestavine <code>A</code>, 3 grame sestavine <code>B</code>, a ne vsebuje sestavine <code>C</code>. Cena hrane prve znamke je 0,20 € na kilogram, cena hrane druge znamke pa je 0,30 € na kilogram.</p>
<p>Najmanjša potrebna mesečna količina sestavin na purana je: 90 gramov sestavine <code>A</code>, 48 gramov sestavine <code>B</code> in 1,5 grama sestavine <code>C</code>.</p>
<p>Pripravi linearni model za pomoč pri odločanju o najboljši mešanici obeh znamk hrane, tako da bodo purani dobili zahtevano mesečno količino vseh treh sestavin po najmanjši ceni. Napiši predikat <code>turkey(Brand1, Brand2, Cost)</code>, ki vrne količino (v kg) prve in druge znamke hrane na purana na mesec in skupno ceno (v €).</p>'''

hint = {}
