name = 'max_adj_pair/2'
slug = 'find the maximal pair of adjacent elements'

description = '''\
<p><code>max_adj_pair(List, Max)</code>: <code>Max</code> is the maximal sum of two adjacent elements in <code>List</code>.</p>
<pre>
?- max_adj_pair([4.5, 3.6, 1.2, 6.7], Max).
  Max = 8.1.
?- max_adj_pair([1.1, 1.2, -12.3, 8.8], Max).
  Max = 2.3.
</pre>'''

hint = {}
