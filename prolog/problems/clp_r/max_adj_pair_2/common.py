from operator import itemgetter
import socket

import prolog.engine
import prolog.util
from server.hints import Hint, HintPopup

id = 156
number = 30
visible = True
facts = None

solution = '''\
max_adj_pair([_], S) :-
    minimize(S).
max_adj_pair([A,B|T], S) :-
    { S >= A + B },
    max_adj_pair([B|T], S).'''

test_cases = [
    ('max_adj_pair([4.2, 5.1], X), abs(X - 9.3) =< 1e-8',
        [{}]),
    ('max_adj_pair([4.5, -1.9, 1.3, -2.5, 3.5], X), abs(X - 2.6) =< 1e-8',
        [{}]),
    ('max_adj_pair([7.5, 8.8, 4.2, 6.5, 2.2], X), abs(X - 16.3) =< 1e-8',
        [{}]),
    ('max_adj_pair([-2.8, -2.2, 5.5, -2.2, 8.5, 0.7], X), abs(X - 9.2) =< 1e-8',
        [{}]),
]

def test(code, aux_code):
    n_correct = 0
    engine_id = None
    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)
        if engine_id is not None and 'error' not in map(itemgetter(0), output):
            # Engine successfully created, and no syntax error in program.
            for query, answers in test_cases:
                if prolog.engine.check_answers(engine_id, query=query, answers=answers, timeout=1.0):
                    n_correct += 1
    except socket.timeout:
        pass
    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_cases)}}]
    return n_correct, len(test_cases), hints

