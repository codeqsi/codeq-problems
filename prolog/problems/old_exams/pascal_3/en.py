name = 'pascal/3'
slug = 'pascal’s triangle'

description = '''\
<p>The first five rows of the Pascal's triangle look like this:</p>
<pre>
    1
   1 1
  1 2 1
 1 3 3 1
1 4 6 4 1
</pre>
<p>
Each row begins and ends with 1. Every other element can be obtained as a sum of the two values above it. Write the predicate <code>pascal(I,J,N)</code> that returns the <code>J</code>-th value in the <code>I</code>-th column of the Pascal's triangle. Your solution should return exactly one answer for any input (the <code>I</code> and <code>J</code> arguments start counting with 0; you can assume that 0 ≤ <code>J</code> ≤ <code>I</code>).

<pre>
?- pascal(0, 0, N).
  N = 1.
?- pascal(2, 1, N).
  N = 2.
?- pascal(4, 3, N).
  N = 4.
</pre>'''

hint = {}
