id = 179
number = 86
visible = False
facts = None

solution = '''\
pascal(_, 0, 1) :- !.
pascal(I, I, 1) :- !.
pascal(I, J, N) :-
  I1 is I - 1,
  J1 is J - 1,
  pascal(I1, J, N1),
  pascal(I1, J1, N2),
  N is N1 + N2.
'''
