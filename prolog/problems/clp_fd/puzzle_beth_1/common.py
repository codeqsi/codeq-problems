from operator import itemgetter
import prolog.engine
import server.problems

id = 155
number = 10
visible = True
facts = None

solution = '''\
puzzle_beth(X) :-
  X + 2 #= 2*(X-5).'''

test_cases = [
    ('puzzle_beth(X)',
        [{'X': '12'}]),
    ('setof(A, puzzle_beth(A), X)',
        [{'X': '[12]'}]),
]

def test(code, aux_code):
    n_correct = 0
    engine_id = None
    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)
        if engine_id is not None and 'error' not in map(itemgetter(0), output):
            # Engine successfully created, and no syntax error in program.
            for query, answers in test_cases:
                if prolog.engine.check_answers(engine_id, query=query, answers=answers, timeout=1.0, inference_limit=None):
                    n_correct += 1
    except socket.timeout:
        pass
    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_cases)}}]
    return n_correct, len(test_cases), hints

def hint(code, aux_code):
    # TODO
    return []
