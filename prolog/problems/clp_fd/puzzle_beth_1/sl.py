name = 'puzzle_beth/1'
slug = 'Uganka z leti: Beti'

description = '''\
<p>Ko so Beti vprašali koliko je stara, je odgovorila takole "Čez dve leti bom dvakrat starejša kot sem bila pet let nazaj".</p>
<p>Napiši predikat <code>puzzle_beth(X)</code>, ki izračuna njeno trenutno starost <code>X</code>.</p>
'''

hint = {}
