name = 'puzzle_beth/1'
slug = 'age puzzle: beth'

description = '''\
<p>When asked how old she was, Beth replied "In two years I will be twice as old as I was five years ago".</p>
<p>Write the predicate <code>puzzle_beth(X)</code> that finds her current age as <code>X</code>.</p>
'''

hint = {}
