from operator import itemgetter
import prolog.engine
import server.problems

id = 151
number = 50
visible = True
facts = None

solution = '''\
magic(L):-
  L = [A1,A2,A3,B1,B2,B3,C1,C2,C3],
  L ins 1..9,
  all_different(L),
  A1+A2+A3 #= B1+B2+B3,
  A1+A2+A3 #= C1+C2+C3,
  A1+B1+C1 #= A2+B2+C2,
  A1+B1+C1 #= A3+B3+C3,
  A1+B2+C3 #= A3+B2+C1,
  labeling([], L).'''

test_cases = [
    ('magic(X)',
        [{'X': '[2, 7, 6, 9, 5, 1, 4, 3, 8]'},
         {'X': '[2, 9, 4, 7, 5, 3, 6, 1, 8]'},
         {'X': '[4, 3, 8, 9, 5, 1, 2, 7, 6]'},
         {'X': '[4, 9, 2, 3, 5, 7, 8, 1, 6]'},
         {'X': '[6, 1, 8, 7, 5, 3, 2, 9, 4]'},
         {'X': '[6, 7, 2, 1, 5, 9, 8, 3, 4]'},
         {'X': '[8, 1, 6, 3, 5, 7, 4, 9, 2]'},
         {'X': '[8, 3, 4, 1, 5, 9, 6, 7, 2]'}]),
]

def test(code, aux_code):
    n_correct = 0
    engine_id = None
    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)
        if engine_id is not None and 'error' not in map(itemgetter(0), output):
            # Engine successfully created, and no syntax error in program.
            for query, answers in test_cases:
                if prolog.engine.check_answers(engine_id, query=query, answers=answers, timeout=1.0, inference_limit=None):
                    n_correct += 1
    except socket.timeout:
        pass
    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_cases)}}]
    return n_correct, len(test_cases), hints

def hint(code, aux_code):
    # TODO
    return []
