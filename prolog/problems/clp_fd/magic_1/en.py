name = 'magic/1'
slug = 'generate a 3x3 magic square'

description = '''\
<p><code>magic(S)</code>: the list <code>S</code> represents a 3×3 magic square (<code>S</code> is a permutation of numbers 1 to 9 - three numbers for each row). The sums of numbers in each row, column and diagonal of a magic squre are equal. Implement this predicate using constraints. Your code should return all possible solutions.</p>
<pre>
?- magic(S).
  S = [2, 7, 6, 9, 5, 1, 4, 3, 8] ;
  …
</pre>'''

hint = {}
