name = 'magic/1'
slug = '3x3 magični kvadrat'

description = '''\
<p><code>magic(S)</code>: seznam <code>S</code> predstavlja 3×3 magični kvadrat (<code>S</code> je permutacija števil 1 do 9 -- tri števila tvorijo eno vrstico). Vsote števil v vsaki vrstici, stolpcu in glavni diagonali magičnega kvadrata so enake. Implementiraj ta predikat z uporabo omejitev. Predikat naj vrne vse rešitve, eno po eno.</p>
<pre>
?- magic(S).
  S = [2, 7, 6, 9, 5, 1, 4, 3, 8] ;
  …
</pre>'''

hint = {}
