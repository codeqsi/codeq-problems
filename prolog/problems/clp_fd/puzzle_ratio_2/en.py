name = 'puzzle_ratio/2'
slug = 'age puzzle: ratio'

description = '''\
<p>Present ages of <code>A</code> and <code>B</code> are in the ratio of 5:4. In three years the ratio of their ages will become 11:9.</p>
<p>Write the predicate <code>puzzle_ratio(A, B)</code> that finds the ages of <code>A</code> and <code>B</code>.</p>'''

hint = {}
