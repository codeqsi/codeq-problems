name = 'puzzle_ratio/2'
slug = 'Uganka z leti: razmerje'

description = '''\
<p>Trenutni starosti osebe <code>A</code> in osebe <code>B</code> sta v razmerju 5:4. Čez tri leta bo razmerje njunih let postalo 11:9.</p>
<p>Napiši predikat <code>puzzle_ratio(A, B)</code>, ki izračuna starost osebe <code>A</code> in osebe <code>B</code>.</p>'''

hint = {}
