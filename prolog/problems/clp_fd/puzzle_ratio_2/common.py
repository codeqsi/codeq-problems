from operator import itemgetter
import prolog.engine
import server.problems

id = 154
number = 40
visible = True
facts = None

solution = '''\
puzzle_ratio(A, B) :-
  A #= 5*X,
  B #= 4*X,
  X #> 0,
  A+3 #= 11*Y,
  B+3 #= 9*Y,
  Y #> 0,
  A #< 200,
  B #< 200,
  labeling([], [A, B]).'''

test_cases = [
    ('puzzle_ratio(A, B)',
        [{'A': '30', 'B': '24'}]),
    ('setof(A/B, puzzle_ratio(A, B), X)',
        [{'X': '[30/24]'}]),
]

def test(code, aux_code):
    n_correct = 0
    engine_id = None
    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)
        if engine_id is not None and 'error' not in map(itemgetter(0), output):
            # Engine successfully created, and no syntax error in program.
            for query, answers in test_cases:
                if prolog.engine.check_answers(engine_id, query=query, answers=answers, timeout=1.0, inference_limit=None):
                    n_correct += 1
    except socket.timeout:
        pass
    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_cases)}}]
    return n_correct, len(test_cases), hints

def hint(code, aux_code):
    # TODO
    return []
