from operator import itemgetter
import prolog.engine
import server.problems

id = 150
number = 70
visible = True
facts = None

solution = '''\
tobase(0, _, 0) :- !.
tobase(N, B, Nb) :-
  B in 2..10,
  indomain(B),
  N #= N1 * B + Rem,
  Rem #>= 0, Rem #< B,
  Nb #= Nb1 * 10 + Rem,
  tobase(N1, B, Nb1).'''

test_cases = [
    ('tobase(214, 2, X)',
        [{'X': '11010110'}]),
    ('tobase(321, 7, X)',
        [{'X': '636'}]),
    ('tobase(X, 8, 777)',
        [{'X': '511'}]),
    ('tobase(X, 8, 132)',
        [{'X': '90'}]),
    ('tobase(653, X, 10103)',
        [{'X': '5'}]),
    ('tobase(123, X, 146)',
        [{'X': '9'}]),
    ('setof(B/N, tobase(123, B, N), X)',
        [{'X': '[2/1111011, 3/11120, 4/1323, 5/443, 6/323, 7/234, 8/173, 9/146, 10/123]'}]),
]

def test(code, aux_code):
    n_correct = 0
    engine_id = None
    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)
        if engine_id is not None and 'error' not in map(itemgetter(0), output):
            # Engine successfully created, and no syntax error in program.
            for query, answers in test_cases:
                if prolog.engine.check_answers(engine_id, query=query, answers=answers, timeout=1.0, inference_limit=None):
                    n_correct += 1
    except socket.timeout:
        pass
    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_cases)}}]
    return n_correct, len(test_cases), hints

def hint(code, aux_code):
    # TODO
    return []
