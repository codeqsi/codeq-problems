name = 'tobase/3'
slug = 'Pretvori števila v/iz desetiškega sistema'

description = '''\
<p><code>tobase(Number, B, X)</code>: število <code>Number</code> je v desetiškem sistemu. <code>X</code> predstavlja to število v sistemu z bazo <code>B</code>. Implementiraj predikat z uporabo omejitev. Omeji vrednost <code>B</code> na interval [2..10].</p>
<pre>
?- tobase(42, 2, X).
  X = 101010.
?- tobase(N, 2, 101010).
  N = 42.
?- tobase(42, B, 101010).
  B = 2.
</pre>'''

hint = {}
