name = 'tobase/3'
slug = 'convert numbers from/to the decimal system'

description = '''\
<p><code>tobase(Number, B, X)</code>: given a <code>Number</code> in the decimal system (base 10), <code>X</code> represents the same number in the system with base <code>B</code>. Implement this predicate using constraints. Limit the value of <code>B</code> to the interval [2..10].</p>
<pre>
?- tobase(42, 2, X).
  X = 101010.
?- tobase(N, 2, 101010).
  N = 42.
?- tobase(42, B, 101010).
  B = 2.
</pre>'''

hint = {}
