name = 'puzzle_abc/3'
slug = 'age puzzle: abc'

description = '''\
<p>Person <code>A</code> is two years older than <code>B</code> who is twice as old as <code>C</code>. The total of the ages of A, B and C is 27.</p>
<p>Write the predicate <code>puzzle_abc(A, B, C)</code> that finds the ages of the three people.</p>'''

hint = {}
