name = 'puzzle_abc/3'
slug = 'Uganka z leti: ABC'

description = '''\
<p>Oseba <code>A</code> je dve leti starejša od osebe <code>B</code>, ki je dvakrat starejša od osebe <code>C</code>. Skupaj so osebe A, B in C stare 27 let.</p>
<p>Napiši predikat <code>puzzle_abc(A, B, C)</code>, ki izračuna koliko so stare osebe A, B in C.</p>'''

hint = {}
