name = 'puzzle_momson/2'
slug = 'Uganka z leti: mama in sin'

description = '''\
<p>Mama in sin sta skupaj stara 66 let. Mamina leta so ravno sinova obrnjena leta (obrnjene so cifre). Koliko sta stara?</p>
<p>Napiši predikat <code>puzzle_momson(M, S)</code>, ki izračuna starost mame <code>M</code> in sina <code>S</code>.</p>'''

hint = {}
