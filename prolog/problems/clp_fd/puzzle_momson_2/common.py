from operator import itemgetter
import prolog.engine
import server.problems

id = 152
number = 30
visible = True
facts = None

solution = '''\
puzzle_momson(M, S) :-
  M #= 10*A + B,
  S #= 10*B + A,
  [A,B] ins 1..9,
  M + S #= 66,
  M #> S,
  labeling([], [M, S]).'''

test_cases = [
    ('puzzle_momson(M, S)',
        [{'M': '42', 'S': '24'}, {'M': '51', 'S': '15'}]),
    ('setof(M/S, puzzle_momson(M, S), X)',
        [{'X': '[42/24, 51/15]'}]),
]

def test(code, aux_code):
    n_correct = 0
    engine_id = None
    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)
        if engine_id is not None and 'error' not in map(itemgetter(0), output):
            # Engine successfully created, and no syntax error in program.
            for query, answers in test_cases:
                if prolog.engine.check_answers(engine_id, query=query, answers=answers, timeout=1.0, inference_limit=None):
                    n_correct += 1
    except socket.timeout:
        pass
    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_cases)}}]
    return n_correct, len(test_cases), hints

def hint(code, aux_code):
    # TODO
    return []
