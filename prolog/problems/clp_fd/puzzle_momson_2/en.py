name = 'puzzle_momson/2'
slug = 'age puzzle: mom & son'

description = '''\
<p>The sum of ages of mother and her son is 66. The mother's age is the son's age reversed. How old are they?</p>
<p>Write the predicate <code>puzzle_momson(M, S)</code> that finds the ages of mother <code>M</code> and son <code>S</code>.</p>'''

hint = {}
