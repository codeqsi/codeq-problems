from operator import itemgetter
import prolog.engine
import server.problems

id = 149
number = 60
visible = True
facts = None

solution = '''\
cd(X, Y, CD):-
  X #= _ * CD,
  Y #= _ * CD,
  indomain(CD).

gcd(X, Y, GCD):-
  cd(X, Y, GCD),
  \+ ( cd(X, Y, CD), CD > GCD ).
'''

test_cases = [
    ('gcd(1, 1, X)',
        [{'X': '1'}]),
    ('gcd(12, 8, X)',
        [{'X': '4'}]),
    ('gcd(18, 36, X)',
        [{'X': '18'}]),
    ('gcd(21, 14, X)',
        [{'X': '7'}]),
    ('gcd(10, 22, X)',
        [{'X': '2'}]),
    ('gcd(66, 60, X)',
        [{'X': '6'}]),
]

def test(code, aux_code):
    n_correct = 0
    engine_id = None
    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)
        if engine_id is not None and 'error' not in map(itemgetter(0), output):
            # Engine successfully created, and no syntax error in program.
            for query, answers in test_cases:
                if prolog.engine.check_answers(engine_id, query=query, answers=answers, timeout=1.0, inference_limit=None):
                    n_correct += 1
    except socket.timeout:
        pass
    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_cases)}}]
    return n_correct, len(test_cases), hints

def hint(code, aux_code):
    # TODO
    return []
