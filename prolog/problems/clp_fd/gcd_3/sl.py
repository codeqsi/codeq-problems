name = 'gcd/3'
slug = 'Največji skupni delitelj'

description = '''\
<p><code>gcd(X, Y, GCD)</code>: <code>GCD</code> je največji skupni delitelj števil <code>X</code> in <code>Y</code>. Implementiraj ta predikat z uporabo omejitev.</p>
<p>Namig: poskusi najprej napisati predikat, ki poišče <em>vse</em> skupne delitelje dveh števil.</p>
<pre>
?- gcd(36, 84, GCD).
  GCD = 12.
</pre>'''

hint = {}
