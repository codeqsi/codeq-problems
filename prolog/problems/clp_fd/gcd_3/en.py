name = 'gcd/3'
slug = 'greatest common divisor'

description = '''\
<p><code>gcd(X, Y, GCD)</code>: <code>GCD</code> is the greatest common divisor of <code>X</code> and <code>Y</code>. Implement this predicate using constraints.</p>
<p>Hint: try writing a predicate to find <em>all</em> common divisors of two numbers first.</p>
<pre>
?- gcd(36, 84, GCD).
  GCD = 12.
</pre>'''

hint = {}
