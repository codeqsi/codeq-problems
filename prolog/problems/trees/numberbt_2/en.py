name = 'numberBT/2'
slug = 'find the number of nodes in a binary tree'

description = '''\
<p><code>numberBT(T, N)</code>: <code>N</code> is the number of nodes in the binary tree <code>T</code>.</p>
<pre>
?- numberBT(b(b(nil,2,nil),1,b(nil,3,nil)), N).
  N = 3.
</pre>'''

hint = {}
