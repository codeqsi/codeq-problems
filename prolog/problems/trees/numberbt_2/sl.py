name = 'numberBT/2'
slug = 'preštej število vozlišč v binarnem drevesu'

description = '''\
<p><code>numberBT(T, N)</code>: <code>N</code> je število vozlišč v binarnem drevesu<code>T</code>.</p>
<pre>
?- numberBT(b(b(nil,2,nil),1,b(nil,3,nil)), N).
  N = 3.
</pre>'''

hint = {}
