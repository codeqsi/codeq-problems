from operator import itemgetter
import socket

import prolog.engine
import prolog.util
from server.hints import Hint, HintPopup

id = 142
number = 80
visible = True
facts = None

solution = '''\
memb142(X, [X|_]).
memb142(X, [_|T]) :-
  memb142(X, T).

memberT(X, Tree) :-
  Tree =.. [t, X|_].
memberT(X, Tree) :-
  Tree =.. [t, _|SubTrees],
  memb142(SubTree, SubTrees),
  memberT(X, SubTree).
'''

test_cases = [
    ('setof(M, memberT(M, t(r)), X), X == [r]',
        [{}]),
    ('setof(M, memberT(M, t(3,t(2,t(3),t(6)),t(-1,t(0,t(8)),t(1)))), X), X == [-1,0,1,2,3,6,8]',
        [{}]),
    ('setof(M, memberT(M, t(r,t(a,t(c),t(d)),t(b,t(e),t(f)))), X), X == [a,b,c,d,e,f,r]',
        [{}]),

]

def test(code, aux_code):
    n_correct = 0
    engine_id = None
    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)
        if engine_id is not None and 'error' not in map(itemgetter(0), output):
            # Engine successfully created, and no syntax error in program.
            for query, answers in test_cases:
                if prolog.engine.check_answers(engine_id, query=query, answers=answers, timeout=1.0):
                    n_correct += 1
    except socket.timeout:
        pass
    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_cases)}}]
    return n_correct, len(test_cases), hints

def hint(code, aux_code):
    # TODO
    return []
