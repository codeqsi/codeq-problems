name = 'memberT/2'
slug = 'poišči element v drevesu'

description = '''\
<p><code>memberT(X, T)</code>: <code>X</code> je element drevesa <code>T</code>. Vozlišče drevesa je predstavljeno s strukturo <code>t(E, T1, T2, …)</code>, kjer je <code>E</code> vrednost v vozlišču, kateri sledi poljubno mnogo poddreves. Prazno drevo predstavlja atom <code>nil</code>.</p>
<pre>
?- memberT(X, t(3, t(1), t(2))).
  X = 3 ;
  X = 1 ;
  X = 2.
</pre>'''

hint = {}
