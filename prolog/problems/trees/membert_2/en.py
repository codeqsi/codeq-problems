name = 'memberT/2'
slug = 'find elements in a tree'

description = '''\
<p><code>memberT(X, T)</code>: <code>X</code> is an element of the tree <code>T</code>. A tree node is represented with the structure <code>t(E, T1, T2...)</code>, where <code>E</code> is the node's value, followed by any number of subtrees. An empty tree is denoted by <code>nil</code>.</p>
<pre>
?- memberT(X, t(3, t(1), t(2))).
  X = 3 ;
  X = 1 ;
  X = 2.
</pre>'''

hint = {}
