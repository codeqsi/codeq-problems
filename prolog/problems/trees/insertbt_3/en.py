name = 'insertBT/3'
slug = 'insert an element into a binary tree'

description = '''\
<p><code>insertBT(X, T, NewT)</code>: the binary tree <code>NewT</code> is obtained from <code>T</code> by inserting the element <code>X</code> at a certain position. This is the opposite of the predicate <code>deleteBT/3</code>. Your code should generate all valid solutions.</p>
<pre>
?- insertBT(2, b(nil,1,nil), T).
  T = b(b(nil,1,nil),2,nil) ;
  T = b(nil,2,b(nil,1,nil)) ;
  T = b(b(nil,2,nil),1,nil) ;
  T = b(nil,1,b(nil,2,nil)).
</pre>'''

hint = {}
