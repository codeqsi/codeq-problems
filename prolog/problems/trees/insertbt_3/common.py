from operator import itemgetter
import socket

import prolog.engine
import prolog.util
from server.hints import Hint, HintPopup

id = 138
number = 70
visible = True
facts = None

solution = '''\
deleteBT138(X, b(nil, X, nil), nil).
deleteBT138(X, b(b(Ls, E, Rs), X, R), b(L, E, R)) :-
  deleteBT138(E, b(Ls, E, Rs), L).
deleteBT138(X, b(L, X, b(Ls, E, Rs)), b(L, E, R)) :-
  deleteBT138(E, b(Ls, E, Rs), R).
deleteBT138(X, b(L, E, R), b(L1, E, R)) :-
  deleteBT138(X, L, L1).
deleteBT138(X, b(L, E, R), b(L, E, R1)) :-
  deleteBT138(X, R, R1).

insertBT(X, T, NewT) :-
  deleteBT138(X, NewT, T).
'''

test_cases = [
    ('setof(T, insertBT(q,nil,T), X), X == [b(nil,q,nil)]',
        [{}]),
    ('''setof(T, insertBT(q, b(b(nil,4,nil),2,nil), T), X),
                  X == [b(b(nil,2,b(nil,4,nil)),q,nil), b(b(nil,4,nil),2,b(nil,q,nil)),
                        b(b(nil,4,nil),q,b(nil,2,nil)), b(b(nil,4,b(nil,q,nil)),2,nil),
                        b(b(nil,q,b(nil,4,nil)),2,nil), b(b(b(nil,4,nil),2,nil),q,nil),
                        b(b(b(nil,4,nil),q,nil),2,nil), b(b(b(nil,q,nil),4,nil),2,nil)]''',
        [{}]),
    ('''setof(T, insertBT(9, b(b(nil,2,b(nil,6,nil)),1,b(nil,3,nil)), T), X),
                  X == [b(b(nil,1,b(nil,2,b(nil,6,nil))),9,b(nil,3,nil)),
                        b(b(nil,1,b(b(nil,6,nil),2,nil)),9,b(nil,3,nil)),
                        b(b(nil,2,b(nil,6,nil)),1,b(nil,3,b(nil,9,nil))),
                        b(b(nil,2,b(nil,6,nil)),1,b(nil,9,b(nil,3,nil))),
                        b(b(nil,2,b(nil,6,nil)),1,b(b(nil,3,nil),9,nil)),
                        b(b(nil,2,b(nil,6,nil)),1,b(b(nil,9,nil),3,nil)),
                        b(b(nil,2,b(nil,6,nil)),9,b(nil,1,b(nil,3,nil))),
                        b(b(nil,2,b(nil,6,nil)),9,b(b(nil,3,nil),1,nil)),
                        b(b(nil,2,b(nil,6,b(nil,9,nil))),1,b(nil,3,nil)),
                        b(b(nil,2,b(nil,9,b(nil,6,nil))),1,b(nil,3,nil)),
                        b(b(nil,2,b(b(nil,6,nil),9,nil)),1,b(nil,3,nil)),
                        b(b(nil,2,b(b(nil,9,nil),6,nil)),1,b(nil,3,nil)),
                        b(b(nil,9,b(nil,2,b(nil,6,nil))),1,b(nil,3,nil)),
                        b(b(nil,9,b(b(nil,6,nil),2,nil)),1,b(nil,3,nil)),
                        b(b(b(nil,2,nil),1,b(nil,6,nil)),9,b(nil,3,nil)),
                        b(b(b(nil,2,nil),9,b(nil,6,nil)),1,b(nil,3,nil)),
                        b(b(b(nil,9,nil),2,b(nil,6,nil)),1,b(nil,3,nil))]''',
        [{}]),
]

def test(code, aux_code):
    n_correct = 0
    engine_id = None
    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)
        if engine_id is not None and 'error' not in map(itemgetter(0), output):
            # Engine successfully created, and no syntax error in program.
            for query, answers in test_cases:
                if prolog.engine.check_answers(engine_id, query=query, answers=answers, timeout=1.0):
                    n_correct += 1
    except socket.timeout:
        pass
    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_cases)}}]
    return n_correct, len(test_cases), hints

def hint(code, aux_code):
    # TODO
    return []
