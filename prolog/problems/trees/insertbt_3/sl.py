name = 'insertBT/3'
slug = 'vstavi element v binarno drevo'

description = '''\
<p><code>insertBT(X, T, NewT)</code>: binarno drevo <code>NewT</code> dobimo tako, da v <code>T</code> vstavimo element <code>X</code> na poljubno mesto. Tvoj program naj vrača vse možne rešitve.</p>
<pre>
?- insertBT(2, b(nil,1,nil), T).
  T = b(b(nil,1,nil),2,nil) ;
  T = b(nil,2,b(nil,1,nil)) ;
  T = b(b(nil,2,nil),1,nil) ;
  T = b(nil,1,b(nil,2,nil)).
</pre>'''

hint = {}
