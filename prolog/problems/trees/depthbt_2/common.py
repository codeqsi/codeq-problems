from operator import itemgetter
import socket

import prolog.engine
import prolog.util
from server.hints import Hint, HintPopup

id = 140
number = 40
visible = True
facts = None

solution = '''\
depthBT(nil, 0).
depthBT(b(L, _, R), D) :-
  depthBT(L, DL),
  depthBT(R, DR),
  (DL >= DR,
   D is DL + 1
   ;
   DL < DR,
   D is DR + 1).
'''

test_cases = [
	('depthBT(nil, X), X == 0',
            [{}]),
	('depthBT(b(b(b(nil,4,nil),2,b(nil,6,nil)),1,b(nil,3,b(nil,5,nil))), X), X == 3',
            [{}]),
	('depthBT(b(b(b(b(nil,8,nil),7,nil),3,b(nil,2,nil)),8,b(b(nil,4,nil),8,b(nil,1,nil))), X), X == 4',
            [{}]),
	('depthBT(b(b(b(b(nil,a,nil),b,nil),c,nil),d,nil), X), X == 4',
            [{}]),
	('depthBT(b(nil,d,b(nil,c,b(nil,e,b(nil,a,nil)))), X), X == 4',
            [{}]),
]

def test(code, aux_code):
    n_correct = 0
    engine_id = None
    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)
        if engine_id is not None and 'error' not in map(itemgetter(0), output):
            # Engine successfully created, and no syntax error in program.
            for query, answers in test_cases:
                if prolog.engine.check_answers(engine_id, query=query, answers=answers, timeout=1.0):
                    n_correct += 1
    except socket.timeout:
        pass
    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_cases)}}]
    return n_correct, len(test_cases), hints

def hint(code, aux_code):
    # TODO
    return []
