name = 'depthBT/2'
slug = 'poišči globino binarnega drevesa'

description = '''\
<p><code>depthBT(T, D)</code>: <code>D</code> je globina binarnega drevesa <code>T</code>.</p>
<pre>
?- depthBT(b(b(b(nil,4,nil),2,b(nil,6,nil)),1,nil), D).
  D = 3.
</pre>'''

hint = {}
