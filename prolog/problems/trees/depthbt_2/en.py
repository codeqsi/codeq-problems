name = 'depthBT/2'
slug = 'find the depth of a binary tree'

description = '''\
<p><code>depthBT(T, D)</code>: <code>D</code> is the depth of the binary tree <code>T</code>.</p>
<pre>
?- depthBT(b(b(b(nil,4,nil),2,b(nil,6,nil)),1,nil), D).
  D = 3.
</pre>'''

hint = {}
