name = 'deleteBT/3'
slug = 'delete an element from a binary tree'

description = '''\
<p><code>deleteBT(X, T, NewT)</code>: the binary tree <code>NewT</code> is obtained from <code>T</code> by deleting one occurence of the element <code>X</code>. If <code>X</code> is not a leaf node, the root of one of its subtrees is moved up. Your code should generate all valid solutions.</p>
<pre>
?- deleteBT(1, b(b(b(nil,4,nil),2,b(nil,6,nil)),1,b(nil,3,b(nil,5,nil))), T).
  T = b(b(nil,4,b(nil,6,nil)),2,b(nil,3,b(nil,5,nil))) ;
  T = b(b(b(nil,4,nil),6,nil),2,b(nil,3,b(nil,5,nil))) ;
  T = b(b(b(nil,4,nil),2,b(nil,6,nil)),3,b(nil,5,nil)).
</pre>'''

hint = {}
