name = 'deleteBT/3'
slug = 'izbriši element iz binarnega drevesa'

description = '''\
<p><code>deleteBT(X, T, NewT)</code>: binarno drevo <code>NewT</code> dobimo iz <code>T</code> tako, da izbrišemo eno pojavitev elementa <code>X</code>. Če <code>X</code> ni v listu, ga zamenjamo s korenom levega ali desnega poddrevesa. Program naj vrača vse veljavne rešitve.</p>
<pre>
?- deleteBT(1, b(b(b(nil,4,nil),2,b(nil,6,nil)),1,b(nil,3,b(nil,5,nil))), T).
  T = b(b(nil,4,b(nil,6,nil)),2,b(nil,3,b(nil,5,nil))) ;
  T = b(b(b(nil,4,nil),6,nil),2,b(nil,3,b(nil,5,nil))) ;
  T = b(b(b(nil,4,nil),2,b(nil,6,nil)),3,b(nil,5,nil)).
</pre>'''

hint = {}
