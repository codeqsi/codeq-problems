from operator import itemgetter
import socket

import prolog.engine
import prolog.util
from server.hints import Hint, HintPopup

id = 137
number = 60
visible = True
facts = None

solution = '''\
deleteBT(X, b(nil, X, nil), nil).
deleteBT(X, b(b(Ls, E, Rs), X, R), b(L, E, R)) :-
  deleteBT(E, b(Ls, E, Rs), L).
deleteBT(X, b(L, X, b(Ls, E, Rs)), b(L, E, R)) :-
  deleteBT(E, b(Ls, E, Rs), R).
deleteBT(X, b(L, E, R), b(L1, E, R)) :-
  deleteBT(X, L, L1).
deleteBT(X, b(L, E, R), b(L, E, R1)) :-
  deleteBT(X, R, R1).
'''

test_cases = [
    ('deleteBT(q, b(nil,q,nil), X), X == nil',
        [{}]),
    ('''setof(T, deleteBT(2, b(b(b(nil,4,nil),2,b(nil,6,nil)),1,b(nil,3,b(nil,5,nil))), T), X),
            X == [b(b(nil,4,b(nil,6,nil)),1,b(nil,3,b(nil,5,nil))),b(b(b(nil,4,nil),6,nil),1,b(nil,3,b(nil,5,nil)))]''',
        [{}]),
    ('''setof(T, deleteBT(8, b(b(b(b(nil,8,nil),7,nil),3,b(nil,2,nil)),8,b(b(nil,4,nil),8,b(nil,1,nil))), T), X),
            X == [b(b(b(nil,7,nil),3,b(nil,2,nil)),8,b(b(nil,4,nil),8,b(nil,1,nil))),
                  b(b(b(nil,8,nil),7,b(nil,2,nil)),3,b(b(nil,4,nil),8,b(nil,1,nil))),
                  b(b(b(b(nil,8,nil),7,nil),2,nil),3,b(b(nil,4,nil),8,b(nil,1,nil))),
                  b(b(b(b(nil,8,nil),7,nil),3,b(nil,2,nil)),8,b(nil,4,b(nil,1,nil))),
                  b(b(b(b(nil,8,nil),7,nil),3,b(nil,2,nil)),8,b(b(nil,4,nil),1,nil))]''',
        [{}]),
    ('''setof(T, deleteBT(2, b(b(b(b(nil,8,nil),7,nil),3,b(nil,2,nil)),8,b(b(nil,4,nil),8,b(nil,1,nil))), T), X),
            X == [b(b(b(b(nil,8,nil),7,nil),3,nil),8,b(b(nil,4,nil),8,b(nil,1,nil)))]''',
        [{}]),
]

def test(code, aux_code):
    n_correct = 0
    engine_id = None
    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)
        if engine_id is not None and 'error' not in map(itemgetter(0), output):
            # Engine successfully created, and no syntax error in program.
            for query, answers in test_cases:
                if prolog.engine.check_answers(engine_id, query=query, answers=answers, timeout=1.0):
                    n_correct += 1
    except socket.timeout:
        pass
    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_cases)}}]
    return n_correct, len(test_cases), hints

def hint(code, aux_code):
    # TODO
    return []
