from operator import itemgetter
import socket

import prolog.engine
import prolog.util
from server.hints import Hint, HintPopup

id = 135
number = 10
visible = True
facts = None

solution = '''\
memberBT(X, b(_, X, _)).
memberBT(X, b(L, _, _)) :-
  memberBT(X, L).
memberBT(X, b(_, _, R)) :-
  memberBT(X, R).
'''

test_cases = [
    ('memberBT(M, b(nil,r,nil))',
        [{'M': 'r'}]),
    ('memberBT(M, b(b(b(nil,4,nil),2,b(nil,6,nil)),1,b(nil,3,b(nil,5,nil))))',
        [{'M': '1'}, {'M': '2'}, {'M': '3'}, {'M': '4'}, {'M': '5'},
         {'M': '6'}]),
    ('memberBT(M, b(b(b(b(nil,8,nil),7,nil),3,b(nil,2,nil)),8,b(b(nil,4,nil),8,b(nil,1,nil))))',
        [{'M': '1'}, {'M': '2'}, {'M': '3'}, {'M': '4'}, {'M': '7'},
         {'M': '8'}]),
]

def test(code, aux_code):
    n_correct = 0
    engine_id = None
    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)
        if engine_id is not None and 'error' not in map(itemgetter(0), output):
            # Engine successfully created, and no syntax error in program.
            for query, answers in test_cases:
                if prolog.engine.check_answers(engine_id, query=query, answers=answers, timeout=1.0):
                    n_correct += 1
    except socket.timeout:
        pass
    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_cases)}}]
    return n_correct, len(test_cases), hints

def hint(code, aux_code):
    # TODO
    return []
