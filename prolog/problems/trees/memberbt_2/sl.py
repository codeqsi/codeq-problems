name = 'memberBT/2'
slug = 'poišči element v binarnem drevesu'

description = '''\
<p><code>memberBT(X, T)</code>: <code>X</code> je element binarnega drevesa <code>T</code>. Vozlišče v binarnem drevesu je predstavljeno s strukturo <code>b(L, E, R)</code>, kjer sta <code>L</code> in <code>R</code> levo in desno poddrevo, <code>E</code> pa je vrednost v tem vozlišču. Prazno drevo predstavlja atom <code>nil</code>.</p>
<pre>
?- memberBT(X, b(b(nil,2,nil),1,b(nil,3,nil))).
  X = 1 ;
  X = 2 ;
  X = 3.
</pre>'''

hint = {}
