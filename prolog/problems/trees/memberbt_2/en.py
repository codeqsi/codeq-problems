name = 'memberBT/2'
slug = 'find elements in a binary tree'

description = '''\
<p><code>memberBT(X, T)</code>: <code>X</code> is an element of the binary tree <code>T</code>. A binary tree node is represented with the structure <code>b(L, E, R)</code>, where <code>L</code> and <code>R</code> are left and right subtrees, respectively, and <code>E</code> is the node's value. An empty tree is denoted by <code>nil</code>.</p>
<pre>
?- memberBT(X, b(b(nil,2,nil),1,b(nil,3,nil))).
  X = 1 ;
  X = 2 ;
  X = 3.
</pre>'''

hint = {}
