name = 'tolistBT/2'
slug = 'vrni seznam elementov binarnega drevesa'

description = '''\
<p><code>tolistBT(T, L)</code>: seznam <code>L</code> vsebuje vse elemente binarnega drevesa <code>T</code> v infiksnem vrstnem redu.</p>
<pre>
?- tolistBT(b(b(nil,2,nil),1,b(nil,3,nil)), L).
  L = [2,1,3].
</pre>'''

hint = {}
