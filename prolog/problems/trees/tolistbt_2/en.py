name = 'tolistBT/2'
slug = 'construct a list with all elements of a binary tree'

description = '''\
<p><code>tolistBT(T, L)</code>: the list <code>L</code> contains all the elements in the binary tree <code>T</code>, in infix order.</p>
<pre>
?- tolistBT(b(b(nil,2,nil),1,b(nil,3,nil)), L).
  L = [2,1,3].
</pre>'''

hint = {}
