from operator import itemgetter
import socket

import prolog.engine
import prolog.util
from server.hints import Hint, HintPopup

id = 136
number = 20
visible = True
facts = None

solution = '''\
mirrorBT(nil, nil).
mirrorBT(b(L, X, R), b(NewR, X, NewL)) :-
  mirrorBT(L, NewL),
  mirrorBT(R, NewR).
'''

test_cases = [
    ('mirrorBT(nil, X)',
        [{'X': 'nil'}]),
    ('\+ mirrorBT(42, 42)',
        [{}]),
    ('mirrorBT(b(nil,q,nil), X), X == b(nil,q,nil)',
        [{}]),
    ('mirrorBT(b(b(nil,a,nil),d,b(nil,c,nil)), X), X == b(b(nil,c,nil),d,b(nil,a,nil))',
        [{}]),
    ('''mirrorBT(b(b(nil,2,nil),1,b(nil,3,b(nil,4,nil))),X),
            X == b(b(b(nil,4,nil),3,nil),1,b(nil,2,nil))''',
        [{}]),
    ('''mirrorBT(b(b(b(nil,4,nil),2,b(nil,6,nil)),1,b(nil,3,b(nil,5,nil))),X),
            X == b(b(b(nil,5,nil),3,nil),1,b(b(nil,6,nil),2,b(nil,4,nil)))''',
        [{}]),
]

def test(code, aux_code):
    n_correct = 0
    engine_id = None
    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)
        if engine_id is not None and 'error' not in map(itemgetter(0), output):
            # Engine successfully created, and no syntax error in program.
            for query, answers in test_cases:
                if prolog.engine.check_answers(engine_id, query=query, answers=answers, timeout=1.0):
                    n_correct += 1
    except socket.timeout:
        pass
    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_cases)}}]
    return n_correct, len(test_cases), hints

def hint(code, aux_code):
    # TODO
    return []
