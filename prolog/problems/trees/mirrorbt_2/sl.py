name = 'mirrorBT/2'
slug = 'prezrcali binarno drevo'

description = '''\
<p><code>mirrorBT(T, NewT)</code>: binarno drevo <code>NewT</code> dobimo tako, da <code>T</code> prezrcalimo čez vertikalo skozi koren.</p>
<pre>
?- mirrorBT(b(b(b(nil,4,nil),2,b(nil,5,nil)),1,b(nil,3,nil)), X).
  X = b(b(nil,3,nil), 1, b(b(nil,5,nil), 2, b(nil,4,nil))).
</pre>'''

hint = {}
