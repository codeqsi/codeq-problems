name = 'mirrorBT/2'
slug = 'flip a binary tree horizontally'

description = '''\
<p><code>mirrorBT(T, NewT)</code>: the binary tree <code>NewT</code> is obtained from <code>T</code> by flipping it over the vertical axis through the root node.</p>
<pre>
?- mirrorBT(b(b(b(nil,4,nil),2,b(nil,5,nil)),1,b(nil,3,nil)), X).
  X = b(b(nil,3,nil), 1, b(b(nil,5,nil), 2, b(nil,4,nil))).
</pre>'''

hint = {}
