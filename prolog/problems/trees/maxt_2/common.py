from operator import itemgetter
import socket

import prolog.engine
import prolog.util
from server.hints import Hint, HintPopup

id = 143
number = 90
visible = True
facts = None

solution = '''\
max143([X], X).
max143([H|T], Max):-
  max143(T, Max1),
  ( H > Max1, Max is H
    ;
    H =< Max1, Max is Max1 ).

maxT(t(E), E) :- !.
maxT(Tree, Max) :-
  Tree =.. [t, E|SubTrees],
  getMaxElems(SubTrees, MaxElems),
  max143([E|MaxElems], Max).

getMaxElems([], []).
getMaxElems([SubTree|SubTrees], [MaxElem|MaxElems]) :-
  maxT(SubTree, MaxElem),
  getMaxElems(SubTrees, MaxElems).
'''

test_cases = [
    ('maxT(t(666), X), X == 666',
        [{}]),
    ('setof(M, maxT(t(3,t(2,t(3),t(6)),t(-1,t(0,t(8)),t(1))), M), X), X == [8]',
        [{}]),
    ('setof(M, maxT(t(-3,t(-2,t(-3),t(-6)),t(-11,t(-1,t(-8)),t(-10))), M), X), X == [-1]',
        [{}]),
]

def test(code, aux_code):
    n_correct = 0
    engine_id = None
    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)
        if engine_id is not None and 'error' not in map(itemgetter(0), output):
            # Engine successfully created, and no syntax error in program.
            for query, answers in test_cases:
                if prolog.engine.check_answers(engine_id, query=query, answers=answers, timeout=1.0):
                    n_correct += 1
    except socket.timeout:
        pass
    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_cases)}}]
    return n_correct, len(test_cases), hints

def hint(code, aux_code):
    # TODO
    return []
