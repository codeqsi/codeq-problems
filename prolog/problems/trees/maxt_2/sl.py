name = 'maxT/2'
slug = 'poišči največji element v drevesu'

description = '''\
<p><code>maxT(Tree, Max)</code>: <code>Max</code> je največji element v drevesu <code>Tree</code>.</p>
<pre>
?- maxT(t(1, t(2), t(3)), Max).
  Max = 3.
</pre>'''

hint = {}
