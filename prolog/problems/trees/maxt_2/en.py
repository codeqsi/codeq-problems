name = 'maxT/2'
slug = 'find the greatest element in a tree'

description = '''\
<p><code>maxT(Tree, Max)</code>: <code>Max</code> is the greatest element in the tree <code>Tree</code>.</p>
<pre>
?- maxT(t(1, t(2), t(3)), Max).
  Max = 3.
</pre>'''

hint = {}
