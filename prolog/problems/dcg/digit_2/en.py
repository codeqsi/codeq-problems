name = 'digit/2'
slug = 'a decimal digit'

description = '''\
<p>Write a DCG with the starting symbol <code>digit</code> for the language defined by the set of words {0, 1, 2, 3, 4, 5, 6, 7, 8, 9}.</p>'''

hint = {}
