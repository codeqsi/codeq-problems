name = 'number_proper/2'
slug = 'numbers without leading zeros'

description = '''\
<p>Write a DCG with the starting symbol <code>number_proper</code> for the language of non-negative integers. The numbers should <em>not</em> contain leading zeros.</p>
<p>Example words: <code>123</code>, <code>54</code>, <code>122</code>, <code>1221</code>, <code>0</code>.</p>'''

hint = {}
