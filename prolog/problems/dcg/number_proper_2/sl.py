name = 'number_proper/2'
slug = 'Števila brez vodilnih ničel'

description = '''\
<p>Napiši gramatiko z začetnim simbolom <code>number_proper</code> za jezik nenegativnih celih števil. Števila <em>ne smejo</em> vsebovati vodilnih ničel.</p>
<p>Primeri veljavnih besed: <code>123</code>, <code>54</code>, <code>122</code>, <code>1221</code>, <code>0</code>.</p>'''

hint = {}
