name = 'paren/3'
slug = 'Pravilno gnezdeni oklepaji s pomenom'

description = '''\
<p>Napiši gramatiko z začetnim simbolom <code>paren</code> za jezik pravilno gnezdenih zaporedij oklepajev. Pomen besede v tem jeziku je največja globina gnezdenja oklepajev.</p>
<pre>
?- paren(D, ['(','(',')',')','(',')'], []).  % (())()
  D = 2.
</pre>
'''

hint = {}
