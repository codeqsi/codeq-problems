name = 'paren/3'
slug = 'properly nested parens with meaning'

description = '''\
<p>Write a DCG with the starting symbol <code>paren</code> for the language of properly nested sequences of parentheses. The meaning of a word in this language is the maximum depth of the nested parentheses.</p>
<pre>
?- paren(D, ['(','(',')',')','(',')'], []).  % (())()
  D = 2.
</pre>
'''

hint = {}
