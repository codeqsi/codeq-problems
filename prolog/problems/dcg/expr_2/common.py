id = 170
number = 90
visible = False
facts = None

solution = '''\
expr --> term170, addterm170.
addterm170 --> [].
addterm170 --> [+], expr.
term170 --> factor170, multfactor170.
multfactor170 --> [].
multfactor170 --> [*], term170.
factor170 --> num170.
factor170 --> ['('], expr, [')'].

num170 --> digit170.
num170 --> nzdigit170, num_next170.
num_next170 --> digit170.
num_next170 --> digit170, num_next170.
digit170 --> ([0] ; [1] ; [2] ; [3] ; [4] ; [5] ; [6] ; [7] ; [8] ; [9]).
nzdigit170 --> ([1] ; [2] ; [3] ; [4] ; [5] ; [6] ; [7] ; [8] ; [9]).
'''
