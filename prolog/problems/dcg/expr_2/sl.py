name = 'expr/2'
slug = 'Aritmetični izrazi'

description = '''\
<p>Napiši gramatiko z začetnim simbolom <code>expr</code> za jezik aritmetičnih izrazov, ki vsebujejo števila (brez vodilnih ničel), vsoto in množenje. Podizrazi se lahko združujejo z oklepaji.</p>
<p>Primeri veljavnih besed: <code>(1+2)*3</code>, <code>42*8*3</code>, <code>(2+1)*(3+4)</code>.</p>
'''

hint = {}
