name = 'paren/2'
slug = 'properly nested parens'

description = '''\
<p>Write a DCG with the starting symbol <code>paren</code> for the language of properly nested sequences of parentheses. The terminal symbols in the grammar should be written like this: <code>['(']</code> and <code>[')']</code>.</p>
<p>Example words: <code>()</code>, <code>(())</code>, <code>()(())</code>, <code>(()())()</code>.</p>
<p>Example non-words: <code>)(</code>, <code>((()</code>, <code>))</code>.</p>'''

hint = {}
