from operator import itemgetter
import socket

import prolog.engine
import prolog.util
from server.hints import Hint, HintPopup

id = 168
number = 70
visible = True
facts = None

solution = '''\
paren --> [].
paren --> ['('], paren, [')'], paren.
'''

test_cases = [
    ('findall(W, (W = [_], paren(W, [])), Words), Words == []',
        [{}]),
    ('findall(W, (W = [_,_,_], paren(W, [])), Words), Words == []',
        [{}]),
    ('findall(W, (W = [_,_,_,_,_], paren(W, [])), Words), Words == []',
        [{}]),
    ("setof(W, [A,B]^(W = [A,B], paren(W, [])), Words), Words == [['(',')']]",
        [{}]),
    ("setof(W, [A,B,C,D]^(W = [A,B,C,D], paren(W, [])), Words), Words == [['(','(',')',')'],['(',')','(',')']]",
        [{}]),
    ("setof(W, [A,B,C,D,E,F]^(W = [A,B,C,D,E,F], paren(W, [])), Words), \
               Words == [['(','(','(',')',')',')'],['(','(',')','(',')',')'], \
                         ['(','(',')',')','(',')'],['(',')','(','(',')',')'], \
                         ['(',')','(',')','(',')']]",
        [{}]),
    ("paren(['(','(','(',')',')',')','(',')','(','(',')',')'], [])",
        [{}]),
]

def test(code, aux_code):
    n_correct = 0
    engine_id = None
    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)
        if engine_id is not None and 'error' not in map(itemgetter(0), output):
            # Engine successfully created, and no syntax error in program.
            for query, answers in test_cases:
                if prolog.engine.check_answers(engine_id, query=query, answers=answers, timeout=1.0):
                    n_correct += 1
    except socket.timeout:
        pass
    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_cases)}}]
    return n_correct, len(test_cases), hints

