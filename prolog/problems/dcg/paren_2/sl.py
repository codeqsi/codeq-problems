name = 'paren/2'
slug = 'Pravilno gnezdeni oklepaji'

description = '''\
<p>Napiši gramatiko z začetnim simbolom <code>paren</code> za jezik pravilno gnezdenih zaporedij oklepajev. Terminale v tej gramatiki piši takole: <code>['(']</code> in <code>[')']</code>.</p>
<p>Primeri veljavnih besed: <code>()</code>, <code>(())</code>, <code>()(())</code>, <code>(()())()</code>.</p>
<p>Primeri neveljavnih besed: <code>)(</code>, <code>((()</code>, <code>))</code>.</p>'''

hint = {}
