from operator import itemgetter
import socket

import prolog.engine
import prolog.util
from server.hints import Hint, HintPopup

id = 165
number = 40
visible = True
facts = None

solution = '''\
digit165 --> ([0] ; [1] ; [2] ; [3] ; [4] ; [5] ; [6] ; [7] ; [8] ; [9]).

number --> digit165.
number --> digit165, number.
'''

test_cases = [
    ('setof(W, [A]^(W = [A], number(W, [])), Words), Words == [[0],[1],[2],[3],[4],[5],[6],[7],[8],[9]]',
        [{}]),
    ('setof(W, [A,B]^(W = [A,B], number(W, [])), Words), length(Words, 100)',
        [{}]),
    ('setof(W, [A,B,C]^(W = [A,B,C], number(W, [])), Words), length(Words, 1000)',
        [{}]),
    ('W = [_,_], number(W, []), W == [0,5]',
        [{}]),
    ('W = [_,_,_], number(W, []), W == [4,2,2]',
        [{}]),
    ('W = [1,0,_,_], number(W, []), W == [1,0,0,1]',
        [{}]),
]

def test(code, aux_code):
    n_correct = 0
    engine_id = None
    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)
        if engine_id is not None and 'error' not in map(itemgetter(0), output):
            # Engine successfully created, and no syntax error in program.
            for query, answers in test_cases:
                if prolog.engine.check_answers(engine_id, query=query, answers=answers, timeout=1.0, inference_limit=None):
                    n_correct += 1
    except socket.timeout:
        pass
    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_cases)}}]
    return n_correct, len(test_cases), hints

