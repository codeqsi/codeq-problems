name = 'number/2'
slug = 'Števila z dovoljenimi vodilnimi ničlami'

description = '''\
<p>Napiši gramatiko z začetnim simbolom <code>number</code> za jezik nenegativnih celih števil. Števila lahko vsebujejo vodilne ničle.</p>
<p>Primeri veljavnih besed: <code>123</code>, <code>54</code>, <code>0122</code>, <code>0001221</code>, <code>0</code>.</p>'''

hint = {}
