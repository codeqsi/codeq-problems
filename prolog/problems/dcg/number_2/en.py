name = 'number/2'
slug = 'numbers with potential leading zeros'

description = '''\
<p>Write a DCG with the starting symbol <code>number</code> for the language of non-negative integers. The numbers may include leading zeros.</p>
<p>Example words: <code>123</code>, <code>54</code>, <code>0122</code>, <code>0001221</code>, <code>0</code>.</p>'''

hint = {}
