name = 'ab/2'
slug = 'a*b*'

description = '''\
<p>Write a DCG with the starting symbol <code>ab</code> for the language <code>a<sup>m</sup>b<sup>n</sup></code>, where m, n ≥ 0.</p>
<p>Example words: <code>[]</code>, <code>a</code>, <code>aab</code>, <code>abbb</code>, <code>bbb</code>.</p>
<p>Hint: to generate words of increasing length, use the query <code>append(Word,_,_), ab(Word,[])</code>.</p>'''

hint = {}
