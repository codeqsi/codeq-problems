from operator import itemgetter
import socket

import prolog.engine
import prolog.util
from server.hints import Hint, HintPopup

id = 162
number = 10
visible = True
facts = None

solution = '''\
ab --> [a], ab.
ab --> t162.
t162 --> [b], t162.
t162 --> [].'''

test_cases = [
    ('ab([], [])',
        [{}]),
    ('setof(W, [A]^(W = [A], ab(W, [])), Words), Words == [[a],[b]]',
        [{}]),
    ('setof(W, [A,B]^(W = [A,B], ab(W, [])), Words), Words == [[a,a],[a,b],[b,b]]',
        [{}]),
    ('setof(W, [A,B,C]^(W = [A,B,C], ab(W, [])), Words), Words == [[a,a,a],[a,a,b],[a,b,b],[b,b,b]]',
        [{}]),
    ('setof(W, [A,B,C,D]^(W = [A,B,C,D], ab(W, [])), Words), Words == [[a,a,a,a],[a,a,a,b],[a,a,b,b],[a,b,b,b],[b,b,b,b]]',
        [{}]),
]

def test(code, aux_code):
    n_correct = 0
    engine_id = None
    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)
        if engine_id is not None and 'error' not in map(itemgetter(0), output):
            # Engine successfully created, and no syntax error in program.
            for query, answers in test_cases:
                if prolog.engine.check_answers(engine_id, query=query, answers=answers, timeout=1.0):
                    n_correct += 1
    except socket.timeout:
        pass
    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_cases)}}]
    return n_correct, len(test_cases), hints

