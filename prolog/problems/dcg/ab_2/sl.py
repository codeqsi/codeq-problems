name = 'ab/2'
slug = 'a*b*'

description = '''\
<p>Napiši DCG gramatiko z začetnim simbolom <code>ab</code> za jezik <code>a<sup>m</sup>b<sup>n</sup></code>, kjer sta m, n ≥ 0.</p>
<p>Primeri veljavnih besed: <code>[]</code>, <code>a</code>, <code>aab</code>, <code>abbb</code>, <code>bbb</code>.</p>
<p>Namig: za generiranje vedno daljših besed uporabi poizvedbo <code>append(Word,_,_), ab(Word,[])</code>.</p>'''

hint = {}
