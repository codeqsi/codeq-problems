name = 'expr/3'
slug = 'arithmetic expressions with meaning'

description = '''\
<p>Write a DCG with the starting symbol <code>expr</code> for the language of arithmetic expressions consisting of numbers (without leading zeros), addition and multiplication. Subexpressions can be grouped using parentheses. The meaning of a word in this language is the numeric value of the represented arithmetic expression.</p>
<p>Example words: <code>(1+2)*3</code>, <code>42*8*3</code>, <code>(2+1)*(3+4)</code>.</p>
<pre>
?- expr(N, ['(',2,'+',1,')','*','(',3,'+',4,')'], []).  % (2+1)*(3+4) = 21
  N = 21.
</pre>
'''

hint = {}
