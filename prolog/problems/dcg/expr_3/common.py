id = 171
number = 100
visible = False
facts = None

solution = '''\
expr(N) --> term171(N).
expr(N) --> term171(N1), [+], expr(N2), {N is N1 + N2}.

term171(N) --> factor171(N).
term171(N) --> factor171(N1), [*], term171(N2), {N is N1 * N2}.

factor171(N) --> number171(N).
factor171(N) --> ['('], expr(N), [')'].

memb171(X, [X|_]).
memb171(X, [_|T]) :-
    memb171(X, T).

digit171(N) --> [N], { memb171(N, [0,1,2,3,4,5,6,7,8,9]) }.
number171(N) --> digit171(N).
number171(N) --> number171(N1), digit171(D), { N is 10*N1 + D }.
'''
