name = 'expr/3'
slug = 'Aritmetični izrazi s pomenom'

description = '''\
<p>Napiši gramatiko z začetnim simbolom <code>expr</code> za jezik aritmetičnih izrazov, ki vsebujejo števila (brez vodilnih ničel), vsoto in množenje. Podizrazi se lahko združujejo z oklepaji. Pomen besede v tem jeziku je numerična vrednost predstavljenega izraza.</p>
<p>Primeri veljavnih besed: <code>(1+2)*3</code>, <code>42*8*3</code>, <code>(2+1)*(3+4)</code>.</p>
<pre>
?- expr(N, ['(',2,'+',1,')','*','(',3,'+',4,')'], []).  % (2+1)*(3+4) = 21
  N = 21.
</pre>
'''

hint = {}
