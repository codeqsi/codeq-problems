from operator import itemgetter
import socket

import prolog.engine
import prolog.util
from server.hints import Hint, HintPopup

id = 167
number = 60
visible = True
facts = None

solution = '''\
number(Num) --> dig167(N), { l2n167(N,Num) }.
number(Num) --> nonzero167([N]), numb_next167(N1), { l2n167([N|N1],Num) }.

zero167([0]) --> [0].
nonzero167([N]) --> [N], {memb167(N, [1,2,3,4,5,6,7,8,9])}.

dig167(N) --> zero167(N).
dig167(N) --> nonzero167(N).
numb_next167(N) --> dig167(N).
numb_next167([N|N1]) --> dig167([N]), numb_next167(N1).

memb167(X, [X|_]).
memb167(X, [_|T]) :-
    memb167(X, T).

l2n167(L, N) :-
    l2n167(L, 0, N).
l2n167([N], S, R) :-
    R is S*10+N.
l2n167([H|T], S, N) :-
    S1 is S*10+H,
    l2n167(T, S1, N).
'''

test_cases = [
    ('number(N, [0], []), N == 0',
        [{}]),
    ('number(N, [3,4,1,5], []), N == 3415',
        [{}]),
    ('number(N, [9,8,7], []), N == 987',
        [{}]),
    ('W = [_], number(0, W, []), W == [0]',
        [{}]),
    ('W = [_,_], number(62, W, []), W == [6,2]',
        [{}]),
]

def test(code, aux_code):
    n_correct = 0
    engine_id = None
    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)
        if engine_id is not None and 'error' not in map(itemgetter(0), output):
            # Engine successfully created, and no syntax error in program.
            for query, answers in test_cases:
                if prolog.engine.check_answers(engine_id, query=query, answers=answers, timeout=1.0, inference_limit=None):
                    n_correct += 1
    except socket.timeout:
        pass
    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_cases)}}]
    return n_correct, len(test_cases), hints

