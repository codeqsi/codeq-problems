name = 'number/3'
slug = 'numbers with meaning'

description = '''\
<p>Write a DCG with the starting symbol <code>number</code> for the language of non-negative integers. The numbers may contain leading zeros. The meaning of a word in this language is the numeric value of the represented number.</p>
<pre>
?- number(N, [1,2,3,4], []).
  N = 1234.
</pre>
'''

hint = {}
