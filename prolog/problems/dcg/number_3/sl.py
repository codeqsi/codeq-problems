name = 'number/3'
slug = 'Števila s pomenom'

description = '''\
<p>Napiši gramatiko z začetnim simbolom <code>number</code> za jezik nenegativnih celih števil. Števila lahko vsebujejo vodilne ničle. Pomen besede v tem jeziku je kar numerična vrednost predstavljenega števila.</p>
<pre>
?- number(N, [1,2,3,4], []).
  N = 1234.
</pre>
'''

hint = {}
