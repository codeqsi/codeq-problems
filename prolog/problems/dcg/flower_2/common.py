from operator import itemgetter
import socket

import prolog.engine
import prolog.util
from server.hints import Hint, HintPopup

id = 163
number = 20
visible = True
facts = None

solution = '''\
flower --> iflower163.
flower --> [+], flower, [+].

iflower163 --> [-].
iflower163 --> [-], iflower163.
'''

test_cases = [
    ('setof(W, [A]^(W = [A], flower(W, [])), Words), Words == [[-]]',
        [{}]),
    ('setof(W, [A,B]^(W = [A,B], flower(W, [])), Words), Words == [[-,-]]',
        [{}]),
    ('setof(W, [A,B,C]^(W = [A,B,C], flower(W, [])), Words), Words == [[+,-,+],[-,-,-]]',
        [{}]),
    ('setof(W, [A,B,C,D]^(W = [A,B,C,D], flower(W, [])), Words), Words == [[+,-,-,+],[-,-,-,-]]',
        [{}]),
    ('setof(W, [A,B,C,D,E]^(W = [A,B,C,D,E], flower(W, [])), Words), Words == [[+,+,-,+,+],[+,-,-,-,+],[-,-,-,-,-]]',
        [{}]),
]

def test(code, aux_code):
    n_correct = 0
    engine_id = None
    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)
        if engine_id is not None and 'error' not in map(itemgetter(0), output):
            # Engine successfully created, and no syntax error in program.
            for query, answers in test_cases:
                if prolog.engine.check_answers(engine_id, query=query, answers=answers, timeout=1.0):
                    n_correct += 1
    except socket.timeout:
        pass
    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_cases)}}]
    return n_correct, len(test_cases), hints

