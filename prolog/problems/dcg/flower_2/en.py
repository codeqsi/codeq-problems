name = 'flower/2'
slug = 'words like +++--+++'

description = '''\
<p>Write a DCG with the starting symbol <code>flower</code> for the language <code>+<sup>n</sup>-<sup>m</sup>+<sup>n</sup></code>, where m > 0 and n ≥ 0.</p>
<p>Example words: <code>-</code>, <code>++-++</code>, <code>+---+</code>.</p>
'''

hint = {}
