

name = 'flower/2'
slug = 'Besede v obliki "rožice", npr. +++--+++'

description = '''\
<p>Napiši gramatiko z začetnim simbolom <code>flower</code> za jezik <code>+<sup>n</sup>-<sup>m</sup>+<sup>n</sup></code>, kjer je m > 0 in n ≥ 0.</p>
<p>Primeri veljavnih besed: <code>-</code>, <code>++-++</code>, <code>+---+</code>.</p>
'''

hint = {}
