name = 'genlist/4'
slug = 'generate a list of random numbers'

description = '''\
<p><code>genlist(L, N, Min, Max)</code>: the list <code>L</code> contains <code>N</code> random numbers from the interval [<code>Min</code>,<code>Max</code>).</p>
<pre>
?- genlist(L, 5, 10, 100).
  L = [12,99,81,24].
</pre>'''

hint = {}
