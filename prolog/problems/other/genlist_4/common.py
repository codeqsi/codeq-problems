id = 127
number = 34
visible = False
facts = None

solution = '''\
genlist([], 0, _, _).
genlist([H|T], N, Min, Max):-
  N > 0,
  N1 is N - 1,
  genlist(T, N1, Min, Max),
  random(Min, Max, H).
'''
