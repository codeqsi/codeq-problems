name = 'ancestor/2'
slug = 'X je prednik od Y'

description = '''\
<p><code>ancestor(X, Y)</code>: <code>X</code> je prednik (oče, mama, babica,
pradedek, ...) od <code>Y</code>.</p> <pre>
?- ancestor(patricia, Y).
  Y = john ;
  Y = michael ;
  Y = michelle.
</pre>
<p><a target="_blank" href="[%@resource famrel.svg%]">Družinska drevesa</a> so
podana s predikati <code>parent/2</code>, <code>male/1</code> in
<code>female/1</code>.</p>
'''

plan = ['''\
<p>Sedaj pa bo potrebna rekurzija... kako lahko problem prevedem na (en korak) manjši problem?</p>
<p><img src="[%@resource Prolog_ancestor_03.svg%]" alt="Starš od nekoga, ki je prednik od Y, je tudi prednik od Y." /></p>
''', '''\
<p>Če je <code>X</code> starš od nekega <code>Z</code> in je
ta <code>Z</code> prednik od <code>Y</code>,
potem je <code>X</code> tudi prednik od <code>Y</code>.</p>
''', '''\
<p><img src="[%@resource Prolog_ancestor_04.svg%]" alt="Če je X starš od Z, ki je prednik od Y, potem je X tudi prednik od Y." /></p>
''']

hint = {
    'gender_is_irrelevant': '''\
<p>Je spol res pomemben?</p>
''',

    'gender_is_irrelevant_markup': '''\
<p>Je to res potrebno?</p>
''',

    'grandparent_used': '''\
<p>Rešitev z "grandparent" bo premalo splošna, poskusi nadomestiti to z rekurzijo.
Skratka, poskusi prevesti na "manjši" problem, npr. prednik v enem koraku manj
(en korak bližji prednik)...</p>
''',

    'grandparent_used_markup': '''\
<p>Bi se dalo rešiti brez tega?</p>
''',

    'base_case': '''\
<p>Si pomislil na robni pogoj? Rekurzija se mora enkrat tudi ustaviti.
Kaj je najbolj enostaven par (prednik, potomec)?</p>
<p><img src="[%@resource Prolog_ancestor_01.svg%]" /></p>
''',    # TODO: morda ta hint naredim z "more": najprej tekst, ob kliku pa še slika... (sicer je preveč očitno)

    'ancestor_to_oneself': '''\
<p>Kako je lahko nekdo potomec samega sebe? Iz trenutne verzije rešitve se da izpeljati tudi to.
Premisli, morda se ti splača tudi grafično skicirati tvojo trenutno rešitev.</p>
''',

    'ancestor_to_oneself_with_or': '''\
<p>Kako je lahko nekdo potomec samega sebe? Iz trenutne verzije rešitve se da izpeljati tudi to.
Premisli, morda se ti splača tudi grafično skicirati tvojo trenutno rešitev.</p>
<p>Morda se ti splača preveriti tudi uporabo podpičja. To praktično naredi dva ločena stavka oz. veji
(eno ali drugo velja, morda tudi oboje). Vendar pazi, ker sta ti dve veji med seboj neodvisni
-- vrednosti iz ene se ne prenašajo v drugo vejo.</p>
''',

    'only_two_levels_deep': '''\
<p>Tvoja rešitev je premalo splošna. Prednik je lahko tudi bolj oddaljen kot so stari starši,
npr. prababica ali prapradedek sta tudi prednika.</p>
''',

    'descendant_need_not_be_parent': '''\
<p>Potomec <code>Y</code> pravzaprav ne rabi imeti otrok...</p>
''',

    'wrong_direction': '''\
<p>Si morda sprogramiral ravno obratno in zamenjal prednika s potomcem?
<code>X</code> naj bo prednik od <code>Y</code> in ne obratno!</p>
<p><img src="[%@resource Prolog_ancestor_02.svg%]" /></p>
''',

    'predicate_always_false': '''\
<p>Vse kaže, da tvoj predikat <emph>vedno</emph> vrne "false". Si mu dal pravilno ime, si se morda pri imenu zatipkal?</p>
<p>Če je ime pravilno, se morda splača preveriti tudi, če se nisi zatipkal kje drugje,
je morda kakšna pika namesto vejice ali obratno, morda kakšna spremenljivka z malo začetnico?</p>
<p>Možno je seveda tudi, da so tvoji pogoji prestrogi ali celo nemogoči (kot bi bila npr. zahteva,
da je <code>X</code> hkrati starš in sestra od <code>Y</code> ali kaj podobnega).</p>
''',

    'timeout': '''\
<p>Je morda na delu potencialno neskončna rekurzija? Kako se bo ustavila?</p>
<p>Morda pa je kriv tudi manjkajoč, neustrezen ali preprosto nekompatibilen (s splošnim primerom) robni pogoj?</p>
''',
}

