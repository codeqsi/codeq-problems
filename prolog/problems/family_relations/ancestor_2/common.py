from operator import itemgetter
import socket
import prolog.engine
import prolog.util
from server.hints import Hint, HintPopup
import server.problems

id = 100
number = 8
visible = True
facts = 'family_relations'

solution = '''\
ancestor(X, Y) :-
  parent(X, Y).
ancestor(X, Y) :-
  parent(X, Z),
  ancestor(Z, Y).
'''

hint_type = {
    'gender_is_irrelevant_markup': HintPopup('gender_is_irrelevant_markup'),
    'gender_is_irrelevant': Hint('gender_is_irrelevant'),
    'grandparent_used': Hint('grandparent_used'),
    'grandparent_used_markup': HintPopup('grandparent_used_markup'),
    'base_case': Hint('base_case'),
    'ancestor_to_oneself': Hint('ancestor_to_oneself'),
    'ancestor_to_oneself_with_or': Hint('ancestor_to_oneself_with_or'),
    'descendant_need_not_be_parent': Hint('descendant_need_not_be_parent'),
    'wrong_direction': Hint('wrong_direction'),
    'only_two_levels_deep': Hint('only_two_levels_deep'),
    'predicate_always_false': Hint('predicate_always_false'),
    'timeout': Hint('timeout'),
}

test_cases = [
    ('ancestor(ana, X)',
        [{'X': 'aleksander'}, {'X': 'luana'}, {'X': 'daniela'}]),
    ('ancestor(morty, X)',
        [{'X': 'jerry'}, {'X': 'anna'}]),
    ('ancestor(X, susan)',
        [{'X': 'vanessa'}, {'X': 'patrick'}, {'X': 'tina'}, {'X': 'thomas'}, {'X': 'william'}]),
]

def test(code, aux_code):
    n_correct = 0
    engine_id = None
    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)
        if engine_id is not None and 'error' not in map(itemgetter(0), output):
            # Engine successfully created, and no syntax error in program.
            for query, answers in test_cases:
                if prolog.engine.check_answers(engine_id, query=query, answers=answers, timeout=1.0):
                    n_correct += 1
    except socket.timeout:
        pass
    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_cases)}}]
    return n_correct, len(test_cases), hints

def hint(code, aux_code):
    tokens = prolog.util.tokenize(code)

    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)

        # gender testing is redundant
        # this is not necessarily wrong, but worth mentioning anyway
        targets = [prolog.util.Token('NAME', 'male'), prolog.util.Token('NAME', 'female')]
        marks = [(t.pos, t.pos + len(t.val)) for t in tokens if t in targets]
        if marks:
            return [{'id': 'gender_is_irrelevant_markup', 'start': m[0], 'end': m[1]} for m in marks] + \
                   [{'id': 'gender_is_irrelevant'}]

        # grandparent is a sign of complications
        # it's likely used instead of recursion
        targets = [prolog.util.Token('NAME', 'grandparent')]
        marks = [(t.pos, t.pos + len(t.val)) for t in tokens if t in targets]
        if marks:
            return [{'id': 'grandparent_used_markup', 'start': m[0], 'end': m[1]} for m in marks] + \
                   [{'id': 'grandparent_used'}]

        # target predicate seems to always be false
        if not prolog.engine.ask_truthTO(engine_id, 'ancestor(_, _)'):
            return [{'id': 'predicate_always_false'}]

        # descendant instead of ancestor (wrong direction)
        # warning: knowledge base dependent
        if prolog.engine.ask_truthTO(engine_id,
            'findall(_, ancestor(william, X), L1), length(L1, 2), \
             findall(_, ancestor(X, william), L2), length(L2, 6)'):
            return [{'id': 'wrong_direction'}]

        # missing/failed base case
        if prolog.engine.ask_truthTO(engine_id,
            'findall(X/Y, parent(X, Y), L), member(X/Y, L), \+ ancestor(X, Y)'):
            return [{'id': 'base_case'}]

        # two levels deep relation (up to grandparent) only
        # warning: knowledge base dependent
        if prolog.engine.ask_truthTO(engine_id,
            'findall(_, ancestor(X, Y), L1), length(L1, 23) ; \
             findall(_, ancestor(X, Y), L2), length(L2, 55)'):
            return [{'id': 'only_two_levels_deep'}]

        # ancestor to oneself
        if prolog.engine.ask_truthTO(engine_id, 'ancestor(X, X)'):
            if prolog.util.Token('SEMI', ';') in tokens:
                return [{'id': 'ancestor_to_oneself_with_or'}]
            else:
                return [{'id': 'ancestor_to_oneself'}]

        # Y does not necessarily need to be a parent
        if prolog.engine.ask_one(engine_id,
            'ancestor(_, Y), \+ parent(Y, _)') == 'false':
            return [{'id': 'descendant_need_not_be_parent'}]

    except socket.timeout as ex:
        return [{'id': 'timeout'}]

    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    return []

