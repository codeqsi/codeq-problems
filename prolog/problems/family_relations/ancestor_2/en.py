name = 'ancestor/2'
slug = 'the ancestor relation'

description = '''\
<p><code>ancestor(X, Y)</code>: <code>X</code> is an ancestor (parent, grandparent, great-grandparent, ...)
of <code>Y</code>.</p>
<pre>
?- ancestor(patricia, X).
  X = john ;
  X = michael ;
  X = michelle.
</pre>
<p><a target="_blank" href="[%@resource famrel.svg%]">Family trees</a> are
described with predicates <code>parent/2</code>, <code>male/1</code>, and
<code>female/1</code>.</p>
'''

plan = ['''\
<p>It's time to use recursion for the first time... how can we reduce the problem into a smaller one
(a single step smaller that is)?</p>
<p><img src="[%@resource Prolog_ancestor_03.svg%]" alt="A parent of someone, who is a parent of Y,
is also an ancestor of Y." /></p>
''', '''\
<p>If <code>X</code> is a parent of some <code>Z</code> and that <code>Z</code> is an ancestor of <code>Y</code>,
then <code>X</code> is also an ancestor of <code>Y</code>.</p>
''', '''\
<p><img src="[%@resource Prolog_ancestor_04.svg%]" alt="If X is a parent of Z, and Z is an ancestor of Y,
then X is also an ancestor of Y." /></p>
''']

hint = {
    'gender_is_irrelevant': '''\
<p>Is gender really important?</p>
''',

    'gender_is_irrelevant_markup': '''\
<p>Is this necessary?</p>
''',

    'grandparent_used': '''\
<p>The solution using the grandparent relation will not be general enough, try using recursion instead.
Try to reduce the problem into a "smaller" one, e.g. an ancestor in one step less (a closer ancestor).</p>
''',

    'grandparent_used_markup': '''\
<p>Can you solve without using this?</p>
''',

    'base_case': '''\
<p>Did you think of a base case? The recursion has to stop at some point.
What is the most obvious pair (ancestor, descendand)?</p>
<p><img src="[%@resource Prolog_ancestor_01.svg%]" /></p>
''',    # TODO: morda ta hint naredim z "more": najprej tekst, ob kliku pa še slika... (sicer je preveč očitno)

    'ancestor_to_oneself': '''\
<p>How can anyone be an ancestor to him- or herself? This can be inferred from the current version of the program.
It might also help to graphically sketch your current solution.</p>
''',

    'ancestor_to_oneself_with_or': '''\
<p>How can anyone be an ancestor to him- or herself? This can be inferred from the current version of the program.
It might help to graphically sketch your current solution.</p>
<p>Perhaps you should also check your use of the semicolon. The semicolon basically creates two separate clauses or
branches (one or the other can hold, or even both). However, be careful, as this two branches are independent of
one another -- assigned values to variables in one branch do not transfer to the other branch.</p>
''',

    'only_two_levels_deep': '''\
<p>Your solution is not general enough. An ancestor can also be someone "more steps away" than grandparents,
e.g. great-grandparents or great-great-grandparents are also ancestors.</p>
''',

    'descendant_need_not_be_parent': '''\
<p>The descendant <code>Y</code> doesn't actually need to have any children...</p>
''',

    'wrong_direction': '''\
<p>Did you perhaps program exactly the reverse relation, descendant instead of ancestor?
<code>X</code> should be an ancestor of <code>Y</code> and not the other way around!</p>
<p><img src="[%@resource Prolog_ancestor_02.svg%]" /></p>
''',

    'predicate_always_false': '''\
<p>It seems your predicate is <emph>always</emph> "false". Did you give it the correct name,
or is it perhaps misspelled?</p>
<p>If the name is correct, check whether something else is misspelled, perhaps there is a full stop instead of
a comma or vice versa, or maybe you typed a variable name in lowercase?</p>
<p>It is, of course, also possible that your conditions are too restrictive, or even impossible to satisfy
(as would be, for example, the condition that <code>X</code> is both a parent and a sister of <code>Y</code>, or
something similarly impossible).</p>
''',

    'timeout': '''\
<p>Is there an infinite recursion at work here? How will it ever stop?</p>
<p>Or perhaps is there a missing, faulty, or simply incompatible (with the general recursive case) base case?</p>
''',
}

