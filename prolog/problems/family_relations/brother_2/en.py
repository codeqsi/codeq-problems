name = 'brother/2'
slug = 'the brother relation'

description = '''\
<p><code>brother(X, Y)</code>: <code>X</code> is a (half)brother of <code>Y</code>.</p>
<pre>
?- brother(jeffrey, X).
  X = william ;
  X = sally.
</pre>
<p><a target="_blank" href="[%@resource famrel.svg%]">Family trees</a> are
described with predicates <code>parent/2</code>, <code>male/1</code>, and
<code>female/1</code>.</p>
'''

plan = [
    '''\
<p><img src="[%@resource Prolog_brother_01.svg%]" alt="A brother is male and has a common parent with Y." /></p>''',
    '''\
<p>If <code>X</code> is male and if <code>X</code> and <code>Y</code> share a common parent
then <code>X</code> is a brother of <code>Y</code>.</p>''',
    '''\
<p><img src="[%@resource Prolog_brother_02.svg%]" alt="Brother X is male and has
at least one parent in common with Y." /></p>''',
]

hint = {
    'x_y_must_be_different': [{'message': '''\
<p><img src="[%@resource Prolog_brother_04.svg%]" /></p>
<p>What if <code>X</code> and <code>Y</code> are the same person? Try the following query:</p>
<pre>
?- brother(william, Y).
</pre>''', 'linkText': 'How can I check that <code>X</code> and <code>Y</code> are different?'},
'''\
<p>Using the operator <code>\==</code> I can check that <code>X</code> and <code>Y</code> are different.
For example: <code>X \== Y</code></p>
'''],

    'x_must_be_male': '''\
<p><img src="[%@resource Prolog_brother_03.svg%]" /></p>
<p>A brother is usually male.</p>
''',

    'y_can_be_of_any_gender': '''\
<p><code>Y</code> can actually be of <em>any</em> gender.</p>
''',

    'neq+_instead_of_neq': '''\
<p>Use the operator \== instead of operators \= or =\=. It succeeds when the two operands are not the same,
while \= succeeds if the operands <em>cannot be made</em> the same (cannot be unified), and =\= checks for
arithmetic inequality and in this case both operands should be immediately arithmetically computable.</p>
''',

    'common_parent_needed': '''\
<p><img src="[%@resource Prolog_brother_05.svg%]" /></p>
<p>It would make sense if <code>X</code> and <code>Y</code> have at least one parent in common.</p>
''',

    'neq_used_too_early': '''\
<p>Did you use the operator <code>\==</code> too early?</p>
<p>Try moving it more towards the end of the rule. The problem is probably that the variables
you are comparing don't yet have values assigned to them and are therefore different by default.
Prolog doesn't check whether they become equal <em>later</em>, it only checks whether they're
equal or not at the moment of comparison.</p>
<p>Or maybe you're simply comparing the wrong two variables?</p>
''',

    'predicate_always_false': '''\
<p>It seems your predicate is <em>always</em> "false". Did you give it the correct name,
or is it perhaps misspelled?</p>
<p>If the name is correct, check whether something else is misspelled, perhaps there is a full stop instead of
a comma or vice versa, or maybe you typed a variable name in lowercase?</p>
<p>It is, of course, also possible that your conditions are too restrictive, or even impossible to satisfy
(as would be, for example, the condition that <code>X</code> is both a parent and a sister of <code>Y</code>, or
something similarly impossible).</p>
''',
}
