name = 'brother/2'
slug = 'X je brat od Y'

description = '''\
<p><code>brother(X, Y)</code>: <code>X</code> je brat od <code>Y</code>.</p>
<pre>
?- brother(jeffrey, X).
  Y = william ;
  Y = sally.
</pre>
<p><a target="_blank" href="[%@resource famrel.svg%]">Družinska drevesa</a> so
podana s predikati <code>parent/2</code>, <code>male/1</code> in
<code>female/1</code>.</p>
'''

plan = [
    '''\
<p><img src="[%@resource Prolog_brother_01.svg%]" alt="Brat je moškega spola in ima skupnega starša z Y." /></p>''',
    '''\
<p>Če je <code>X</code> moški in če imata <code>X</code> in <code>Y</code> skupnega starša,
potem je <code>X</code> brat od <code>Y</code>.</p>''',
    '''\
<p><img src="[%@resource Prolog_brother_02.svg%]" alt="Brat X je moškega spola in ima vsaj enega skupnega starša z Y." /></p>''',
]

hint = {
    'x_y_must_be_different': [{'message': '''\
<p><img src="[%@resource Prolog_brother_04.svg%]" /></p>
<p>Si pomislil, da sta <code>X</code> in <code>Y</code> lahko ista oseba? Poskusi naslednjo poizvedbo:</p>
<pre>
?- brother(william, Y).
</pre>''', 'linkText': 'Kako lahko preverim, da sta <code>X</code> in <code>Y</code> različna?'},
'''\
<p>S pomočjo operatorja <code>\==</code> lahko preverim, da sta <code>X</code> in <code>Y</code> med seboj različna.
Na primer: <code>X \== Y</code></p>
'''],

    'x_must_be_male': '''\
<p><img src="[%@resource Prolog_brother_03.svg%]" /></p>
<p>Brat je navadno moškega spola.</p>
''',

    'y_can_be_of_any_gender': '''\
<p><code>Y</code> je pravzaprav lahko poljubnega spola.</p>
''',

    'neq+_instead_of_neq': '''\
<p>Uporabi raje operator \== namesto operatorjev \= ali =\=. Prvi preveri, da oba operanda nista enaka,
drugi (\=) ju poskusi unificirati (narediti enaka), tretji (=\=) pa je samo za aritmetično primerjanje
in morata oba operanda biti takoj izračunljiva aritmetična izraza.</p>
''',

    'common_parent_needed': '''\
<p><img src="[%@resource Prolog_brother_05.svg%]" /></p>
<p>Dobro bi bilo, da imata <code>X</code> in <code>Y</code> vsaj enega skupnega starša.</p>
''',

    'neq_used_too_early': '''\
<p>Si morda uporabil operator <code>\==</code> prezgodaj?</p>
<p>Poskusi ga pomakniti bolj proti koncu. Razlog za probleme je verjetno,
da spremenljivki, ki ju primerjaš, še nimata določene vrednosti in sta zato različni.
Prolog ne preverja, če kasneje morda postaneta enaki, ampak samo kakšni sta v trenutku,
ko naleti na primerjavo.</p>
<p>Morda pa preprosto preverjaš napačni spremenljivki?</p>
''',

    'predicate_always_false': '''\
<p>Vse kaže, da tvoj predikat vedno vrne "false". Si mu dal pravilno ime, si se morda pri imenu zatipkal?</p>
<p>Če je ime pravilno, se morda splača preveriti tudi, če se nisi zatipkal kje drugje,
je morda kakšna pika namesto vejice ali obratno, morda kakšna spremenljivka z malo začetnico?</p>
<p>Možno je seveda tudi, da so tvoji pogoji prestrogi ali celo nemogoči (kot bi bila npr. zahteva,
da je <code>X</code> hkrati starš in sestra od <code>Y</code>).</p>
''',
}
