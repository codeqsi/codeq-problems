name = 'mother/2'
slug = 'X je mama od Y'

description = '''\
<p><code>mother(X, Y)</code>: <code>X</code> je mama od <code>Y</code>.</p>
<pre>
?- mother(tina, william).
  true.
?- mother(nevia, Y).
  Y = luana ;
  Y = daniela.
</pre>
<p><a target="_blank" href="[%@resource famrel.svg%]">Družinska drevesa</a> so
podana s predikati <code>parent/2</code>, <code>male/1</code> in
<code>female/1</code>.</p>
'''

plan = [
    '''\
<p><img src="[%@resource Prolog_mother_01.svg%]" alt="Mama ima otroka in je ženskega spola." /></p>
''',
    '''\
<p><img src="[%@resource Prolog_mother_02.svg%]" alt="Mama X je starš od Y in je ženskega spola." /></p>''',
    '''\
<p>Če je <code>X</code> ženska in je hkrati <code>X</code> starš od <code>Y</code>,
potem je <code>X</code> mama od <code>Y</code>.</p>'''
]

hint = {
    'or_instead_of_and': '''\
<p>Si morda uporabil podpičje (ki pomeni ALI) namesto vejice (ki pomeni IN)?</p>
''',

    'or_instead_of_and_two_rules': '''\
<p>Si morda zapisal dve pravili, eno za spol in eno za "starševstvo"?
Pozor: velja eno ALI drugo, ne nujno eno IN drugo!</p>
''',

    'x_must_be_female': '''\
<p>Mama je navadno ženskega spola.</p>
<p><img src="[%@resource Prolog_mother_03.svg%]" /></p>
''',

    'x_must_be_parent': '''\
<p>Mama naj bi imela vsaj enega otroka... torej je starš od nekoga.</p>
<p><img src="[%@resource Prolog_mother_04.svg%]" /></p>
''',

    'y_can_be_of_any_gender': '''\
<p><code>Y</code> je pravzaprav lahko poljubnega spola.</p>
''',

    'y_need_not_be_parent': '''\
<p><code>Y</code> pravzaprav ne rabi imeti otrok, da ima mamo...</p>
''',

    'predicate_always_false': '''\
<p>Vse kaže, da tvoj predikat vedno vrne "false". Si mu dal pravilno ime, si se morda pri imenu zatipkal?</p>
<p>Če je ime pravilno, se morda splača preveriti tudi, če se nisi zatipkal kje drugje,
je morda kakšna pika namesto vejice ali obratno, morda kakšna spremenljivka z malo začetnico?</p>
<p>Možno je seveda tudi, da so tvoji pogoji prestrogi ali celo nemogoči (kot bi bila npr. zahteva,
da je <code>X</code> hkrati starš in sestra od <code>Y</code>).</p>
''',
}
