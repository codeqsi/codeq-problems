name = 'cousin/2'
slug = 'the cousin relation'

description = '''\
<p><code>cousin(X, Y)</code>: <code>X</code> is a cousin of <code>Y</code>.</p>
<pre>
?- cousin(andrew, X).
  X = vanessa ;
  X = patricia.
</pre>
<p><a target="_blank" href="[%@resource famrel.svg%]">Family trees</a> are
described with predicates <code>parent/2</code>, <code>male/1</code>, and
<code>female/1</code>.</p>
'''

plan = ['''\
<p>Perhaps you can make use of the solution to one of the previous exercises?</p>
<p><img src="[%@resource Prolog_cousin_01.svg%]" alt="I have a child, my sibling also has a child..." /></p>
<p>But of course it's also possible to solve without any of the previous solutions...</p>
''', '''\
<p>If <code>PX</code> is a parent of <code>X</code> and
<code>PY</code> is a parent of <code>Y</code> and if
<code>PX</code> and <code>PY</code> are siblings,
then <code>X</code> is a cousin to <code>Y</code>.</p>
''', '''\
<p><img src="[%@resource Prolog_cousin_03.svg%]" alt="I have a child, my sibling has a child, these two children
are therefore..." /></p>
''']

hint = {
    'gender_is_irrelevant': '''\
<p>Is gender really important?</p>
''',

    'gender_is_irrelevant_markup': '''\
<p>Is this necessary?</p>
''',

    'precedence_fail': '''\
<p>Did you mix up the precedence of operators <em>or</em> & <em>and</em>?</p>
<p>Operator <em>and</em> has higher precedence than operator <em>or</em>.
If you wish to change that you can always use the brackets.</p>
''',

    'cousin_vs_sibling': '''\
<p>How can a cousin <code>X</code> at the same time also be a sibling of <code>Y</code>?
Did you perhaps forget that the parents of <code>X</code> and <code>Y</code> should not be the same?
(Be careful: if both a mother and a father are listed in the database, then prolog could find them as two
<em>different</em> parents, which is logically absolutely correct.)</p>
<p><img src="[%@resource Prolog_cousin_04.svg%]" /></p>
''',

    'cousin_to_oneself': '''\
<p>How can someone be a cousin to him- or herself?
Perhaps <code>X</code> and <code>Y</code> have the same parent?</p>
<p><img src="[%@resource Prolog_cousin_04.svg%]" /></p>
<p>Try this query: <code>?- cousin(X, X).</code></p>
''',

    'cousin_need_not_be_parent': '''\
<p>A cousin doesn't need to have children...</p>
''',

    'predicate_always_false': '''\
<p>It seems your predicate is <em>always</em> "false". Did you give it the correct name,
or is it perhaps misspelled?</p>
<p>If the name is correct, check whether something else is misspelled, perhaps there is a full stop instead of
a comma or vice versa, or maybe you typed a variable name in lowercase?</p>
<p>It is, of course, also possible that your conditions are too restrictive, or even impossible to satisfy
(as would be, for example, the condition that <code>X</code> is both a parent and a sister of <code>Y</code>, or
something similarly impossible).</p>
''',

#     'interesting_tidbit': '''\
# <p>Zanimivost: nalogo bi lahko rešil tudi z uporabo rešitve za relacijo "sister".
# Teta je namreč sestra od starša od <code>Y</code>.</p>
# ''',
}

