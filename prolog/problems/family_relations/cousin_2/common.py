from operator import itemgetter
import socket
import prolog.engine
import prolog.util
from server.hints import Hint, HintPopup
import server.problems

id = 99
number = 7
visible = True
facts = 'family_relations'

solution = '''\
sister99(X, Y) :-
  parent(P, X),
  parent(P, Y),
  female(X),
  X \== Y.

brother99(X, Y) :-
  parent(P, X),
  parent(P, Y),
  male(X),
  X \== Y.

cousin(X, Y) :-
  parent(PX, X),
  parent(PY, Y),
  ( brother99(PX, PY)
    ;
    sister99(PX, PY) ).
'''

hint_type = {
    'gender_is_irrelevant_markup': HintPopup('gender_is_irrelevant_markup'),
    'gender_is_irrelevant': Hint('gender_is_irrelevant'),
    'precedence_fail': Hint('precedence_fail'),
    'cousin_vs_sibling': Hint('cousin_vs_sibling'),
    'cousin_to_oneself': Hint('cousin_to_oneself'),
    'cousin_need_not_be_parent': Hint('cousin_need_not_be_parent'),
    'predicate_always_false': Hint('predicate_always_false'),
}

test_cases = [
    ('cousin(X, _)',
        [{'X': 'vanessa'}, {'X': 'patricia'}, {'X': 'melanie'}, {'X': 'andrew'}, {'X': 'john'}, {'X': 'susan'}]),
    ('cousin(X, john)',
        [{'X': 'susan'}]),
    ('cousin(patricia, X)',
        [{'X': 'andrew'}, {'X': 'melanie'}]),
]

def test(code, aux_code):
    n_correct = 0
    engine_id = None
    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)
        if engine_id is not None and 'error' not in map(itemgetter(0), output):
            # Engine successfully created, and no syntax error in program.
            for query, answers in test_cases:
                if prolog.engine.check_answers(engine_id, query=query, answers=answers, timeout=1.0):
                    n_correct += 1
    except socket.timeout:
        pass
    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_cases)}}]
    return n_correct, len(test_cases), hints

def hint(code, aux_code):
    tokens = prolog.util.tokenize(code)

    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)

        # gender testing is redundant
        # this is not necessarily wrong, but worth mentioning anyway
        targets = [prolog.util.Token('NAME', 'male'), prolog.util.Token('NAME', 'female')]
        marks = [(t.pos, t.pos + len(t.val)) for t in tokens if t in targets]
        if marks:
            return [{'id': 'gender_is_irrelevant_markup', 'start': m[0], 'end': m[1]} for m in marks] + \
                   [{'id': 'gender_is_irrelevant'}]

        # target predicate seems to always be false
        if not prolog.engine.ask_truth(engine_id, 'cousin(_, _)'):
            return [{'id': 'predicate_always_false'}]

        # precedence fail (AND block vs OR block)
        # case in point: parent(PX, X), parent(PY, Y), brother(PX, PY) ; sister(PX, PY)
        #  or this case: parent(PX, X), parent(PY, Y), sister(PX, PY) ; brother(PX, PY)
        # warning: knowledge base dependent
        if prolog.util.Token('SEMI', ';') in tokens and prolog.engine.ask_truth(engine_id,
            'findall(_, cousin(X, Y), L), (length(L, 14) ; length(L, 16))'):
            return [{'id': 'precedence_fail'}]

        # cousin to him/herself
        # just common grandparent is not enough
        if prolog.engine.ask_truth(engine_id, 'cousin(X, X)'):
            return [{'id': 'cousin_to_oneself'}]

        # cousin should not be brother or sister
        # common parent, probably solved via grandparent
        if prolog.engine.ask_truth(engine_id,
            'cousin(X, Y), parent(P, X), parent(P, Y)'):
            return [{'id': 'cousin_vs_sibling'}]

        # X (or Y) does not necessarily need to be a parent
        # cousin is a symmetrical relation, so X and Y are covered by this
        if prolog.engine.ask_one(engine_id,
            'cousin(X, _), \+ parent(X, _)') == 'false':
            return [{'id': 'cousin_need_not_be_parent'}]

    except socket.timeout as ex:
        pass

    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    return None

