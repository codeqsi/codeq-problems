name = 'cousin/2'
slug = 'X je bratranec ali sestrična od Y'

description = '''\
<p><code>cousin(X, Y)</code>: <code>X</code> je bratranec ali sestrična od
<code>Y</code>.</p>
<pre>
?- cousin(andrew, Y).
  Y = vanessa ;
  Y = patricia.
</pre>
<p><a target="_blank" href="[%@resource famrel.svg%]">Družinska drevesa</a> so
podana s predikati <code>parent/2</code>, <code>male/1</code> in
<code>female/1</code>.</p>
'''

plan = ['''\
<p>Verjetno bi se sedaj splačalo uporabiti rešitev kakšne prejšnje naloge?</p>
<p><img src="[%@resource Prolog_cousin_01.svg%]" alt="Jaz imam otroka, moj brat ali sestra ima tudi otroka..." /></p>
<p>Seveda se da rešiti tudi brez prejšnjih rešitev... </p>
''', '''\
<p>Če je <code>PX</code> starš od <code>X</code> in je
<code>PY</code> starš od <code>Y</code> ter sta
<code>PX</code> in <code>PY</code> brat ali sestra,
potem je <code>X</code> bratranec/sestrična od <code>Y</code>.</p>
''', '''\
<p><img src="[%@resource Prolog_cousin_03.svg%]" alt="Jaz imam otroka, moj brat ali sestra ima tudi otroka, najina otroka sta zato..." /></p>
''']

hint = {
    'gender_is_irrelevant': '''\
<p>Je spol res pomemben?</p>
''',

    'gender_is_irrelevant_markup': '''\
<p>Je to res potrebno?</p>
''',

    'precedence_fail': '''\
<p>Si morda narobe upošteval prioriteto operatorjev IN ter ALI?</p>
<p>Operator IN veže močneje od ALI; če želiš spremeniti prioriteto,
lahko uporabiš oklepaje.</p>
''',

    'cousin_vs_sibling': '''\
<p>Kako je lahko bratranec/sestrična <code>X</code> hkrati tudi brat/sestra od <code>Y</code>?
Si morda pozabil, da starša od <code>X</code> in <code>Y</code> ne smeta biti ista (pazi: če sta oba starša
navedena v bazi, potem ju lahko prolog najde kot dva različna, logično je to čisto res)?</p>
<p><img src="[%@resource Prolog_cousin_04.svg%]" /></p>
''',

    'cousin_to_oneself': '''\
<p>Kako je lahko nekdo bratranec/sestrična samemu sebi?
Imata morda <code>X</code> in <code>Y</code> istega starša?</p>
<p><img src="[%@resource Prolog_cousin_04.svg%]" /></p>
<p>Poskusi prolog vprašati tole: <code>?- cousin(X, X).</code></p>
''',

    'cousin_need_not_be_parent': '''\
<p>Bratranec/sestrična pravzaprav ne rabi imeti otrok...</p>
''',

    'predicate_always_false': '''\
<p>Vse kaže, da tvoj predikat vedno vrne "false". Si mu dal pravilno ime, si se morda pri imenu zatipkal?</p>
<p>Če je ime pravilno, se morda splača preveriti tudi, če se nisi zatipkal kje drugje,
je morda kakšna pika namesto vejice ali obratno, morda kakšna spremenljivka z malo začetnico?</p>
<p>Morda kakšnega pomožnega predikata nisi definiral ali si se zatipkal pri njegovem imenu?</p>
<p>Možno je seveda tudi, da so tvoji pogoji prestrogi ali celo nemogoči (kot bi bila npr. zahteva,
da je <code>X</code> hkrati starš in sestra od <code>Y</code>).</p>
''',

#     'interesting_tidbit': '''\
# <p>Zanimivost: nalogo bi lahko rešil tudi z uporabo rešitve za relacijo "sister".
# Teta je namreč sestra od starša od <code>Y</code>.</p>
# ''',
}

