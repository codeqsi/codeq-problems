name = 'Family relations'
description = '''\
<p>
<a target="_blank" href="[%@resource intro_en.html%]">First steps in Prolog</a>: defining rules for various family relations.
For tasks in this set a database of <a target="_blank" href="[%@resource famrel.svg%]">family trees</a> is already defined.
In Prolog, this information is represented with the predicates <code>parent/2</code>,
<code>male/1</code> and <code>female/1</code>.
</p>
'''
