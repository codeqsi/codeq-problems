name = 'sister/2'
slug = 'X je sestra od Y'

description = '''\
<p><code>sister(X, Y)</code>: <code>X</code> je (pol)sestra od <code>Y</code>. Primer:</p>
<pre>
?- sister(vanessa, Y).
  Y = patricia.
</pre>
<p><a target="_blank" href="[%@resource famrel.svg%]">Družinska drevesa</a> so
podana s predikati <code>parent/2</code>, <code>male/1</code> in
<code>female/1</code>.</p>
'''

plan = [
    '''\
<p><img src="[%@resource Prolog_sister_01.svg%]" alt="Sestra je ženskega spola in ima skupnega starša z Y." /></p>''',
    '''\
<p>Če je <code>X</code> ženska in če imata <code>X</code> in <code>Y</code> skupnega starša,
potem je <code>X</code> sestra od <code>Y</code>.</p>''',
    '''\
<p><img src="[%@resource Prolog_sister_02.svg%]" alt="Sestra X je ženskega spola in ima vsaj enega skupnega starša z Y." /></p>''',
]

hint = {
    'x_y_must_be_different': [{'message': '''\
<p><img src="[%@resource Prolog_sister_04.svg%]" /></p>
<p>Si pomislil, da sta <code>X</code> in <code>Y</code> lahko ista oseba? Poskusi naslednjo poizvedbo:</p>
<pre>
?- sister(sally, Y).
</pre>''', 'linkText': 'Kako lahko preverim, da sta <code>X</code> in <code>Y</code> različna?'},
'''\
<p>S pomočjo operatorja <code>\==</code> lahko preverim, da sta <code>X</code> in <code>Y</code> med seboj različna.
Na primer: <code>X \== Y</code></p>
'''],

    'x_must_be_female': '''\
<p><img src="[%@resource Prolog_sister_03.svg%]" /></p>
<p>Sestra je navadno ženskega spola.</p>
''',

    'y_can_be_of_any_gender': '''\
<p><code>Y</code> je pravzaprav lahko poljubnega spola.</p>
''',

    'neq+_instead_of_neq': '''\
<p>Uporabi raje operator <code>\==</code> namesto operatorjev <code>\=</code> ali <code>=\=</code>. Prvi preveri, da oba operanda nista enaka,
drugi (<code>\=</code>) ju poskusi unificirati (narediti enaka), tretji (<code>=\=</code>) pa je samo za aritmetično primerjanje
in morata oba operanda biti takoj izračunljiva aritmetična izraza.</p>
''',

    'common_parent_needed': '''\
<p><img src="[%@resource Prolog_sister_05.svg%]" /></p>
<p>Dobro bi bilo, da imata <code>X</code> in <code>Y</code> vsaj enega skupnega starša.</p>
''',

    'neq_used_too_early': '''\
<p>Si morda uporabil operator <code>\==</code> prezgodaj?</p>
<p>Poskusi ga pomakniti bolj proti koncu. Razlog za probleme je verjetno,
da spremenljivki, ki ju primerjaš, še nimata določene vrednosti in sta zato različni.
Prolog ne preverja, če kasneje morda postaneta enaki, ampak samo kakšni sta v trenutku,
ko naleti na primerjavo.</p>
<p>Morda pa preprosto preverjaš napačni spremenljivki?</p>
''',

    'predicate_always_false': '''\
<p>Vse kaže, da tvoj predikat vedno vrne "false". Si mu dal pravilno ime, si se morda pri imenu zatipkal?</p>
<p>Če je ime pravilno, se morda splača preveriti tudi, če se nisi zatipkal kje drugje,
je morda kakšna pika namesto vejice ali obratno, morda kakšna spremenljivka z malo začetnico?</p>
<p>Možno je seveda tudi, da so tvoji pogoji prestrogi ali celo nemogoči (kot bi bila npr. zahteva,
da je <code>X</code> hkrati starš in sestra od <code>Y</code>).</p>
''',

    'final_hint': '''\
<p>Pri tej nalogi si uporabil operator <code>\==</code> (oziroma <code>\=</code>). Ta preverja, če njegova operanda nista enaka točno v tistem
trenutku, ko prolog naleti nanj. Če ga daš na začetek svoje rešitve, ta ne deluje kot bi si želel.
(Če želiš, lahko to seveda preveriš.)</p>
''',
}
