from operator import itemgetter
import socket
import prolog.engine
import prolog.util
from server.hints import Hint
import server.problems

id = 96
number = 4
visible = True
facts = 'family_relations'

solution = '''\
sister(X, Y) :-
  parent(P, X),
  parent(P, Y),
  female(X),
  X \== Y.
'''

hint_type = {
    'x_must_be_female': Hint('x_must_be_female'),
    'common_parent_needed': Hint('common_parent_needed'),
    'y_can_be_of_any_gender': Hint('y_can_be_of_any_gender'),
    'x_y_must_be_different': Hint('x_y_must_be_different'),
    'neq_used_too_early': Hint('neq_used_too_early'),
    'neq+_instead_of_neq': Hint('neq+_instead_of_neq'),
    'predicate_always_false': Hint('predicate_always_false'),
    'final_hint': Hint('final_hint'),
}

test_cases = [
    ('sister(X, _)',
        [{'X': 'anna'}, {'X': 'daniela'}, {'X': 'luana'}, {'X': 'melanie'},
         {'X': 'michelle'}, {'X': 'nevia'}, {'X': 'patricia'}, {'X': 'sally'},
         {'X': 'vanessa'}]),
    ('sister(melanie, X)',
        [{'X': 'andrew'}]),
    ('sister(X, andrew)',
        [{'X': 'melanie'}]),
]

def test(code, aux_code):
    n_correct = 0
    engine_id = None
    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)
        if engine_id is not None and 'error' not in map(itemgetter(0), output):
            # Engine successfully created, and no syntax error in program.
            for query, answers in test_cases:
                if prolog.engine.check_answers(engine_id, query=query, answers=answers, timeout=1.0):
                    n_correct += 1
    except socket.timeout:
        pass
    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_cases)}}]
    if n_correct == len(test_cases):
        hints += [{'id': 'final_hint'}]
    return n_correct, len(test_cases), hints

def hint(code, aux_code):
    # how do I know which general hints were already shown?
    # how do I know enough time has elapsed for general hints to be shown?
    # how do I know whether the hint button was pressed?
    # should we have another hint button which appears in hint window (like more...)
    #   when a hint is available after testing the current version of the program
    #   and this button triggers code-specific hints (i.e. non-general ones)

    # one way to do hints is to use a hierarchy of hints...
    # similar to how some rule-based systems work
    # the trigger-testing could be easier that way as well
    # what I have in mind is:
    # e.g. when I test if X is sister to itself, I've already done other trigger-tests
    # and I don't need to do them again

    # first general hints (triggered by the hint button)
    # do we do it like this? this button triggers general hints only?
    # not sure this is ok.. discuss with others (include Janez)

    # if hbutton.pressed:
        # trigger generalhint(#buttonpress)

    # code-specific hints next

    # tokenize (and similar) only if *needed* for a given exercise
    # to reduce server processor load
    tokens = prolog.util.tokenize(code)

    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)

        # use of \= or =\= instead of \==
        if prolog.util.Token('NEQU', '\=') in tokens or prolog.util.Token('NEQA', '=\=') in tokens:
            return [{'id': 'neq+_instead_of_neq'}]

        # target predicate seems to always be false
        if not prolog.engine.ask_truth(engine_id, 'sister(_, _)'):
            return [{'id': 'predicate_always_false'}]

        # X must be female
        if prolog.engine.ask_truth(engine_id, 'male(X), sister(X, _)'):
            return [{'id': 'x_must_be_female'}]

        # X and Y must have a common parent
        if prolog.engine.ask_truth(engine_id,
            'sister(X, Y), \+ (parent(P, X), parent(P, Y))'):
            return [{'id': 'common_parent_needed'}]

        # Y can be of any gender, incl. male
        # as this is after previous hints, by this stage the code probably
        # does return some sensible results already
        # this is an example of using the ordering of hints for easier trigger checks
        if prolog.engine.ask_truth(engine_id, 'sister(_, Y)') and \
           prolog.engine.ask_one(engine_id, 'sister(_, Y), male(Y)') == 'false':
            return [{'id': 'y_can_be_of_any_gender'}]

        # X and Y must be different
        # this is the last code-specific hint (when everything else doesn't trigger anymore)
        if prolog.util.Token('NEQ', '\==') not in tokens and prolog.engine.ask_truth(engine_id,
            'setof(X, (member(X, [sally, nevia, vanessa]), sister(X, X)), L), length(L, 3)'):
            return [{'id': 'x_y_must_be_different'}]

        # X and Y must be different, but the user used '\=='
        # most likely this means that \== was used before X and Y are known
        # this is the last code-specific hint (when everything else doesn't trigger anymore)
        if prolog.util.Token('NEQ', '\==') in tokens and prolog.engine.ask_truth(engine_id,
            'setof(X/Y, sister(X, Y), L), member(nevia/nevia, L), member(sally/sally, L), member(vanessa/vanessa, L)'):
            return [{'id': 'neq_used_too_early'}]

        # last hints are connected with programming style
        # here, this means detecting such code:
        # parent(P1, X), parent(P2, Y), P1 == P2 (or P1 = P2)
        # I guess program tokens can be used for this
        # do we require that the program is correct for this?
        # or is it enough that this is after ALL code-specific hints AND it has to trigger?

        # regular expressions, anyone?

        # QUESTION to discuss:
        # where do AI hints come into play?
        # only after all code-specific and non stylistic hints are exhausted?
        # or randomly before? or?

        # QUESTION to discuss:
        # I suggest hint triggers (if code-based or also token-based?)
        # are used as CLASSES of unit tests
        # although.. this makes random counterexamples harder to implement
        # (does it?)

    except socket.timeout as ex:
        pass

    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    return []
