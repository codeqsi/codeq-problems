name = 'sister/2'
slug = 'the sister relation'

description = '''\
<p><code>sister(X, Y)</code>: <code>X</code> is a (half)sister of <code>Y</code>.</p>
<pre>
?- sister(vanessa, X).
  X = patricia.
</pre>
<p><a target="_blank" href="[%@resource famrel.svg%]">Family trees</a> are
described with predicates <code>parent/2</code>, <code>male/1</code>, and
<code>female/1</code>.</p>
'''

plan = [
    '''\
<p><img src="[%@resource Prolog_sister_01.svg%]" alt="A sister is female and has a common parent with Y." /></p>''',
    '''\
<p>If <code>X</code> is female and if <code>X</code> and <code>Y</code> share a common parent
then <code>X</code> is a sister of <code>Y</code>.</p>''',
    '''\
<p><img src="[%@resource Prolog_sister_02.svg%]" alt="Sister X is female and has
at least one parent in common with Y." /></p>''',
]

hint = {
    'x_y_must_be_different': [{'message': '''\
<p><img src="[%@resource Prolog_sister_04.svg%]" /></p>
<p>What if <code>X</code> and <code>Y</code> are the same person? Try the following query:</p>
<pre>
?- sister(sally, Y).
</pre>''', 'linkText': 'How can I check that <code>X</code> and <code>Y</code> are different?'},
'''\
<p>Using the operator <code>\==</code> I can check that <code>X</code> and <code>Y</code> are different.
For example: <code>X \== Y</code></p>
'''],

    'x_must_be_female': '''\
<p><img src="[%@resource Prolog_sister_03.svg%]" /></p>
<p>A sister is usually female.</p>
''',

    'y_can_be_of_any_gender': '''\
<p><code>Y</code> can actually be of <em>any</em> gender.</p>
''',

    'neq+_instead_of_neq': '''\
<p>Use the operator <code>\==</code> instead of operators <code>\=</code> or <code>=\=</code>.
It succeeds when the two operands are not the same,
while <code>\=</code> succeeds if the operands <em>cannot be made</em> the same (cannot be unified), and <code>=\=</code> checks for
arithmetic inequality and in this case both operands should be immediately arithmetically computable.</p>
''',

    'common_parent_needed': '''\
<p><img src="[%@resource Prolog_sister_05.svg%]" /></p>
<p>It would make sense if <code>X</code> and <code>Y</code> have at least one parent in common.</p>
''',

    'neq_used_too_early': '''\
<p>Did you use the operator <code>\==</code> too early?</p>
<p>Try moving it more towards the end of the rule. The problem is probably that the variables
you are comparing don't yet have values assigned to them and are therefore different by default.
Prolog doesn't check whether they become equal <em>later</em>, it only checks whether they're
equal or not at the moment of comparison.</p>
<p>Or maybe you're simply comparing the wrong two variables?</p>
''',

    'predicate_always_false': '''\
<p>It seems your predicate is <em>always</em> "false". Did you give it the correct name,
or is it perhaps misspelled?</p>
<p>If the name is correct, check whether something else is misspelled, perhaps there is a full stop instead of
a comma or vice versa, or maybe you typed a variable name in lowercase?</p>
<p>It is, of course, also possible that your conditions are too restrictive, or even impossible to satisfy
(as would be, for example, the condition that <code>X</code> is both a parent and a sister of <code>Y</code>, or
something similarly impossible).</p>
''',

    'final_hint': '''\
<p>In this exercise you used the <code>\==</code> (or <code>\=</code>) operator.
It succeeds if its two operands are not the same
<em>at the moment</em> of comparison. If you put this goal at the start of your rule, it will
not work as expected. (Go ahead, try and see what happens!)</p>
''',
}
