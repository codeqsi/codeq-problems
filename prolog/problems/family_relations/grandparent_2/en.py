name = 'grandparent/2'
slug = 'the grandparent relation'

description = '''\
<p><code>grandparent(P, C)</code>: <code>P</code> is a grandparent of <code>C</code>.</p>
<pre>
?- grandparent(tina, X).
  X = vanessa ;
  X = patricia.
?- grandparent(tina, vanessa).
  true.
</pre>
<p><a target="_blank" href="[%@resource famrel.svg%]">Family trees</a> are
described with predicates <code>parent/2</code>, <code>male/1</code>, and
<code>female/1</code>.</p>
'''

plan = ['''\
<p><img src="[%@resource Prolog_grandparent_01.svg%]" alt="A grandparent has a child that has a child." /></p>
''', '''\
<p>If <code>X</code> is a parent of a parent of <code>Y</code>,
then <code>X</code> is a grandparent of <code>Y</code>.</p>
''', '''\
<p><img src="[%@resource Prolog_grandparent_02.svg%]" alt="A grandparent has a child that has a child of
his or her own." /></p>
''']

hint = {
    'no_common_z': '''\
<p>Did you connect <code>X</code> and <code>Y</code> through some common person?</p>
''',

    'gender_is_irrelevant': '''\
<p>We're looking for a grandparent and a grandchild, the gender is not important.</p>
''',

    'gender_is_irrelevant_markup': '''\
<p>Is this necessary?</p>
''',

    'or_instead_of_and': '''\
<p>Did you perhaps use a semicolon (that represents logical OR) instead of a comma (logical AND)?</p>
''',

    'x_must_be_parent': '''\
<p>A grandparent surely has a child, right? So he or she is a parent of someone...</p>
<p><img src="[%@resource Prolog_grandparent_03.svg%]" </p>
''',

    'x_need_not_have_parent': '''\
<p>A parent of <code>X</code> is not really important if we're looking for a grandchild...</p>
''',

    'y_must_have_parent': '''\
<p>A grandchild <code>Y</code> must have a parent, right? And that parent is...</p>
<p><img src="[%@resource Prolog_grandparent_04.svg%]" </p>
''',

    'y_need_not_be_parent': '''\
<p>A grandchild <code>Y</code> doesn't really need to have children...</p>
''',

    'predicate_always_false': '''\
<p>It seems your predicate is <em>always</em> "false". Did you give it the correct name,
or is it perhaps misspelled?</p>
<p>If the name is correct, check whether something else is misspelled, perhaps there is a full stop instead of
a comma or vice versa, or maybe you typed a variable name in lowercase?</p>
<p>It is, of course, also possible that your conditions are too restrictive, or even impossible to satisfy
(as would be, for example, the condition that <code>X</code> is both a parent and a sister of <code>Y</code>, or
something similarly impossible).</p>
''',
}
