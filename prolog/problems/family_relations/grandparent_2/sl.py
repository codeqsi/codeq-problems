name = 'grandparent/2'
slug = 'X je dedek ali babica od Y'

description = '''\
<p><code>grandparent(X, Y)</code>: <code>X</code> je dedek ali babica od
<code>Y</code>.</p>
<pre>
?- grandparent(tina, Y).
  Y = vanessa ;
  Y = patricia.
?- grandparent(tina, vanessa).
  true.
</pre>
<p><a target="_blank" href="[%@resource famrel.svg%]">Družinska drevesa</a> so
podana s predikati <code>parent/2</code>, <code>male/1</code> in
<code>female/1</code>.</p>
'''

plan = ['''\
<p><img src="[%@resource Prolog_grandparent_01.svg%]" alt="Dedek ali babica ima otroka, ki ima otroka." /></p>
''', '''\
<p>Če je <code>X</code> starš od starša od <code>Y</code>,
potem je <code>X</code> stari starš od <code>Y</code>.</p>
''', '''\
<p><img src="[%@resource Prolog_grandparent_02.svg%]" alt="Dedek ali babica ima otroka, ki ima otroka." /></p>
''']

hint = {
    'no_common_z': '''\
<p>Si povezal <code>X</code> in <code>Y</code> preko neke skupne (iste!) osebe?</p>
''',  # TODO: Tim, tale namig.. nisem prepričan, da mi je všeč... (kako je izražen) -- morda nova slika!?

    'gender_is_irrelevant': '''\
<p>Iščemo starega starša in vnuka, spol pri tem ni pomemben.</p>
''',

    'gender_is_irrelevant_markup': '''\
<p>Je to res potrebno?</p>
''',

    'or_instead_of_and': '''\
<p>Si morda uporabil podpičje (ki pomeni ALI) namesto vejice (ki pomeni IN)?</p>
''',

    'x_must_be_parent': '''\
<p>Dedek ali babica ima gotovo kakšnega otroka, kajne? Torej je starš od nekoga...</p>
<p><img src="[%@resource Prolog_grandparent_03.svg%]" </p>
''',

    'x_need_not_have_parent': '''\
<p>Starš od <code>X</code> ni pomemben, ko iščemo vnuka...</p>
''',

    'y_must_have_parent': '''\
<p>Vnuk <code>Y</code> mora imeti starša, kajne? Ta starš pa je...</p>
<p><img src="[%@resource Prolog_grandparent_04.svg%]" </p>
''',

    'y_need_not_be_parent': '''\
<p>Vnuk <code>Y</code> pravzaprav ne rabi imeti otrok, da ima dedka ali babico...</p>
''',

    'predicate_always_false': '''\
<p>Vse kaže, da tvoj predikat vedno vrne "false". Si mu dal pravilno ime, si se morda pri imenu zatipkal?</p>
<p>Če je ime pravilno, se morda splača preveriti tudi, če se nisi zatipkal kje drugje,
je morda kakšna pika namesto vejice ali obratno, morda kakšna spremenljivka z malo začetnico?</p>
<p>Možno je seveda tudi, da so tvoji pogoji prestrogi ali celo nemogoči (kot bi bila npr. zahteva,
da je <code>X</code> hkrati starš in sestra od <code>Y</code> ali kaj podobnega).</p>
''',
}
