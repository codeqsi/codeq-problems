name = 'father/2'
slug = 'the father-child relation'

description = '''\
<p><code>father(X, Y)</code>: <code>X</code> is the father of <code>Y</code>.</p>
<pre>
?- father(thomas, william).
  true.
?- father(aleksander, X).
  X = luana ;
  X = daniela.
</pre>
<p><a target="_blank" href="[%@resource famrel.svg%]">Family trees</a> are
described with predicates <code>parent/2</code>, <code>male/1</code>, and
<code>female/1</code>.</p>
'''

plan = [
    '''\
<p><img src="[%@resource Prolog_father_01.svg%]" alt="A father is male and has a child." /></p>''',
    '''\
<p><img src="[%@resource Prolog_father_02.svg%]" alt="Father X is a parent of Y and is male." /></p>''',
    '''\
<p>If <code>X</code> is male and <code>X</code> is parent to <code>Y</code>,
then <code>X</code> is a father of <code>Y</code>.</p>'''
]

hint = {
    'or_instead_of_and': '''\
<p>Did you perhaps use a semicolon (that represents logical OR) instead of a comma (logical AND)?</p>
''',

    'or_instead_of_and_two_rules': '''\
<p>Did you perhaps write two rules: one for gender and one for parenthood?
Be careful: this is understood as one <em>or</em> the other, not as one <em>and</em> the other!</p>
''',

    'x_must_be_male': '''\
<p>A father is usually male.</p>
<p><img src="[%@resource Prolog_father_03.svg%]" /></p>
''',

    'x_must_be_parent': '''\
<p>A father is supposed to have at least one child... so he's a parent of somebody.</p>
<p><img src="[%@resource Prolog_father_04.svg%]" /></p>
''',

    'y_can_be_of_any_gender': '''\
<p><code>Y</code> can be of any gender.</p>
''',

    'y_need_not_be_parent': '''\
<p><code>Y</code> doesn't need to have children...</p>
''',

    'predicate_always_false': '''\
<p>It seems your predicate is <em>always</em> "false". Did you give it the correct name,
or is it perhaps misspelled?</p>
<p>If the name is correct, check whether something else is misspelled, perhaps there is a full stop instead of
a comma or vice versa, or maybe you typed a variable name in lowercase?</p>
<p>It is, of course, also possible that your conditions are too restrictive, or even impossible to satisfy
(as would be, for example, the condition that <code>X</code> is both a parent and a sister of <code>Y</code>, or
something similarly impossible).</p>
''',
}
