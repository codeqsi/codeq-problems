from operator import itemgetter
import socket
import prolog.engine
import prolog.util
from server.hints import Hint
import server.problems

id = 181
number = 2
visible = True
facts = 'family_relations'

solution = '''\
father(X, Y) :-
  parent(X, Y),
  male(X).
'''

hint_type = {
    'or_instead_of_and': Hint('or_instead_of_and'),
    'or_instead_of_and_two_rules': Hint('or_instead_of_and_two_rules'),
    'x_must_be_male': Hint('x_must_be_male'),
    'x_must_be_parent': Hint('x_must_be_parent'),
    'y_can_be_of_any_gender': Hint('y_can_be_of_any_gender'),
    'y_need_not_be_parent': Hint('y_need_not_be_parent'),
    'predicate_always_false': Hint('predicate_always_false'),
}

test_cases = [
    ('father(X, _)',
        [{'X': 'aleksander'}, {'X': 'aleksandr'}, {'X': 'andrew'}, {'X': 'frank'},
         {'X': 'george'}, {'X': 'jerry'}, {'X': 'john'}, {'X': 'morty'},
         {'X': 'patrick'}, {'X': 'thomas'}, {'X': 'william'}]),
    ('father(_, X)',
        [{'X': 'aleksander'}, {'X': 'anna'}, {'X': 'daniela'}, {'X': 'george'},
         {'X': 'jeffrey'}, {'X': 'jerry'}, {'X': 'joanne'}, {'X': 'kramer'},
         {'X': 'luana'}, {'X': 'michael'}, {'X': 'michelle'}, {'X': 'patricia'},
         {'X': 'sally'}, {'X': 'susan'}, {'X': 'vanessa'}, {'X': 'william'}]),
    ('father(X, aleksander)',
        [{'X': 'aleksandr'}]),
]

def test(code, aux_code):
    n_correct = 0
    engine_id = None
    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)
        if engine_id is not None and 'error' not in map(itemgetter(0), output):
            # Engine successfully created, and no syntax error in program.
            for query, answers in test_cases:
                if prolog.engine.check_answers(engine_id, query=query, answers=answers, timeout=1.0):
                    n_correct += 1
    except socket.timeout:
        pass
    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_cases)}}]
    return n_correct, len(test_cases), hints

def hint(code, aux_code):
    tokens = prolog.util.tokenize(code)

    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)

        # target predicate seems to always be false
        if not prolog.engine.ask_truth(engine_id, 'father(_, _)'):
            return [{'id': 'predicate_always_false'}]

        # OR (;) instead of AND (,)
        # this hint has to be before the next two
        # as otherwise those two would always override it
        # and not convey the same (amount of) help/information
        # warning: due to speed considerations this (48) is knowledge base dependent
        # independent: findall(_, (parent(X, Y) ; male(X)), L2)
        if prolog.util.Token('SEMI', ';') in tokens and prolog.engine.ask_truth(engine_id,
            'findall(_, father(X, Y), L), length(L, 48)'):
            return [{'id': 'or_instead_of_and'}]

        # OR instead of AND written with two rules, namely:
        #    (r1) father(X, Y):- male(X).  (r2) father(X, Y):- parent(X, Y).
        if prolog.engine.ask_truth(engine_id,
            'findall(_, father(X, Y), L), length(L, 48)'):
            return [{'id': 'or_instead_of_and_two_rules'}]

        # X must be male
        if prolog.engine.ask_truth(engine_id, 'female(X), father(X, _)'):
            return [{'id': 'x_must_be_male'}]

        # X must be a parent
        if prolog.engine.ask_truth(engine_id,
            'father(X, _), \+ parent(X, _)'):
            return [{'id': 'x_must_be_parent'}]

        # Y can be of any gender, incl. female
        if prolog.engine.ask_truth(engine_id, 'father(_, Y)') and \
           prolog.engine.ask_one(engine_id, 'father(_, Y), female(Y)') == 'false':
            return [{'id': 'y_can_be_of_any_gender'}]

        # Y does not necessarily need to be a parent
        if prolog.engine.ask_truth(engine_id, 'father(_, Y)') and \
           prolog.engine.ask_one(engine_id, 'father(_, Y), \+ parent(Y, _)') == 'false':
            return [{'id': 'y_need_not_be_parent'}]

    except socket.timeout as ex:
        pass

    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    return None
