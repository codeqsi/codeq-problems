name = 'father/2'
slug = 'X je oče od Y'

description = '''\
<p><code>father(X, Y)</code>: <code>X</code> je oče od <code>Y</code>.</p>
<pre>
?- father(thomas, william).
  true.
?- father(aleksander, Y).
  Y = luana ;
  Y = daniela.
</pre>
<p><a target="_blank" href="[%@resource famrel.svg%]">Družinska drevesa</a> so
podana s predikati <code>parent/2</code>, <code>male/1</code> in
<code>female/1</code>.</p>
'''

plan = [
    '''\
<p><img src="[%@resource Prolog_father_01.svg%]" alt="Oče ima otroka in je moškega spola." /></p>''',
    '''\
<p><img src="[%@resource Prolog_father_02.svg%]" alt="Oče X je starš od Y in je moškega spola." /></p>''',
    '''\
<p>Če je <code>X</code> moški in je hkrati <code>X</code> starš od <code>Y</code>,
potem je <code>X</code> oče od <code>Y</code>.</p>'''
]

hint = {
    'or_instead_of_and': '''\
<p>Si morda uporabil podpičje (ki pomeni ALI) namesto vejice (ki pomeni IN)?</p>
''',

    'or_instead_of_and_two_rules': '''\
<p>Si morda zapisal dve pravili, eno za spol in eno za "starševstvo"?
Pozor: velja eno ALI drugo, ne nujno eno IN drugo!</p>
''',

    'x_must_be_male': '''\
<p>Oče je navadno moškega spola.</p>
<p><img src="[%@resource Prolog_father_03.svg%]" /></p>
''',

    'x_must_be_parent': '''\
<p>Oče naj bi imel vsaj enega otroka... torej je starš od nekoga.</p>
<p><img src="[%@resource Prolog_father_04.svg%]" /></p>
''',

    'y_can_be_of_any_gender': '''\
<p><code>Y</code> je pravzaprav lahko poljubnega spola.</p>
''',

    'y_need_not_be_parent': '''\
<p><code>Y</code> pravzaprav ne rabi imeti otrok, da ima očeta...</p>
''',

    'predicate_always_false': '''\
<p>Vse kaže, da tvoj predikat vedno vrne "false". Si mu dal pravilno ime, si se morda pri imenu zatipkal?</p>
<p>Če je ime pravilno, se morda splača preveriti tudi, če se nisi zatipkal kje drugje,
je morda kakšna pika namesto vejice ali obratno, morda kakšna spremenljivka z malo začetnico?</p>
<p>Možno je seveda tudi, da so tvoji pogoji prestrogi ali celo nemogoči (kot bi bila npr. zahteva,
da je <code>X</code> hkrati starš in sestra od <code>Y</code>).</p>
''',
}
