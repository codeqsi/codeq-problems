name = 'Družinske relacije'
description = '''\
<p>
<a target="_blank" href="[%@resource intro_sl.html%]">Prvi koraki v prologu</a>
– pisanje pravil za različne družinske relacije. Za naloge v tem sklopu je že
definirana baza podatkov o
<a target="_blank" href="[%@resource famrel.svg%]">družinskih drevesih</a>.
V prologu so te informacije predstavljene s predikati <code>parent/2</code>,
<code>male/1</code> in <code>female/1</code>.
</p>
'''
