from operator import itemgetter
import socket
import prolog.engine
import prolog.util
from server.hints import Hint
import server.problems

id = 98
number = 6
visible = True
facts = 'family_relations'

solution = '''\
sister98(X, Y) :-
  parent(P, X),
  parent(P, Y),
  female(X),
  X \== Y.

aunt(X, Y) :-
  sister98(X, Z),
  parent(Z, Y).
'''

hint_type = {
    'x_and_y_mixed_up': Hint('x_and_y_mixed_up'),
    'precedence_fail': Hint('precedence_fail'),
    'x_must_have_sibling': Hint('x_must_have_sibling'),
    'x_must_be_female': Hint('x_must_be_female'),
    'y_must_have_parent': Hint('y_must_have_parent'),
    'aunt_vs_mother': Hint('aunt_vs_mother'),
    'x_need_not_be_parent': Hint('x_need_not_be_parent'),
    'y_need_not_be_parent': Hint('y_need_not_be_parent'),
    'predicate_always_false': Hint('predicate_always_false'),
    'nephews_parent_can_be_male': Hint('nephews_parent_can_be_male'),
}

test_cases = [
    ('aunt(X, _)',
        [{'X': 'sally'}, {'X': 'melanie'}, {'X': 'vanessa'}, {'X': 'patricia'}]),
    ('aunt(_, X)',
        [{'X': 'vanessa'}, {'X': 'patricia'}, {'X': 'joanne'}, {'X': 'john'}, {'X': 'susan'}]),
    ('aunt(sally, X)',
        [{'X': 'vanessa'}, {'X': 'patricia'}]),
]

def test(code, aux_code):
    n_correct = 0
    engine_id = None
    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)
        if engine_id is not None and 'error' not in map(itemgetter(0), output):
            # Engine successfully created, and no syntax error in program.
            for query, answers in test_cases:
                if prolog.engine.check_answers(engine_id, query=query, answers=answers, timeout=1.0):
                    n_correct += 1
    except socket.timeout:
        pass
    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_cases)}}]
    return n_correct, len(test_cases), hints

def hint(code, aux_code):
    tokens = prolog.util.tokenize(code)

    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)

        # target predicate seems to always be false
        if not prolog.engine.ask_truth(engine_id, 'aunt(_, _)'):
            return [{'id': 'predicate_always_false'}]

        # X and Y mixed up
        # warning: knowledge base dependent
        # independent: match on findall(X/Y, (sister(Y, P), parent(P, X)), L)
        if prolog.engine.ask_truth(engine_id,
            'setof(X/Y, aunt(X, Y), [alessandro/daniela, alessandro/luana, jeffrey/andrew, \
             jeffrey/melanie, patricia/susan, vanessa/john, william/andrew, william/melanie])') or \
           prolog.engine.ask_truth(engine_id,
            'setof(X/Y, aunt(X, Y), [joanne/melanie, patricia/sally, susan/patricia, vanessa/sally])'):
            return [{'id': 'x_and_y_mixed_up'}]

        # precedence fail (AND block vs OR block)
        # case in point: female(X), parent(P, Y), brother(P, X) ; sister(P, X)
        # warning: knowledge base dependent
        if prolog.util.Token('SEMI', ';') in tokens and prolog.engine.ask_truth(engine_id,
            'findall(_, aunt(X, Y), L), length(L, 15)'):
            return [{'id': 'precedence_fail'}]

        # X must be female
        if prolog.engine.ask_truth(engine_id,
            'aunt(X, _), male(X)'):
            return [{'id': 'x_must_be_female'}]

        # nephew's parent can also be male
        # this mistake comes from: parent(P, Y), sister(P, X)...
        if prolog.engine.ask_truth(engine_id,
            'setof(X/Y, aunt(X, Y), [patricia/susan, vanessa/john])'):
            return [{'id': 'nephews_parent_can_be_male'}]

        # Y must have a parent
        # perhaps risky as only one such nephew exists in DB (susan)
        if prolog.engine.ask_truth(engine_id,
            'aunt(_, Y), \+ parent(_, Y)'):
            return [{'id': 'y_must_have_parent'}]

        # X and P can be the same person
        # this can occur if the problem is not solved using sister/2
        if prolog.engine.ask_truth(engine_id,
            'aunt(X, Y), female(X), parent(X, Y)'):
            return [{'id': 'aunt_vs_mother'}]

        # X must have sibling
        if prolog.engine.ask_truth(engine_id,
            'aunt(X, _), \+ (sister(X, _) ; brother(X, _))'):       # TODO: Tim, can I use sister/brother predicates here? Or sister98?
            return [{'id': 'x_must_have_sibling'}]

        # X does not necessarily need to be a parent
        # perhaps risky as only one aunt that is not a parent exists in DB (melanie)
        if prolog.engine.ask_one(engine_id,
            'aunt(X, _), \+ parent(X, _)') == 'false':
            return [{'id': 'x_need_not_be_parent'}]

        # Y does not necessarily need to be a parent
        # perhaps risky as only one such nephew exists in DB (susan)
        if prolog.engine.ask_one(engine_id,
            'aunt(_, Y), \+ parent(Y, _)') == 'false':
            return [{'id': 'y_need_not_be_parent'}]

    except socket.timeout as ex:
        pass

    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    return None

