name = 'aunt/2'
slug = 'X je teta od Y'

description = '''\
<p><code>aunt(X, Y)</code>: <code>X</code> je teta od <code>Y</code>.</p>
<pre>
?- aunt(sally, Y).
  Y = vanessa ;
  Y = patricia.
</pre>
<p><a target="_blank" href="[%@resource famrel.svg%]">Družinska drevesa</a> so
podana s predikati <code>parent/2</code>, <code>male/1</code> in
<code>female/1</code>.</p>
'''

plan = ['''\
<p>Morda lahko uporabiš rešitev kakšne prejšnje naloge?</p>
<p><img src="[%@resource Prolog_aunt_06.svg%]" alt="Moj brat ali sestra ima otroka..." /></p>
''', '''\
<p>Če je <code>X</code> sestra od starša od <code>Y</code>,
potem je <code>X</code> teta od <code>Y</code>.</p>
''', '''\
<p><img src="[%@resource Prolog_aunt_07.svg%]" alt="Sestra od starša od Y, je teta od Y." /></p>
''']

hint = {
    'x_and_y_mixed_up': '''\
<p><code>X</code> mora biti teta od <code>Y</code>, ne obratno.</p>
''',

    'precedence_fail': '''\
<p>Si morda narobe upošteval prioriteto operatorjev IN ter ALI?</p>
<p>Operator IN veže močneje od ALI, če želiš spremeniti prioriteto,
lahko uporabiš oklepaje.</p>
''',

    'x_must_have_sibling': '''\
<p>Teta ima navadno brata ali sestro...</p>
<p><img src="[%@resource Prolog_aunt_02.svg%]" /></p>
''',

    'x_must_be_female': '''\
<p>Teta je navadno ženskega spola.</p>
<p><img src="[%@resource Prolog_aunt_05.svg%]" /></p>
''',  # TODO: Loni should mark female gender on some aunt hints!

    'y_must_have_parent': '''\
<p>Verjetno bi bilo smiselno, da ima nečak kakšnega starša?</p>
<p><img src="[%@resource Prolog_aunt_01.svg%]" /></p>
''',

    'aunt_vs_mother': '''\
<p>Kako je teta lahko hkrati še mama od <code>Y</code>?
Si morda pozabil, da teta in starš od <code>Y</code> ne smeta biti ista oseba?</p>
<p><img src="[%@resource Prolog_aunt_04.svg%]" /></p>
''',

    'x_need_not_be_parent': '''\
<p><code>X</code> pravzaprav ne rabi imeti otrok, da je lahko teta od nekoga...</p>
''',

    'y_need_not_be_parent': '''\
<p>Nečak ali nečakinja <code>Y</code> pravzaprav ne rabi imeti otrok, da ima teto...</p>
''',

    'nephews_parent_can_be_male': '''\
<p>Starš od nečaka <code>Y</code> je lahko tudi moškega spola
(kar pomeni, da ne more biti sestra od <code>X</code>).</p>
''',

    'predicate_always_false': '''\
<p>Vse kaže, da tvoj predikat vedno vrne "false". Si mu dal pravilno ime, si se morda pri imenu zatipkal?</p>
<p>Če je ime pravilno, se morda splača preveriti tudi, če se nisi zatipkal kje drugje,
je morda kakšna pika namesto vejice ali obratno, morda kakšna spremenljivka z malo začetnico?</p>
<p>Možno je seveda tudi, da so tvoji pogoji prestrogi ali celo nemogoči (kot bi bila npr. zahteva,
da je <code>X</code> hkrati starš in sestra od <code>Y</code>).</p>
''',

    'interesting_tidbit': '''\
<p>Zanimivost: nalogo bi lahko rešil tudi z uporabo rešitve za relacijo "sister".
Teta je namreč sestra od starša od <code>Y</code>.</p>
<p><img src="[%@resource Prolog_aunt_07.svg%]" /></p>
''',    # TODO: only trigger if sister is not part of correct solution
}

