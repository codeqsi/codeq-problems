name = 'aunt/2'
slug = 'the aunt relation'

description = '''\
<p><code>aunt(X, Y)</code>: <code>X</code> is an aunt of <code>Y</code>.</p>
<pre>
?- aunt(sally, X).
  X = vanessa ;
  X = patricia.
</pre>
<p><a target="_blank" href="[%@resource famrel.svg%]">Family trees</a> are
described with predicates <code>parent/2</code>, <code>male/1</code>, and
<code>female/1</code>.</p>
'''

plan = ['''\
<p>Perhaps you can make use of the solution to one of the previous exercises?</p>
<p><img src="[%@resource Prolog_aunt_06.svg%]" alt="My sibling has a child..." /></p>
''', '''\
<p>If <code>X</code> is a sister of the parent of <code>Y</code>,
then <code>X</code> is the aunt of <code>Y</code>.</p>
''', '''\
<p><img src="[%@resource Prolog_aunt_07.svg%]" alt="A sister of Y's parent is the aunt of Y." /></p>
''']

hint = {
    'x_and_y_mixed_up': '''\
<p><code>X</code> should be the aunt of <code>Y</code>, not the other way around.</p>
''',

    'precedence_fail': '''\
<p>Did you mix up the precedence of operators <em>or</em> & <em>and</em>?</p>
<p>Operator <em>and</em> has higher precedence than operator <em>or</em>.
If you wish to change that you can use brackets (as usual in mathematics).</p>
''',

    'x_must_have_sibling': '''\
<p>An aunt usually has a sibling...</p>
<p><img src="[%@resource Prolog_aunt_02.svg%]" /></p>
''',

    'x_must_be_female': '''\
<p>An aunt is usually female.</p>
<p><img src="[%@resource Prolog_aunt_05.svg%]" /></p>
''',  # TODO: Loni should mark female gender on some aunt hints!

    'y_must_have_parent': '''\
<p>It would probably make sense if the nephew has a parent, right?</p>
<p><img src="[%@resource Prolog_aunt_01.svg%]" /></p>
''',

    'aunt_vs_mother': '''\
<p>How can an aunt also be the mother of <code>Y</code>?
Did you forget to specify that an aunt and the parent of <code>Y</code> cannot be the same person?</p>
<p><img src="[%@resource Prolog_aunt_04.svg%]" /></p>
''',

    'x_need_not_be_parent': '''\
<p><code>X</code> doesn't need to have children to be somebody's aunt...</p>
''',

    'y_need_not_be_parent': '''\
<p>A nephew <code>Y</code> doesn't need to have children...</p>
''',

    'nephews_parent_can_be_male': '''\
<p>A parent of nephew <code>Y</code> can also be male
(which means that this parent cannot be a sister of <code>X</code>).</p>
''',

    'predicate_always_false': '''\
<p>It seems your predicate is <em>always</em> "false". Did you give it the correct name,
or is it perhaps misspelled?</p>
<p>If the name is correct, check whether something else is misspelled, perhaps there is a full stop instead of
a comma or vice versa, or maybe you typed a variable name in lowercase?</p>
<p>It is, of course, also possible that your conditions are too restrictive, or even impossible to satisfy
(as would be, for example, the condition that <code>X</code> is both a parent and a sister of <code>Y</code>, or
something similarly impossible).</p>
''',

    'interesting_tidbit': '''\
<p>Interesting tidbit. This exercise could also be solved using the solution to the <code>sister/2</code>
relation. An aunt is a sister of some parent of <code>Y</code>.</p>
<p><img src="[%@resource Prolog_aunt_07.svg%]" /></p>
''',    # TODO: only trigger if sister is not part of correct solution
}

