from operator import itemgetter
import prolog.engine
import server.problems

id = 102
number = 9
visible = False
facts = 'family_relations'

solution = '''\
connected(X, X, _).
connected(X, Y, N) :-
  N > 0,
  N1 is N - 1,
  ( parent(X, Z)
    ;
    parent(Z, X) ),
  connected(Z, Y, N1).
'''

test_cases = [
#    ('descendant(anna, X)',
#        [{'X': 'jerry'}, {'X': 'elaine'}, {'X': 'morty'}, {'X': 'helen'}]),
#    ('descendant(daniela, X)',
#        [{'X': 'nevia'}, {'X': 'aleksander'}, {'X': 'margaret'}, {'X': 'ana'}, {'X': 'aleksandr'}]),
#    ('descendant(X, william)',
#        [{'X': 'vanessa'}, {'X': 'patricia'}, {'X': 'susan'}, {'X': 'john'}, {'X': 'michael'}, {'X': 'michelle'}]),
]

def test(code, aux_code):
    n_correct = 0
    engine_id = None
    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)
        if engine_id is not None and 'error' not in map(itemgetter(0), output):
            # Engine successfully created, and no syntax error in program.
            for query, answers in test_cases:
                if prolog.engine.check_answers(engine_id, query=query, answers=answers, timeout=1.0):
                    n_correct += 1
    except socket.timeout:
        pass
    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_cases)}}]
    return n_correct, len(test_cases), hints
