name = 'connected/3'
slug = 'check if two people are connected in the family tree'

description = '''\
<p><code>connected(X, Y, N)</code>: <code>X</code> and <code>Y</code> are connected with a series of (no more than <code>N</code>) parent/child relations.</p>
<pre>
?- connected(ana, morty, 10).
  false.
?- connected(ana, margaret, 10).
  true.
</pre>'''

hint = {}
