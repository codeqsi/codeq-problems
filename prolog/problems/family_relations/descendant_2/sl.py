name = 'descendant/2'
slug = 'the descendant relation'

description = '''\
<p><code>descendant(X, Y)</code>: <code>X</code> je potomec (otrok, vnuk,
pravnukinja, ...) od <code>Y</code>.</p>
<pre>
?- descendant(patricia, Y).
  Y = william ;
  Y = tina ;
  Y = thomas.
</pre>
<p><a target="_blank" href="[%@resource famrel.svg%]">Družinska drevesa</a> so
podana s predikati <code>parent/2</code>, <code>male/1</code> in
<code>female/1</code>.</p>
'''

plan = ['''\
<p>Brez rekurzije ne bo šlo... kako lahko problem prevedem na (en korak) manjši problem?</p>
<p><img src="[%@resource Prolog_descendant_03.svg%]" alt="Potomec od nekoga, ki je otrok od Y, je tudi potomec od Y." /></p>
''', '''\
<p>Če je nek <code>Z</code> starš od <code>X</code> in je
ta <code>Z</code> hkrati potomec od <code>Y</code>,
potem je tudi <code>X</code> potomec od <code>Y</code>.</p>
''', '''\
<p><img src="[%@resource Prolog_descendant_04.svg%]" alt="Če je X potomec od Z, ki je otrok od Y, potem je X tudi potomec od Y." /></p>
''']

hint = {
    'gender_is_irrelevant': '''\
<p>Je spol res pomemben?</p>
''',

    'gender_is_irrelevant_markup': '''\
<p>Je to res potrebno?</p>
''',

    'grandparent_used': '''\
<p>Rešitev z "grandparent" bo premalo splošna, poskusi nadomestiti to z rekurzijo.
Skratka, poskusi prevesti na "manjši" problem, npr. potomec v enem koraku manj
(en korak bližji potomec)...</p>
''',

    'grandparent_used_markup': '''\
<p>Bi se dalo rešiti brez tega?</p>
''',

    'base_case': '''\
<p>Si pomislil na robni pogoj? Rekurzija se mora enkrat tudi ustaviti.
Kaj je najbolj enostaven par (potomec, prednik)?</p>
<p><img src="[%@resource Prolog_descendant_01.svg%]" /></p>
''',    # TODO: morda ta hint naredim z "more": najprej tekst, ob kliku pa še slika... (sicer je preveč očitno)

    'descendant_of_oneself': '''\
<p>Kako je lahko nekdo potomec samega sebe? Iz trenutne verzije rešitve se da izpeljati tudi to.
Premisli, morda se ti splača tudi grafično skicirati tvojo trenutno rešitev.</p>
''',

    'descendant_of_oneself_with_or': '''\
<p>Kako je lahko nekdo potomec samega sebe? Iz trenutne verzije rešitve se da izpeljati tudi to.
Premisli, morda se ti splača tudi grafično skicirati tvojo trenutno rešitev.</p>
<p>Morda se ti splača preveriti tudi uporabo podpičja. To praktično naredi dva ločena stavka oz. veji
(eno ali drugo velja, morda tudi oboje). Vendar pazi, ker sta ti dve veji med seboj neodvisni
-- vrednosti iz ene se ne prenašajo v drugo vejo.</p>
''',

    'descendant_need_not_be_parent': '''\
<p>Potomec <code>X</code> pravzaprav ne rabi imeti otrok...</p>
''',

    'predicate_always_false': '''\
<p>Vse kaže, da tvoj predikat vedno vrne "false". Si mu dal pravilno ime, si se morda pri imenu zatipkal?</p>
<p>Če je ime pravilno, se morda splača preveriti tudi, če se nisi zatipkal kje drugje,
je morda kakšna pika namesto vejice ali obratno, morda kakšna spremenljivka z malo začetnico?</p>
<p>Možno je seveda tudi, da so tvoji pogoji prestrogi ali celo nemogoči (kot bi bila npr. zahteva,
da je <code>X</code> hkrati starš in sestra od <code>Y</code> ali kaj podobnega).</p>
''',

    'timeout': '''\
<p>Je morda na delu potencialno neskončna rekurzija? Kako se bo ustavila?</p>
<p>Morda pa je kriv tudi manjkajoč, neustrezen ali preprosto nekompatibilen (s splošnim primerom) robni pogoj?</p>
''',

    'wrong_direction': '''\
<p>Si morda sprogramiral ravno obratno in zamenjal prednika s potomcem?
<code>X</code> naj bo potomec od <code>Y</code> in ne obratno!</p>
<p><img src="[%@resource Prolog_descendant_02.svg%]" /></p>
''',    # TODO: Tim, kako bi lahko še query poslali med parametri v Hint dict? Je to predvideno? Tukaj bi recimo pasalo...

    'final_hint': '''\
<p>Zanimivost: nalogo bi lahko rešil tudi z uporabo rešitve za relacijo <code>ancestor/2</code>.
Samo obrni spremenljivki <code>X</code> in <code>Y</code>;
če je <code>X</code> potomec od <code>Y</code>, potem je <code>Y</code> prednik od <code>X</code>.</p>
''',
}

