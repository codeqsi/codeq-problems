name = 'descendant/2'
slug = 'the descendant relation'

description = '''\
<p><code>descendant(X, Y)</code>: <code>X</code> is a descendant (child, grandchild, great-grandchild, ...)
of <code>Y</code>.</p>
<pre>
?- descendant(patricia, X).
  X = william ;
  X = tina ;
  X = thomas.
</pre>
<p><a target="_blank" href="[%@resource famrel.svg%]">Family trees</a> are
described with predicates <code>parent/2</code>, <code>male/1</code>, and
<code>female/1</code>.</p>
'''

plan = ['''\
<p>I believe recursion is needed... how can we reduce the problem into a smaller one
(a single step smaller that is)?</p>
<p><img src="[%@resource Prolog_descendant_03.svg%]" alt="A descendant of someone, who is a child of Y,
is also a descendant of Y." /></p>
''', '''\
<p>If <code>Z</code> is a parent of some <code>X</code> and at the same time
<code>Z</code> is a descendant of <code>Y</code>,
then <code>X</code> is also a descendant of <code>Y</code>.</p>
''', '''\
<p><img src="[%@resource Prolog_descendant_04.svg%]" alt="If X is a descendant of Z, and Z is a child of Y,
then X is also a descendant of Y." /></p>
''']

hint = {
    'gender_is_irrelevant': '''\
<p>Is gender really important?</p>
''',

    'gender_is_irrelevant_markup': '''\
<p>Is this necessary?</p>
''',

    'grandparent_used': '''\
<p>The solution using the grandparent relation will not be general enough, try using recursion instead.
Try to reduce the problem into a "smaller" one, e.g. a descendant in one step less (a closer descendant).</p>
''',

    'grandparent_used_markup': '''\
<p>Can you solve without using this?</p>
''',

    'base_case': '''\
<p>Did you think of a base case? The recursion has to stop at some point.
What is the most obvious pair (descendand, ancestor)?</p>
<p><img src="[%@resource Prolog_descendant_01.svg%]" /></p>
''',    # TODO: morda ta hint naredim z "more": najprej tekst, ob kliku pa še slika... (sicer je preveč očitno)

    'descendant_of_oneself': '''\
<p>How can anyone be a descendant of him- or herself? This can be inferred from the current version of the program.
It might also help to graphically sketch your current solution.</p>
''',

    'descendant_of_oneself_with_or': '''\
<p>How can anyone be a descendant of him- or herself? This can be inferred from the current version of the program.
It might help to graphically sketch your current solution.</p>
<p>Perhaps you should also check your use of the semicolon. The semicolon basically creates two separate clauses or
branches (one or the other can hold, or even both). However, be careful, as this two branches are independent of
one another -- assigned values to variables in one branch do not transfer to the other branch.</p>
''',

    'descendant_need_not_be_parent': '''\
<p>The descendant <code>X</code> doesn't actually need to have any children...</p>
''',

    'predicate_always_false': '''\
<p>It seems your predicate is <em>always</em> "false". Did you give it the correct name,
or is it perhaps misspelled?</p>
<p>If the name is correct, check whether something else is misspelled, perhaps there is a full stop instead of
a comma or vice versa, or maybe you typed a variable name in lowercase?</p>
<p>It is, of course, also possible that your conditions are too restrictive, or even impossible to satisfy
(as would be, for example, the condition that <code>X</code> is both a parent and a sister of <code>Y</code>, or
something similarly impossible).</p>
''',

    'timeout': '''\
<p>Is there an infinite recursion at work here? How will it ever stop?</p>
<p>Or perhaps is there a missing, faulty, or simply incompatible (with the general recursive case) base case?</p>
''',

    'wrong_direction': '''\
<p>Did you perhaps program exactly the reverse relation, ancestor instead of descendant?
<code>X</code> should be a descendant of <code>Y</code> and not the other way around!</p>
<p><img src="[%@resource Prolog_descendant_02.svg%]" /></p>
''',    # TODO: Tim, kako bi lahko še query poslali med parametri v Hint dict? Je to predvideno? Tukaj bi recimo pasalo...

    'final_hint': '''\
<p>Interesting tidbit. This exercise could also be solved using the solution to the <code>ancestor/2</code>
relation. Simply reverse the variables <code>X</code> and <code>Y</code>;
if <code>X</code> is a descendant of <code>Y</code>, then <code>Y</code> is an ancestor of <code>X</code>.</p>
''',
}

