name = 'joindigits/2'
slug = 'Združi sosednja števila v seznamu na vse možne načine'

description = '''\
<p><code>joindigits(L, NL)</code>: seznam <code>NL</code> dobimo iz seznama <code>L</code> tako, da poljubno združujemo sosednja števila. Predikat naj vrača vse možnosti, eno po eno.</p>
<pre>
?- joindigits([3,2,4], NL).
  NL = [324] ;
  NL = [32,4] ;
  NL = [3,24] ;
  NL = [3,2,4].
</pre>'''

hint = {}
