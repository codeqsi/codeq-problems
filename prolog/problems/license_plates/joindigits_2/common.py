from operator import itemgetter
import prolog.engine
import server.problems

id = 145
number = 20
visible = True
facts = None

solution = '''\
joindigits([X], [X]).
joindigits([X,Y|T], NT) :-
  XY is 10*X + Y,
  joindigits([XY|T], NT).
joindigits([X,Y|T], [X|NT]) :-
  joindigits([Y|T], NT).
'''

test_cases = [
    ('joindigits([2, 3, 1, 4], X)',
        [{'X': '[2, 3, 1, 4]'}, {'X': '[2, 3, 14]'}, {'X': '[2, 31, 4]'}, {'X': '[2, 314]'},
         {'X': '[23, 1, 4]'}, {'X': '[23, 14]'}, {'X': '[231, 4]'}, {'X': '[2314]'}]),
    ('joindigits([2, 3, 1], X)',
        [{'X': '[2, 3, 1]'}, {'X': '[2, 31]'}, {'X': '[23, 1]'}, {'X': '[231]'}]),
]

def test(code, aux_code):
    n_correct = 0
    engine_id = None
    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)
        if engine_id is not None and 'error' not in map(itemgetter(0), output):
            # Engine successfully created, and no syntax error in program.
            for query, answers in test_cases:
                if prolog.engine.check_answers(engine_id, query=query, answers=answers, timeout=1.0):
                    n_correct += 1
    except socket.timeout:
        pass
    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_cases)}}]
    return n_correct, len(test_cases), hints

def hint(code, aux_code):
    # TODO
    return []
