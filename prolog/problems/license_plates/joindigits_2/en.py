name = 'joindigits/2'
slug = 'join adjacent numbers in a list'

description = '''\
<p><code>joindigits(L, NL)</code>: the list <code>NL</code> is obtained from <code>L</code> by arbitrarily joining neighboring digits. Your code should generate all valid solutions.</p>
<pre>
?- joindigits([3,2,4], NL).
  NL = [324] ;
  NL = [32,4] ;
  NL = [3,24] ;
  NL = [3,2,4].
</pre>'''

hint = {}
