from operator import itemgetter
import prolog.engine
import server.problems

id = 148
number = 50
visible = True
facts = None

solution = '''\
conc148([], L, L).
conc148([H|T], L2, [H|L]) :-
  conc148(T, L2, L).

memb148(X, [X|_]).
memb148(X, [_|T]) :-
  memb148(X, T).

getdigits148([], []).
getdigits148([X|T], [X|NT]) :-
  number(X), !,
  getdigits148(T, NT).
getdigits148([_|T], NT) :-
  getdigits148(T, NT).

joindigits148([X], [X]).
joindigits148([X,Y|T], NT) :-
  XY is 10*X + Y,
  joindigits148([XY|T], NT).
joindigits148([X,Y|T], [X|NT]) :-
  joindigits148([Y|T], NT).

genexp148([Exp], Exp).
genexp148(L, Exp) :-
  conc148(Before, [N1,N2|After], L),
  memb148(Op, ['+','-','*','/']),
  NExp =.. [Op, N1, N2],
  conc148(Before, [NExp|After], L1),
  genexp148(L1, Exp).

firstMinus148(L, L).
firstMinus148([X|T], [Y|T]) :-
  Y is -X.

checkLicensePlate(LP, E1, E2) :-
  getdigits148(LP, Digs),
  conc148(L1, L2, Digs),
  joindigits148(L1, N1),
  joindigits148(L2, N2),
  firstMinus148(N1, MN1),
  firstMinus148(N2, MN2),
  genexp148(MN1, E1),
  genexp148(MN2, E2),
  E1 =:= E2.
'''

test_cases = [
    ('''checkLicensePlate([l, j, l, 3, '-', 2, 4, 2], E1, E2)''',
        [{'E1': '-3', 'E2': '(-2-4)/2'}, {'E1': '3', 'E2': '(2+4)/2'},
         {'E1': '-3*2', 'E2': '-4-2'}, {'E1': '3*2', 'E2': '4+2'},
         {'E1': '-3*2+4', 'E2': '-2'}, {'E1': '3*2-4', 'E2': '2'}]),
    ('''checkLicensePlate([k, r, a, 3, '-', 2, 1, 7], E1, E2)''',
        [{'E1': '-3', 'E2': '-21/7'}, {'E1': '3', 'E2': '21/7'},
         {'E1': '-3*2', 'E2': '1-7'}, {'E1': '3*2', 'E2': '-1+7'},
         {'E1': '3*2+1', 'E2': '7'}, {'E1': '-3*2-1', 'E2': '-7'}]),
    ('''checkLicensePlate([g, o, c, 8, '-', 2, 1, 3], E1, E2)''',
        [{'E1': '-8', 'E2': '-2*(1+3)'}, {'E1': '8', 'E2': '2*(1+3)'},
         {'E1': '-8/2+1', 'E2': '-3'}, {'E1': '8/2-1', 'E2': '3'},
         {'E1': '-8/2', 'E2': '-1-3'}, {'E1': '8/2', 'E2': '1+3'}]),
]

def test(code, aux_code):
    n_correct = 0
    engine_id = None
    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)
        if engine_id is not None and 'error' not in map(itemgetter(0), output):
            # Engine successfully created, and no syntax error in program.
            for query, answers in test_cases:
                if prolog.engine.check_answers(engine_id, query=query, answers=answers, timeout=1.0):
                    n_correct += 1
    except socket.timeout:
        pass
    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_cases)}}]
    return n_correct, len(test_cases), hints

def hint(code, aux_code):
    # TODO
    return []
