name = 'checkLicensePlate/3'
slug = 'check if the numbers in a license plate form an equation'

description = '''\
<p><code>checkLicensePlate(LP, E1, E2)</code>: the digits in the list <code>LP</code> can be combined into a valid equation <code>E1</code> = <code>E2</code>. <code>E1</code> and <code>E2</code> are arithmetic expressions, obtained from sublists of <code>Plate</code> by inserting arithmetic operators (<code>+</code>, <code>-</code>, <code>*</code> and <code>/</code>) between elements. An additional unary minus can be inserted before the leftmost number of <code>E1</code> and <code>E2</code>.</p>
<pre>
?- checkLicensePlate([l,j,l,3,-,2,1,7], E1, E2).
  E1 = 3,       E2 = 21/7 ;
  E1 = -3,      E2 = -21/7 ;
  E1 = 3*2,     E2 = -1+7 ;
  E1 = -3*2,    E2 = 1-7 ;
  E1 = 3*2+1,   E2 = 7 ;
  E1 = -3*2-1,  E2 = -7.
</pre>'''

hint = {}
