name = 'checkLicensePlate/3'
slug = 'Preveri, če lahko iz številk v registrski tablici sestaviš veljavno enačbo'

description = '''\
<p><code>checkLicensePlate(LP, E1, E2)</code>: iz cifer v seznamu <code>LP</code> se da sestaviti veljavno enačbo <code>E1</code> = <code>E2</code>. <code>E1</code> in <code>E2</code> sta aritmetična izraza, sestavljena iz podseznamov <code>Plate</code> z uporabo aritmetičnih operatorjev (<code>+</code>, <code>-</code>, <code>*</code> and <code>/</code>). Dodaten unarni minus se lahko vstavi na začetek izrazov <code>E1</code> in <code>E2</code>.</p>
<pre>
?- checkLicensePlate([l,j,l,3,-,2,1,7], E1, E2).
  E1 = 3,       E2 = 21/7 ;
  E1 = -3,      E2 = -21/7 ;
  E1 = 3*2,     E2 = -1+7 ;
  E1 = -3*2,    E2 = 1-7 ;
  E1 = 3*2+1,   E2 = 7 ;
  E1 = -3*2-1,  E2 = -7.
</pre>'''

hint = {}
