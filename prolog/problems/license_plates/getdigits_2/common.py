from operator import itemgetter
import prolog.engine
import server.problems

id = 144
number = 10
visible = True
facts = None

solution = '''\
getdigits([], []).
getdigits([X|T], [X|NT]) :-
  number(X), !,
  getdigits(T, NT).
getdigits([_|T], NT) :-
  getdigits(T, NT).
'''

test_cases = [
    ('getdigits([], X)',
        [{'X': '[]'}]),
    ('''getdigits([1, [], 2, a, b, -3, '+', 4], X)''',
        [{'X': '[1, 2, -3, 4]'}]),
    ('''getdigits([l, j, k, 3, '-', 2, 4, 2], X)''',
        [{'X': '[3, 2, 4, 2]'}]),
]

def test(code, aux_code):
    n_correct = 0
    engine_id = None
    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)
        if engine_id is not None and 'error' not in map(itemgetter(0), output):
            # Engine successfully created, and no syntax error in program.
            for query, answers in test_cases:
                if prolog.engine.check_answers(engine_id, query=query, answers=answers, timeout=1.0):
                    n_correct += 1
    except socket.timeout:
        pass
    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_cases)}}]
    return n_correct, len(test_cases), hints

def hint(code, aux_code):
    # TODO
    return []
