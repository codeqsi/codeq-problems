name = 'getdigits/2'
slug = 'remove non-numeric elements from a list'

description = '''\
<p><code>getdigits(L, DL)</code>: the list <code>DL</code> contains the numeric elements of <code>L</code>, in the same order as in the original list.</p>
<pre>
?- getdigits([2,3,e,-,4,b], DL).
  DL = [2,3,4].
</pre>'''

hint = {}
