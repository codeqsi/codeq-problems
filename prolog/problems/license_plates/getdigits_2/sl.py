name = 'getdigits/2'
slug = 'Vrni vse številke iz podanega seznama'

description = '''\
<p><code>getdigits(L, DL)</code>: seznam <code>DL</code> vsebuje vse številke iz seznama <code>L</code>, vrstni red elementov se ohrani.</p>
<pre>
?- getdigits([2,3,e,-,4,b], DL).
  DL = [2,3,4].
</pre>'''

hint = {}
