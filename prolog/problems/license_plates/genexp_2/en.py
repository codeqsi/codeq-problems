name = 'genexp/2'
slug = 'generate an arithmetic expression from a list'

description = '''\
<p><code>genexp(L, E)</code>: the expression <code>E</code> is obtained from the list <code>L</code> by inserting arithmetic operators between list elements. Your code should generate all valid solutions.</p>
<pre>
?- genexp([1,2,3], L).
  L = 1+2+3 ;
  L = 1+2-3 ;
  L = (1+2)*3 ;
  L = (1+2)/3 ;
  L = 1-2+3 ;
  L = 1-2-3 ;
  …
</pre>'''

hint = {}
