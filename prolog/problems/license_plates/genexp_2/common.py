from operator import itemgetter
import prolog.engine
import server.problems

id = 146
number = 40
visible = True
facts = None

solution = '''\
memb146(X, [X|_]).
memb146(X, [_|T]) :-
  memb146(X, T).

conc146([], L, L).
conc146([H|T], L2, [H|L]) :-
  conc146(T, L2, L).

genexp([Exp], Exp).
genexp(L, Exp) :-
  conc146(Before, [N1,N2|After], L),
  memb146(Op, ['+','-','*','/']),
  NExp =.. [Op, N1, N2],
  conc146(Before, [NExp|After], L1),
  genexp(L1, Exp).
'''

test_cases = [
    ('genexp([1], X)',
        [{'X': '1'}]),
    ('genexp([1, 2], X)',
        [{'X': '1*2'}, {'X': '1+2'}, {'X': '1-2'}, {'X': '1/2'}]),
    ('genexp([1, 2, 3], X)',
        [{'X': '1+2+3'}, {'X': '1+2-3'}, {'X': '(1+2)*3'}, {'X': '(1+2)/3'},
         {'X': '1-2+3'}, {'X': '1-2-3'}, {'X': '(1-2)*3'}, {'X': '(1-2)/3'},
         {'X': '1*2+3'}, {'X': '1*2-3'}, {'X': '1*2*3'}, {'X': '1*2/3'},
         {'X': '1/2+3'}, {'X': '1/2-3'}, {'X': '1/2*3'}, {'X': '1/2/3'},
         {'X': '1+(2+3)'}, {'X': '1-(2+3)'}, {'X': '1*(2+3)'}, {'X': '1/(2+3)'},
         {'X': '1+(2-3)'}, {'X': '1-(2-3)'}, {'X': '1*(2-3)'}, {'X': '1/(2-3)'},
         {'X': '1+2*3'}, {'X': '1-2*3'}, {'X': '1*(2*3)'}, {'X': '1/(2*3)'},
         {'X': '1+2/3'}, {'X': '1-2/3'}, {'X': '1*(2/3)'}, {'X': '1/(2/3)'}]),
]

def test(code, aux_code):
    n_correct = 0
    engine_id = None
    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)
        if engine_id is not None and 'error' not in map(itemgetter(0), output):
            # Engine successfully created, and no syntax error in program.
            for query, answers in test_cases:
                if prolog.engine.check_answers(engine_id, query=query, answers=answers, timeout=1.0):
                    n_correct += 1
    except socket.timeout:
        pass
    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_cases)}}]
    return n_correct, len(test_cases), hints

def hint(code, aux_code):
    # TODO
    return []
