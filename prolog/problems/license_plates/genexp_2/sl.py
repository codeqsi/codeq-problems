name = 'genexp/2'
slug = 'Sestavi aritmetični izraz iz številk v podanem seznamu'

description = '''\
<p><code>genexp(L, E)</code>: Izraz <code>E</code> je zgrajen iz številk v seznamu <code>L</code> z dodajanjem aritmetičnih operatorjev. Predikat naj vrača vse možne izraze, enega po enega.</p>
<pre>
?- genexp([1,2,3], L).
  L = 1+2+3 ;
  L = 1+2-3 ;
  L = (1+2)*3 ;
  L = (1+2)/3 ;
  L = 1-2+3 ;
  L = 1-2-3 ;
  …
</pre>'''

hint = {}
