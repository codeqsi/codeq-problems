name = 'firstMinus/2'
slug = 'Negiraj prvi element v danem seznamu števil'

description = '''\
<p><code>firstMinus(L1, L2)</code>: seznam <code>L2</code> je enak kot <code>L1</code>, razen prvega elementa, ki je lahko negiran ali pa ne. Predikat naj vrne obe rešitvi, eno za drugo.</p>
<pre>
?- firstMinus([1,2,3], L).
  L = [1,2,3] ;
  L = [-1,2,3].
</pre>'''

hint = {}
