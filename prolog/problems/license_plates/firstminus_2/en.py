name = 'firstMinus/2'
slug = 'negate the first element in a list of numbers'

description = '''\
<p><code>firstMinus(L1, L2)</code>: the list <code>L2</code> is the same as <code>L1</code>, except for the first element that may be negated. Your code should return both solutions.</p>
<pre>
?- firstMinus([1,2,3], L).
  L = [1,2,3] ;
  L = [-1,2,3].
</pre>'''

hint = {}
