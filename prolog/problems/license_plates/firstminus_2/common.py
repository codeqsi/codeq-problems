from operator import itemgetter
import prolog.engine
import server.problems

id = 147
number = 30
visible = True
facts = None

solution = '''\
firstMinus(L, L).
firstMinus([X|T], [Y|T]) :-
  Y is -X.
'''

test_cases = [
    ('firstMinus([2, 3, 4], X)',
        [{'X': '[2, 3, 4]'}, {'X': '[-2, 3, 4]'}]),
    ('firstMinus([5, 2, 3, 4], X)',
        [{'X': '[5, 2, 3, 4]'}, {'X': '[-5, 2, 3, 4]'}]),
]

def test(code, aux_code):
    n_correct = 0
    engine_id = None
    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)
        if engine_id is not None and 'error' not in map(itemgetter(0), output):
            # Engine successfully created, and no syntax error in program.
            for query, answers in test_cases:
                if prolog.engine.check_answers(engine_id, query=query, answers=answers, timeout=1.0):
                    n_correct += 1
    except socket.timeout:
        pass
    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_cases)}}]
    return n_correct, len(test_cases), hints

def hint(code, aux_code):
    # TODO
    return []
