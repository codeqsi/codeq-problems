name = 'prog_listswap/3'
slug = 'list-manipulation language with semantics'

description = '''\
<p>
Write a DCG for manipulating list elements. The first symbol in every word is
<code>[begin]</code>, followed by any sequence of "instruction" symbols from
the set {<code>left</code>, <code>right</code>, <code>swap</code>}, and finally
<code>[end]</code>. The starting symbol should be named
<code>prog_listswap</code>.
</p>

<p>
The meaning of a word (program) in this language has the form
<code>In-->Out</code>, mapping from input to output lists. Besides the list
contents, internal states also hold the current cursor position. The
<code>left</code> and <code>right</code> instructions move the cursor one step
in the given direction, while the <code>swap</code> instruction swaps the
element under the cursor with its left neighbor (and fails if cursor is
currently pointing to the first element of the list).
</p>

<pre>
?- prog_listswap([1,2,3,4]-->Out, [begin,right,swap,end], []).
  Out = [2,1,3,4].
</pre>

<p>
Helper predicates (already defined):
</p>
<ul>
<li><code>swap(List,I,NewList)</code> creates <code>NewList</code> by
swapping the <code>I</code>th element with its left neighbor in
<code>List</code></li>
</ul>

<p>This exercise has already been solved for you. Check how it works in the console.</p>
'''

hint = {}
