from operator import itemgetter
import socket

import prolog.engine
import prolog.util
from server.hints import Hint, HintPopup

id = 174
number = 20
visible = True
facts = 'denotational_semantics_aux__predicates'

solution = '''\
prog_listswap(In-->Out) -->
    [begin], instructs174((In,1)-->(Out,_)), [end].

instructs174((R0,C0)-->(R,C)) -->
    instr174((R0,C0)-->(R,C)).
instructs174((R0,C0)-->(R,C)) -->
    instr174((R0,C0)-->(R1,C1)),
    instructs174((R1,C1)-->(R,C)).

instr174((R0,C0)-->(R0,C)) -->
    [left], { C0 > 1, C is C0 - 1 ; C0 =< 1, C is C0 }.
instr174((R0,C0)-->(R0,C)) -->
  [right], { length(R0, LenR0),
            ( C0 < LenR0, C is C0 + 1 ; C0 >= LenR0, C is C0 ) }.

instr174((R0,C0)-->(R,C0)) -->
  [swap], {swap(R0,C0,R)}.'''

# nothing to do in this exercise
initial = '''\
prog_listswap(In-->Out) -->
    [begin], instructs((In,1)-->(Out,_)), [end].

% one instruction
instructs((R0,C0)-->(R,C)) -->
    instr((R0,C0)-->(R,C)).
% sequence of instructions
instructs((R0,C0)-->(R,C)) -->
    instr((R0,C0)-->(R1,C1)),
    instructs((R1,C1)-->(R,C)).

% "left" and "right" instructions move the cursor (but not outside list bounds)
instr((R0,C0)-->(R0,C)) -->
    [left],
    { C0 > 1, C is C0 - 1 ; C0 =< 1, C is C0 }.
instr((R0,C0)-->(R0,C)) -->
    [right],
    { length(R0, LenR0), (C0 < LenR0, C is C0 + 1 ; C0 >= LenR0, C is C0) }.

% "swap" instruction swaps the element under cursor with the element on the left
instr((R0,C0)-->(R,C0)) -->
    [swap],
    { swap(R0, C0, R) }.
'''

test_cases = [
    ('findall(W, (W = [_], prog_listswap(_, W, [])), Words), Words == []',
        [{}]),
    ('prog_listswap([1,2,3,4]-->Out, [begin,right,right,swap,end], []), Out == [1,3,2,4]',
        [{}]),
    ('prog_listswap([a,b,c,d,e]-->Out, [begin,right,right,swap,left,swap,end], []), Out == [c,a,b,d,e]',
        [{}]),
]

def test(code, aux_code):
    n_correct = 0
    engine_id = None
    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)
        if engine_id is not None and 'error' not in map(itemgetter(0), output):
            # Engine successfully created, and no syntax error in program.
            for query, answers in test_cases:
                if prolog.engine.check_answers(engine_id, query=query, answers=answers, timeout=1.0):
                    n_correct += 1
    except socket.timeout:
        pass
    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_cases)}}]
    return n_correct, len(test_cases), hints

