name = 'prog_listswap/3'
slug = 'jezik za rokovanje s seznami s pomenom'

description = '''\
<p>
Napiši DCG za rokovanje s seznami. Prvi simbol vsake besede je <code>[begin]</code>, temu pa sledi
poljubno zaporedje "ukazov" iz nabora {<code>left</code>, <code>right</code>, <code>swap</code>},
na koncu pa je simbol <code>[end]</code>. Začetni nekončni simbol naj se imenuje <code>prog_listswap</code>.
</p>

<p>
Pomen besede (programa) v tem jeziku ima obliko <code>In-->Out</code> in pomeni preslikavo
med vhodnim seznamom <code>In</code> in izhodnim seznamom <code>Out</code>. Notranje stanje
gramatike, hranjeno v argumentih nekončnih simbolov, poleg trenutnega stanja seznama vodi tudi
pozicijo kurzorja s katerim rokujemo s seznamom. Ukaza <code>left</code> in <code>right</code>
kurzor premakneta za eno mesto v ustrezno smer, ukaz <code>swap</code> pa zamenja mesti elementov
med katerima stoji kurzor. Če je kurzor na začetku seznama, ukaz <code>swap</code> ne uspe.
</p>

<pre>
?- prog_listswap([1,2,3,4]-->Out, [begin,right,swap,end], []).
  Out = [2,1,3,4].
</pre>

<p>
Pomožni predikat (že definiran):
</p>
<ul>
<li><code>swap(List,I,NewList)</code> ustvari nov seznam <code>NewList</code> tako, da
zamenja mesti <code>I</code>-tega elementa in njegovega levega soseda v seznamu
<code>List</code></li>
</ul>

<p>Ta naloga je že rešena. Preveri kako deluje v konzoli.</p>
'''

hint = {}
