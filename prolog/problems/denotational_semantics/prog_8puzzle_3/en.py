name = 'prog_8puzzle/3'
slug = '8-puzzle-solving language with semantics'

description = '''\
<p>
Write a DCG for solving 8-puzzles. The syntax for this language should be the
same as in the previous exercise: the first symbol in every word is
<code>[begin]</code>, followed by any sequence of "instruction" symbols from
the set {<code>left</code>, <code>right</code>, <code>up</code>,
<code>down</code>}, and finally <code>[end]</code>. The starting symbol should
be named <code>prog_8puzzle</code>.
</p>

<p>
The meaning of a word (program) in this language has the form
<code>In-->Out</code>, mapping from input to output states. Each state is a
(permuted) list of numbers from 0 to 8, where 0 stands for the empty square and
other numbers for the corresponding tiles. The first three numbers in the list
correspond to the top row of the 8-puzzle, the next three numbers to the middle
row, and the last three numbers to the last row. The meaning of instructions
<code>left</code>, <code>right</code>, <code>up</code> and <code>down</code> is
to move the blank tile in the given direction.
</p>

<pre>
?- prog_8puzzle([0,1,2,3,4,5,6,7,8]-->Out, [begin,down,right,end], []).
  Out = [3,1,2,4,0,5,6,7,8].
</pre>

<p>
Helper predicates (already defined):
</p>
<ul>
<li><code>findblank(List,I)</code> returns the 1-based index <code>I</code> of the element 0 in <code>List</code></li>
<li><code>swap(List,I,J,NewList)</code> creates <code>NewList</code> by swapping elements <code>I</code> and <code>J</code> in <code>List</code></li>
</ul>
'''

hint = {}
