name = 'prog_8puzzle/3'
slug = 'jezik za reševanje igre Osem s pomenom'

description = '''\
<p>
Napiši DCG za jezik igre Osem (angl. 8-puzzle). Sintaksa tega jezika naj bo enaka kot v prejšnji nalogi.
Prvi simbol vsake besede je simbol <code>[begin]</code>, temu pa sledi poljubno zaporedje "ukazov" iz nabora
{<code>left</code>, <code>right</code>, <code>up</code> <code>down</code>}, na koncu pa je simbol
<code>[end]</code>. Začetni nekončni simbol naj se imenuje <code>prog_8puzzle</code>.
</p>

<p>
Pomen besede (programa) v tem jeziku ima obliko <code>In-->Out</code> in pomeni preslikavo
med vhodnim seznamom <code>In</code> in izhodnim seznamom <code>Out</code>. Stanje je seznam (permutiran)
števil od 0 do 8, kjer 0 pomeni prazno polje, ostala števila pa pomenijo oznako ustrezne ploščice. Prva
tri števila v seznamu predstavljajo prvo vrstico stanja igre Osem, naslednja tri srednjo vrstico in zadnja
tri spodnjo vrstico. Pomen ukazov <code>left</code>, <code>right</code>, <code>up</code> in <code>down</code>
je "premik" praznega polja v ustrezni smeri.
</p>

<pre>
?- prog_8puzzle([0,1,2,3,4,5,6,7,8]-->Out, [begin,down,right,end], []).
  Out = [3,1,2,4,0,5,6,7,8].
</pre>

<p>
Pomožni predikati (že definirani):
</p>
<ul>
<li><code>findblank(List,I)</code> vrne položaj (indeks z bazo 1) <code>I</code> elementa 0 v seznamu <code>List</code></li>
<li><code>swap(List,I,J,NewList)</code> ustvari seznam <code>NewList</code> tako, da zamenja elementa na mestih <code>I</code> in <code>J</code> v seznamu <code>List</code></li>
</ul>
'''

hint = {}
