from operator import itemgetter
import socket

import prolog.engine
import prolog.util
from server.hints import Hint, HintPopup

id = 172
number = 30
visible = True
facts = None

solution = '''\
prog_8puzzle --> [begin], instructs172, [end].

instructs172 --> instr172.
instructs172 --> instr172, instructs172.

instr172 --> [left].
instr172 --> [right].
instr172 --> [up].
instr172 --> [down].
'''

test_cases = [
    ('findall(W, (W = [_], prog_8puzzle(W, [])), Words), Words == []',
        [{}]),
    ('setof(W, [A,B,C]^(W = [A,B,C], prog_8puzzle(W, [])), Words), \
      Words == [[begin,down,end],[begin,left,end],[begin,right,end],[begin,up,end]]',
        [{}]),
    ('setof(W, [A,B,C,D]^(W = [A,B,C,D], prog_8puzzle(W, [])), Words), length(Words, 16)',
        [{}]),
    ('prog_8puzzle([begin,down,down,up,up,left,left,right,end], [])',
        [{}]),
]

def test(code, aux_code):
    n_correct = 0
    engine_id = None
    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)
        if engine_id is not None and 'error' not in map(itemgetter(0), output):
            # Engine successfully created, and no syntax error in program.
            for query, answers in test_cases:
                if prolog.engine.check_answers(engine_id, query=query, answers=answers, timeout=1.0):
                    n_correct += 1
    except socket.timeout:
        pass
    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_cases)}}]
    return n_correct, len(test_cases), hints

