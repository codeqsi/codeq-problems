name = 'prog_8puzzle/2'
slug = 'jezik za reševanje igre Osem'

description = '''\
<p>
Napiši DCG za jezik igre Osem (angl. 8-puzzle). Prvi simbol vsake besede je simbol <code>[begin]</code>, temu pa
sledi poljubno zaporedje "ukazov" iz nabora {<code>left</code>, <code>right</code>, <code>up</code> <code>down</code>},
na koncu pa je simbol <code>[end]</code>. Začetni nekončni simbol naj se imenuje <code>prog_8puzzle</code>.
</p>

<p>
Primeri veljavnih besed: <code>[begin,left,down,right,end]</code>, <code>[begin,down,end]</code>.
</p>
'''

hint = {}
