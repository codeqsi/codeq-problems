name = 'prog_8puzzle/2'
slug = '8-puzzle-solving language'

description = '''\
<p>
Write a DCG for solving 8-puzzles. The first symbol in every word is
<code>[begin]</code>, followed by any sequence of "instruction" symbols from
the set {<code>left</code>, <code>right</code>, <code>up</code>,
<code>down</code>}, and finally <code>[end]</code>. The starting symbol should
be named <code>prog_8puzzle</code>.
</p>

<p>
Example words: <code>[begin,left,down,right,end]</code>, <code>[begin,down,end]</code>.
</p>
'''

hint = {}
