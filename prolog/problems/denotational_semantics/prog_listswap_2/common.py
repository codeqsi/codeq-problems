from operator import itemgetter
import socket

import prolog.engine
import prolog.util
from server.hints import Hint, HintPopup

id = 175
number = 10
visible = True
facts = None

solution = '''\
prog_listswap --> [begin], instructs175, [end].

instructs175 --> instr175.
instructs175 --> instr175, instructs175.

instr175 --> [left].
instr175 --> [right].
instr175 --> [swap].
'''

# nothing to do in this exercise
initial = '''\
prog_listswap --> [begin], instructs, [end].

instructs --> instr.
instructs --> instr, instructs.

instr --> [left].
instr --> [right].
instr --> [swap].
'''

test_cases = [
    ('findall(W, (W = [_], prog_listswap(W, [])), Words), Words == []',
        [{}]),
    ('setof(W, [A,B,C,D]^(W = [A,B,C,D], prog_listswap(W, [])), Words), length(Words, 9)',
        [{}]),
    ('prog_listswap([begin,right,right,swap,end], [])',
        [{}]),
    ('prog_listswap([begin,right,right,swap,left,swap,end], [])',
        [{}]),
]

def test(code, aux_code):
    n_correct = 0
    engine_id = None
    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)
        if engine_id is not None and 'error' not in map(itemgetter(0), output):
            # Engine successfully created, and no syntax error in program.
            for query, answers in test_cases:
                if prolog.engine.check_answers(engine_id, query=query, answers=answers, timeout=1.0):
                    n_correct += 1
    except socket.timeout:
        pass
    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_cases)}}]
    return n_correct, len(test_cases), hints

