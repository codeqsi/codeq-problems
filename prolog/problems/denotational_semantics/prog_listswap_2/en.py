name = 'prog_listswap/2'
slug = 'list-manipulation language'

description = '''\
<p>
Write a DCG for manipulating list elements. The first symbol in every word is
<code>[begin]</code>, followed by any sequence of "instruction" symbols from
the set {<code>left</code>, <code>right</code>, <code>swap</code>}, and finally
the symbol <code>[end]</code>. The starting symbol should be named
<code>prog_listswap</code>.
</p>

<p>This exercise has already been solved for you. Check how it works in the console.</p>

<p>
Example words: <code>[begin,right,swap,end]</code>, <code>[begin,right,right,swap,left,swap,end]</code>.
</p>
'''

hint = {}
