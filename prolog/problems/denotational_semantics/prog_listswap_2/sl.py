name = 'prog_listswap/2'
slug = 'jezik za rokovanje s seznami'

description = '''\
<p>
Napiši DCG za rokovanje s seznami. Prvi simbol vsake besede je <code>[begin]</code>, temu pa sledi
poljubno zaporedje "ukazov" iz nabora {<code>left</code>, <code>right</code>, <code>swap</code>},
na koncu pa je simbol <code>[end]</code>. Začetni nekončni simbol naj se imenuje <code>prog_listswap</code>.
</p>

<p>Ta naloga je že rešena. Preveri kako deluje v konzoli.</p>

<p>
Primeri veljavnih besed: <code>[begin,right,swap,end]</code>, <code>[begin,right,right,swap,left,swap,end]</code>.
</p>
'''

hint = {}
