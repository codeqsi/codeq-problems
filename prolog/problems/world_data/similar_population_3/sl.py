name = 'simpop/3'
slug = 'državi z najbolj podobnim številom prebivalcev'

description = '''\
<p><code>simpop(X, Y, Diff)</code>: <code>X</code> in <code>Y</code> je par držav z najbolj podobnim številom
prebivalcev, <code>Diff</code> je razlika med njima.</p>
<pre>
?- simpop('Fiji', Y, Diff).
  Y = 'Cyprus',
  Diff = 20152.
</pre>
<p>Uporabna dejstva so podana v obliki <code>country(Name, ID, Capital, CapitalProvince, Size, Population).</code></p>
'''

hint = {}
