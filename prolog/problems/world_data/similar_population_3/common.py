from operator import itemgetter
import socket
import prolog.engine
from server.hints import Hint
import server.problems

id = 10008
number = 80
visible = True
facts = 'mondial'

solution = '''\
simpop(X, Y, D):-
  country(X,_,_,_,_,PX),
  country(Y,_,_,_,_,PY),
  X \== Y,
  D is abs(PX-PY),
  \+ (country(Z,_,_,_,_,PZ), Z \== X, D > abs(PX-PZ)).
'''

test_cases = [
    ("simpop('Tonga', Y, D)",
        [{'Y': "'Grenada'", 'D': '76'}]),
    ("simpop('Slovenia', Y, D)",
        [{'Y': "'Macedonia'", 'D': '973'}]),
    ("simpop(X, 'Slovenia', D)",
        [{'X': "'Macedonia'", 'D': '973'}, {'X': "'Botswana'", 'D': '20593'}]),
    ("simpop(X, Y, 1176831)",
        [{'Y': "'Uzbekistan'", 'X': "'Venezuela'"}]),
]

def test(code, aux_code):
    n_correct = 0
    engine_id = None
    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=5.0)
        if engine_id is not None and 'error' not in map(itemgetter(0), output):
            # Engine successfully created, and no syntax error in program.
            for query, answers in test_cases:
                if prolog.engine.check_answers(engine_id, query=query, answers=answers, timeout=1.0, inference_limit = None):
                    n_correct += 1
    except socket.timeout:
        pass
    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_cases)}}]
    return n_correct, len(test_cases), hints

