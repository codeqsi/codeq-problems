name = 'simpop/3'
slug = 'countries with most similar number of residents'

description = '''\
<p><code>simpop(X, Y, Diff)</code>: <code>X</code> and <code>Y</code> are countries with the most similar numbers of
residents. <code>Diff</code> represents the difference between them.</p>
<pre>
?- simpop('Fiji', Y, Diff).
  Y = 'Cyprus',
  Diff = 20152.
</pre>
<p>Useful facts are given as <code>country(Name, ID, Capital, CapitalProvince, Size, Population).</code></p>
'''

hint = {}
