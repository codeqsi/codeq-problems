name = 'grant/1'
slug = 'Otroka kapitana Granta'

description = '''\
<p>Otroka kapitana Granta, tako pravi Jules Verne, sta našla njegovo pismo v steklenici. Izgubil naj bi se v
brodolomu nekje na morju. Vse kar vesta o njegovi lokaciji je, da leži na 37 poldnevniku južne širine. Povprašaj
prolog v katerem mestu ali na katerem otoku se lahko nahaja, da ga rešimo. Privzemimo, da poiščemo vse
lokacije med -36 in -38 stopinjami južne širine (minus ker je pod ekvatorjem).</p>
<p><code>grant(Location)</code>: predikat eno po eno vrne vse lokacije <code>Location</code>, ki ustrezajo
pogojem. <code>Location</code> predstavlja <em>ime</em> lokacije.</p>
<p>Prav ti znajo priti podatki v obliki <code>city(Name, CountryCode, Province, Population, Latitude, Longitude,
Elevation)</code> in <code>island(Name, Group, Area, Elevation, Type, Latitude, Longitude)</code>.</p>
'''

hint = {}
