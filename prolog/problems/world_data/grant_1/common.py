from operator import itemgetter
import socket
import prolog.engine
from server.hints import Hint
import server.problems

id = 10007
number = 45
visible = True
facts = 'mondial'

solution = '''\
grant(Loc):-
  (city(Loc, _, _, _, Lat, _, _)
   ;
   island(Loc, _, _, _, _, Lat, _)),
  number(Lat),
  Lat > -38,
  Lat < -36.
'''

test_cases = [
    ("grant(Loc)",
        [{'Loc': "'Melbourne'"}, {'Loc': "'Auckland'"}, {'Loc': "'Hamilton'"}, {'Loc': "'Santa Rosa'"},
         {'Loc': "'Concepción'"}, {'Loc': "'Talcahuano'"}, {'Loc': "'Tristan Da Cunha'"}]),
    ("\+ grant('Ljubljana')",
        [{}]),
    ("grant('Tristan Da Cunha')",
        [{}]),
]

def test(code, aux_code):
    n_correct = 0
    engine_id = None
    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=5.0)
        if engine_id is not None and 'error' not in map(itemgetter(0), output):
            # Engine successfully created, and no syntax error in program.
            for query, answers in test_cases:
                if prolog.engine.check_answers(engine_id, query=query, answers=answers, timeout=1.0, inference_limit = None):
                    n_correct += 1
    except socket.timeout:
        pass
    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_cases)}}]
    return n_correct, len(test_cases), hints

