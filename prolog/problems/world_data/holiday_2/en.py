name = 'holiday/2'
slug = 'V katero sosednjo državo gremo lahko na morje?'

description = '''\
<p>V katero <em>sosednjo</em> državo gremo lahko na morje?</p>
<p><code>holiday(Country, Neighbour)</code>: <code>Neighbour</code> je sosednja država od <code>Country</code>,
ki ima morje.</p>
<pre>
?- holiday('Slovenia', Neighbour).
  Neighbour = 'Italy' ;
  Neighbour = 'Croatia'
</pre>
<p>Prav ti znajo priti podatki v obliki <code>borders(CountryCode1, CountryCode2, LenOfBorder)</code> in
<code>geo_sea(Sea, CountryCode, Province)</code>.</p>
'''

hint = {}
