from operator import itemgetter
import socket
import prolog.engine
from server.hints import Hint
import server.problems

id = 10005
number = 50
visible = True
facts = 'mondial'

solution = '''\
holiday(Country, Neighbour):-
  country(Country,CC1,_,_,_,_),
  (borders(CC1,CC2,_) ; borders(CC2,CC1,_)),
  once(geo_sea(_,CC2,_)),
  country(Neighbour,CC2,_,_,_,_).
'''

test_cases = [
    ("holiday('Slovenia', N)",
        [{'N': "'Italy'"}, {'N': "'Croatia'"}]),
    ("holiday(C, 'Canada')",
        [{'C': "'United States'"}]),
    ("holiday('Venezuela', 'Guyana')",
        [{}]),
    ("\+ holiday('Uzbekistan', _)",
        [{}]),
]

def test(code, aux_code):
    n_correct = 0
    engine_id = None
    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=5.0)
        if engine_id is not None and 'error' not in map(itemgetter(0), output):
            # Engine successfully created, and no syntax error in program.
            for query, answers in test_cases:
                if prolog.engine.check_answers(engine_id, query=query, answers=answers, timeout=1.0, inference_limit = None):
                    n_correct += 1
    except socket.timeout:
        pass
    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_cases)}}]
    return n_correct, len(test_cases), hints

