from operator import itemgetter
import socket
import prolog.engine
import prolog.util
from server.hints import Hint
import server.problems

id = 10001
number = 10
visible = True
facts = 'mondial'

solution = '''\
capital(Country, Capital) :-
    country(Country, _, Capital, _, _, _).
'''

hint_type = {
}

test_cases = [
    ("capital('France', X)", [{'X': "'Paris'"}]),
    ("capital('Turkmenistan', X)", [{'X': "'Ashgabat'"}]),
    ("capital('Cocos Islands', X)", [{'X': "'West Island'"}]),
    ("capital(X, 'Basseterre')", [{'X': "'Saint Kitts and Nevis'"}]),
    ("capital(X, X)",
        [{'X': "'Luxembourg'"}, {'X': "'Monaco'"}, {'X': "'Gibraltar'"},
         {'X': "'Ceuta'"}, {'X': "'Melilla'"}, {'X': "'San Marino'"},
         {'X': "'Hong Kong'"}, {'X': "'Macao'"}, {'X': "'Singapore'"},
         {'X': "'Djibouti'"}]),
]

def test(code, aux_code):
    n_correct = 0
    engine_id = None
    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=5.0)
        if engine_id is not None and 'error' not in map(itemgetter(0), output):
            # Engine successfully created, and no syntax error in program.
            for query, answers in test_cases:
                if prolog.engine.check_answers(engine_id, query=query, answers=answers, timeout=1.0, inference_limit=None):
                    n_correct += 1
    except socket.timeout:
        pass
    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_cases)}}]
    if n_correct == len(test_cases):
        hints += [{'id': 'final_hint'}]
    return n_correct, len(test_cases), hints
