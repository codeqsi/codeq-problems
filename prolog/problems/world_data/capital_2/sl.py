name = 'capital/2'
slug = 'glavna mesta'

description = '''\
<p><code>capital(X, Y)</code>: država <code>X</code> ima glavno mesto <code>Y</code>.</p>
<pre>
?- capital('Ireland', Y).
  Y = 'Dublin'.
</pre>
<p>Uporabna dejstva so podana v obliki <code>country(Name, ID, Capital, CapitalProvince, Size, Population)</code>.</p>
'''

hint = {}
