name = 'capital/2'
slug = 'capitals'

description = '''\
<p><code>capital(X, Y)</code>: <code>Y</code> is the capital city of country <code>X</code>.</p>
<pre>
?- capital('Ireland', Y).
  Y = 'Dublin'.
</pre>
<p>Useful facts are given as <code>country(Name, ID, Capital, CapitalProvince, Size, Population)</code>.</p>
'''

hint = {}
