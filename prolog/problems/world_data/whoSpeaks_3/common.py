from operator import itemgetter
import socket
import prolog.engine
from server.hints import Hint
import server.problems

id = 10004
number = 40
visible = True
facts = 'mondial'

solution = '''\
whoSpeaks(Lang, Country, Num):-
  language(CC, Lang, Perc),
  country(Country, CC, _, _, _, Pop),
  Num is Pop*Perc/100.
'''

test_cases = [
    ("whoSpeaks('Dutch', Country, _)",
        [{'Country': "'Belgium'"}, {'Country': "'Netherlands'"}, {'Country': "'Monaco'"},
         {'Country': "'Curacao'"}, {'Country': "'Sint Maarten'"}]),
    ("whoSpeaks('Slovenian', 'Slovenia', Num)",
        [{'Num': '1873527.11'}]),
    ("\+ whoSpeaks('Italian', 'Uzbekistan', _)",
        [{}]),
]

def test(code, aux_code):
    n_correct = 0
    engine_id = None
    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=5.0)
        if engine_id is not None and 'error' not in map(itemgetter(0), output):
            # Engine successfully created, and no syntax error in program.
            for query, answers in test_cases:
                if prolog.engine.check_answers(engine_id, query=query, answers=answers, timeout=1.0, inference_limit = None):
                    n_correct += 1
    except socket.timeout:
        pass
    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_cases)}}]
    return n_correct, len(test_cases), hints

