name = 'whoSpeaks/3'
slug = 'poišči države v katerih govorijo določen jezik'

description = '''\
<p>Določen jezik ne govorijo nujno samo v eni državi. Pravzaprav za večino jezikov velja, da jih govorijo
v več državah na svetu. Napiši predikat, ki ugotovi v katerih državah govorijo določen jezik in koliko je
ljudi, ki ga govorijo. Morda te rezultati presenetijo!</p>
<p><code>whoSpeaks(Lang, Country, NumOfSpeakers)</code>: predikat eno po eno vrne vse države <code>Country</code>
v kateri govorijo jezik <code>Lang</code> in obenem vrne tudi koliko ljudi </code>NumOfSpeakers</code> v tej državi
govori ta jezik.</p>
<pre>
?- whoSpeaks('Spanish', Country, NumOfSpeakers).
  Country = 'Spain', NumOfSpeakers = 34643777 ;
  Country = 'Andorra', NumOfSpeakers = 25777 ;
  Country = 'Belize', NumOfSpeakers = 143966 ;
  ...
</pre>
<p>Prav ti znajo priti podatki v obliki <code>language(CountryCode, Language, PercentageOfSpeakers)</code>.</p>
<p>S pomočjo napisanega predikata lahko hitro ugotoviš v koliko državah govorijo nek jezik ali v kateri državi je
najbolj pogost. Lahko poskusiš postaviti ustrezno vprašanje prologu v konzoli.</p>
'''

hint = {}
