name = 'people_in_capitals/1'
slug = 'number of people living in capitals'

description = '''\
<p><code>people_in_capitals(N)</code>: <code>N</code> is the total number of people living in capital cities of the world.</p>
<p>Useful predicates:</p>
<li><code>country(Name, ID, Capital, CapitalProvince, Size, Population)</code></li>
<li><code>city(Name, Country ID, Province, Population, Lat, Lon, Elevation)</code>.</li>
'''

hint = {}
