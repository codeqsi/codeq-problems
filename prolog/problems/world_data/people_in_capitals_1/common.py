from operator import itemgetter
import socket
import prolog.engine
import prolog.util
from server.hints import Hint
import server.problems

id = 10003
number = 30
visible = True
facts = 'mondial'

solution = '''\
people_in_capitals(N) :-
    findall(Pop,
        (country(_,ID,Capital,_,_,_),
         city(Capital,ID,_,Pop,_,_,_),
         number(Pop)), L),
    sum_list(L, N).
'''

hint_type = {
}

test_cases = [
    ('people_in_capitals(N)', [{'N': '313163317'}]),        # old solution without equal IDs [{'N': '316277426'}]
]

def test(code, aux_code):
    n_correct = 0
    engine_id = None
    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=5.0)
        if engine_id is not None and 'error' not in map(itemgetter(0), output):
            # Engine successfully created, and no syntax error in program.
            for query, answers in test_cases:
                if prolog.engine.check_answers(engine_id, query=query, answers=answers, timeout=1.0, inference_limit=None):
                    n_correct += 1
    except socket.timeout:
        pass
    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_cases)}}]
    if n_correct == len(test_cases):
        hints += [{'id': 'final_hint'}]
    return n_correct, len(test_cases), hints
