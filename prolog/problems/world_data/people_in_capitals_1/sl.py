name = 'people_in_capitals/1'
slug = 'število prebivalcev glavnih mest'

description = '''\
<p><code>people_in_capitals(N)</code>: <code>N</code> je število ljudi, ki živijo v glavnih mestih po svetu.</p>
<p>Uporabni predikati:</p>
<li><code>country(Name, ID, Capital, CapitalProvince, Size, Population)</code></li>
<li><code>city(Name, Country ID, Province, Population, Lat, Lon, Elevation)</code>.</li>
'''

hint = {}
