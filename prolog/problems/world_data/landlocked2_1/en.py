name = 'landlocked2/1'
slug = 'poišči države brez morja, ki ne mejijo na države z morjem'

description = '''\
<p>Nekatere države nimajo morja. Spet druge države imajo še manj sreče -- ne samo, da nimajo morja,
tudi ne mejijo na nobeno državo, ki bi morje imela. Po angleško takim državam rečemo "double landlocked".</p>
<p><code>landlocked2(Country)</code>: predikat eno po eno vrne vse države, ki nimajo ne morja, ne sosede
z morjem.</p>
<p>Prav ti znajo priti podatki v obliki <code>geo_sea(Sea, CountryCode, Province)</code>.</p>
'''

hint = {}
