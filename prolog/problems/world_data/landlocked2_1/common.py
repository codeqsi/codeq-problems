from operator import itemgetter
import socket
import prolog.engine
from server.hints import Hint
import server.problems

id = 10006
number = 60
visible = True
facts = 'mondial'

solution = '''\
landlocked2(Country):-
  findall(CC, (country(_,CC,_,_,_,_), \+ geo_sea(_,CC,_)), LL),
  member(C, LL),
  \+ ( (borders(C,C1,_) ; borders(C1,C,_)),
       \+ member(C1, LL) ),
  country(Country,C,_,_,_,_).
'''

test_cases = [
    ("landlocked2(Country)",
        [{'Country': "'Liechtenstein'"}, {'Country': "'Uzbekistan'"}]),
    ("\+ landlocked2('Slovenia')",
        [{}]),
    ("landlocked2('Uzbekistan')",
        [{}]),
]

def test(code, aux_code):
    n_correct = 0
    engine_id = None
    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=5.0)
        if engine_id is not None and 'error' not in map(itemgetter(0), output):
            # Engine successfully created, and no syntax error in program.
            for query, answers in test_cases:
                if prolog.engine.check_answers(engine_id, query=query, answers=answers, timeout=1.0, inference_limit = None):
                    n_correct += 1
    except socket.timeout:
        pass
    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_cases)}}]
    return n_correct, len(test_cases), hints

