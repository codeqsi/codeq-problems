name = 'all_capitals/1'
slug = 'glavna mesta'

description = '''\
<p><code>all_capitals(List)</code>: <code>List</code> je seznam vseh glavnih mest na svetu.</p>
<pre>
?- all_capitals(List).
  List = ['Tirana', 'Athina', 'Skopje', 'Beograd', …]
</pre>
<p>Uporabna dejstva so podana v obliki <code>country(Name, ID, Capital, CapitalProvince, Size, Population)</code>.</p>
'''

hint = {}
