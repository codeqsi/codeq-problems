name = 'all_capitals/1'
slug = 'capital cities'

description = '''\
<p><code>all_capitals(List)</code>: <code>List</code> is a list of all capitals in the world.</p>
<pre>
?- all_capitals(List).
  List = ['Tirana', 'Athina', 'Skopje', 'Beograd', …]
</pre>
<p>Useful facts are given as <code>country(Name, ID, Capital, CapitalProvince, Size, Population)</code>.</p>
'''

hint = {}
