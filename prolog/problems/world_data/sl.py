name = 'Države sveta'
description = '''\
<p>
Naučimo se vprašati prolog po različnih dejstvih o našem svetu, npr. skozi
katere države teče neka reka ali kateri dve državi sta si čimbolj podobni po
številu prebivalcev. Za ta namen je naložena
<a target="_blank" href="[%@resource intro_sl.html%]">baza podatkov</a>
iz almanaha svetovnih dejstev "The World Factbook".
</p>
'''
