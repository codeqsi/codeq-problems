name = 'The World Factbook'
description = '''\
<p>
Let's ask prolog some questions about our world, e.g. through which countries a given river flows or which
two countries are most similar in terms of the size of their populations. For this purpose a <a target="_blank" href="[%@resource intro_en.html%]">knowledge base</a> with facts about the world has been loaded. The database comes from the almanac "The World Factbook".
</p>
'''
