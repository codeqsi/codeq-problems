name = 'flows/2'
slug = ''

description = '''\
<p><code>flows(X, Y)</code>: reka <code>X</code> se izliva v morje <code>Y</code>.
Predikat naj sledi toku skozi različne reke - Drina se npr. priključi Savi,
ki se priključi Donavi, ki teče v Črno morje, zato naj program odgovori tako:</p>
<pre>
?- flows('Drina', Y).
  Y = 'Black Sea'.
</pre>
<p>Podatki o rekah so predstavljeni s predikatom</p>
<pre>
river(Name, FlowsToRiver, FlowsToLake, FlowsToSea, Length, Area,
      SourceLat, SourceLon, SourceName, SourceElevation, MouthLat, MouthLon).
</pre>
<p>Za vsako reko je podan največ en izmed argumentov <code>FlowsToRiver</code>,
<code>FlowsToLake</code> in <code>FlowsToSea</code>, ki pove, v katero reko,
jezero oziroma morje se izliva; ostala dva argumenta pa sta <code>null</code>.
'''

plan = [
    '''\
<p>Reke, ki se izlivajo neposredno v morje, lahko dobimo s poizvedbo:</p>
<pre>
?- river(River, _, _, Sea, _, _, _, _, _, _, _, _), Sea \= null.
  River = 'Thjorsa', Sea = 'Atlantic Ocean' ;
  River = 'Thames', Sea = 'North Sea' ;
  …
</pre>
''',
]

hint = {}
