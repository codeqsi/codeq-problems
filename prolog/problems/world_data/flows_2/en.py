name = 'flows/2'
slug = ''

description = '''\
<p><code>flows(X, Y)</code>: the river <code>X</code> flows to sea <code>Y</code>.
The predicate should follow the flow from <code>X</code> through different rivers
until it reaches a sea - for example, Drina flows into Save, which flows into Donau,
ending in the Black Sea:</p>
<pre>
?- flows('Drina', Y).
  Y = 'Black Sea'.
</pre>
<p>River data is given by the predicate</p>
<pre>
river(Name, FlowsToRiver, FlowsToLake, FlowsToSea, Length, Area,
      SourceLat, SourceLon, SourceName, SourceElevation, MouthLat, MouthLon).
</pre>
'''

hint = {}
