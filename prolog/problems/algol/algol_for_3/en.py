name = 'algol_for/3'
slug = 'interpreter for mini-algol with for-statement'

description = '''\
<p>Extend the given DCG for mini-algol to support the for-statement. Example:</p>
<pre>
% for a := 0 & a < 5 & a := a + 1 do
% begin
%   print(a)
% end
?- _Program = [begin,for,a,:=,0,&,a,<,5,&,a,:=,a+1,do,begin,print(a),end,end],
    algol_for(_F, _Program, []),
    apply([a=2], Output, _F).
  Output = [a=5,printout=[0,1,2,3,4]].
</pre>
'''

hint = {}
