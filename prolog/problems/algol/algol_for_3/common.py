from operator import itemgetter
import socket

import prolog.engine
import prolog.util
from server.hints import Hint, HintPopup

id = 178
number = 30
visible = True
facts = None

solution = '''\
algol_for(fun(S0,S,apply178([printout=[]|S0],S,Minstructs))) -->
  [begin], instructs178(Minstructs), [end].

instructs178(Minstr) --> instr178(Minstr).
instructs178(fun(S0,S,
  (apply178(S0,S1,Minstr),
   apply178(S1,S,Minstructs))))
  -->
  instr178(Minstr), instructs178(Minstructs).

instr178(Massign) --> assign178(Massign).
instr178(fun(S0,[printout = L1|S1],
  (memb(X = V,S0),
   del(printout = L0,S0,S1),
   conc(L0,[V],L1))))
  -->
  [print(X)].
instr178(fun(S0,S,
  loop178(S0,Mcond,Minstructs,S)))
  -->
  [while], cond178(Mcond), [do,begin], instructs178(Minstructs), [end].
instr178(fun(S0, S,
  (apply178(S0, S1, Minit),
   for178(S1, Mcond, Mstep, Mbody, S))))
  -->
  [for], assign178(Minit), [&], cond178(Mcond), [&], assign178(Mstep), [do, begin], instructs178(Mbody), [end].

assign178(fun(S0,[X = Value|S1],
  (apply178(S0,Value,Mexpr),
   del(X = _,S0,S1))))
  -->
  var178(X), [:=], expr178(Mexpr).

cond178( fun( S, TruthVal,
  (apply178(S,Val1,ME1),
   apply178(S,Val2,ME2),
   (Val1 < Val2,!,TruthVal = true ; TruthVal = false))))
  -->
  expr178(ME1), [<], expr178(ME2).

var178(X) --> [X], {atom(X)}.

expr178(fun(S,Value,eval(Expr,S,Value))) -->
  [Expr].

apply178(In, Out, fun(In, Out, Goals)) :-
  call(Goals).

loop178( State0, Mcond, _, State0) :-
  apply178( State0, false, Mcond), !.
loop178( S0, Mcond, MBody, S) :-
  copy_term( MBody, MBodyCopy),
  apply178( S0, S1, MBody),
  loop178( S1, Mcond, MBodyCopy, S).

for178(S0, Mcond, _, _, S0) :-
  apply178(S0, false, Mcond), !.
for178(S0, Mcond, Mstep, Mbody, S) :-
  copy_term(Mbody, Mbody2),
  copy_term(Mstep, Mstep2),
  apply178(S0, S1, Mbody),
  apply178(S1, S2, Mstep),
  for178(S2, Mcond, Mstep2, Mbody2, S).

eval178( N, _, N) :-
  number178( N), !.
eval178( X, State, Val) :- % A program variable
  atom( X), !,
  memb( X = Val, State).
eval178( E1 + E2, State, Val) :- !,
  eval178( E1, State, V1),
  eval178( E2, State, V2),
  Val is V1 + V2.
eval178( E1 - E2, State, Val) :- !,
  eval178( E1, State, V1),
  eval178( E2, State, V2),
  Val is V1 - V2.
eval178( E1 * E2, State, Val) :- !,
  eval178( E1, State, V1),
  eval178( E2, State, V2),
  Val is V1 * V2.
eval178( E1 / E2, State, Val) :- !,
  eval178( E1, State, V1),
  eval178( E2, State, V2),
  Val is V1 /  V2.
'''

initial = '''\
% program
algol_for(fun(S0, S, apply([printout=[]|S0], S, Minstructs))) -->
    [begin], instructs(Minstructs), [end].

% sequence of instructions
instructs(Minstr) --> instr(Minstr).
instructs(fun(S0,S,
    (apply(S0,S1,Minstr),
     apply(S1,S,Minstructs))))
    -->
    instr(Minstr), instructs(Minstructs).

% statement (instruction)
instr(Massign) --> assign(Massign).

% put the current value of a variable into the output list
instr(fun(S0, [printout = L1|S1],
    (memb(X = V,S0),
     del(printout = L0, S0, S1),
     conc(L0, [V], L1))))
    -->
    [print(X)].

% while-loop
instr(fun(S0, S,
    loop(S0, Mcond, Minstructs, S)))
    -->
    [while], cond(Mcond), [do, begin], instructs(Minstructs), [end].

% assignment statement
assign(fun(S0, [X = Value|S1],
    (apply(S0, Value, Mexpr),
     del(X = _, S0, S1))))
    -->
    var(X), [':='], expr(Mexpr).

% conditional expression
cond(fun(S, TruthVal,
    (apply(S, Val1, ME1),
     apply(S, Val2, ME2),
     (Val1 < Val2, !, TruthVal = true ; TruthVal = false))))
    -->
    expr(ME1), ['<'], expr(ME2).

% variable
var(X) --> [X], { atom(X) }.

% expression
expr(fun(S, Value, eval(Expr, S, Value))) -->
    [Expr].   % Expr is a Prolog arithmetic expression, e.g. x + 2*y + 3

% helper predicates
% function call
apply(In, Out, fun(In, Out, Goals)):-
    call(Goals).

% loop
loop(State0, Mcond, _, State0) :-
    apply(State0, false, Mcond), !. % condition is false
loop(S0, Mcond, MBody, S) :-
    copy_term(MBody, MBodyCopy),    % copy goals
    apply(S0, S1, MBody),           % run loop body
    loop(S1, Mcond, MBodyCopy, S).  % next iteration

% expression evaluation
eval(N, _, N) :-
    number(N), !.
eval(X, State, Val) :-  % a program variable
    atom(X), !,
    memb(X = Val, State).
eval(E1 + E2, State, Val) :- !,
    eval(E1, State, V1),
    eval(E2, State, V2),
    Val is V1 + V2.
eval(E1 - E2, State, Val) :- !,
    eval(E1, State, V1),
    eval(E2, State, V2),
    Val is V1 - V2.
eval(E1 * E2, State, Val) :- !,
    eval(E1, State, V1),
    eval(E2, State, V2),
    Val is V1 * V2.
eval(E1 / E2, State, Val) :- !,
    eval(E1, State, V1),
    eval(E2, State, V2),
    Val is V1 / V2.
'''

test_cases = [
    ('findall(W, (W = [_], algol_for(_, W, [])), Words), Words == []',
        [{}]),
    ('algol_for(F, [begin,for,a,:=,0,&,a,<,9,&,a,:=,a+1,do,begin,print(a),a,:=,a+1,end,end], []), \
                   apply([a=0], Out, F), msort(Out, OutSorted), \
		           OutSorted == [a=10,printout=[0,2,4,6,8]]',
        [{}]),
    ('algol_for(F, [begin,for,a,:=,1,&,a,<,32,&,a,:=,a*2,do,begin,print(a),end,end], []), \
                   apply([a=0], Out, F), msort(Out, OutSorted), \
		           OutSorted == [a=32,printout=[1,2,4,8,16]]',
        [{}]),
]

def test(code, aux_code):
    n_correct = 0
    engine_id = None
    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)
        if engine_id is not None and 'error' not in map(itemgetter(0), output):
            # Engine successfully created, and no syntax error in program.
            for query, answers in test_cases:
                if prolog.engine.check_answers(engine_id, query=query, answers=answers, timeout=1.0):
                    n_correct += 1
    except socket.timeout:
        pass
    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_cases)}}]
    return n_correct, len(test_cases), hints

