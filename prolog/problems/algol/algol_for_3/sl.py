name = 'algol_for/3'
slug = 'interpreter za mini-algol s for stavkom'

description = '''\
<p>Razširi podano gramatiko za jezik mini-algol, da bo le-ta podprla uporabo stavka "for". Primer:</p>
<pre>
% for a := 0 & a < 5 & a := a + 1 do
% begin
%   print(a)
% end
?- _Program = [begin,for,a,:=,0,&,a,<,5,&,a,:=,a+1,do,begin,print(a),end,end],
    algol_for(_F, _Program, []),
    apply([a=2], Output, _F).
  Output = [a=5,printout=[0,1,2,3,4]].
</pre>
'''

hint = {}
