name = 'algol/3'
slug = 'interpreter za mini-algol'

description = '''\
<p>DCG za mini-algol.</p>
<pre>
% uporabi (apply) funkcijo nad začetnim stanjem
?- apply([a=2], Out, fun(_In, Out, eval(a+3, _In, Out))).
  Out = 5.

% a := a+b
% b := a-b
% a := a-b
?- _Program = [begin,a,:=,a+b,b,:=,a-b,a,:=,a-b,end],
    algol(_F, _Program, []),
    apply([a=3,b=5], Output, _F).
  Output = [a=5,b=3,printout=[]].

% a := 0
% while a < 10 do
% begin
%   print(a)
%   a := a+1
% end
?- _Program = [begin,a,:=,0,while,a,<,10,do,begin,print(a),a,:=,a+1,end,end],
    algol(_F, _Program, []),
    apply([a=3], Output, _F).
  Output = [a=10,printout=[0,1,2,3,4,5,6,7,8,9]].
</pre>
'''

hint = {}
