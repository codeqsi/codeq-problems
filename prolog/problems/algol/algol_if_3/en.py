name = 'algol_if/3'
slug = 'interpreter for mini-algol with if-statement'

description = '''\
<p>Extend the given DCG for mini-algol to support the if-statement. You can assume that both branches are present in every if-statement. Example:</p>
<pre>
% if a < b then
%   print(a)
% else
%   print(b)
% end
?- _Program = [begin,if,a,<,b,then,print(a),else,print(b),end,end],
    algol_if(_F, _Program, []),
    apply([a=3,b=5], Output, _F).
  Output = [a=3,b=5,printout=[3]].
</pre>
'''

hint = {}
