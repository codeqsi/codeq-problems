name = 'algol_if/3'
slug = 'interpreter za mini-algol z if stavkom'

description = '''\
<p>Razširi podano gramatiko za jezik mini-algol, da bo le-ta podprla uporabo stavka "if-then-else".
Privzemi, da morata obe veji (then in else) biti prisotni v stavku. Primer:</p>
<pre>
% if a < b then
%   print(a)
% else
%   print(b)
% end
?- _Program = [begin,if,a,<,b,then,print(a),else,print(b),end,end],
    algol_if(_F, _Program, []),
    apply([a=3,b=5], Output, _F).
  Output = [a=3,b=5,printout=[3]].
</pre>
'''

hint = {}
