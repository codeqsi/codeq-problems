from operator import itemgetter
import prolog.engine
import server.problems

id = 129
number = 30
visible = True
facts = None

solution = '''\
memb129(X, [X|_]).
memb129(X, [_|T]) :-
  memb129(X, T).

intersect([], _, []).
intersect([H|T], S2, [H|I]) :-
  memb129(H, S2), !,
  intersect(T, S2, I).
intersect([_|T], S2, I):-
  intersect(T, S2, I).
'''

test_cases = [
    ('intersect([], [a, b, d], X)',
        [{'X': '[]'}]),
    ('intersect([g, h, r], [], X)',
        [{'X': '[]'}]),
    ('intersect([2, 5, 4, 7, 1, 9, 8], [0, 2, 5, 6, 7, 9, 1], A), msort(A, X)',
        [{'X': '[1, 2, 5, 7, 9]'}]),
    ('intersect([4, 7, 1, 9, 8], [4, 2, 5, 6, 7, 9, 1], A), msort(A, X)',
        [{'X': '[1, 4, 7, 9]'}]),
    ('intersect([9, 3, 4, 2, 1, 7], [a, b, d], X)',
        [{'X': '[]'}]),
    ('intersect([9, 3, 4, 2, 1, 7], [4, 2, 5, 6, 7, 9, 1], A), msort(A, X)',
        [{'X': '[1, 2, 4, 7, 9]'}]),
]

def test(code, aux_code):
    n_correct = 0
    engine_id = None
    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)
        if engine_id is not None and 'error' not in map(itemgetter(0), output):
            # Engine successfully created, and no syntax error in program.
            for query, answers in test_cases:
                if prolog.engine.check_answers(engine_id, query=query, answers=answers, timeout=1.0):
                    n_correct += 1
    except socket.timeout:
        pass
    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_cases)}}]
    return n_correct, len(test_cases), hints

def hint(code, aux_code):
    # TODO
    return []
