name = 'intersect/3'
slug = 'Poišči presek dveh množic'

description = '''\
<p><code>intersect(S1, S2, I)</code>: seznam <code>I</code> vsebuje presek elementov v <code>S1</code> in <code>S2</code>.</p>
<pre>
?- intersect([1,5,6,3,4,2], [8,1,5,9,4,3], I).
  I = [1,5,3,4].
</pre>'''

hint = {}
