name = 'intersect/3'
slug = 'find the intersection of two sets'

description = '''\
<p><code>intersect(S1, S2, I)</code>: the list <code>I</code> contains every element that appears in both <code>S1</code> and <code>S2</code>, with no duplicates.</p>
<pre>
?- intersect([1,5,6,3,4,2], [8,1,5,9,4,3], I).
  I = [1,5,3,4].
</pre>'''

hint = {}
