name = 'count/3'
slug = 'find the number of occurrences of an element in list'

description = '''\
<p><code>count(X, L, N)</code>: <code>N</code> is the number of times the element <code>X</code> appears in the list <code>L</code>.</p>
<pre>
?- count(1, [1,2,1,3,1], N).
  N = 3.
</pre>'''

hint = {}
