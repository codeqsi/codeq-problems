from operator import itemgetter
import prolog.engine
import server.problems

id = 131
number = 50
visible = True
facts = None

solution = '''\
memb131(X, [X|_]).
memb131(X, [_|T]) :-
  memb131(X, T).

is_superset(_, []).
is_superset(S1, [H|T]) :-
  memb131(H, S1),
  is_superset(S1, T).
'''

test_cases = [
    ('is_superset([], X)',
        [{'X': '[]'}]),
    ('is_superset([3, 2, 1, 4, 6, 7], [2, 1, 3])',
        [{}]),
    ('\+ is_superset([3, 2, 1, 4, 6, 7], [2, 1, 0, 3])',
        [{}]),
    ('is_superset([3, 2, 1, 4, 6, 7], [3, 2, 1, 4, 6, 7])',
        [{}]),
]

def test(code, aux_code):
    n_correct = 0
    engine_id = None
    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)
        if engine_id is not None and 'error' not in map(itemgetter(0), output):
            # Engine successfully created, and no syntax error in program.
            for query, answers in test_cases:
                if prolog.engine.check_answers(engine_id, query=query, answers=answers, timeout=1.0):
                    n_correct += 1
    except socket.timeout:
        pass
    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_cases)}}]
    return n_correct, len(test_cases), hints

def hint(code, aux_code):
    # TODO
    return []
