name = 'is_superset/2'
slug = 'check if one set is a superset of the other'

description = '''\
<p><code>is_superset(S1, S2)</code>: the set <code>S1</code> is a superset (contains all elements) of <code>S2</code>.</p>
<pre>
?- is_superset([3,2,1,4,5,9], [2,1,3,5]).
  true.
?- is_superset([3,2,1,4,5,9], [2,1,3,5,0]).
  false.
</pre>'''

hint = {}
