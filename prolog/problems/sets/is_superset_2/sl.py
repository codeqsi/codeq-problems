name = 'is_superset/2'
slug = 'Preveri, če je prva množica nadmnožica od druge'

description = '''\
<p><code>is_superset(S1, S2)</code>: množica <code>S1</code> je nadmnožica (vsebuje vse elemente) od <code>S2</code>.</p>
<pre>
?- is_superset([3,2,1,4,5,9], [2,1,3,5]).
  true.
?- is_superset([3,2,1,4,5,9], [2,1,3,5,0]).
  false.
</pre>'''

hint = {}
