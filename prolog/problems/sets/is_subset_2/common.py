from operator import itemgetter
import prolog.engine
import server.problems

id = 132
number = 60
visible = True
facts = None

solution = '''\
memb132(X, [X|_]).
memb132(X, [_|T]) :-
  memb132(X, T).

is_subset([], _).
is_subset([H|T], S2) :-
  memb132(H, S2),
  is_subset(T, S2).
'''

test_cases = [
    ('is_subset(X, [])',
        [{'X': '[]'}]),
    ('is_subset([2, 1, 3], [3, 2, 1, 4, 6, 7])',
        [{}]),
    ('\+ is_subset([2, 1, 0, 3], [3, 2, 1, 4, 6, 7])',
        [{}]),
    ('is_subset([3, 2, 1, 4, 6, 7], [3, 2, 1, 4, 6, 7])',
        [{}]),
]

def test(code, aux_code):
    n_correct = 0
    engine_id = None
    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)
        if engine_id is not None and 'error' not in map(itemgetter(0), output):
            # Engine successfully created, and no syntax error in program.
            for query, answers in test_cases:
                if prolog.engine.check_answers(engine_id, query=query, answers=answers, timeout=1.0):
                    n_correct += 1
    except socket.timeout:
        pass
    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_cases)}}]
    return n_correct, len(test_cases), hints

def hint(code, aux_code):
    # TODO
    return []
