name = 'is_subset/2'
slug = 'check if one set is a subset of another'

description = '''\
<p><code>is_subset(S1, S2)</code>: the set <code>S1</code> is a subset of <code>S2</code>.</p>
<pre>
?- is_subset([2,1,3,5,0], [3,2,1,4,5,9]).
  false.
?- is_subset([2,1,3,5], [3,2,1,4,5,9]).
  true.
</pre>'''

hint = {}
