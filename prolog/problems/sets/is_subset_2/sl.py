name = 'is_subset/2'
slug = 'Preveri, če je prva množica podmnožica od druge'

description = '''\
<p><code>is_subset(S1, S2)</code>: množica <code>S1</code> je podmnožica od <code>S2</code>.</p>
<pre>
?- is_subset([2,1,3,5,0], [3,2,1,4,5,9]).
  false.
?- is_subset([2,1,3,5], [3,2,1,4,5,9]).
  true.
</pre>'''

hint = {}
