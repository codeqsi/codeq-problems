name = 'diff/3'
slug = 'Poišči razliko dveh množic'

description = '''\
<p><code>diff(S1, S2, D)</code>: seznam <code>D</code> vsebuje elemente iz <code>S1</code>, ki niso v <code>S2</code>.</p>
<pre>
?- diff([2,3,5,1,7,9], [3,7,4,5,6], D).
  D = [2,1,9].
</pre>'''

hint = {}
