name = 'diff/3'
slug = 'find the difference of two sets'

description = '''\
<p><code>diff(S1, S2, D)</code>: the list <code>D</code> contains all elements of <code>S1</code> that don't appear in <code>S2</code>, with no duplicates.</p>
<pre>
?- diff([2,3,5,1,7,9], [3,7,4,5,6], D).
  D = [2,1,9].
</pre>'''

hint = {}
