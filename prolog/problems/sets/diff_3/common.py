from operator import itemgetter
import prolog.engine
import server.problems

id = 130
number = 40
visible = True
facts = None

solution = '''\
memb130(X, [X|_]).
memb130(X, [_|T]) :-
  memb130(X, T).

diff([], _, []).
diff([H|T], S2, [H|D]) :-
  \+ memb130(H, S2),
  diff(T, S2, D).
diff([H|T], S2, D) :-
  memb130(H, S2),
  diff(T, S2, D).
'''

test_cases = [
    ('diff([], [t, e, q], X)',
        [{'X': '[]'}]),
    ('diff([q, r, t], [], A), msort(A, X)',
        [{'X': '[q, r, t]'}]),
    ('diff([1, 2, 3, 8, 4, 6], [6, 0, 3, 4], A), msort(A, X)',
        [{'X': '[1, 2, 8]'}]),
    ('diff([1, 2, 3, 8, 7, 4, 6], [7, 8, 6], A), msort(A, X)',
        [{'X': '[1, 2, 3, 4]'}]),
    ('diff([1, 8, 2, 3, 7, 5], [7], A), msort(A, X)',
        [{'X': '[1, 2, 3, 5, 8]'}]),
    ('diff([t, w, q], [t, e, q, w, b], X)',
        [{'X': '[]'}]),
]

def test(code, aux_code):
    n_correct = 0
    engine_id = None
    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)
        if engine_id is not None and 'error' not in map(itemgetter(0), output):
            # Engine successfully created, and no syntax error in program.
            for query, answers in test_cases:
                if prolog.engine.check_answers(engine_id, query=query, answers=answers, timeout=1.0):
                    n_correct += 1
    except socket.timeout:
        pass
    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_cases)}}]
    return n_correct, len(test_cases), hints

def hint(code, aux_code):
    # TODO
    return []
