from operator import itemgetter
import prolog.engine
import server.problems

id = 134
number = 80
visible = True
facts = None

solution = '''\
subset134([], []).
subset134([H|T], [H|T1]) :-
  subset134(T, T1).
subset134([_|T], T1) :-
  subset134(T, T1).

powerset(Set, PowerSet) :-
  findall(S, subset134(Set, S), PowerSet).
'''

test_cases = [
    ('powerset([], X)',
        [{'X': '[[]]'}]),
    ('powerset([1, 2], A), length(A, 4), lists:flatten(A, Y), msort(Y, X)',
        [{'X': '[1, 1, 2, 2]'}]),
    ('powerset([1, 2, 3], A), length(A, 8), lists:flatten(A, Y), msort(Y, X)',
        [{'X': '[1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3]'}]),
]

def test(code, aux_code):
    n_correct = 0
    engine_id = None
    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)
        if engine_id is not None and 'error' not in map(itemgetter(0), output):
            # Engine successfully created, and no syntax error in program.
            for query, answers in test_cases:
                if prolog.engine.check_answers(engine_id, query=query, answers=answers, timeout=1.0):
                    n_correct += 1
    except socket.timeout:
        pass
    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_cases)}}]
    return n_correct, len(test_cases), hints

def hint(code, aux_code):
    # TODO
    return []
