name = 'powerset/2'
slug = 'find all subsets of a set'

description = '''\
<p><code>powerset(Set, Powerset)</code>: the list <code>Powerset</code> contains all subsets of <code>Set</code>.</p>
<pre>
?- powerset([1,2,3], L).
  L = [[1,2,3],[1,2],[1,3],[1],[2,3],[2],[3],[]].
</pre>'''

hint = {}
