name = 'powerset/2'
slug = 'Poišči potenčno množico od dane množice'

description = '''\
<p><code>powerset(Set, Powerset)</code>: množica <code>Powerset</code> vsebuje vse podmnožice od množice <code>Set</code>.</p>
<pre>
?- powerset([1,2,3], L).
  L = [[1,2,3],[1,2],[1,3],[1],[2,3],[2],[3],[]].
</pre>'''

hint = {}
