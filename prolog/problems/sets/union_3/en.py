name = 'union/3'
slug = 'find the union of two sets'

description = '''\
<p><code>union(S1, S2, U)</code>: the list <code>U</code> contains all elements of <code>S1</code> and <code>S2</code>, with no duplicates.</p>
<pre>
?- union([1,5,2,3], [3,4,8,2], U).
  U = [1,5,3,4,8,2].
</pre>'''

hint = {}
