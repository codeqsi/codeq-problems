name = 'union/3'
slug = 'Poišči unijo dveh množic'

description = '''\
<p><code>union(S1, S2, U)</code>: seznam <code>U</code> predstavlja unijo elementov v seznamih <code>S1</code> in <code>S2</code>, duplikatov (kot se za množice spodobi) ni.</p>
<pre>
?- union([1,5,2,3], [3,4,8,2], U).
  U = [1,5,3,4,8,2].
</pre>'''

hint = {}
