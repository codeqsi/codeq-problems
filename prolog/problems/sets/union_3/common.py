from operator import itemgetter
import prolog.engine
import server.problems

id = 128
number = 20
visible = True
facts = None

solution = '''\
memb128(X, [X|_]).
memb128(X, [_|T]) :-
  memb128(X, T).

union([], S2, S2).
union([H|T], S2, [H|U]) :-
  \+ memb128(H, S2),
  union(T, S2, U).
union([H|T], S2, U) :-
  memb128(H, S2),
  union(T, S2, U).
'''

test_cases = [
    ('union([], [3, 2, 5, 1], A), msort(A, X)',
        [{'X': '[1, 2, 3, 5]'}]),
    ('union([3, 2, 5, 1], [], A), msort(A, X)',
        [{'X': '[1, 2, 3, 5]'}]),
    ('union([3, 2, 5, 1], [8, 4, 2, 3, 5, 1, 9], A), msort(A, X)',
        [{'X': '[1, 2, 3, 4, 5, 8, 9]'}]),
    ('union([2, 3, 5, 1, 9], [3, 0, 9, 5, 6], A), msort(A, X)',
        [{'X': '[0, 1, 2, 3, 5, 6, 9]'}]),
]

def test(code, aux_code):
    n_correct = 0
    engine_id = None
    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)
        if engine_id is not None and 'error' not in map(itemgetter(0), output):
            # Engine successfully created, and no syntax error in program.
            for query, answers in test_cases:
                if prolog.engine.check_answers(engine_id, query=query, answers=answers, timeout=1.0):
                    n_correct += 1
    except socket.timeout:
        pass
    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_cases)}}]
    return n_correct, len(test_cases), hints

def hint(code, aux_code):
    # TODO
    return []
