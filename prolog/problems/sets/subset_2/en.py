name = 'subset/2'
slug = 'generate all subsets of a set'

description = '''\
<p><code>subset(Set, Subset)</code>: the set <code>Subset</code> is a subset of <code>Set</code>. This predicate should generate all valid solutions, one by one.</p>
<pre>
?- subset([1,2,3], SS).
  SS = [1,2,3] ;
  SS = [1,2] ;
  SS = [1,3] ;
  SS = [1] ;
  SS = [2,3] ;
  SS = [2] ;
  SS = [3] ;
  SS = [].
</pre>'''

hint = {}
