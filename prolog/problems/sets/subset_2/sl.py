name = 'subset/2'
slug = 'Generiraj vse podmnožice dane množice'

description = '''\
<p><code>subset(Set, Subset)</code>: množica <code>Subset</code> je podmnožica od <code>Set</code>. Ta predikat naj, eno po eno, generira vse veljavne podmnožice.</p>
<pre>
?- subset([1,2,3], SS).
  SS = [1,2,3] ;
  SS = [1,2] ;
  SS = [1,3] ;
  SS = [1] ;
  SS = [2,3] ;
  SS = [2] ;
  SS = [3] ;
  SS = [].
</pre>'''

hint = {}
