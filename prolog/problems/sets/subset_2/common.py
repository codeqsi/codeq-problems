from operator import itemgetter
import prolog.engine
import server.problems

id = 133
number = 70
visible = True
facts = None

solution = '''\
subset([], []).
subset([H|T], [H|T1]) :-
  subset(T, T1).
subset([_|T], T1) :-
  subset(T, T1).
'''

test_cases = [
    ('subset([], X)',
        [{'X': '[]'}]),
    ('subset([1, 3, 5, 7], A), msort(A, X)',
        [{'X': '[]'}, {'X': '[7]'}, {'X': '[5]'}, {'X': '[5, 7]'}, {'X': '[3]'},
         {'X': '[3, 7]'}, {'X': '[3, 5]'}, {'X': '[3, 5, 7]'}, {'X': '[1]'},
         {'X': '[1, 7]'}, {'X': '[1, 5]'}, {'X': '[1, 5, 7]'}, {'X': '[1, 3]'},
         {'X': '[1, 3, 7]'}, {'X': '[1, 3, 5]'}, {'X': '[1, 3, 5, 7]'}]),
    ('subset([8, 4, 6, 2], A), msort(A, X)',
        [{'X': '[]'}, {'X': '[2]'}, {'X': '[6]'}, {'X': '[2, 6]'}, {'X': '[4]'},
         {'X': '[2, 4]'}, {'X': '[4, 6]'}, {'X': '[2, 4, 6]'}, {'X': '[8]'},
         {'X': '[2, 8]'}, {'X': '[6, 8]'}, {'X': '[2, 6, 8]'}, {'X': '[4, 8]'},
         {'X': '[2, 4, 8]'}, {'X': '[4, 6, 8]'}, {'X': '[2, 4, 6, 8]'}]),
    ('subset([5, 6, 7, 0, 2], A), msort(A, X)',
        [{'X': '[]'}, {'X': '[2]'}, {'X': '[0]'}, {'X': '[0, 2]'}, {'X': '[7]'},
         {'X': '[2, 7]'}, {'X': '[0, 7]'}, {'X': '[0, 2, 7]'}, {'X': '[6]'},
         {'X': '[2, 6]'}, {'X': '[0, 6]'}, {'X': '[0, 2, 6]'}, {'X': '[6, 7]'},
         {'X': '[2, 6, 7]'}, {'X': '[0, 6, 7]'}, {'X': '[0, 2, 6, 7]'},
         {'X': '[5]'}, {'X': '[2, 5]'}, {'X': '[0, 5]'}, {'X': '[0, 2, 5]'},
         {'X': '[5, 7]'}, {'X': '[2, 5, 7]'}, {'X': '[0, 5, 7]'},
         {'X': '[0, 2, 5, 7]'}, {'X': '[5, 6]'}, {'X': '[2, 5, 6]'},
         {'X': '[0, 5, 6]'}, {'X': '[0, 2, 5, 6]'}, {'X': '[5, 6, 7]'},
         {'X': '[2, 5, 6, 7]'}, {'X': '[0, 5, 6, 7]'}, {'X': '[0, 2, 5, 6, 7]'}]),
]

def test(code, aux_code):
    n_correct = 0
    engine_id = None
    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)
        if engine_id is not None and 'error' not in map(itemgetter(0), output):
            # Engine successfully created, and no syntax error in program.
            for query, answers in test_cases:
                if prolog.engine.check_answers(engine_id, query=query, answers=answers, timeout=1.0):
                    n_correct += 1
    except socket.timeout:
        pass
    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_cases)}}]
    return n_correct, len(test_cases), hints

def hint(code, aux_code):
    # TODO
    return []
