name = 'quick_sort/2'
slug = 'Uredi seznam z algoritmom quicksort'

description = '''\
<p><code>quick_sort(L, SL)</code>: seznam <code>SL</code> vsebuje elemente iz <code>L</code> urejene v naraščajočem vrstnem redu. Uporabi predikat <code>pivoting/4</code> za implementacijo algoritma quicksort.</p>
<pre>
?- quick_sort([2,3,1,5,4], L).
  L = [1,2,3,4,5].
</pre>'''

plan = ['''\
<p>Deli in vladaj! In uporabi prejšnje rešitve, seveda :)</p>
''', '''\
<p>Vzami glavo stran, uporabi jo za pivot, razdeli rep na manjše in večje elemente. Na dobljenih seznamih
uporabi rekurzijo saj sta manjša (v najslabšem primeru samo za glavo krajša -- tukaj tudi lepo vidiš, zakaj je
quicksort slab ravno pri že urejenih seznamih). Na koncu vse skupaj preprosto združi!</p>
''', '''\
<p>Če je seznam <code>L</code> sestavljen iz glave <code>P</code> in repa <code>T</code> in če rep
razdelimo na seznam manjših in seznam večjih elementov glede na pivot <code>P</code> ter predpostavimo, da ta
seznama pravilno uredi rekurzija v seznama <code>SortedSmallerElems</code> in <code>SortedGreaterElems</code>
ter potem ta seznama preprosto staknemo enega za drugim in med njiju dodamo pivot/glavo <code>P</code>, potem
dobimo pravilno urejen začetni seznam <code>L</code>.</p>
''']

hint = {
    'eq_instead_of_equ': '''\
<p>Operator <code>==</code> je strožji od operatorja <code>=</code> v smislu, da je za slednjega dovolj,
da elementa lahko naredi enaka (unifikacija).</p>
<p>Seveda pa lahko nalogo rešiš brez obeh omenjenih operatorjev, spomni se, da lahko unifikacijo narediš
implicitno že kar v argumentih predikata (glavi stavka).</p>
''',

    'eq_instead_of_equ_markup': '''\
<p>Morda bi bil bolj primeren operator za unifikacijo (<code>=</code>)?</p>
''',

    'base_case': '''\
<p>Si pomislil na robni pogoj? Kateri seznam lahko urediš povsem brez dela?</p>
''',

    'recursive_case': '''\
<p>Robni primer deluje. Kaj pa rekurzivni, splošni, primer?</p>
''',

    'predicate_always_false': '''\
<p>Vse kaže, da tvoj predikat vedno vrne "false". Si mu dal pravilno ime, si se morda pri imenu zatipkal?</p>
<p>Če je ime pravilno, se morda splača preveriti tudi, če se nisi zatipkal kje drugje,
je morda kakšna pika namesto vejice ali obratno, morda kakšna spremenljivka z malo začetnico?</p>
<p>Možno je seveda tudi, da so tvoji pogoji prestrogi ali celo nemogoči (kot bi bila npr. zahteva,
da je <code>X</code> <em>hkrati</em> večji in manjši od <code>Y</code> ali kaj podobno logično sumljivega).</p>
''',

    'timeout': '''\
<p>Je morda na delu potencialno neskončna rekurzija? Kako se bo ustavila?</p>
<p>Morda pa je kriv tudi manjkajoč, neustrezen ali preprosto nekompatibilen (s splošnim primerom) robni pogoj?</p>
''',

    'arbitrary_base_case': '''\
<p>Kako je lahko urejen prazen seznam karkoli ali seznam s poljubnim elementom (spremenljivka brez vrednosti)?</p>
''',

    'forgotten_pivot': '''\
<p>Si morda pozabil glavo, ki si jo uporabil za pivot, ob vračanju dati nazaj?</p>
''',
}
