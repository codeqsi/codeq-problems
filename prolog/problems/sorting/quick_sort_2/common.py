from operator import itemgetter
import socket

import prolog.engine
from server.hints import Hint, HintPopup

id = 125
number = 60
visible = True
facts = None

solution = '''\
conc125([], L, L).
conc125([H|T], L2, [H|L]) :-
  conc125(T, L2, L).
pivoting125(_, [], [], []).
pivoting125(P, [H|T], [H|S], G) :-
  H =< P,
  pivoting125(P, T, S, G).
pivoting125(P, [H|T], S, [H|G]) :-
  H > P,
  pivoting125(P, T, S, G).
quick_sort([], []).
quick_sort([Pivot|T], Sorted) :-
  pivoting125(Pivot, T, Smaller, Greater),
  quick_sort(Smaller, SortedSmaller),
  quick_sort(Greater, SortedGreater),
  conc125(SortedSmaller, [Pivot|SortedGreater], Sorted).
'''

hint_type = {
    'eq_instead_of_equ_markup': HintPopup('eq_instead_of_equ_markup'),
    'eq_instead_of_equ': Hint('eq_instead_of_equ'),
    'predicate_always_false': Hint('predicate_always_false'),
    'base_case': Hint('base_case'),
    'recursive_case': Hint('recursive_case'),
    'timeout': Hint('timeout'),
    'arbitrary_base_case': Hint('arbitrary_base_case'),
    'forgotten_pivot': Hint('forgotten_pivot'),
}

test_cases = [
    ('quick_sort([], X)',
        [{'X': '[]'}]),
    ('quick_sort([5, 3, 5, 5, 1, 2, 0], X)',
        [{'X': '[0, 1, 2, 3, 5, 5, 5]'}]),
    ('quick_sort([2, 1, 3, 5, 6, 0, 22, 3], X)',
        [{'X': '[0, 1, 2, 3, 3, 5, 6, 22]'}]),
    ('quick_sort([5, 4, 3, 2, 1], X)',
        [{'X': '[1, 2, 3, 4, 5]'}]),
]

def test(code, aux_code):
    n_correct = 0
    engine_id = None
    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)
        if engine_id is not None and 'error' not in map(itemgetter(0), output):
            # Engine successfully created, and no syntax error in program.
            for query, answers in test_cases:
                if prolog.engine.check_answers(engine_id, query=query, answers=answers, timeout=1.0):
                    n_correct += 1
    except socket.timeout:
        pass
    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_cases)}}]
    return n_correct, len(test_cases), hints

def hint(code, aux_code):
    tokens = prolog.util.tokenize(code)

    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)

        # strict equality testing instead of simple matching
        # this is usually (but not necessarily) wrong
        targets = [prolog.util.Token('EQ', '==')]
        marks = [(t.pos, t.pos + len(t.val)) for t in tokens if t in targets]
        if marks:
            return [{'id': 'eq_instead_of_equ_markup', 'start': m[0], 'end': m[1]} for m in marks] + \
                   [{'id': 'eq_instead_of_equ'}]

        if prolog.engine.ask_truthTO(engine_id, 'quick_sort([], [yowza])'):
            return [{'id': 'arbitrary_base_case'}]

        if prolog.engine.ask_truthTO(engine_id, '''\
                quick_sort([3, 5, 9, 6, 3, 7, 2], []),
                asserta(quick_sort([], [q])),
                quick_sort([3, 5, 9, 6, 3, 7, 2], [q, q, q, q, q, q, q, q]),
                retract(quick_sort([], [q]))'''):
            return [{'id': 'forgotten_pivot'}]

        # missing/failed base case
        if not prolog.engine.ask_truthTO(engine_id, 'quick_sort([], [])'):
            return [{'id': 'base_case'}]

        # target predicate seems to always be false
        if not prolog.engine.ask_truthTO(engine_id, 'quick_sort(_, _)'):
            return [{'id': 'predicate_always_false'}]

        # base case works, the recursive doesn't (but it doesn't timeout)
        # this may be left as the last, most generic hint
        if not prolog.engine.ask_truth(engine_id,
                'quick_sort([41, 82, -2, 100], [-2, 41, 82, 100])'):
            return [{'id': 'recursive_case'}]

    except socket.timeout as ex:
        return [{'id': 'timeout'}]

    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    return []
