name = 'isort/2'
slug = 'Uredi seznam z uporabo urejanja z vstavljanjem'

description = '''\
<p><code>isort(L, SL)</code>: seznam <code>SL</code> vsebuje elemente iz seznama <code>L</code> urejene v naraščajočem vrstnem redu. Uporabi predikat <code>sins/3</code> za implementacijo urejanja z vstavljanjem.</p>
<pre>
?- isort([2,3,1,5,4], L).
  L = [1,2,3,4,5].
</pre>'''

plan = ['''\
<p>Ob sprehodu po seznamu (pravzaprav ob vračanju) vsakič vstavi trenutni element na pravo mesto.</p>
''', '''\
<p>Ko greš po seznamu na vsakem koraku vzameš stran glavo (le-ta gre na sklad), rep pa gre v rekurzijo
(problem/seznam je nekoliko krajši in je to možno). Rekurzija ti vrne urejen rep, vse kar moraš narediti ti je,
da na pravo mesto vstaviš prej odvzeto glavo. Seveda za vstavljanje izkoristiš rešitev kakšne prejšnje naloge.</p>
''', '''\
<p>Če je seznam <code>L</code> sestavljen iz glave <code>H</code> in repa <code>T</code> in če predpostavimo,
da rep <code>T</code> rekurzija pravilno uredi v <code>SortedTail</code> in če v urejen rep <code>SortedTail</code>
na ustrezno mesto vstavimo glavo <code>H</code>, potem smo dobili urejen celoten seznam <code>L</code>.</p>
''']

hint = {
    'eq_instead_of_equ': '''\
<p>Operator <code>==</code> je strožji od operatorja <code>=</code> v smislu, da je za slednjega dovolj,
da elementa lahko naredi enaka (unifikacija).</p>
<p>Seveda pa lahko nalogo rešiš brez obeh omenjenih operatorjev, spomni se, da lahko unifikacijo narediš
implicitno že kar v argumentih predikata (glavi stavka).</p>
''',

    'eq_instead_of_equ_markup': '''\
<p>Morda bi bil bolj primeren operator za unifikacijo (<code>=</code>)?</p>
''',

    'base_case': '''\
<p>Si pomislil na robni pogoj? Kateri seznam lahko urediš povsem brez dela?</p>
''',

    'recursive_case': '''\
<p>Robni primer deluje. Kaj pa rekurzivni, splošni, primer?</p>
''',

    'predicate_always_false': '''\
<p>Vse kaže, da tvoj predikat vedno vrne "false". Si mu dal pravilno ime, si se morda pri imenu zatipkal?</p>
<p>Če je ime pravilno, se morda splača preveriti tudi, če se nisi zatipkal kje drugje,
je morda kakšna pika namesto vejice ali obratno, morda kakšna spremenljivka z malo začetnico?</p>
<p>Možno je seveda tudi, da so tvoji pogoji prestrogi ali celo nemogoči (kot bi bila npr. zahteva,
da je <code>X</code> <em>hkrati</em> večji in manjši od <code>Y</code> ali kaj podobno logično sumljivega).</p>
''',

    'timeout': '''\
<p>Je morda na delu potencialno neskončna rekurzija? Kako se bo ustavila?</p>
<p>Morda pa je kriv tudi manjkajoč, neustrezen ali preprosto nekompatibilen (s splošnim primerom) robni pogoj?</p>
''',

    'min_used': '''\
<p>Poskusi rešiti nalogo brez uporabe predikata <code>min/2</code>.</p>
''',
}
