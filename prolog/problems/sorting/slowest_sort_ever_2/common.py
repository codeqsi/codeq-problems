from operator import itemgetter
import socket

import prolog.engine
from server.hints import Hint, HintPopup

id = 126
number = 20
visible = True
facts = None

solution = '''\
del126(X, [X|T], T).
del126(X, [Y|T], [Y|L]) :-
  del126(X, T, L).

permute126([], []).
permute126(L, [X|P]) :-
  del126(X, L, L1),
  permute126(L1, P).

is_sorted126([_]).
is_sorted126([H1,H2|T]) :-
  H1 =< H2,
  is_sorted126([H2|T]).

slowest_sort_ever(L, S) :-
  permute126(L, S),
  is_sorted126(S).
'''

hint_type = {
    'eq_instead_of_equ_markup': HintPopup('eq_instead_of_equ_markup'),
    'eq_instead_of_equ': Hint('eq_instead_of_equ'),
    'predicate_always_false': Hint('predicate_always_false'),
    'timeout': Hint('timeout'),
    'no_permute': Hint('no_permute'),
    'no_isSorted': Hint('no_isSorted'),
}

test_cases = [
    ('slowest_sort_ever([], X)',
        [{'X': '[]'}]),
    ('slowest_sort_ever([5, 3, 5, 1], X)',
        [{'X': '[1, 3, 5, 5]'}]),
    ('slowest_sort_ever([2, 1, 5, 6], X)',
        [{'X': '[1, 2, 5, 6]'}]),
    ('slowest_sort_ever([5, 4, 3, 2], X)',
        [{'X': '[2, 3, 4, 5]'}]),
]

def test(code, aux_code):
    n_correct = 0
    engine_id = None
    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)
        if engine_id is not None and 'error' not in map(itemgetter(0), output):
            # Engine successfully created, and no syntax error in program.
            for query, answers in test_cases:
                if prolog.engine.check_answers(engine_id, query=query, answers=answers, timeout=1.0):
                    n_correct += 1
    except socket.timeout:
        pass
    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_cases)}}]
    return n_correct, len(test_cases), hints

def hint(code, aux_code):
    tokens = prolog.util.tokenize(code)

    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)

        # strict equality testing instead of simple matching
        # this is usually (but not necessarily) wrong
        targets = [prolog.util.Token('EQ', '==')]
        marks = [(t.pos, t.pos + len(t.val)) for t in tokens if t in targets]
        if marks:
            return [{'id': 'eq_instead_of_equ_markup', 'start': m[0], 'end': m[1]} for m in marks] + \
                   [{'id': 'eq_instead_of_equ'}]

        # does not call permute
        if not any(t.val == 'permute' for t in tokens):
            return [{'id': 'no_permute'}]

        # calls permute but not is_sorted
        if (any(t.val == 'permute' for t in tokens) and
                not any(t.val == 'is_sorted' for t in tokens)):
            return [{'id': 'no_isSorted'}]

        # target predicate seems to always be false
        if not prolog.engine.ask_truthTO(engine_id, 'slowest_sort_ever(_, _)'):
            return [{'id': 'predicate_always_false'}]

    except socket.timeout as ex:
        return [{'id': 'timeout'}]

    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    return []
