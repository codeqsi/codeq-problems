name = 'slowest_sort_ever/2'
slug = 'Uredi seznam elementov s pomočjo naključnih permutacij'

description = '''\
<p><code>slowest_sort_ever(L, SL)</code>: seznam <code>SL</code> vsebuje elemente seznama <code>L</code> v naraščajočem vrstnem redu. Namig: časovna zahtevnost algoritma naj bo zanemarljivih O(n * n!).</p>
<pre>
?- slowest_sort_ever([2,3,1,5,4], L).
  L = [1,2,3,4,5].
</pre>'''

plan = ['''\
<p>Ta naloga je malce bolj za šalo... Vse kar potrebuješ sta dve vrstici oz. dva prologova cilja.</p>
''', '''\
<p>Morda lahko izkoristiš rešitev kakšnih prejšnjih nalog?</p>
''', '''\
<p>Katera izmed prejšnjih nalog ima časovno zahtevnost O(n!)? Uporabi jo!</p>
''']

hint = {
    'eq_instead_of_equ': '''\
<p>Operator <code>==</code> je strožji od operatorja <code>=</code> v smislu, da je za slednjega dovolj,
da elementa lahko naredi enaka (unifikacija).</p>
<p>Seveda pa lahko nalogo rešiš brez obeh omenjenih operatorjev, spomni se, da lahko unifikacijo narediš
implicitno že kar v argumentih predikata (glavi stavka).</p>
''',

    'eq_instead_of_equ_markup': '''\
<p>Morda bi bil bolj primeren operator za unifikacijo (<code>=</code>)?</p>
''',

    'predicate_always_false': '''\
<p>Vse kaže, da tvoj predikat vedno vrne "false". Si mu dal pravilno ime, si se morda pri imenu zatipkal?</p>
<p>Če je ime pravilno, se morda splača preveriti tudi, če se nisi zatipkal kje drugje,
je morda kakšna pika namesto vejice ali obratno, morda kakšna spremenljivka z malo začetnico?</p>
<p>Možno je seveda tudi, da so tvoji pogoji prestrogi ali celo nemogoči (kot bi bila npr. zahteva,
da je <code>X</code> <em>hkrati</em> večji in manjši od <code>Y</code> ali kaj podobno logično sumljivega).</p>
''',

    'timeout': '''\
<p>Je morda na delu potencialno neskončna rekurzija? Kako se bo ustavila?</p>
<p>Morda pa je kriv tudi manjkajoč, neustrezen ali preprosto nekompatibilen (s splošnim primerom) robni pogoj?</p>
''',

    'no_permute': '''\
<p>Hmmm, premisli, katera izmed nalog, ki smo jih reševali do sedaj ima časovno zahtevnost O(n!)?
Kako bi jo lahko izkoristil?</p>
''',

    'no_isSorted': '''\
<p>Si na pravi poti, še malo. Morda lahko uporabiš še kakšno prejšnjo rešitev?</p>
''',
}
