name = 'slowest_sort_ever/2'
slug = 'sort a list by randomly permuting elements'

description = '''\
<p><code>slowest_sort_ever(L, SL)</code>: the list <code>SL</code> contains the elements of <code>L</code> sorted in non-decreasing order. Average and worst case running time is O(n * n!).</p>
<pre>
?- slowest_sort_ever([2,3,1,5,4], L).
  L = [1,2,3,4,5].
</pre>'''

plan = ['''\
<p>This exercise is mostly for fun... all you need are two lines, i.e. two prolog goals.</p>
''', '''\
<p>Perhaps you can reuse some previous solutions?</p>
''', '''\
<p>Which of the previous solutions has time complexity of O(n!)? Use it!</p>
''']

hint = {
    'eq_instead_of_equ': '''\
<p>The operator <code>==</code> is "stricter" than operator <code>=</code> in the sense that
for the latter it is enough to be able to make the two operands equal (unification).</p>
<p>Of course, you can also solve the exercise without explicit use of either of these two operators, just
remember that unification is implicitly performed with the predicate's arguments (head of clause).</p>
''',

    'eq_instead_of_equ_markup': '''\
<p>Perhaps the operator for unification (=) would be better?</p>
''',

    'predicate_always_false': '''\
<p>It seems your predicate is <em>always</em> "false". Did you give it the correct name,
or is it perhaps misspelled?</p>
<p>If the name is correct, check whether something else is misspelled, perhaps there is a full stop instead of
a comma or vice versa, or maybe you typed a variable name in lowercase?</p>
<p>It is, of course, also possible that your conditions are too restrictive, or even impossible to satisfy
(as would be, for example, the condition that <code>X</code> is <em>simultaneously</em> smaller and greater than
<code>Y</code>, or something similarly impossible).</p>
''',

    'timeout': '''\
<p>Is there an infinite recursion at work here? How will it ever stop?</p>
<p>Or perhaps is there a missing, faulty, or simply incompatible (with the general recursive case) base case?</p>
''',

    'no_permute': '''\
<p>Hmmm, which of the previous exercises has time complexity of O(n!)? How can you use it here?</p>
''',

    'no_isSorted': '''\
<p>You're on the right path, just a bit more to go. Perhaps you can reuse another previous exercise?</p>
''',
}
