name = 'is_sorted/1'
slug = 'Preveri, če so elementi seznama naraščajoče urejeni'

description = '''\
<p><code>is_sorted(L)</code>: elementi v seznamu <code>L</code> morajo biti urejeni v naraščajočem vrstnem redu.</p>
<pre>
?- is_sorted([2,3,6,8,12]).
  true.
?- is_sorted([2,3,1,6,5]).
  false.
</pre>'''

plan = ['''\
<p>Kot vedno, poskusi prevesti na manjši problem. Naredi ustrezno primerjavo na začetku seznama, rep
pa prepusti rekurziji.</p>
''', '''\
<p>Saj znaš dostopati do prvih dveh elementov seznama, kajne? Aritmetične operatorje za primerjanje pa smo
spoznali v prejšnjem sklopu.</p>
''', '''\
<p>Če je podani seznam <code>L</code> sestavljen iz glav <code>H1</code> in <code>H2</code> ter repa <code>T</code>
in predpostavimo, da je rep <code>T</code> z drugo glavo vred urejen ter nadalje velja, da je <code>H1</code> manjši
ali enak <code>H2</code>, potem je celoten seznam <code>L</code> urejen.</p>
''']

hint = {
    'eq_instead_of_equ': '''\
<p>Operator <code>==</code> je strožji od operatorja <code>=</code> v smislu, da je za slednjega dovolj,
da elementa lahko naredi enaka (unifikacija).</p>
<p>Seveda pa lahko nalogo rešiš brez obeh omenjenih operatorjev, spomni se, da lahko unifikacijo narediš
implicitno že kar v argumentih predikata (glavi stavka).</p>
''',

    'eq_instead_of_equ_markup': '''\
<p>Morda bi bil bolj primeren operator za unifikacijo (<code>=</code>)?</p>
''',

    'base_case': '''\
<p>Si pomislil na robni pogoj? Kaj je eden izmed najkrajših urejenih seznamov?</p>
''',

    'recursive_case': '''\
<p>Robni primer deluje. Kaj pa rekurzivni, splošni, primer?</p>
''',

    'predicate_always_false': '''\
<p>Vse kaže, da tvoj predikat vedno vrne "false". Si mu dal pravilno ime, si se morda pri imenu zatipkal?</p>
<p>Če je ime pravilno, se morda splača preveriti tudi, če se nisi zatipkal kje drugje,
je morda kakšna pika namesto vejice ali obratno, morda kakšna spremenljivka z malo začetnico?</p>
<p>Možno je seveda tudi, da so tvoji pogoji prestrogi ali celo nemogoči (kot bi bila npr. zahteva,
da je <code>X</code> <em>hkrati</em> večji in manjši od <code>Y</code> ali kaj podobno logično sumljivega).</p>
''',

    'timeout': '''\
<p>Je morda na delu potencialno neskončna rekurzija? Kako se bo ustavila?</p>
<p>Morda pa je kriv tudi manjkajoč, neustrezen ali preprosto nekompatibilen (s splošnim primerom) robni pogoj?</p>
''',

    '[]_base_case_missing': '''\
<p>Da bo rešitev popolna, morda dodatno poskrbiš še za poseben primer, to je prazen seznam. A ob tem
ne poruši prejšnjih rešitev.</p>
''',

    'duplicates_fail': '''\
<p>Si morda pozabil, da so v seznamu lahko tudi duplikati? Tudi spodnji seznam je urejen!</p>
<p><code>?- is_sorted([25,25,25,25]).</code></p>
''',

    'H1_instead_of_H2_sent_into_recursion': '''\
<p>Si morda v rekurzijo poslal napačno izmed obeh "glav" seznama?</p>
''',

    'base_case_at_len_1_missing': '''\
<p>Splošni (rekurzivni) primer pri tej nalogi zahteva dva elementa, pa četudi enega daš potem nazaj, ko
rep pošlješ v rekurzijo. Kaj pa se zgodi, ko ti ostane samo en element v seznamu? Premisli, to bo verjetno
glavni robni pogoj!</p>
''',

    'both_heads_omitted_from_recursion': '''\
<p>Jemlješ po dva elementa iz seznama preden greš v rekurzijo? Poskušaš malce prihraniti pri primerjavah, kajne? ;)
Ampak žal to ne gre! Od spodnjih dveh primerov eden deluje pravilno in eden ne, ugotovi v čem je razlika.</p>
<p><code>?- is_sorted([1,3,14,16,24,25]).</code></p>
<p><code>?- is_sorted([24,25,14,16,1,3]).</code></p>
''',

    'min_used': '''\
<p>Poskusi rešiti nalogo brez uporabe predikata <code>min/2</code>. Tvoja rešitev naj ima časovno zahtevnost O(n).
Z uporabo <code>min/2</code> je le-ta tipično O(n*n).</p>
''',
}
