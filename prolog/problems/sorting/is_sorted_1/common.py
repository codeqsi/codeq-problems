from operator import itemgetter
import socket

import prolog.engine
from server.hints import Hint, HintPopup

id = 121
number = 10
visible = True
facts = None

solution = '''\
is_sorted([]).
is_sorted([_]).
is_sorted([H1,H2|T]) :-
  H1 =< H2,
  is_sorted([H2|T]).
'''

hint_type = {
    'eq_instead_of_equ_markup': HintPopup('eq_instead_of_equ_markup'),
    'eq_instead_of_equ': Hint('eq_instead_of_equ'),
    'predicate_always_false': Hint('predicate_always_false'),
    'base_case': Hint('base_case'),
    'recursive_case': Hint('recursive_case'),
    'timeout': Hint('timeout'),
    '[]_base_case_missing': Hint('[]_base_case_missing'),
    'duplicates_fail': Hint('duplicates_fail'),
    'H1_instead_of_H2_sent_into_recursion': Hint('H1_instead_of_H2_sent_into_recursion'),
    'base_case_at_len_1_missing': Hint('base_case_at_len_1_missing'),
    'both_heads_omitted_from_recursion': Hint('both_heads_omitted_from_recursion'),
    'min_used': Hint('min_used'),
}

test_cases = [
    ('is_sorted([])',
        [{}]),
    ('is_sorted([5, 6, 13, 555, 2111, 2112])',
        [{}]),
    ('is_sorted([2, 5, 5, 13, 555, 2111, 2112])',
        [{}]),
    ('is_sorted([-2])',
        [{}]),
    ('is_sorted([-42, -42, -42, -42])',
        [{}]),
    ('\+ is_sorted([3, 4, 5, 6, 1, 2, 3])',
        [{}]),
    ('\+ is_sorted([3, 4, 5, 6, 1, 2, 3, 0])',
        [{}]),
    ('\+ is_sorted([3, 13, 6, 39])',
        [{}]),
]

def test(code, aux_code):
    n_correct = 0
    engine_id = None
    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)
        if engine_id is not None and 'error' not in map(itemgetter(0), output):
            # Engine successfully created, and no syntax error in program.
            for query, answers in test_cases:
                if prolog.engine.check_answers(engine_id, query=query, answers=answers, timeout=1.0):
                    n_correct += 1
    except socket.timeout:
        pass
    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_cases)}}]
    return n_correct, len(test_cases), hints

def hint(code, aux_code):
    tokens = prolog.util.tokenize(code)

    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)

        # strict equality testing instead of simple matching
        # this is usually (but not necessarily) wrong
        targets = [prolog.util.Token('EQ', '==')]
        marks = [(t.pos, t.pos + len(t.val)) for t in tokens if t in targets]
        if marks:
            return [{'id': 'eq_instead_of_equ_markup', 'start': m[0], 'end': m[1]} for m in marks] + \
                   [{'id': 'eq_instead_of_equ'}]

        if prolog.engine.ask_truthTO(engine_id, '''\
                is_sorted([1, 3, 5, 6, 9]),
                \+ is_sorted([1, 9, 3, 6]),
                is_sorted([39]),
                \+ is_sorted([])'''):
            return [{'id': '[]_base_case_missing'}]

        # H1 < H2 instead of H1 =< H2
        if prolog.engine.ask_truthTO(engine_id, '''\
                is_sorted([1, 3, 5, 6, 9]),
                \+ is_sorted([1, 9, 3, 6]),
                \+ is_sorted([1, 3, 5, 5, 7, 9])'''):
            return [{'id': 'duplicates_fail'}]

        if prolog.engine.ask_truthTO(engine_id, '''\
                \+ is_sorted([3, 13, 1, 39]),
                is_sorted([3, 13, 16, 39]),
                is_sorted([3, 13, 6, 39])'''):
            return [{'id': 'H1_instead_of_H2_sent_into_recursion'}]

        if prolog.engine.ask_truthTO(engine_id, '''\
                (is_sorted([13, 39])
                 ;
                 is_sorted([])),
                \+ is_sorted([39])'''):
            return [{'id': 'base_case_at_len_1_missing'}]

        if prolog.engine.ask_truthTO(engine_id, '''\
                \+ is_sorted([1, 0, 2, 25, 39]),
                is_sorted([1, 3, 9, 16, 39]),
                is_sorted([1, 3, 2, 16, 39])'''):
            return [{'id': 'both_heads_omitted_from_recursion'}]

        if any(t.val == 'min' for t in tokens):
            return [{'id': 'min_used'}]

        # missing/failed base case
        if not prolog.engine.ask_truthTO(engine_id, 'is_sorted([])'):
            return [{'id': 'base_case'}]

        # target predicate seems to always be false
        if not prolog.engine.ask_truthTO(engine_id, 'is_sorted(_)'):
            return [{'id': 'predicate_always_false'}]

        # base case works, the recursive doesn't (but it doesn't timeout)
        # this may be left as the last, most generic hint
        if not prolog.engine.ask_truth(engine_id, 'is_sorted([-2, 41, 82, 100])'):
            return [{'id': 'recursive_case'}]

    except socket.timeout as ex:
        return [{'id': 'timeout'}]

    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    return []
