from operator import itemgetter
import socket

import prolog.engine
from server.hints import Hint, HintPopup

id = 122
number = 30
visible = True
facts = None

solution = '''\
sins(X, [], [X]).
sins(X, [Y|T], [X,Y|T]) :-
  X =< Y.
sins(X, [Y|T], [Y|L]) :-
  X > Y,
  sins(X, T, L).
'''

hint_type = {
    'eq_instead_of_equ_markup': HintPopup('eq_instead_of_equ_markup'),
    'eq_instead_of_equ': Hint('eq_instead_of_equ'),
    'predicate_always_false': Hint('predicate_always_false'),
    'base_case': Hint('base_case'),
    'recursive_case': Hint('recursive_case'),
    'timeout': Hint('timeout'),
    'bad_[]_case': Hint('bad_[]_case'),
    'returns_elem_instead_of_list': Hint('returns_elem_instead_of_list'),
    'maxEl_base_case_missing': Hint('maxEl_base_case_missing'),
    'x_and_head_swapped': Hint('x_and_head_swapped'),
    'duplicates_incorrect': Hint('duplicates_incorrect'),
    'unprotected_branch': Hint('unprotected_branch'),
    'forgotten_heads': Hint('forgotten_heads'),
}

test_cases = [
    ('sins(4, [], X)',
        [{'X': '[4]'}]),
    ('sins(4, [1, 2, 5, 6, 8], X)',
        [{'X': '[1, 2, 4, 5, 6, 8]'}]),
    ('sins(8, [1, 2, 3, 4, 5, 6, 7], X)',
        [{'X': '[1, 2, 3, 4, 5, 6, 7, 8]'}]),
    ('sins(1, [2, 3, 4, 5, 6, 7], X)',
        [{'X': '[1, 2, 3, 4, 5, 6, 7]'}]),
    ('sins(2, [2, 3, 4, 5, 6, 7], X)',
        [{'X': '[2, 2, 3, 4, 5, 6, 7]'}]),
]

def test(code, aux_code):
    n_correct = 0
    engine_id = None
    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)
        if engine_id is not None and 'error' not in map(itemgetter(0), output):
            # Engine successfully created, and no syntax error in program.
            for query, answers in test_cases:
                if prolog.engine.check_answers(engine_id, query=query, answers=answers, timeout=1.0):
                    n_correct += 1
    except socket.timeout:
        pass
    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_cases)}}]
    return n_correct, len(test_cases), hints

def hint(code, aux_code):
    tokens = prolog.util.tokenize(code)

    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)

        # strict equality testing instead of simple matching
        # this is usually (but not necessarily) wrong
        targets = [prolog.util.Token('EQ', '==')]
        marks = [(t.pos, t.pos + len(t.val)) for t in tokens if t in targets]
        if marks:
            return [{'id': 'eq_instead_of_equ_markup', 'start': m[0], 'end': m[1]} for m in marks] + \
                   [{'id': 'eq_instead_of_equ'}]

        if prolog.engine.ask_truthTO(engine_id, '''\
                sins(yowza, [], [])
                ;
                sins(yowza, [], cob(Q, q))'''):
            return [{'id': 'bad_[]_case'}]

        if prolog.engine.ask_truthTO(engine_id, 'sins(yowza, [], yowza)'):
            return [{'id': 'returns_elem_instead_of_list'}]

        if prolog.engine.ask_truthTO(engine_id, '''\
                sins(4, [1, 3, 5, 6], [1, 3, 4, 5, 6]),
                \+ sins(9, [1, 3, 5, 6], [1, 3, 5, 6, 9]),
                \+ sins(39, [], [39])'''):
            return [{'id': 'maxEl_base_case_missing'}]

        if prolog.engine.ask_truthTO(engine_id,
                'sins(4, [1, 3, 5, 6], [1, 3, 5, 4, 9])'):
            return [{'id': 'x_and_head_swapped'}]

        if prolog.engine.ask_truthTO(engine_id, '''\
                sins(4, [1, 3, 5, 6], [1, 3, 4, 5, 6]),
                \+ sins(5, [1, 3, 5, 6], [1, 3, 5, 5, 6])
                ;
                findall(L, sins(5, [1, 3, 5, 6], L),
                        [[1, 3, 5, 5, 6], [1, 3, 5, 6, 5]])'''):
            return [{'id': 'duplicates_incorrect'}]

        # incorrect solutions after first one
        if prolog.engine.ask_truthTO(engine_id, '''\
                findall(X, sins(4, [1, 2, 3, 5, 6, 7], X),
                        [[1, 2, 3, 4, 5, 6, 7] | L]),
                length(L, N), N > 0'''):
            return [{'id': 'unprotected_branch'}]

        if prolog.engine.ask_truthTO(engine_id, '''\
                sins(4, [1, 3, 5, 6], [4, 5, 6]),
                sins(9, [1, 3, 5, 6, 8, 39], [9, 39])'''):
            return [{'id': 'forgotten_heads'}]

        # missing/failed base case
        if not prolog.engine.ask_truthTO(engine_id, 'sins(42, [], [42])'):
            return [{'id': 'base_case'}]

        # target predicate seems to always be false
        if not prolog.engine.ask_truthTO(engine_id, 'sins(_, _, _)'):
            return [{'id': 'predicate_always_false'}]

        # base case works, the recursive doesn't (but it doesn't timeout)
        # this may be left as the last, most generic hint
        if not prolog.engine.ask_truth(engine_id,
                'sins(42, [-2, 41, 82, 100], [-2, 41, 42, 82, 100])'):
            return [{'id': 'recursive_case'}]

    except socket.timeout as ex:
        return [{'id': 'timeout'}]

    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    return []
