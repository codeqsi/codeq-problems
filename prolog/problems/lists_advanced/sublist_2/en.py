name = 'sublist/2'
slug = 'generate sublists of a list'

description = '''\
<p><code>sublist(L, SL)</code>: <code>SL</code> is a continuous sublist of
list <code>L</code>. Your program should return every possible sublist; each
answer may be returned more than once.</p>
<pre>
?- sublist([1,2,3], X).
  X = [] ;
  X = [1] ;
  X = [1,2] ;
  X = [1,2,3] ;
  X = [2] ;
  X = [2,3] ;
  X = [3].
</pre>'''

plan = ['''\
<p>First a reminder: we're looking for sublists, not subsets. The difference is that sublists contain a number of
<em>consecutive</em> elements of the original list.</p>
<p>Perhaps you should look for some pattern? And which predicate is ideal to search for patterns?
You already know that. ;)</p>
''', '''\
<p>Of course, predicate <code>conc/3</code> can be used to search for patterns in lists. Perhaps this time
we don't even need explicite recursion? Is that really possible in prolog? ;)</p>
''', '''\
<p>So, what could the pattern be? Well, a part of the original list! Imagine that the original list is a tube
that you want to shorten -- a little bit from the left, a little bit from the right -- what's left is a sublist!
You chop off some elements from the front of the original list, and then you chop off some from the end.</p>
''']

hint = {
    'eq_instead_of_equ': '''\
<p>The operator <code>==</code> is "stricter" than operator <code>=</code> in the sense that
for the latter it is enough to be able to make the two operands equal (unification). Perhaps by using <code>=</code>
you can make the predicate <code>sublist/2</code> more general (e.g. able to work with output arguments becoming inputs).</p>
<p>Of course, you can also solve the exercise without explicit use of either of these two operators, just
remember that unification is implicitly performed with the predicate's arguments (head of clause).</p>
''',

    'eq_instead_of_equ_markup': '''\
<p>Perhaps the operator for unification (=) would be better?</p>
''',

    'base_case': '''\
<p>Did you think of a base case?</p>
''',

    'recursive_case': '''\
<p>The base case is ok. However, what about the general recursive case?</p>
''',

    'predicate_always_false': '''\
<p>It seems your predicate is <emph>always</emph> "false". Did you give it the correct name,
or is it perhaps misspelled?</p>
<p>If the name is correct, check whether something else is misspelled, perhaps there is a full stop instead of
a comma or vice versa, or maybe you typed a variable name in lowercase?</p>
<p>It is, of course, also possible that your conditions are too restrictive, or even impossible to satisfy
(as would be, for example, the condition that <code>N</code> is equal to <code>N + 1</code>,
or something similarly impossible).</p>
''',

    'timeout': '''\
<p>Is there an infinite recursion at work here? How will it ever stop?</p>
<p>Or perhaps is there a missing, faulty, or simply incompatible (with the general recursive case) base case?</p>
''',
}
