name = 'sublist/2'
slug = 'Generiraj vse podsezname danega seznama'

description = '''\
<p><code>sublist(L, SL)</code>: <code>SL</code> je podseznam seznama
<code>L</code>. Predikat naj vrne vse možne podsezname, enega po enega;
odgovori se lahko tudi podvajajo.</p>
<pre>
?- sublist([1,2,3], X).
  X = [] ;
  X = [1] ;
  X = [1,2] ;
  X = [1,2,3] ;
  X = [2] ;
  X = [2,3] ;
  X = [3].
</pre>'''

plan = ['''\
<p>Najprej se spomnimo, da naloga zahteva podsezname, ne podmnožice. Razlika je v tem, da so v podseznamu
elementi originalnega seznama, ki pa se "držijo skupaj", torej niso kar poljubni elementi iz originalnega seznama.
Morda poiščeš nek vzorec? In s katerim predikatom iščeš vzorce? To gotovo že veš. ;)</p>
''', '''\
<p>Seveda, vzorce lahko poiščeš s predikatom <code>conc/3</code>. Morda celo rekurzija tokrat ni potrebna? Je to
mogoče v Prologu? :)</p>
''', '''\
<p>No, kakšen bi bil lahko vzorec? Hja, en kos originalnega seznama! Predstavljaj si, da je originalni seznam
cev, ki jo hočeš skrajšati (čisto kot v oni češki risanki "A je to!"). Malo jo boš skrajšal z leve, malo z desne in...
in tisto kar ostane je podseznam! Originalnemu seznamu malo odsekaš spredaj in malo zadaj.</p>
''']

hint = {
    'eq_instead_of_equ': '''\
<p>Operator <code>==</code> je strožji od operatorja <code>=</code> v smislu, da je za slednjega dovolj,
da elementa lahko naredi enaka (unifikacija). Morda z uporabo <code>=</code> narediš predikat
<code>sublist/2</code> delujoč tudi v kakšni drugi smeri.</p>
<p>Seveda pa lahko nalogo rešiš brez obeh omenjenih operatorjev, spomni se, da lahko unifikacijo narediš
implicitno že kar v argumentih predikata (glavi stavka).</p>
''',

    'eq_instead_of_equ_markup': '''\
<p>Morda bi bil bolj primeren operator za unifikacijo (=)?</p>
''',

    'base_case': '''\
<p>Si pomislil na robni pogoj? Kaj je najbolj enostaven primer, ko je element v seznamu?
Do katerega elementa najlažje prideš?</p>
''',

    'recursive_case': '''\
<p>Robni primer deluje. Kaj pa rekurzivni, splošni, primer?</p>
''',

    'predicate_always_false': '''\
<p>Vse kaže, da tvoj predikat vedno vrne "false". Si mu dal pravilno ime, si se morda pri imenu zatipkal?</p>
<p>Če je ime pravilno, se morda splača preveriti tudi, če se nisi zatipkal kje drugje,
je morda kakšna pika namesto vejice ali obratno, morda kakšna spremenljivka z malo začetnico?</p>
<p>Možno je seveda tudi, da so tvoji pogoji prestrogi ali celo nemogoči (kot bi bila npr. zahteva,
da je <code>N</code> enako kot <code>N + 1</code> ali kaj podobno logično zlobnega).</p>
''',

    'timeout': '''\
<p>Je morda na delu potencialno neskončna rekurzija? Kako se bo ustavila?</p>
<p>Morda pa je kriv tudi manjkajoč, neustrezen ali preprosto nekompatibilen (s splošnim primerom) robni pogoj?</p>
''',
}
