name = 'sum/2'
slug = 'Izračunaj vsoto elementov v seznamu'

description = '''\
<p><code>sum(L, Sum)</code>: <code>Sum</code> je vsota vseh elementov v seznamu <code>L</code>.</p>
<pre>
?- sum([1,2,3], Sum).
  Sum = 6.
</pre>'''

plan = ['''\
<p>Vsota elementov praznega seznama ni prav velika. Sicer pa seštevamo, element po element, lepo rekurzivno.</p>
''', '''\
<p>Če je vsota elementov v repu (seznamu brez glave <code>H</code>) enaka <code>SumT</code>, potem je vsota elementov
celotnega seznama enaka <code>SumT + H</code>.</p>
''', '''\
<p>Če vzamem stran eno glavo, rekurzija mi reši (za ena) manjši problem in če rezultatu rekurzije prištejem vrednost
prej odvzete glave, potem sem dobil ravno vsoto elementov celega seznama.</p>
''']

hint = {
    'eq_instead_of_equ': '''\
<p>Operator <code>==</code> je strožji od operatorja <code>=</code> v smislu, da je za slednjega dovolj,
da elementa lahko naredi enaka (unifikacija). Morda z uporabo <code>=</code> narediš predikat
<code>sum/2</code> delujoč tudi v kakšni drugi smeri.</p>
<p>Seveda pa lahko nalogo rešiš brez obeh omenjenih operatorjev, spomni se, da lahko unifikacijo narediš
implicitno že kar v argumentih predikata (glavi stavka).</p>
''',

    'eq_instead_of_equ_markup': '''\
<p>Morda bi bil bolj primeren operator za unifikacijo (=)?</p>
''',

    'base_case': '''\
<p>Si pomislil na robni pogoj? Koliko je vsota elementov v praznem seznamu?</p>
''',

    'recursive_case': '''\
<p>Robni primer deluje. Kaj pa rekurzivni, splošni, primer?</p>
''',

    'predicate_always_false': '''\
<p>Vse kaže, da tvoj predikat vedno vrne "false". Si mu dal pravilno ime, si se morda pri imenu zatipkal?</p>
<p>Če je ime pravilno, se morda splača preveriti tudi, če se nisi zatipkal kje drugje,
je morda kakšna pika namesto vejice ali obratno, morda kakšna spremenljivka z malo začetnico?</p>
<p>Možno je seveda tudi, da so tvoji pogoji prestrogi ali celo nemogoči (kot bi bila npr. zahteva,
da je <code>N</code> enako kot <code>N + 1</code> ali kaj podobno logično zlobnega).</p>
''',

    'timeout': '''\
<p>Je morda na delu potencialno neskončna rekurzija? Kako se bo ustavila?</p>
<p>Morda pa je kriv tudi manjkajoč, neustrezen ali preprosto nekompatibilen (s splošnim primerom) robni pogoj?</p>
''',

    'arbitrary_base_case': '''\
<p>Koliko je vsota elementov praznega seznama? Podaj konkretno številko!</p>
''',

    'args_not_instantiated': '''\
<p>Napaka, ki si jo dobil od prologa, pomeni, da ob uporabi aritmetike niso podane vse vrednosti spremenljivk.
Pri aritmetiki je vrstni red ciljev (konjunktov) žal pomemben.</p>
<p>Morda poskusiš pomakniti aritmetiko bolj na konec?</p>
''',

    '=_instead_of_is': '''\
<p>Si morda uporabil <code>=</code> namesto <code>is</code>? Operator <code>=</code> je namenjen prilagajanju
(unifikaciji) in poskusi pustiti obe strani čimbolj nespremenjeni, medtem ko operator <code>is</code> izvede
dejanski aritmetični izračun izraza na svoji desni strani ter ga šele potem poskusi prilagoditi (!)
svoji levi strani.</p>
''',

    '+1_instead_of_+H': '''\
<p>Kaj si res prištel ena namesto vrednosti elementa (glave)? Kopiranje od prejšnje naloge, morda? ;)</p>
''',

    'forcing_result_onto_recursion': '''
<p>Ne vsiljuj rekurziji kaj naj vrne, prepusti se ji. To je tisti del, ko narediš predpostavko,
če je ta izpolnjena, potem bo tvoje pravilo delovalo za večji primer.</p>
<p>Je tvoj rekurzivni klic oblike <code>sum(Tail, SumTail + H)</code>? S tem vsiljuješ rekurziji
da mora vrniti vsoto <em>celega</em> seznama in ne samo repa. To ni v redu, za vrednost glave moraš ti povečati
rezultat, ki ti ga rekurzija vrne. Skratka, prištevanje naredi izven rekurzivnega klica.</p>
''',

    'same_var_on_both_sides_of_is': '''\
<p>Si morda zapisal pogoj oblike <code>N is N + H</code>? Predpostavi, da je <code>N</code> recimo enak 3.
S tem si v resnici rekel, da mora 3 biti enako kot 3+H. Prolog je logičen jezik in zato z največjim veseljem
reče ne (seveda če H ni enako nič, he he)! Uporabi novo spremenljivko, za odvečne bo tako ali tako
poskrbel "garbage collector".</p>
''',
}
