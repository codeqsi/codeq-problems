name = 'shiftright/2'
slug = 'Premakni elemente seznama za eno mesto v desno'

description = '''\
<p><code>shiftright(L1, L2)</code>: elemente v seznamu <code>L1</code> za eno mesto premaknemo v desno ("circular shift").</p>
<pre>
?- shiftright([1,2,3,4,5], X).
  X = [5,1,2,3,4].
</pre>'''

plan = ['''\
<p>V seznamu vzamem zadnji element in ga dodam preostanku na začetek. Saj se še spomniš od prejšnjič kako vzameš
zadnji element seznamu? Dodajanje na začetek pa je povsem preprosto, kajne?</p>
''', '''\
<p>Seznam dolžine ena je videti kot vzorec <code>[X]</code>. To bi znalo priti prav, kakor tudi predikat
<code>conc/3</code>.</p>
''', '''\
<p>Če je podani seznam <code>L</code> sestavljen iz zadnjega elementa <code>E</code> in preostanka <code>L1</code>
in če <code>E</code> dodamo <em>na začetek</em> <code>L1</code>, potem je rezultat seznam <code>L</code> premaknjen
v desno.</p>
''']

hint = {
    'eq_instead_of_equ': '''\
<p>Operator <code>==</code> je strožji od operatorja <code>=</code> v smislu, da je za slednjega dovolj,
da elementa lahko naredi enaka (unifikacija). Morda z uporabo <code>=</code> narediš predikat
<code>shiftright/2</code> delujoč tudi v kakšni drugi smeri.</p>
<p>Seveda pa lahko nalogo rešiš brez obeh omenjenih operatorjev, spomni se, da lahko unifikacijo narediš
implicitno že kar v argumentih predikata (glavi stavka).</p>
''',

    'eq_instead_of_equ_markup': '''\
<p>Morda bi bil bolj primeren operator za unifikacijo (=)?</p>
''',

    'predicate_always_false': '''\
<p>Vse kaže, da tvoj predikat vedno vrne "false". Si mu dal pravilno ime, si se morda pri imenu zatipkal?</p>
<p>Če je ime pravilno, se morda splača preveriti tudi, če se nisi zatipkal kje drugje,
je morda kakšna pika namesto vejice ali obratno, morda kakšna spremenljivka z malo začetnico?</p>
<p>Možno je seveda tudi, da so tvoji pogoji prestrogi ali celo nemogoči (kot bi bila npr. zahteva,
da je <code>N</code> enako kot <code>N + 1</code> ali kaj podobno logično zlobnega).</p>
''',

    'timeout': '''\
<p>Je morda na delu potencialno neskončna rekurzija? Kako se bo ustavila?</p>
<p>Morda pa je kriv tudi manjkajoč, neustrezen ali preprosto nekompatibilen (s splošnim primerom) robni pogoj?</p>
''',

'conc_2nd_argument_not_1elem_list': '''\
<p>Se spomniš kako izgleda "vzorec", ki predstavlja seznam s točno enim elementom? Ne tako kot drugi argument, ki
si ga podal predikatu <code>conc/3</code>. ;)</p>''',

    'arbitrary_result': '''\
<p>Si pravilno povezal vse spremenljivke? Zgleda namreč, da vračaš kar poljuben rezultat (spremenljivko brez
določene vrednosti). Opozorila "singleton variables" večinoma ni pametno ignorirati.</p>
''',

'shiftleftish_solution': '''\
<p>Rep seznama je vedno seznam in ne element. Kako si dobil zadnji element? Tako ne bo šlo...</p>''',

'last_used': '''\
<p>S pomočjo predikata <code>last/2</code> bo težko, ker zadnji element ostane v seznamu. Poskusi raje s
predikatom <code>conc/3</code>.</p>''',

'final_hint': '''\
<p>Predikata <code>shiftleft/2</code> in <code>shiftright/2</code> naredita ravno nasprotni stvari. Če samo
obrneš argumente, dobiš drugega. Tako bi lahko sprogramiral <code>shiftright/2</code> tudi samo s klicem
<code>shiftleft/2</code>. Saj veš, v prologu večinoma ni določeno kaj so vhodi in kaj izhodi.</p>''',
}
