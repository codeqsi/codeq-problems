from operator import itemgetter
import socket

import prolog.engine
import prolog.util
from server.hints import Hint, HintPopup

id = 114
number = 20
visible = True
facts = None

solution = '''\
conc114([], L, L).
conc114([H|T], L2, [H|L]) :-
  conc114(T, L2, L).
shiftright(L1, [H|T]) :-
  conc114(T, [H], L1).
'''

hint_type = {
    'eq_instead_of_equ_markup': HintPopup('eq_instead_of_equ_markup'),
    'eq_instead_of_equ': Hint('eq_instead_of_equ'),
    'predicate_always_false': Hint('predicate_always_false'),
    'timeout': Hint('timeout'),
    'conc_2nd_argument_not_1elem_list': Hint('conc_2nd_argument_not_1elem_list'),
    'arbitrary_result': Hint('arbitrary_result'),
    'shiftleftish_solution': Hint('shiftleftish_solution'),
    'last_used': Hint('last_used'),
    'final_hint': Hint('final_hint'),
}

test_cases = [
    ('shiftright([i], X)',
        [{'X': '[i]'}]),
    ('shiftright([h, e, u, l], X)',
        [{'X': '[l, h, e, u]'}]),
    ('shiftright([h, A, B, l, c], [C, D, x, y, E])',
        [{'A': 'x', 'B': 'y', 'C': 'c', 'D': 'h', 'E': 'l'}]),
]

def test(code, aux_code):
    n_correct = 0
    engine_id = None
    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)
        if engine_id is not None and 'error' not in map(itemgetter(0), output):
            # Engine successfully created, and no syntax error in program.
            for query, answers in test_cases:
                if prolog.engine.check_answers(engine_id, query=query, answers=answers, timeout=1.0):
                    n_correct += 1
    except socket.timeout:
        pass
    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_cases)}}]
    if n_correct == len(test_cases):
        tokens = prolog.util.tokenize(code)
        if prolog.util.Token('NAME', 'shiftleft') not in tokens:
            hints += [{'id': 'final_hint'}]

    return n_correct, len(test_cases), hints

def hint(code, aux_code):
    tokens = prolog.util.tokenize(code)

    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)

        # strict equality testing instead of simple matching
        # this is usually (but not necessarily) wrong
        targets = [prolog.util.Token('EQ', '==')]
        marks = [(t.pos, t.pos + len(t.val)) for t in tokens if t in targets]
        if marks:
            return [{'id': 'eq_instead_of_equ_markup', 'start': m[0], 'end': m[1]} for m in marks] + \
                   [{'id': 'eq_instead_of_equ'}]

        # second argument to conc is not first element in list
        if prolog.engine.ask_truthTO(engine_id,
                'findall(L, shiftright([a, b, c], L), [[[a, b, c]], [[b, c], a], [[c], a, b], [[], a, b, c]])'):
            return [{'id': 'conc_2nd_argument_not_1elem_list'}]

        # arbitrary result
        if prolog.engine.ask_truthTO(engine_id, '''\
                shiftright([a, b, c], [yowza, brix, cob])
                ;
                shiftright([a, b, yowza], [yowza | kokos(1)])
                ;
                findall(L, shiftright([a, b, c], L), [[[a, b, c] | b], [[b, c] | r], [[c] | i], [[] | x]])'''):
            return [{'id': 'arbitrary_result'}]

        # shiftleftish solution
        if prolog.engine.ask_truthTO(engine_id, 'shiftright([a, b, c, yowza], [[b, c, yowza] | a])'):
            return [{'id': 'shiftleftish_solution'}]

        # last/2 used
        if any(t.val in ('last', 'last_elem') for t in tokens):
            return [{'id': 'last_used'}]

        # target predicate seems to always be false
        if not prolog.engine.ask_truthTO(engine_id, 'shiftright(_, _)'):
            return [{'id': 'predicate_always_false'}]

    except socket.timeout as ex:
        return [{'id': 'timeout'}]

    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    return []
