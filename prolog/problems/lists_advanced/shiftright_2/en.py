name = 'shiftright/2'
slug = 'shift a list right'

description = '''\
<p><code>shiftright(L1, L2)</code>: the list <code>L2</code> is obtained from L1 by shifting elements to the right by one (circular shift).</p>
<pre>
?- shiftright([1,2,3,4,5], X).
  X = [5,1,2,3,4].
</pre>'''

plan = ['''\
<p>I take the last element from the given list and add it back at the start of the list's remainder.
You probably still remember how we took the last element of the list?  And adding an element at the start
is quite simple, isn't it?</p>
''', '''\
<p>A list of length one is represented as a pattern <code>[X]</code>. This might come in handy, as well
as the predicate <code>conc/3</code>.</p>
''', '''\
<p>If the given list <code>L</code> is composed of last element <code>E</code> and the remainder <code>L1</code>,
and if we put <code>E</code> at <em>the start</em> of <code>L1</code>, then we get list <code>L</code>
shifted right.</p>
''']

hint = {
    'eq_instead_of_equ': '''\
<p>The operator <code>==</code> is "stricter" than operator <code>=</code> in the sense that
for the latter it is enough to be able to make the two operands equal (unification). Perhaps by using <code>=</code>
you can make the predicate <code>shiftleft/2</code> more general (e.g. able to work with output arguments becoming inputs).</p>
<p>Of course, you can also solve the exercise without explicit use of either of these two operators, just
remember that unification is implicitly performed with the predicate's arguments (head of clause).</p>
''',

    'eq_instead_of_equ_markup': '''\
<p>Perhaps the operator for unification (=) would be better?</p>
''',

    'predicate_always_false': '''\
<p>It seems your predicate is <emph>always</emph> "false". Did you give it the correct name,
or is it perhaps misspelled?</p>
<p>If the name is correct, check whether something else is misspelled, perhaps there is a full stop instead of
a comma or vice versa, or maybe you typed a variable name in lowercase?</p>
<p>It is, of course, also possible that your conditions are too restrictive, or even impossible to satisfy
(as would be, for example, the condition that <code>N</code> is equal to <code>N + 1</code>,
or something similarly impossible).</p>
''',

    'timeout': '''\
<p>Is there an infinite recursion at work here? How will it ever stop?</p>
<p>Or perhaps is there a missing, faulty, or simply incompatible (with the general recursive case) base case?</p>
''',

'conc_2nd_argument_not_1elem_list': '''\
<p>Do you remember how the "pattern" representing a list with exactly one element looks like? Not like
the second argument you gave to predicate <code>conc/3</code>. ;)</p>''',

    'arbitrary_result': '''\
<p>Did you connect (use) all the variables? It seems as if you're returning an arbitrary result
(a variable without an assigned value). It's usually not a good idea to ignore the warning about
"singleton variables".</p>
''',

'shiftleftish_solution': '''\
<p>The tail of the list is always another list and never just an element. How did you get the last
element? This will not work...</p>''',

'last_used': '''\
<p>By using predicate <code>last/2</code> it will be difficult to solve this exercise as it leaves the last
element in the original list. Rather try using predicate <code>conc/3</code> instead.</p>''',

'final_hint': '''\
<p>Predicates <code>shiftleft/2</code> and <code>shiftright/2</code> perform exactly the opposite function.
If you simply swap the order of their arguments, you get the other predicate. In this way you could
program <code>shiftright/2</code> just by calling <code>shiftleft/2</code>. You know that in prolog inputs
and outputs can often be interchanged.</p>''',
}
