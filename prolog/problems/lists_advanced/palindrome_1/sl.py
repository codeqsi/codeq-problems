name = 'palindrome/1'
slug = 'Preveri, če elementi seznama tvorijo palindrom'

description = '''\
<p><code>palindrome(L)</code>: Elementi seznama <code>L</code> se preberejo v istem vrstnem redu z začetka in s konca seznama.</p>
<pre>
?- palindrome([1,2,3,2,1]).
  true.
?- palindrome([1,2,3]).
  false.
?- palindrome([a,b,b,a]).
  true.
</pre>'''

plan = ['''\
<p>Palindrom je seznam (ok, beseda), ki se od spredaj in od zadaj bere enako. Perica reže raci reP. ;)</p>
''', '''\
<p>Problem, kot vedno, želim zmanjšati. Odsekam glavo in zadnji element, če sta enaka, grem rekurzivno naprej.</p>
''', '''\
<p>Če je glava <code>H</code> seznama <code>L</code> enaka njegovemu zadnjemu elementu in če je preostanek (vmesni del)
palindrom, potem je tudi celoten seznam <code>L</code> palindrom.</p>
''']

hint = {
    'eq_instead_of_equ': '''\
<p>Operator <code>==</code> je strožji od operatorja <code>=</code> v smislu, da je za slednjega dovolj,
da elementa lahko naredi enaka (unifikacija).</p>
<p>Seveda pa lahko nalogo rešiš brez obeh omenjenih operatorjev, spomni se, da lahko unifikacijo narediš
implicitno že kar v argumentih predikata (glavi stavka).</p>
''',

    'eq_instead_of_equ_markup': '''\
<p>Morda bi bil bolj primeren operator za unifikacijo (=)?</p>
''',

    'base_case': '''\
<p>Si pomislil na robni pogoj? Kateri seznam predstavlja najkrajši možen palindrom?</p>
''',

    'predicate_always_false': '''\
<p>Vse kaže, da tvoj predikat vedno vrne "false". Si mu dal pravilno ime, si se morda pri imenu zatipkal?</p>
<p>Če je ime pravilno, se morda splača preveriti tudi, če se nisi zatipkal kje drugje,
je morda kakšna pika namesto vejice ali obratno, morda kakšna spremenljivka z malo začetnico?</p>
<p>Možno je seveda tudi, da so tvoji pogoji prestrogi ali celo nemogoči (kot bi bila npr. zahteva,
da je <code>N</code> enako kot <code>N + 1</code> ali kaj podobno logično zlobnega).</p>
''',

    'timeout': '''\
<p>Je morda na delu potencialno neskončna rekurzija? Kako se bo ustavila?</p>
<p>Morda pa je kriv tudi manjkajoč, neustrezen ali preprosto nekompatibilen (s splošnim primerom) robni pogoj?</p>
''',

    '[X,X]_instead_of_[]_base_case': '''\
<p>Vsekakor je <code>[X,X]</code> povsem dober robni pogoj, a ne pokrije posebnega primera, ko je vhod kar
prazen seznam. To je seveda stvar definicije, a da bomo imeli vsi enake rešitve, prosim, popravi.</p>
''',

    'one_base_case_missing': '''\
<p>Jemlješ po <em>dva</em> elementa stran ob vsakem prehodu rekurzije? Kako se to zaključi? Liho, sodo? ;)</p>
<p>Poskusi naslednja dva primera, eden bo lepo deloval, drugi ne. V čem je razlika?</p>
<p><code>?- palindrome([a,b,b,a]).</code></p>
<p><code>?- palindrome([l,e,v,e,l]).</code></p>
''',

    'arbitrary_base_case': '''\
<p>Pazi, <code>_</code> ni enako kot <code>[_]</code>. Prvo predstavlja poljubno spremenljivko, drugo seznam
z <em>enim</em> poljubnim elementom.</p>
''',

    'last_used': '''\
<p>Predikat <code>last/2</code> je tukaj štorast, ker pusti zadnji element v seznamu.</p>
''',

    'final_hint_1': '''\
<p>Zanimivost: veš, da bi to nalogo lahko rešil tudi s samo enim klicem? Kaj se zgodi, če palindrom... khm,
obrneš? ;)</p>
''',

    'final_hint_2': '''\
<p>Rešitev lahko še dodatno skrajšaš. Kako se lahko znebiš operatorja <code>=</code> (<code>==</code>)
oziroma ga narediš implicitnega?</p>
''',
}
