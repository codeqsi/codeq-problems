from operator import itemgetter
import socket

import prolog.engine
import prolog.util
from server.hints import Hint, HintPopup

id = 112
number = 40
visible = True
facts = None

solution = '''\
conc112([], L, L).
conc112([H|T], L2, [H|L]) :-
  conc112(T, L2, L).
rev112([], []).
rev112([H|T], R):-
  rev112(T, R1),
  conc112(R1, [H], R).
palindrome(L) :-
  rev112(L, L).
'''

hint_type = {
    'eq_instead_of_equ_markup': HintPopup('eq_instead_of_equ_markup'),
    'eq_instead_of_equ': Hint('eq_instead_of_equ'),
    'predicate_always_false': Hint('predicate_always_false'),
    'base_case': Hint('base_case'),
    'timeout': Hint('timeout'),
    '[X,X]_instead_of_[]_base_case': Hint('[X,X]_instead_of_[]_base_case'),
    'one_base_case_missing': Hint('one_base_case_missing'),
    'arbitrary_base_case': Hint('arbitrary_base_case'),
    'last_used': Hint('last_used'),
    'final_hint_1': Hint('final_hint_1'),
    'final_hint_2': Hint('final_hint_2'),
}

test_cases = [
    ('palindrome([])',
        [{}]),
    ('palindrome([a, b, e, A, B, C])',
        [{'A': 'e', 'B': 'b', 'C': 'a'}]),
    ('palindrome([b, e, X])',
        [{'X': 'b'}]),
]

def test(code, aux_code):
    n_correct = 0
    engine_id = None
    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)
        if engine_id is not None and 'error' not in map(itemgetter(0), output):
            # Engine successfully created, and no syntax error in program.
            for query, answers in test_cases:
                if prolog.engine.check_answers(engine_id, query=query, answers=answers, timeout=1.0):
                    n_correct += 1
    except socket.timeout:
        pass
    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_cases)}}]
    if n_correct == len(test_cases):
        tokens = prolog.util.tokenize(code)
        if prolog.util.Token('NAME', 'rev') not in tokens:
            hints += [{'id': 'final_hint_1'}]
        if prolog.util.Token('NAME', 'rev') in tokens and \
           (prolog.util.Token('EQU', '=') in tokens or prolog.util.Token('EQ', '==') in tokens):
            hints += [{'id': 'final_hint_2'}]
    return n_correct, len(test_cases), hints

def hint(code, aux_code):
    tokens = prolog.util.tokenize(code)

    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)

        # strict equality testing instead of simple matching
        # this is usually (but not necessarily) wrong
        targets = [prolog.util.Token('EQ', '==')]
        marks = [(t.pos, t.pos + len(t.val)) for t in tokens if t in targets]
        if marks:
            return [{'id': 'eq_instead_of_equ_markup', 'start': m[0], 'end': m[1]} for m in marks] + \
                   [{'id': 'eq_instead_of_equ'}]

        if prolog.engine.ask_truthTO(engine_id,
                'palindrome([q, q]), \+ palindrome([]), \+ palindrome([q, a])'):
            return [{'id': '[X,X]_instead_of_[]_base_case'}]

        if prolog.engine.ask_truthTO(engine_id, '''\
                palindrome([a, b, b, a]),
                \+ palindrome([c, e, p, e, c])
                ;
                \+ palindrome([a, b, b, a]),
                palindrome([c, e, p, e, c])'''):
            return [{'id': 'one_base_case_missing'}]

        if prolog.engine.ask_truthTO(engine_id, 'palindrome(kokos(1))'):
            return [{'id': 'arbitrary_base_case'}]

        if any(t.val in ('last', 'last_elem') for t in tokens):
            return [{'id': 'last_used'}]

        # target predicate seems to always be false
        if not prolog.engine.ask_truthTO(engine_id, 'palindrome(_)'):
            return [{'id': 'predicate_always_false'}]

        # missing/failed base case
        if not prolog.engine.ask_truthTO(engine_id, 'palindrome([])'):
            return [{'id': 'base_case'}]

    except socket.timeout as ex:
        return [{'id': 'timeout'}]

    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    return []
