name = 'palindrome/1'
slug = 'check if list is a palindrome'

description = '''\
<p><code>palindrome(L)</code>: the elements of list <code>L</code> are the same when read from the front or back of the list.</p>
<pre>
?- palindrome([1,2,3,2,1]).
  true.
?- palindrome([1,2,3]).
  false.
?- palindrome([a,b,b,a]).
  true.
</pre>'''

plan = ['''\
<p>A palindrome is a list (ok, a word) that reads the same from front or back. Like aibohphobia!
Was it a car or a cat I saw? ;)</p>
''', '''\
<p>As always we want to reduce the problem into a smaller one. Let's chop off the first and the last element
of a list, and, if equal, proceed recursively.</p>
''', '''\
<p>If head <code>H</code> of list <code>L</code> is equal to the list's last element, and if the remainder
(middle part) is a palindrome, then list <code>L</code> is also a palindrome.</p>
''']

hint = {
    'eq_instead_of_equ': '''\
<p>The operator <code>==</code> is "stricter" than operator <code>=</code> in the sense that
for the latter it is enough to be able to make the two operands equal (unification).</p>
<p>Of course, you can also solve the exercise without explicit use of either of these two operators, just
remember that unification is implicitly performed with the predicate's arguments (head of clause).</p>
''',

    'eq_instead_of_equ_markup': '''\
<p>Perhaps the operator for unification (=) would be better?</p>
''',

    'base_case': '''\
<p>Did you think of a base case? Which list represents the shortest possible palindrome?</p>
''',

    'predicate_always_false': '''\
<p>It seems your predicate is <emph>always</emph> "false". Did you give it the correct name,
or is it perhaps misspelled?</p>
<p>If the name is correct, check whether something else is misspelled, perhaps there is a full stop instead of
a comma or vice versa, or maybe you typed a variable name in lowercase?</p>
<p>It is, of course, also possible that your conditions are too restrictive, or even impossible to satisfy
(as would be, for example, the condition that <code>N</code> is equal to <code>N + 1</code>,
or something similarly impossible).</p>
''',

    'timeout': '''\
<p>Is there an infinite recursion at work here? How will it ever stop?</p>
<p>Or perhaps is there a missing, faulty, or simply incompatible (with the general recursive case) base case?</p>
''',

    '[X,X]_instead_of_[]_base_case': '''\
<p>Well, <code>[X,X]</code> is definitely a good base case. However, it doesn't cover one special case,
and that is an empty list. Of course, this is a matter of definition (taste even?), but please correct it
so that we all have solutions working in the same way.</p>
''',

    'one_base_case_missing': '''\
<p>Do you take <em>two</em> elements out with each recursive call? How does this end? Odd, even? ;)</p>
<p>Try the following two queries. One will work nicely, the other one won't. What's the difference?</p>
<p><code>?- palindrome([a,b,b,a]).</code></p>
<p><code>?- palindrome([l,e,v,e,l]).</code></p>
''',

    'arbitrary_base_case': '''\
<p>Note that <code>_</code> is not the same as <code>[_]</code>. The first pattern represents an arbitrary
variable (anything), the second a list with <em>a single</em> arbitrary element.</p>
''',

    'last_used': '''\
<p>By using predicate <code>last/2</code> it will be difficult to solve this exercise as it leaves the last
element in the original list.</p>''',

    'final_hint_1': '''\
<p>Interesting tidbit: do you know that you could have solved this exercise with just a single predicate call?
What happens with the palindrome if you... hmm, reverse it? ;)</p>
''',

    'final_hint_2': '''\
<p>You can make the solution even shorter! How can you get rid of the operator <code>=</code> (<code>==</code>)
or rather make it implicit?</p>
''',
}
