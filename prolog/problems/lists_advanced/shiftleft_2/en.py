name = 'shiftleft/2'
slug = 'shift a list left'

description = '''\
<p><code>shiftleft(L1, L2)</code>: the list <code>L2</code> is obtained from L1 by shifting elements to the left by one (circular shift).</p>
<pre>
?- shiftleft([1,2,3,4,5], X).
  X = [2,3,4,5,1].
</pre>'''

plan = ['''\
<p>I take the first element of a list, let's call it <code>H</code>, and add it <em>at the end</em> of
the list's remainder (let's call the remainder <code>T</code>). As simple as that! You probably still
remember how we took the last element of the list? Adding an element is the same operation as taking it,
just from the opposite view ;)</p>
''', '''\
<p>A list of length one is represented as a pattern <code>[X]</code>. This might come in handy, as well
as the predicate <code>conc/3</code>.</p>
''', '''\
<p>If the given list <code>L</code> is composed of head <code>H</code> and tail <code>T</code>, and if
we add <code>H</code> at <em>the end</em> of <code>T</code>, then we get list <code>L</code>
shifted left.</p>
''']

hint = {
    'eq_instead_of_equ': '''\
<p>The operator <code>==</code> is "stricter" than operator <code>=</code> in the sense that
for the latter it is enough to be able to make the two operands equal (unification). Perhaps by using <code>=</code>
you can make the predicate <code>shiftleft/2</code> more general (e.g. able to work with output arguments becoming inputs).</p>
<p>Of course, you can also solve the exercise without explicit use of either of these two operators, just
remember that unification is implicitly performed with the predicate's arguments (head of clause).</p>
''',

    'eq_instead_of_equ_markup': '''\
<p>Perhaps the operator for unification (=) would be better?</p>
''',

    'predicate_always_false': '''\
<p>It seems your predicate is <emph>always</emph> "false". Did you give it the correct name,
or is it perhaps misspelled?</p>
<p>If the name is correct, check whether something else is misspelled, perhaps there is a full stop instead of
a comma or vice versa, or maybe you typed a variable name in lowercase?</p>
<p>It is, of course, also possible that your conditions are too restrictive, or even impossible to satisfy
(as would be, for example, the condition that <code>N</code> is equal to <code>N + 1</code>,
or something similarly impossible).</p>
''',

    'timeout': '''\
<p>Is there an infinite recursion at work here? How will it ever stop?</p>
<p>Or perhaps is there a missing, faulty, or simply incompatible (with the general recursive case) base case?</p>
''',

    'conc_arg_not_list': '''\
<p>All three arguments of predicate <code>conc/3</code> are <em>lists</em>.
Are you sure you used it properly?</p>
''',

    'arbitrary_result': '''\
<p>Did you connect (use) all the variables? It seems as if you're returning an arbitrary result
(a variable without an assigned value). It's usually not a good idea to ignore the warning about
"singleton variables".</p>
''',

    'tail_must_be_list': '''\
<p>The list's tail is always another <em>list</em> and never an element!</p>
''',
}
