name = 'shiftleft/2'
slug = 'Premakni elemente seznama za eno mesto v levo'

description = '''\
<p><code>shiftleft(L1, L2)</code>: elemente v seznamu <code>L1</code> za eno mesto premaknemo v levo ("circular shift").</p>
<pre>
?- shiftleft([1,2,3,4,5], X).
  X = [2,3,4,5,1].
</pre>'''

plan = ['''\
<p>V seznamu vzamem prvi element (recimu mu <code>H</code>) in ga dodam preostanku (recimu mu <code>T</code>)
na konec. Preprosto! Saj se še spomniš od prejšnjič kako vzameš zadnji element seznamu? Dodajanje je ista
operacija, samo v drugo smer ;)</p>
''', '''\
<p>Seznam dolžine ena je videti kot vzorec <code>[X]</code>. To bi znalo priti prav, kakor tudi predikat
<code>conc/3</code>.</p>
''', '''\
<p>Če je podani seznam <code>L</code> sestavljen iz glave <code>H</code> in repa <code>T</code> in če <code>H</code>
dodamo <em>na konec</em> <code>T</code>, potem je rezultat seznam <code>L</code> premaknjen v levo.</p>
''']

hint = {
    'eq_instead_of_equ': '''\
<p>Operator <code>==</code> je strožji od operatorja <code>=</code> v smislu, da je za slednjega dovolj,
da elementa lahko naredi enaka (unifikacija). Morda z uporabo <code>=</code> narediš predikat
<code>shiftleft/2</code> delujoč tudi v kakšni drugi smeri.</p>
<p>Seveda pa lahko nalogo rešiš brez obeh omenjenih operatorjev, spomni se, da lahko unifikacijo narediš
implicitno že kar v argumentih predikata (glavi stavka).</p>
''',

    'eq_instead_of_equ_markup': '''\
<p>Morda bi bil bolj primeren operator za unifikacijo (=)?</p>
''',

    'predicate_always_false': '''\
<p>Vse kaže, da tvoj predikat vedno vrne "false". Si mu dal pravilno ime, si se morda pri imenu zatipkal?</p>
<p>Če je ime pravilno, se morda splača preveriti tudi, če se nisi zatipkal kje drugje,
je morda kakšna pika namesto vejice ali obratno, morda kakšna spremenljivka z malo začetnico?</p>
<p>Možno je seveda tudi, da so tvoji pogoji prestrogi ali celo nemogoči (kot bi bila npr. zahteva,
da je <code>N</code> enako kot <code>N + 1</code> ali kaj podobno logično zlobnega).</p>
''',

    'timeout': '''\
<p>Je morda na delu potencialno neskončna rekurzija? Kako se bo ustavila?</p>
<p>Morda pa je kriv tudi manjkajoč, neustrezen ali preprosto nekompatibilen (s splošnim primerom) robni pogoj?</p>
''',

    'conc_arg_not_list': '''\
<p>Vsi trije argumenti predikata <code>conc/3</code> morajo biti <em>seznami</em>. Si prepričan,
da si ga tako uporabil?</p>
''',

    'arbitrary_result': '''\
<p>Si pravilno povezal vse spremenljivke? Zgleda namreč, da vračaš kar poljuben rezultat (spremenljivko brez
določene vrednosti). Opozorila "singleton variables" večinoma ni pametno ignorirati.</p>
''',

    'tail_must_be_list': '''\
<p>Rep seznama mora vedno biti <em>seznam</em> in ne element!</p>
''',
}
