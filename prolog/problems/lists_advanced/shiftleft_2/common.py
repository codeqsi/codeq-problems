from operator import itemgetter
import socket

import prolog.engine
import prolog.util
from server.hints import Hint, HintPopup

id = 113
number = 10
visible = True
facts = None

solution = '''\
conc113([], L, L).
conc113([H|T], L2, [H|L]) :-
  conc113(T, L2, L).
shiftleft([H|T], L2) :-
  conc113(T, [H], L2).
'''

hint_type = {
    'eq_instead_of_equ_markup': HintPopup('eq_instead_of_equ_markup'),
    'eq_instead_of_equ': Hint('eq_instead_of_equ'),
    'predicate_always_false': Hint('predicate_always_false'),
    'timeout': Hint('timeout'),
    'conc_arg_not_list': Hint('conc_arg_not_list'),
    'arbitrary_result': Hint('arbitrary_result'),
    'tail_must_be_list': Hint('tail_must_be_list'),
}

test_cases = [
    ('shiftleft([i], X)',
        [{'X': '[i]'}]),
    ('shiftleft([h, e, u, l], X)',
        [{'X': '[e, u, l, h]'}]),
    ('shiftleft([h, A, B, l, c], [x, y, C, D, E])',
        [{'A': 'x', 'B': 'y', 'C': 'l', 'D': 'c', 'E': 'h'}]),
]

def test(code, aux_code):
    n_correct = 0
    engine_id = None
    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)
        if engine_id is not None and 'error' not in map(itemgetter(0), output):
            # Engine successfully created, and no syntax error in program.
            for query, answers in test_cases:
                if prolog.engine.check_answers(engine_id, query=query, answers=answers, timeout=1.0):
                    n_correct += 1
    except socket.timeout:
        pass
    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_cases)}}]
    return n_correct, len(test_cases), hints

def hint(code, aux_code):
    tokens = prolog.util.tokenize(code)

    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)

        # strict equality testing instead of simple matching
        # this is usually (but not necessarily) wrong
        targets = [prolog.util.Token('EQ', '==')]
        marks = [(t.pos, t.pos + len(t.val)) for t in tokens if t in targets]
        if marks:
            return [{'id': 'eq_instead_of_equ_markup', 'start': m[0], 'end': m[1]} for m in marks] + \
                   [{'id': 'eq_instead_of_equ'}]

        # arbitrary result
        if prolog.engine.ask_truthTO(engine_id,
                'shiftleft([27, 81, 243], granddad(Q, q))'):
            return [{'id': 'arbitrary_result'}]

        # conc/3 argument not a list
        if prolog.engine.ask_truthTO(engine_id,
                'shiftleft([9, 3, 1, 27, 81, 243], [3, 1, 27, 81, 243 | 9])'):
            return [{'id': 'conc_arg_not_list'}]

        # tail must be list
        if prolog.engine.ask_truthTO(engine_id,
                'shiftleft([27, 9, 3, 1, 81, 243], [[9, 3, 1, 81, 243] | 27])'):
            return [{'id': 'tail_must_be_list'}]

        # target predicate seems to always be false
        if not prolog.engine.ask_truthTO(engine_id, 'shiftleft(_, _)'):
            return [{'id': 'predicate_always_false'}]

    except socket.timeout as ex:
        return [{'id': 'timeout'}]

    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    return []
