name = 'max/2'
slug = 'Poišči največji element v danem seznamu'

description = '''\
<p><code>max(L, Max)</code>: <code>Max</code> je največji element v seznamu <code>L</code>.</p>
<pre>
?- max([5,4,1,6], M).
  M = 6.
?- max([3,2,2], M).
  M = 3.
</pre>'''

plan = ['''\
<p>Kot vedno, poskusi prevesti na manjši problem. Recimo, da že imaš <em>največji</em> element v
<em>repu</em> seznama...</p>
''', '''\
<p>Največji element v repu (seznamu brez glave <code>H</code>) primerjaj z vrednostjo glave <code>H</code>, tisti,
ki je večji zmaga in ga vrneš!</p>
''', '''\
<p>Če je podani seznam <code>L</code> sestavljen iz glave <code>H</code> in repa <code>T</code> ter predpostavimo,
da je največji element v <code>T</code> enak <code>MaxT</code> ter dalje velja še, da je vrednost <code>H</code>
večja od <code>MaxT</code>, potem je <code>H</code> največji element v <code>L</code>. <em>Ali</em> pa velja, da je
vrednost <code>H</code> manjša od <code>MaxT</code>, v tem primeru pa je <code>MaxT</code> največji element v
<code>L</code>.</p>
''']

hint = {
    'eq_instead_of_equ': '''\
<p>Operator <code>==</code> je strožji od operatorja <code>=</code> v smislu, da je za slednjega dovolj,
da elementa lahko naredi enaka (unifikacija). Morda z uporabo <code>=</code> narediš predikat
<code>max/2</code> delujoč tudi v kakšni drugi smeri.</p>
<p>Seveda pa lahko nalogo rešiš brez obeh omenjenih operatorjev, spomni se, da lahko unifikacijo narediš
implicitno že kar v argumentih predikata (glavi stavka).</p>
''',

    'eq_instead_of_equ_markup': '''\
<p>Morda bi bil bolj primeren operator za unifikacijo (=)?</p>
''',

    'base_case': '''\
<p>Si pomislil na robni pogoj? Kaj je najkrajši seznam, ki ima očiten največji element?</p>
''',

    'recursive_case': '''\
<p>Robni primer deluje. Kaj pa rekurzivni, splošni, primer?</p>
''',

    'predicate_always_false': '''\
<p>Vse kaže, da tvoj predikat vedno vrne "false". Si mu dal pravilno ime, si se morda pri imenu zatipkal?</p>
<p>Če je ime pravilno, se morda splača preveriti tudi, če se nisi zatipkal kje drugje,
je morda kakšna pika namesto vejice ali obratno, morda kakšna spremenljivka z malo začetnico?</p>
<p>Možno je seveda tudi, da so tvoji pogoji prestrogi ali celo nemogoči (kot bi bila npr. zahteva,
da je <code>N</code> enako kot <code>N + 1</code> ali kaj podobno logično zlobnega).</p>
''',

    'timeout': '''\
<p>Je morda na delu potencialno neskončna rekurzija? Kako se bo ustavila?</p>
<p>Morda pa je kriv tudi manjkajoč, neustrezen ali preprosto nekompatibilen (s splošnim primerom) robni pogoj?</p>
''',

    'empty_list_base_case': '''\
<p>V praznem seznamu boš težko našel največji element. Kaj, če se tokrat ustaviš malo prej?</p>
''',

    'list_instead_of_elem_base_case': '''\
<p>Vrni element in ne seznam!</p>
''',

    'duplicates_not_covered': '''\
<p>Si pomislil, da so v seznamu lahko tudi duplikati?</p>
''',

    'args_not_instantiated': '''\
<p>Napaka, ki si jo dobil od prologa, pomeni, da ob uporabi aritmetike niso podane vse vrednosti spremenljivk.</p>
<p>Si morda pozabil, da konjunkcija veže močneje od disjunkcije oz. da je vsak prologov stavek (veja, pravilo)
samostojen v smislu dosega spremenljivk? Morda je to problem. Pozorno poglej oba bloka kode (pred in za podpičjem),
oziroma obe pravili.</p>
''',

    'unprotected_branch': '''\
<p>Zdi se mi, da si pozabil "zaščititi" eno izmed obeh vej. Obe veji (disjunkciji) potrebujeta pogoj, ne zanašaj se
na to, da če je prišel v drugo vejo, potem prva ne velja. Velja OR in ne XOR. Zato jih moraš ti narediti
ekskluzivne. Poskusi tole vprašanje in poglej <em>vse možne</em> rešitve, boš videl v čem je problem.</p>
<p><code>?- max([6,9,3,8,1], Max).</code></p>
''',

    'one_branch_missing': '''\
<p>Si morda pozabil na eno izmed možnosti? Glava je lahko <em>ali</em> večja <em>ali</em> manjša od
največjega elementa v repu.</p>
''',
}
