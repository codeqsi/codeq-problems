from operator import itemgetter
import socket

import prolog.engine
import prolog.util
from server.hints import Hint, HintPopup

id = 109
number = 90
visible = True
facts = None

solution = '''\
max([X], X).
max([H|T], Max):-
  max(T, Max1),
  ( H > Max1, Max is H
    ;
    H =< Max1, Max is Max1 ).
'''

hint_type = {
    'eq_instead_of_equ_markup': HintPopup('eq_instead_of_equ_markup'),
    'eq_instead_of_equ': Hint('eq_instead_of_equ'),
    'predicate_always_false': Hint('predicate_always_false'),
    'base_case': Hint('base_case'),
    'recursive_case': Hint('recursive_case'),
    'timeout': Hint('timeout'),
    'empty_list_base_case': Hint('empty_list_base_case'),
    'list_instead_of_elem_base_case': Hint('list_instead_of_elem_base_case'),
    'duplicates_not_covered': Hint('duplicates_not_covered'),
    'args_not_instantiated': Hint('args_not_instantiated'),
    'unprotected_branch': Hint('unprotected_branch'),
    'one_branch_missing': Hint('one_branch_missing'),
}

test_cases = [
    ('max([15], X)',
        [{'X': '15'}]),
    ('max([22, 13, 81], X)',
        [{'X': '81'}]),
    ('max([42, 42, 42, 42], X)',
        [{'X': '42'}]),
    ('max([-22, -113, -41], X)',
        [{'X': '-22'}]),
]

def test(code, aux_code):
    n_correct = 0
    engine_id = None
    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)
        if engine_id is not None and 'error' not in map(itemgetter(0), output):
            # Engine successfully created, and no syntax error in program.
            for query, answers in test_cases:
                if prolog.engine.check_answers(engine_id, query=query, answers=answers, timeout=1.0):
                    n_correct += 1
    except socket.timeout:
        pass
    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_cases)}}]
    return n_correct, len(test_cases), hints

def hint(code, aux_code):
    tokens = prolog.util.tokenize(code)

    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)

        # strict equality testing instead of simple matching
        # this is usually (but not necessarily) wrong
        targets = [prolog.util.Token('EQ', '==')]
        marks = [(t.pos, t.pos + len(t.val)) for t in tokens if t in targets]
        if marks:
            return [{'id': 'eq_instead_of_equ_markup', 'start': m[0], 'end': m[1]} for m in marks] + \
                   [{'id': 'eq_instead_of_equ'}]

        # target predicate seems to always be false
        if not prolog.engine.ask_truthTO(engine_id, 'max(_, _)'):
            return [{'id': 'predicate_always_false'}]

        if prolog.engine.ask_truthTO(engine_id, 'max([], _)'):
            return [{'id': 'empty_list_base_case'}]

        if prolog.engine.ask_truthTO(engine_id, 'max([39], [39])'):
            return [{'id': 'list_instead_of_elem_base_case'}]

        if prolog.engine.ask_truthTO(engine_id,
                'max([1, 3, 9], 9), max([3, 9, 27, 1], 27), \+ max([3, 3, 3], 3)'):
            return [{'id': 'duplicates_not_covered'}]

        reply, output = prolog.engine.ask(engine_id,
                'max([1, 2, 2, 2, 3], 3), max([3, 2, 2, 2, 1], 3)', timeout=1)
        if reply.get('code') == 'instantiation_error':
            return [{'id': 'args_not_instantiated'}]

        # TODO is the ! necessary here?
        if prolog.engine.ask_truthTO(engine_id,
                'findall(M, max([3, 5, 2, 4, 1], M), L), member(X, L), X < 5, !'):
            return [{'id': 'unprotected_branch'}]

        if prolog.engine.ask_truthTO(engine_id, '''\
                max([1, 2, 3, 4, 5], 5),
                \+ max([5, 4, 3, 2, 1], 5)
                ;
                \+ max([1, 2, 3, 4, 5], 5),
                max([5, 4, 3, 2, 1], 5)'''):
            return [{'id': 'one_branch_missing'}]

        # missing/failed base case
        if not prolog.engine.ask_truthTO(engine_id, 'max([-45], -45)'):
            return [{'id': 'base_case'}]

        # base case works, the recursive doesn't (but it doesn't timeout)
        # this may be left as the last, most generic hint
        if not prolog.engine.ask_truth(engine_id, 'max([2, 41, -22], 41)'):
            return [{'id': 'recursive_case'}]

    except socket.timeout as ex:
        return [{'id': 'timeout'}]

    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    return []
