from operator import itemgetter
import socket

import prolog.engine
import prolog.util
from server.hints import Hint, HintPopup

id = 116
number = 50
visible = True
facts = None

solution = '''\
evenlen([]).
evenlen([_,_|T]) :-
  evenlen(T).

oddlen([_]).
oddlen([_,_|T]) :-
  oddlen(T).
'''

hint_type = {
    'eq_instead_of_equ_markup': HintPopup('eq_instead_of_equ_markup'),
    'eq_instead_of_equ': Hint('eq_instead_of_equ'),
    'predicate_always_false': Hint('predicate_always_false'),
    'base_case': Hint('base_case'),
    'extra_base_case': Hint('extra_base_case'),
    'arbitrary_base_case': Hint('arbitrary_base_case'),
    'arithmetics_used': Hint('arithmetics_used'),
    'odd_and_even_mixed_up': Hint('odd_and_even_mixed_up'),
    'recursive_case': Hint('recursive_case'),
    'timeout': Hint('timeout'),
}

test_cases = [
    ('evenlen([])',
        [{}]),
    ('evenlen([r, w, t, q])',
        [{}]),
    ('\+ evenlen([r, w, t, q, l])',
        [{}]),
    ('\+ oddlen([r, w, t, q])',
        [{}]),
    ('oddlen([r, w, t, q, l])',
        [{}]),
]

def test(code, aux_code):
    n_correct = 0
    engine_id = None
    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)
        if engine_id is not None and 'error' not in map(itemgetter(0), output):
            # Engine successfully created, and no syntax error in program.
            for query, answers in test_cases:
                if prolog.engine.check_answers(engine_id, query=query, answers=answers, timeout=1.0):
                    n_correct += 1
    except socket.timeout:
        pass
    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_cases)}}]
    return n_correct, len(test_cases), hints

def hint(code, aux_code):
    tokens = prolog.util.tokenize(code)

    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)

        # strict equality testing instead of simple matching
        # this is usually (but not necessarily) wrong
        targets = [prolog.util.Token('EQ', '==')]
        marks = [(t.pos, t.pos + len(t.val)) for t in tokens if t in targets]
        if marks:
            return [{'id': 'eq_instead_of_equ_markup', 'start': m[0], 'end': m[1]} for m in marks] + \
                   [{'id': 'eq_instead_of_equ'}]

        # missing/failed base case
        if not prolog.engine.ask_truthTO(engine_id, 'evenlen([])'):
            return [{'id': 'base_case'}]

        # redundant base case when evenlen and oddlen call each other
        if prolog.engine.ask_truthTO(engine_id,
                'findall(q, (oddlen([a, b, c, d, e]) ; evenlen([a, b, c, d, e, f])), [q, q, q, q])'):
            return [{'id': 'extra_base_case'}]

        # arbitrary base case
        if prolog.engine.ask_truthTO(engine_id,
                'evenlen(qQ), evenlen(parent(X, q)) ; oddlen(qQ), oddlen(parent(X, q))'):
            return [{'id': 'arbitrary_base_case'}]

        # arithmetics used
        if any(t.val in ('is', 'len', 'length') for t in tokens):
            return [{'id': 'arithmetics_used'}]

        # odd and even mixed up
        # TODO base_case will typically fire first when this is true
        if prolog.engine.ask_truthTO(engine_id,
                'oddlen([]), evenlen([q]), ! ; oddlen([q, q]), evenlen([q, q, q])'):
            return [{'id': 'odd_and_even_mixed_up'}]

        # target predicate seems to always be false
        if not prolog.engine.ask_truthTO(engine_id, 'evenlen(_) ; oddlen(_)'):
            return [{'id': 'predicate_always_false'}]

        # base case works, the recursive doesn't (but it doesn't timeout)
        # this may be left as the last, most generic hint
        if not prolog.engine.ask_truth(engine_id, 'evenlen([2, 41, -22, 13])'):
            return [{'id': 'recursive_case'}]

    except socket.timeout as ex:
        return [{'id': 'timeout'}]

    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    return []
