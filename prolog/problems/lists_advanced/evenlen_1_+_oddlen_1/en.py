name = 'evenlen/1 + oddlen/1'
slug = 'check if the length of a list is even or odd (without arithmetics)'

description = '''\
<p><code>evenlen(L)</code>: the list <code>L</code> has an even number of elements.<br />
<code>oddlen(L)</code>: the list <code>L</code> has an odd number of elements.</p>
<p>Don't use arithmetic operations with this exercise, it destroys the point of it!</p>
<pre>
?- oddlen([1,2,3,4,5]).
  true.
?- oddlen([1,2,3,4]).
  false.
?- evenlen([1,2,3,4]).
  true.
</pre>'''

plan = ['''\
<p>You can solve this exercise as two separate, albeit similar, ones, or as a single exercise that
intertwines two predicates. The second option is probably more interesting.</p>
''', '''\
<p>Intertwining in this case means that one predicate calls the other and vice versa. Even. Odd. Even. Odd.</p>
''', '''\
<p>If the tail (list without a single head) is of <em>even</em> length, then the whole list is of
<em>odd</em> length. And vice versa.</p>
''']

hint = {
    'eq_instead_of_equ': '''\
<p>The operator <code>==</code> is "stricter" than operator <code>=</code> in the sense that
for the latter it is enough to be able to make the two operands equal (unification). Perhaps by using <code>=</code>
you can make the predicates <code>oddlen/1</code> and <code>evenlen/1</code> more general
(e.g. able to work with output arguments becoming inputs).</p>
<p>Of course, you can also solve the exercise without explicit use of either of these two operators, just
remember that unification is implicitly performed with the predicate's arguments (head of clause).</p>
''',

    'eq_instead_of_equ_markup': '''\
<p>Perhaps the operator for unification (=) would be better?</p>
''',

    'base_case': '''\
<p>Did you think of a base case? What's the simplest case that you can trivially solve?</p>
''',

    'extra_base_case': '''\
<p>You're getting duplicate solutions. It's enough to only have a single base case for this pair of
predicates; you don't need one for <code>evenlen/1</code> and one for <code>oddlen/1</code>.</p>
''',

    'arbitrary_base_case': '''\
<p>It seems that you accept an arbitrary result (a variable without an assigned value). This will not be ok.</p>
<p>Note that <code>_</code> is not the same as <code>[_]</code>. The first pattern represents an arbitrary
variable (anything), the second a list with <em>a single</em> arbitrary element.</p>
''',

    'arithmetics_used': '''\
<p>Please don't use arithmetics to solve this exercise, not even for counting the length of the list.
It can be solved without that and it's also a much nicer idea or two.</p>
''',

'odd_and_even_mixed_up': '''\
<p>Did you mix up odd and even? Zero is even, one is odd, two is... ;)</p>
''',

    'recursive_case': '''\
<p>The base case is ok. However, what about the general recursive case?</p>
''',

    'predicate_always_false': '''\
<p>It seems your predicate is <emph>always</emph> "false". Did you give it the correct name,
or is it perhaps misspelled?</p>
<p>If the name is correct, check whether something else is misspelled, perhaps there is a full stop instead of
a comma or vice versa, or maybe you typed a variable name in lowercase?</p>
<p>It is, of course, also possible that your conditions are too restrictive, or even impossible to satisfy
(as would be, for example, the condition that <code>N</code> is equal to <code>N + 1</code>,
or something similarly impossible).</p>
''',

    'timeout': '''\
<p>Is there an infinite recursion at work here? How will it ever stop?</p>
<p>Or perhaps is there a missing, faulty, or simply incompatible (with the general recursive case) base case?</p>
''',
}
