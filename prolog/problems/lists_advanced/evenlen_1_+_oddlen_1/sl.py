name = 'evenlen/1 + oddlen/1'
slug = 'Brez aritmetike preveri, če je seznam sode ali lihe dolžine'

description = '''\
<p><code>evenlen(L)</code>: seznam <code>L</code> ima sodo število elementov.<br />
<code>oddlen(L)</code>: seznam <code>L</code> ima liho število elementov.</p>
<p>Ne uporabljaj aritmetike pri tej nalogi, ker to uniči njeno poanto!</p>
<pre>
?- oddlen([1,2,3,4,5]).
  true.
?- oddlen([1,2,3,4]).
  false.
?- evenlen([1,2,3,4]).
  true.
</pre>'''

plan = ['''\
<p>To nalogo se da reševati kot dve ločeni, a podobni, nalogi ali pa kot eno, prepletajočo se, nalogo.
Druga verzija je verjetno bolj zanimiva.</p>
''', '''\
<p>Prepletanje tu pomeni, da ena naloga kliče drugo in obratno. Sodo. Liho. Sodo. Liho.</p>
''', '''\
<p>Če je rep (seznam brez ene glave) <em>sode</em> dolžine, potem je celoten seznam <em>lihe</em> dolžine.
In obratno.</p>
''']

hint = {
    'eq_instead_of_equ': '''\
<p>Operator <code>==</code> je strožji od operatorja <code>=</code> v smislu, da je za slednjega dovolj,
da elementa lahko naredi enaka (unifikacija). Morda z uporabo <code>=</code> narediš predikata
<code>oddlen/1</code> in <code>evenlen/1</code> delujoča tudi v kakšni drugi smeri.</p>
<p>Seveda pa lahko nalogo rešiš brez obeh omenjenih operatorjev, spomni se, da lahko unifikacijo narediš
implicitno že kar v argumentih predikata (glavi stavka).</p>
''',

    'eq_instead_of_equ_markup': '''\
<p>Morda bi bil bolj primeren operator za unifikacijo (=)?</p>
''',

    'base_case': '''\
<p>Si pomislil na robni pogoj? Kaj je najbolj enostaven primer, ki ga lahko trivialno rešiš?</p>
''',

    'extra_base_case': '''\
<p>Rešitve se ti podvajajo. Zadosti je le en robni pogoj; ne potrebuješ enega za <code>evenlen/1</code>
in enega za <code>oddlen/1</code>.</p>
''',

    'arbitrary_base_case': '''\
<p>Zgleda da sprejmeš kar poljuben rezultat (spremenljivko brez določene vrednosti). To gotovo ne bo v redu.</p>
<p>Pazi <code>_</code> ni enako kot <code>[_]</code>. Prvo predstavlja poljubno spremenljivko, drugo seznam
z <em>enim</em> poljubnim elementom.</p>
''',

    'arithmetics_used': '''\
<p>Pri tej nalogi si ne pomagaj z aritmetiko oz. računanjem dolžine seznama. Rešiti se da brez tega in
tudi veliko bolj poučno je.</p>
''',

'odd_and_even_mixed_up': '''\
<p>Si morda pomešal sode in lihe dolžine? Nič je sodo (even), ena je liho (odd), dva je... ;)</p>
''',

    'recursive_case': '''\
<p>Robni primer deluje. Kaj pa rekurzivni, splošni, primer?</p>
''',

    'predicate_always_false': '''\
<p>Vse kaže, da tvoj predikat vedno vrne "false". Si mu dal pravilno ime, si se morda pri imenu zatipkal?</p>
<p>Če je ime pravilno, se morda splača preveriti tudi, če se nisi zatipkal kje drugje,
je morda kakšna pika namesto vejice ali obratno, morda kakšna spremenljivka z malo začetnico?</p>
<p>Možno je seveda tudi, da so tvoji pogoji prestrogi ali celo nemogoči (kot bi bila npr. zahteva,
da je <code>N</code> enako kot <code>N + 1</code> ali kaj podobno logično zlobnega).</p>
''',

    'timeout': '''\
<p>Je morda na delu potencialno neskončna rekurzija? Kako se bo ustavila?</p>
<p>Morda pa je kriv tudi manjkajoč, neustrezen ali preprosto nekompatibilen (s splošnim primerom) robni pogoj?</p>
''',
}
