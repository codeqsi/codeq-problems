from operator import itemgetter
import socket

import prolog.engine
import prolog.util
from server.hints import Hint, HintPopup

id = 111
number = 30
visible = True
facts = None

solution = '''\
conc111([], L, L).
conc111([H|T], L2, [H|L]) :-
  conc111(T, L2, L).
rev([], []).
rev([H|T], R):-
  rev(T, R1),
  conc111(R1, [H], R).
'''

hint_type = {
    'eq_instead_of_equ_markup': HintPopup('eq_instead_of_equ_markup'),
    'eq_instead_of_equ': Hint('eq_instead_of_equ'),
    'predicate_always_false': Hint('predicate_always_false'),
    'base_case': Hint('base_case'),
    'recursive_case': Hint('recursive_case'),
    'timeout': Hint('timeout'),
    'invalid_insert_at_end': Hint('invalid_insert_at_end'),
    'base_case_at_len1': Hint('base_case_at_len1'),
    'arbitrary_base_case': Hint('arbitrary_base_case'),
    'forcing_result_onto_recursion': Hint('forcing_result_onto_recursion'),
    'using_other_solutions': Hint('using_other_solutions'),
    'insertion_at_beginning': Hint('insertion_at_beginning'),
    'conc_arg_not_list': Hint('conc_arg_not_list'),
}

test_cases = [
    ('rev([], X)',
        [{'X': '[]'}]),
    ('rev(X, [])',
        [{'X': '[]'}]),
    ('rev([42], X)',
        [{'X': '[42]'}]),
    ('rev([s, k, c, d, d], X)',
        [{'X': '[d, d, c, k, s]'}]),
    ('rev(X, [s, k, c, d, d])',
        [{'X': '[d, d, c, k, s]'}]),
]

def test(code, aux_code):
    n_correct = 0
    engine_id = None
    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)
        if engine_id is not None and 'error' not in map(itemgetter(0), output):
            # Engine successfully created, and no syntax error in program.
            for query, answers in test_cases:
                if prolog.engine.check_answers(engine_id, query=query, answers=answers, timeout=1.0, inference_limit=1000):
                    n_correct += 1
    except socket.timeout:
        pass
    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_cases)}}]
    return n_correct, len(test_cases), hints

def hint(code, aux_code):
    tokens = prolog.util.tokenize(code)

    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)

        # strict equality testing instead of simple matching
        # this is usually (but not necessarily) wrong
        targets = [prolog.util.Token('EQ', '==')]
        marks = [(t.pos, t.pos + len(t.val)) for t in tokens if t in targets]
        if marks:
            return [{'id': 'eq_instead_of_equ_markup', 'start': m[0], 'end': m[1]} for m in marks] + \
                   [{'id': 'eq_instead_of_equ'}]

        if prolog.engine.ask_truthTO(engine_id, 'rev([q], [q]), \+ rev([], [])'):
            return [{'id': 'base_case_at_len1'}]

        if prolog.engine.ask_truthTO(engine_id, 'rev([], kokos(1))'):
            return [{'id': 'arbitrary_base_case'}]

        if prolog.engine.ask_truthTO(engine_id,  '''\
                \+ rev([yowza, brix, cob], R),
                asserta(rev([brix, cob], [[cob, brix] | yowza])),
                rev([yowza, brix, cob], [cob, brix]),
                retract(rev([brix, cob], [[cob, brix] | yowza]))'''):
            return [{'id': 'forcing_result_onto_recursion'}]

        if any(t.val in ('last', 'last_elem', 'shiftleft', 'shiftright') for t in tokens):
            return [{'id': 'using_other_solutions'}]

        if prolog.engine.ask_truthTO(engine_id, 'rev([qa, qb, qc, qd], [qa, qb, qc, qd])'):
            return [{'id': 'insertion_at_beginning'}]

        if prolog.engine.ask_truthTO(engine_id, 'rev([a, b, c], [[[[] | c] | b] | a])'):
            return [{'id': 'invalid_insert_at_end'}]

        if prolog.engine.ask_truthTO(engine_id, '''\
                \+ rev([yowza, brix, cob], R),
                asserta(rev([brix, cob], [cob, brix])),
                rev([yowza, brix, cob], [cob, brix | yowza]),
                retract(rev([brix, cob], [cob, brix]))'''):
            return [{'id': 'conc_arg_not_list'}]

        # missing/failed base case
        if not prolog.engine.ask_truthTO(engine_id, 'rev([], [])'):
            return [{'id': 'base_case'}]

        # target predicate seems to always be false
        if not prolog.engine.ask_truthTO(engine_id, 'rev(_, _)'):
            return [{'id': 'predicate_always_false'}]

        # base case works, the recursive doesn't (but it doesn't timeout)
        # this may be left as the last, most generic hint
        if not prolog.engine.ask_truth(engine_id, 'rev([2, 41, -22], [-22, 41, 2])'):
            return [{'id': 'recursive_case'}]

    except socket.timeout as ex:
        return [{'id': 'timeout'}]

    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    return []
