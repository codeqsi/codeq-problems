name = 'rev/2'
slug = 'Obrni seznam'

description = '''\
<p><code>rev(L1, L2)</code>: seznam <code>L2</code> ima elemente v obratnem vrstnem redu kot seznam <code>L1</code>.</p>
<pre>
?- rev([1,2,3], X).
  X = [3,2,1].
?- rev([], X).
  X = [].
</pre>'''

plan = ['''\
<p>To je ena najlepših in najbolj poučnih nalog. Klasična rekurzija! Problem poskusi zmanjšati. Seveda, prevedi na
krajši seznam.</p>
''', '''\
<p>Seznamu odvzamem glavo, rekurzija mi obrne rep in vse, kar moram na koncu narediti jaz je, da glavo vstavim nazaj
v obrnjen rep na pravo mesto.</p>
''', '''\
<p>Če je podani seznam <code>L</code> sestavljen iz glave <code>H</code> in repa <code>T</code>
in če predpostavim, da rekurzija obrne <code>T</code> v obrnjen rep <code>RT</code> in če <code>RT</code> na konec
dodam <code>H</code>, potem je rezultat obrnjen seznam <code>L</code>.</p>
''']

hint = {
    'eq_instead_of_equ': '''\
<p>Operator <code>==</code> je strožji od operatorja <code>=</code> v smislu, da je za slednjega dovolj,
da elementa lahko naredi enaka (unifikacija). Morda z uporabo <code>=</code> narediš predikat
<code>rev/2</code> delujoč tudi v kakšni drugi smeri.</p>
<p>Seveda pa lahko nalogo rešiš brez obeh omenjenih operatorjev, spomni se, da lahko unifikacijo narediš
implicitno že kar v argumentih predikata (glavi stavka).</p>
''',

    'eq_instead_of_equ_markup': '''\
<p>Morda bi bil bolj primeren operator za unifikacijo (=)?</p>
''',

    'base_case': '''\
<p>Si pomislil na robni pogoj? Kateri seznam je trivialno obrniti?</p>
''',

    'recursive_case': '''\
<p>Robni primer deluje. Kaj pa rekurzivni, splošni, primer?</p>
''',

    'predicate_always_false': '''\
<p>Vse kaže, da tvoj predikat vedno vrne "false". Si mu dal pravilno ime, si se morda pri imenu zatipkal?</p>
<p>Če je ime pravilno, se morda splača preveriti tudi, če se nisi zatipkal kje drugje,
je morda kakšna pika namesto vejice ali obratno, morda kakšna spremenljivka z malo začetnico?</p>
<p>Možno je seveda tudi, da so tvoji pogoji prestrogi ali celo nemogoči (kot bi bila npr. zahteva,
da je <code>N</code> enako kot <code>N + 1</code> ali kaj podobno logično zlobnega).</p>
''',

    'timeout': '''\
<p>Je morda na delu potencialno neskončna rekurzija? Kako se bo ustavila?</p>
<p>Morda pa je kriv tudi manjkajoč, neustrezen ali preprosto nekompatibilen (s splošnim primerom) robni pogoj?</p>
''',

    'base_case_at_len1': '''\
<p>Ta robni pogoj je seveda povsem smiseln, a ga vseeno popravi tako, da bo deloval tudi za prazen seznam.
Vendar ne imej dveh robnih pogojev, ker bo to podvajalo rešitve./p>
''',

    'arbitrary_base_case': '''\
<p>Kaj je rezultat obračanja praznega seznama? Gotovo ne karkoli (spremenljivka brez določene vrednosti)!</p>
''',

    'forcing_result_onto_recursion': '''
<p>Ne vsiljuj rekurziji kaj naj vrne, prepusti se ji. To je tisti del, ko narediš predpostavko,
če je ta izpolnjena, potem bo tvoje pravilo delovalo za večji primer.</p>
<p>Je tvoj rekurzivni klic oblike <code>rev(T, [RevTail|H])</code>? S tem vsiljuješ rekurziji
da mora <emph>vrniti</emph> na zadnjem mestu tudi glavo, ki je sploh ne pozna, ker si jo ravnokar vzel stran! To moraš
narediti ti z rezultatom, ki ti ga rekurzija vrne. Skratka, element <code>H</code> vstavi izven rekurzivnega klica.</p>
''',

    'using_other_solutions': '''\
<p>Od predikatov <code>last/2</code>, <code>shiftleft/2</code> ali <code>shiftright/2</code> tukaj ne bo veliko
pomoči. Poskusi raje brez, bo lažje.</p>
''',

    'insertion_at_beginning': '''\
<p>Vstavljaš morda glavo nazaj <em>na začetek</em> obrnjenega repa? To ni pravo mesto, ker s tem samo sestaviš nazaj
enak seznam kot na začetku.</p>
''',

    'invalid_insert_at_end': '''\
<p>Spomni se, rep seznama je vedno seznam! Kako vstaviš element na zadnje mesto?</p>
''',

    'conc_arg_not_list': '''\
<p>Vsi trije argumenti predikata <code>conc/3</code> morajo biti <em>seznami</em>. Si prepričan,
da si ga tako uporabil?</p>
''',
}
