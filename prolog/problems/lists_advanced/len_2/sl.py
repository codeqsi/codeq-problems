name = 'len/2'
slug = 'Izračunaj dolžino danega seznama'

description = '''\
<p><code>len(L, Len)</code>: <code>Len</code> je dolžina seznama <code>L</code>.</p>
<pre>
?- len([1,2,3], Len).
  Len = 3.
</pre>'''

plan = ['''\
<p>Prazen seznam ni prav pretirano dolg, če pa ni prazen, pa ima glavo in rep.</p>
''', '''\
<p>Če je rep (seznam brez ene glave) dolg <code>LenT</code>, potem je celoten seznam dolg <code>LenT + 1</code>.</p>
''', '''\
<p>Če vzamem stran eno glavo, rekurzija mi reši (za ena) manjši problem in če rezultatu rekurzije prištejem 1,
potem sem dobil ravno dolžino celotnega seznama.</p>
''']

hint = {
    'eq_instead_of_equ': '''\
<p>Operator <code>==</code> je strožji od operatorja <code>=</code> v smislu, da je za slednjega dovolj,
da elementa lahko naredi enaka (unifikacija). Morda z uporabo <code>=</code> narediš predikat
<code>len/2</code> delujoč tudi v kakšni drugi smeri.</p>
<p>Seveda pa lahko nalogo rešiš brez obeh omenjenih operatorjev, spomni se, da lahko unifikacijo narediš
implicitno že kar v argumentih predikata (glavi stavka).</p>
''',

    'eq_instead_of_equ_markup': '''\
<p>Morda bi bil bolj primeren operator za unifikacijo (=)?</p>
''',

    'base_case': '''\
<p>Si pomislil na robni pogoj? Kateri seznam je najkrajši od vseh seznamov na svetu?</p>
''',

    'recursive_case': '''\
<p>Robni primer deluje. Kaj pa rekurzivni, splošni, primer?</p>
''',

    'predicate_always_false': '''\
<p>Vse kaže, da tvoj predikat vedno vrne "false". Si mu dal pravilno ime, si se morda pri imenu zatipkal?</p>
<p>Če je ime pravilno, se morda splača preveriti tudi, če se nisi zatipkal kje drugje,
je morda kakšna pika namesto vejice ali obratno, morda kakšna spremenljivka z malo začetnico?</p>
<p>Možno je seveda tudi, da so tvoji pogoji prestrogi ali celo nemogoči (kot bi bila npr. zahteva,
da je <code>N</code> enako kot <code>N + 1</code> ali kaj podobno logično zlobnega).</p>
''',

    'timeout': '''\
<p>Je morda na delu potencialno neskončna rekurzija? Kako se bo ustavila?</p>
<p>Morda pa je kriv tudi manjkajoč, neustrezen ali preprosto nekompatibilen (s splošnim primerom) robni pogoj?</p>
''',

    'arbitrary_base_case': '''\
<p>Kako dolg je prazen seznam? Podaj konkretno številko!</p>
''',

    'args_not_instantiated': '''\
<p>Napaka, ki si jo dobil od prologa, pomeni, da ob uporabi aritmetike niso podane vse vrednosti spremenljivk.
Pri aritmetiki je vrstni red ciljev (konjunktov) žal pomemben.</p>
<p>Morda poskusiš pomakniti aritmetiko bolj na konec?</p>
''',

    '=_instead_of_is': '''\
<p>Si morda uporabil <code>=</code> namesto <code>is</code>? Operator <code>=</code> je namenjen prilagajanju
(unifikaciji) in poskusi pustiti obe strani čimbolj nespremenjeni, medtem ko operator <code>is</code> izvede
dejanski aritmetični izračun izraza na svoji desni strani ter ga šele potem poskusi prilagoditi (!)
svoji levi strani.</p>
''',

    '+H_instead_of_+1': '''\
<p>Kaj si res prištel vrednost elementa (glave) namesto njegove dolžine (ena)? ;)</p>
''',

    'forcing_result_onto_recursion': '''
<p>Ne vsiljuj rekurziji kaj naj vrne, prepusti se ji. To je tisti del, ko narediš predpostavko,
če je ta izpolnjena, potem bo tvoje pravilo delovalo za večji primer.</p>
<p>Je tvoj rekurzivni klic oblike <code>len(Tail, LenTail + 1)</code>? S tem vsiljuješ rekurziji
da mora vrniti dolžino <em>celega</em> seznama in ne samo repa. To ni v redu, za ena moraš ti povečati
rezultat, ki ti ga rekurzija vrne. Skratka, prištevanje naredi izven rekurzivnega klica.</p>
''',

    'same_var_on_both_sides_of_is': '''\
<p>Si morda zapisal pogoj oblike <code>N is N + 1</code>? Predpostavi, da je <code>N</code> recimo enak 3. S tem
si v resnici rekel, da mora 3 biti enako kot 4 (3+1). Prolog je logičen jezik in zato z največjim veseljem
reče ne! Uporabi novo spremenljivko, za odvečne bo tako ali tako poskrbel "garbage collector".</p>
''',
}
