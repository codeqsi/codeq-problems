from operator import itemgetter
import socket

import prolog.engine
import prolog.util
from server.hints import Hint, HintPopup

id = 119
number = 60
visible = True
facts = None

solution = '''\
len([], 0).
len([_|T], Len) :-
  len(T, LenT),
  Len is LenT + 1.
'''

hint_type = {
    'eq_instead_of_equ_markup': HintPopup('eq_instead_of_equ_markup'),
    'eq_instead_of_equ': Hint('eq_instead_of_equ'),
    'predicate_always_false': Hint('predicate_always_false'),
    'base_case': Hint('base_case'),
    'recursive_case': Hint('recursive_case'),
    'timeout': Hint('timeout'),
    'arbitrary_base_case': Hint('arbitrary_base_case'),
    'args_not_instantiated': Hint('arbitrary_base_case'),
    '=_instead_of_is': Hint('=_instead_of_is'),
    '+H_instead_of_+1': Hint('+H_instead_of_+1'),
    'forcing_result_onto_recursion': Hint('forcing_result_onto_recursion'),
    'same_var_on_both_sides_of_is': Hint('same_var_on_both_sides_of_is'),
}

test_cases = [
    ('len([8, 3, 5, 1], X)',
        [{'X': '4'}]),
    ('len([2, -3, 5, 0, 3], X)',
        [{'X': '5'}]),
    ('len([], X)',
        [{'X': '0'}]),
]

def test(code, aux_code):
    n_correct = 0
    engine_id = None
    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)
        if engine_id is not None and 'error' not in map(itemgetter(0), output):
            # Engine successfully created, and no syntax error in program.
            for query, answers in test_cases:
                if prolog.engine.check_answers(engine_id, query=query, answers=answers, timeout=1.0):
                    n_correct += 1
    except socket.timeout:
        pass
    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_cases)}}]
    return n_correct, len(test_cases), hints

def hint(code, aux_code):
    tokens = prolog.util.tokenize(code)

    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)

        # strict equality testing instead of simple matching
        # this is usually (but not necessarily) wrong
        targets = [prolog.util.Token('EQ', '==')]
        marks = [(t.pos, t.pos + len(t.val)) for t in tokens if t in targets]
        if marks:
            return [{'id': 'eq_instead_of_equ_markup', 'start': m[0], 'end': m[1]} for m in marks] + \
                   [{'id': 'eq_instead_of_equ'}]

        # target predicate seems to always be false
        if not prolog.engine.ask_truthTO(engine_id, 'len(_, _)'):
            return [{'id': 'predicate_always_false'}]

        # arbitrary base case: len([], _)
        if prolog.engine.ask_truthTO(engine_id, 'len([], q), len([], child(Q, q))'):
            return [{'id': 'arbitrary_base_case'}]

        # missing/failed base case
        if not prolog.engine.ask_truthTO(engine_id, 'len([], 0)'):
            return [{'id': 'base_case'}]

        # arguments not instantiated
        reply, output = prolog.engine.ask(engine_id, 'len([a, b, c], Len)', timeout=1)
        if reply.get('code') == 'instantiation_error':
            return [{'id': 'args_not_instantiated'}]

        # = instead of is
        if prolog.engine.ask_truthTO(engine_id, 'len([qa, qb, qc, qd], _+_)'):
            return [{'id': '=_instead_of_is'}]

        # +H instead of +1
        if prolog.engine.ask_truthTO(engine_id, 'len([1, 3, 9, 27, 81], 121)'):
            return [{'id': '+H_instead_of_+1'}]

        # forcing result onto recursion
        if prolog.engine.ask_truthTO(engine_id, '''\
                asserta(len([9, 81], 2+1)), len([27, 9, 81], 2), retract(len([9, 81], 2+1))
                ;
                asserta(len([9, 81], 1+2)), len([27, 9, 81], 2), retract(len([9, 81], 1+2))'''):
            return [{'id': 'forcing_result_onto_recursion'}]

        # same var on both sides of is
        if prolog.engine.ask_truth(engine_id, 'len([], 0), \+ len([a, b, c], _)'):
            return [{'id': 'same_var_on_both_sides_of_is'}]

        # base case works, the recursive doesn't (but it doesn't timeout)
        # this may be left as the last, most generic hint
        if not prolog.engine.ask_truth(engine_id, 'len([2, 41, -22], 3)'):
            return [{'id': 'recursive_case'}]

    except socket.timeout as ex:
        return [{'id': 'timeout'}]

    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    return []
