name = 'divide/3'
slug = 'Razdeli dani seznam na dva podseznama (približno) enake velikosti'

description = '''\
<p><code>divide(L, L1, L2)</code>: seznam <code>L1</code> vsebuje elemente na lihih pozicijah v seznamu <code>L</code>, seznam <code>L2</code> pa elemente na sodih pozicijah v <code>L</code>. Rešuj brez uporabe aritmetike.</p>
<pre>
?- divide([a,b,c,d,e,f,g], X, Y).
  X = [a,c,e,g], Y = [b,d,f].
?- divide([a,b,c,d,e,f], X, Y).
  X = [a,c,e], Y = [b,d,f].
</pre>'''

plan = ['''\
<p><img src="[%@resource plan.svg%]" /></p>
<p>Saj veš kako je šlo v osnovni šoli: prvi, drugi, prvi, ...</p>
''', '''\
<p>Znaš vzeti dva elementa z začetka seznama? Vzorec je <code>[H1,H2|T]</code>.</p>
''', '''\
<p>Vzameš dva elementa z začetka, preostanek rekurzivno razdeliš in to, kar vrne rekurzija, primerno
dopolniš s prej odvzetima elementoma. S tem, ko si vzel dva elementa z začetka, si problem zmanjšal.</p>
''', '''\
<p>Če predpostavim, da rekurzija razdeli rep <code>T</code> na podseznama <code>L1</code> in <code>L2</code>
ter ob vračanju v <code>L1</code> na začetek dodam <code>H1</code> in v <code>L2</code> na začetek
dodam <code>H2</code>, potem sem razdelil začetni seznam, ki je oblike <code>[H1,H2|T]</code>.</p>
''']

hint = {
    'eq_instead_of_equ': '''\
<p>Operator <code>==</code> je strožji od operatorja <code>=</code> v smislu, da je za slednjega dovolj,
da elementa lahko naredi enaka (unifikacija).</p>
<p>Seveda pa lahko nalogo rešiš brez obeh omenjenih operatorjev, spomni se, da lahko unifikacijo narediš
implicitno že kar v argumentih predikata (glavi stavka).</p>
''',

    'eq_instead_of_equ_markup': '''\
<p>Morda bi bil bolj primeren operator za unifikacijo (=)?</p>
''',

    'base_case': '''\
<p>Si pomislil na robni pogoj? Kaj je najbolj enostaven primer? Kaj, če je seznam prazen?</p>
''',

    'base_case_arbitrary': '''\
<p>Kako je lahko rezultat delitve seznama poljuben seznam oz. karkoli?</p>
<p>Če je tvoj robni pogoj v stilu <code>divide([], _, _)</code> ali <code>divide([X], [X|_], ...)</code>,
ga še enkrat premisli: kaj je rezultat delitve, kaj vračaš? Robni pogoj je vedno dokončno specificirana
rešitev, tu načeloma ni neznank (<code>_</code> ali spremenljivk brez prirejenih vrednosti) v tem kar se vrača.</p>
''',

    'second_base_case_missing': '''\
<p>Rekurzija se ne konča vedno uspešno. Sta morda dva različna primera kako se lahko izteče? Saj veš,
sodo in liho ;) Je morda potreben še kakšen robni pogoj? Poskusi naslednja klica, eden bo uspešen, drugi pa ne:</p>
<p><code>?- divide([a,b,c], L1, L2).</code></p>
<p><code>?- divide([a,b,c,d], L1, L2).</code></p>
''',

    'unsuccessful_conc_use': '''\
<p>Uporabljaš <code>conc/3</code>? Pri tej nalogi to ni najboljša ideja, ker <code>conc/3</code> deli "v kosih",
težko boš prišel do posameznih elementov. Poskusi raje brez.</p>
''',

    'forcing_result_onto_recursion': '''
<p>Ne vsiljuj rekurziji kaj naj vrne, prepusti se ji. To je tisti del, ko narediš predpostavko,
če je ta izpolnjena, potem bo tvoje pravilo delovalo za večji primer.</p>
<p>Je tvoj rekurzivni klic oblike <code>divide(T, [H1|...], [H2|...])</code>? S tem vsiljuješ rekurziji
da mora <emph>vrniti</emph> tudi obe glavi, ki jih sploh ne pozna, ker si jih ravnokar vzel stran! To moraš
narediti ti z rezultati, ki jih rekurzija vrne. Skratka, elementa <code>H1</code> in <code>H2</code>
dodaj izven rekurzivnega klica.</p>
''',

    'recursive_case': '''\
<p>Robni primeri delujejo. Kaj pa rekurzivni, splošni, primer?</p>
''',

    'predicate_always_false': '''\
<p>Vse kaže, da tvoj predikat vedno vrne "false". Si mu dal pravilno ime, si se morda pri imenu zatipkal?</p>
<p>Če je ime pravilno, se morda splača preveriti tudi, če se nisi zatipkal kje drugje,
je morda kakšna pika namesto vejice ali obratno, morda kakšna spremenljivka z malo začetnico?</p>
<p>Možno je seveda tudi, da so tvoji pogoji prestrogi ali celo nemogoči (kot bi bila npr. zahteva,
da je <code>X</code> hkrati starš in sestra od <code>Y</code> ali kaj podobno zlobnega).</p>
''',

    'timeout': '''\
<p>Je morda na delu potencialno neskončna rekurzija? Kako se bo ustavila?</p>
<p>Morda pa je kriv tudi manjkajoč, neustrezen ali preprosto nekompatibilen (s splošnim primerom) robni pogoj?</p>
''',
}
