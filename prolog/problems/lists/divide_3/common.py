from operator import itemgetter
import prolog.engine
import prolog.util
import socket
from server.hints import Hint, HintPopup
import server.problems

id = 115
number = 7
visible = True
facts = None

solution = '''\
divide([], [], []).
divide([X], [X], []).
divide([H1,H2|T], [H1|L1], [H2|L2]) :-
  divide(T, L1, L2).
'''

hint_type = {
    'eq_instead_of_equ_markup': HintPopup('eq_instead_of_equ_markup'),
    'eq_instead_of_equ': Hint('eq_instead_of_equ'),
    'predicate_always_false': Hint('predicate_always_false'),
    'base_case': Hint('base_case'),
    'recursive_case': Hint('recursive_case'),
    'timeout': Hint('timeout'),
    'base_case_arbitrary': Hint('base_case_arbitrary'),
    'second_base_case_missing': Hint('second_base_case_missing'),
    'unsuccessful_conc_use': Hint('unsuccessful_conc_use'),
    'forcing_result_onto_recursion': Hint('forcing_result_onto_recursion'),
}

test_cases = [
    ('divide([], A, B)',
        [{'A': '[]', 'B': '[]'}]),
    ('divide([q], A, B)',
        [{'A': '[q]', 'B': '[]'}]),
    ('divide([h, e, t, y, q, w], A, B)',
        [{'A': '[h, t, q]', 'B': '[e, y, w]'}]),
    ('divide([j, e, y, u, i], A, B)',
        [{'A': '[j, y, i]', 'B': '[e, u]'}]),
    ('divide(A, [t, q, y], [l, e, r])',
        [{'A': '[t, l, q, e, y, r]'}]),
]

def test(code, aux_code):
    n_correct = 0
    engine_id = None
    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)
        if engine_id is not None and 'error' not in map(itemgetter(0), output):
            # Engine successfully created, and no syntax error in program.
            for query, answers in test_cases:
                if prolog.engine.check_answers(engine_id, query=query, answers=answers, timeout=1.0):
                    n_correct += 1
    except socket.timeout:
        pass
    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_cases)}}]
    return n_correct, len(test_cases), hints

def hint(code, aux_code):
    tokens = prolog.util.tokenize(code)

    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)

        # strict equality testing instead of simple matching
        # this is usually (but not necessarily) wrong
        targets = [prolog.util.Token('EQ', '==')]
        marks = [(t.pos, t.pos + len(t.val)) for t in tokens if t in targets]
        if marks:
            return [{'id': 'eq_instead_of_equ_markup', 'start': m[0], 'end': m[1]} for m in marks] + \
                   [{'id': 'eq_instead_of_equ'}]

        # general unsuccessful use of conc (not appropriate for this exercise)
        if prolog.util.Token('NAME', 'conc') in tokens:
            return [{'id': 'unsuccessful_conc_use'}]

        # recursion is getting bigger and bigger


        # forcing result onto recursion; case of divide(T, [H1|L1], [H2|L2])
        if not prolog.engine.ask_truthTO(engine_id, 'divide([a,b,c], _, _)') and \
           prolog.engine.ask_truthTO(engine_id,
           'asserta( divide([cob], [yowza,cob], [brix]) ), divide([yowza,brix,cob], [cob], []), retract( divide([cob], [yowza,cob], [brix]) )'):
            return [{'id': 'forcing_result_onto_recursion'}]

        # base case succeeds with arbitrary result
        if prolog.engine.ask_truthTO(engine_id, 'divide([], L1, L2), (var(L1) ; var(L2))') or \
           prolog.engine.ask_truthTO(engine_id, 'divide([q], L1, L2), (var(L1) ; var(L2))') or \
           prolog.engine.ask_truthTO(engine_id, 'divide([q], [q|L1], L2), (var(L1) ; var(L2))'):
            return [{'id': 'base_case_arbitrary'}]

        # one base case ok, the other one is missing (or bad)
        # only trigger when recursion also works, because that makes more sense to the student
        # than immediately telling him or her that there is another base case missing
        if prolog.engine.ask_truthTO(engine_id,
           'divide([a,b], [a], [b]), \+ divide([a,b,c], [a,c], [b]) ; \+ divide([a,b], [a], [b]), divide([a,b,c], [a,c], [b])'):
            return [{'id': 'second_base_case_missing'}]

        # missing/failed base case
        if not prolog.engine.ask_truth(engine_id, 'divide([], [], [])'):
            return [{'id': 'base_case'}]

#        # target predicate seems to always be false
#        if not prolog.engine.ask_truthTO(engine_id, 'divide([_,_,_,_,_], _, _)') and \
#           not prolog.engine.ask_truthTO(engine_id, 'divide([_,_,_,_,_,_], _, _)'):
#            return [{'id': 'predicate_always_false'}]

        # base cases work, the recursive doesn't (but it doesn't timeout)
        # this may be left as the last, most generic hint
        if not prolog.engine.ask_truth(engine_id, 'divide([qa,qb,qc,qd], [qa,qc], [qb,qd])'):
            return [{'id': 'recursive_case'}]

    except socket.timeout as ex:
        return [{'id': 'timeout'}]

    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    return []
