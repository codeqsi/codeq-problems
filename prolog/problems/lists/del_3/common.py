from operator import itemgetter
import prolog.engine
import prolog.util
import socket
from server.hints import Hint, HintPopup
import server.problems

id = 105
number = 3
visible = True
facts = None

solution = '''\
del(X, [X|T], T).
del(X, [Y|T], [Y|L]) :-
  del(X, T, L).
'''

hint_type = {
    'eq_instead_of_equ_markup': HintPopup('eq_instead_of_equ_markup'),
    'eq_instead_of_equ': Hint('eq_instead_of_equ'),
    'predicate_always_false': Hint('predicate_always_false'),
    'base_case': Hint('base_case'),
    'timeout': Hint('timeout'),
    'final_hint': Hint('final_hint'),
    'del_from_empty_list_success': Hint('del_from_empty_list_success'),
    'lost_heads': Hint('lost_heads'),
    'recursive_case': Hint('recursive_case'),
    'leading_heads_all_x': Hint('leading_heads_all_x'),
}

test_cases = [
    ('del(1, [1], X)',
        [{'X': '[]'}]),
    ('del(X, [t, e, g], [t, g])',
        [{'X': 'e'}]),
    ('del(e, [t, e, g, e], X)',
        [{'X': '[t, g, e]'}, {'X': '[t, e, g]'}]),
    ('del(X, [Y, g, Z], [u, f])',
        [{'X': 'g', 'Y': 'u', 'Z': 'f'}]),
]

def test(code, aux_code):
    n_correct = 0
    engine_id = None
    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)
        if engine_id is not None and 'error' not in map(itemgetter(0), output):
            # Engine successfully created, and no syntax error in program.
            for query, answers in test_cases:
                if prolog.engine.check_answers(engine_id, query=query, answers=answers, timeout=1.0):
                    n_correct += 1
    except socket.timeout:
        pass
    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_cases)}}]
    if n_correct == len(test_cases):
        tokens = prolog.util.tokenize(code)
        if prolog.util.Token('NAME', 'insert') not in tokens:
            hints += [{'id': 'final_hint'}]
    return n_correct, len(test_cases), hints

def hint(code, aux_code):
    tokens = prolog.util.tokenize(code)

    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)

        # strict equality testing instead of simple matching
        # this is usually (but not necessarily) wrong
        targets = [prolog.util.Token('EQ', '==')]
        marks = [(t.pos, t.pos + len(t.val)) for t in tokens if t in targets]
        if marks:
            return [{'id': 'eq_instead_of_equ_markup', 'start': m[0], 'end': m[1]} for m in marks] + \
                   [{'id': 'eq_instead_of_equ'}]

        # recursion is getting bigger and bigger


        # deleting from empty list succeeds with an emtpy list result
        if prolog.engine.ask_truthTO(engine_id, 'del(_, [], _)'):
            return [{'id': 'del_from_empty_list_success'}]

        # missing/failed base case
        if not prolog.engine.ask_one(engine_id, 'del(qQ, [qQ,qa,qb,qc], [qa,qb,qc])'):
            return [{'id': 'base_case'}]

        # target predicate seems to always be false
        if not prolog.engine.ask_truth(engine_id, 'del(_, [_,_,_,_,_,_], _)'):
            return [{'id': 'predicate_always_false'}]

        # forgot to put list head(s) back in the list when deleting an element
        if prolog.engine.ask_truth(engine_id, 'del(q, [a,b,c,d,q,x,y,z], [x,y,z])'):
            return [{'id': 'lost_heads'}]

        # used [X|T] instead of more general [H|T] in recursive case
        if prolog.engine.ask_truth(engine_id, \
                'findall(NL, del(q, [q,q,q,a,b,c], NL), [[q,q,a,b,c],[q,q,a,b,c],[q,q,a,b,c]]).') and \
           not prolog.engine.ask_truth(engine_id, 'del(q, [a,b,c,q,x,y,z], [a,b,c,x,y,z])'):
            return [{'id': 'leading_heads_all_x'}]

        # L=[H|T] at the end of the rule; this can be detected with test cases and can be wrong (in some sense)
        # TODO: implement it, and provide a good explanation (might be hard)

        # base case works, the recursive doesn't (but it doesn't timeout)
        # this may be left as the last, most generic hint
        if not prolog.engine.ask_truth(engine_id, 'del(qQ, [qa,qb,qQ,qc], [qa,qb,qc])'):
            return [{'id': 'recursive_case'}]


    except socket.timeout as ex:
        return [{'id': 'timeout'}]

    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    return []
