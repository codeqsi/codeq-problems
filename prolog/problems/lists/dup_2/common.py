from operator import itemgetter
import prolog.engine
import prolog.util
import socket
from server.hints import Hint, HintPopup
import server.problems

id = 110
number = 4
visible = True
facts = None

solution = '''\
dup([], []).
dup([H|T], [H,H|TT]) :-
  dup(T, TT).
'''

hint_type = {
    'eq_instead_of_equ_markup': HintPopup('eq_instead_of_equ_markup'),
    'eq_instead_of_equ': Hint('eq_instead_of_equ'),
    'predicate_always_false': Hint('predicate_always_false'),
    'base_case': Hint('base_case'),
    'timeout': Hint('timeout'),
    'recursive_case': Hint('recursive_case'),
    'forcing_result_onto_recursion': Hint('forcing_result_onto_recursion'),
    'base_case_arbitrary': Hint('base_case_arbitrary'),
    'base_case_missing_[]': Hint('base_case_missing_[]'),
}

test_cases = [
    ('dup([], X)',
        [{'X': '[]'}]),
    ('dup([y], X)',
        [{'X': '[y, y]'}]),
    ('dup([k, f, f, g, a], X)',
        [{'X': '[k, k, f, f, f, f, g, g, a, a]'}]),
    ('dup(X, [k, k, f, f, f, f, g, g, a, a])',
        [{'X': '[k, f, f, g, a]'}]),
]

def test(code, aux_code):
    n_correct = 0
    engine_id = None
    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)
        if engine_id is not None and 'error' not in map(itemgetter(0), output):
            # Engine successfully created, and no syntax error in program.
            for query, answers in test_cases:
                if prolog.engine.check_answers(engine_id, query=query, answers=answers, timeout=1.0):
                    n_correct += 1
    except socket.timeout:
        pass
    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_cases)}}]
    return n_correct, len(test_cases), hints

def hint(code, aux_code):
    tokens = prolog.util.tokenize(code)

    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)

        # strict equality testing instead of simple matching
        # this is usually (but not necessarily) wrong
        targets = [prolog.util.Token('EQ', '==')]
        marks = [(t.pos, t.pos + len(t.val)) for t in tokens if t in targets]
        if marks:
            return [{'id': 'eq_instead_of_equ_markup', 'start': m[0], 'end': m[1]} for m in marks] + \
                   [{'id': 'eq_instead_of_equ'}]

        # recursion is getting bigger and bigger


        # base case succeeds with arbitrary result
        if prolog.engine.ask_truthTO(engine_id, 'dup([], L), var(L)'):
            return [{'id': 'base_case_arbitrary'}]

        # base case does not work for empty list
        if prolog.engine.ask_truthTO(engine_id, 'dup([q], [q,q]), \+ dup([], [])'):
            return [{'id': 'base_case_missing_[]'}]

        # forcing result onto recursion; case of dup(T, [H,H|L]) or dup(T, [H,H|T])
        if not prolog.engine.ask_truthTO(engine_id, 'dup([a,b,c,d,e], _)') and \
           (prolog.engine.ask_truth(engine_id,
           'asserta( dup([b], [a,a,yowza]) ), dup([a,b], [yowza]), retract( dup([b], [a,a,yowza]) )') or \
           prolog.engine.ask_truth(engine_id,
           'asserta( dup([b], [a,a,b]) ), dup([a,b], [b]), retract( dup([b], [a,a,b]) )')):
            return [{'id': 'forcing_result_onto_recursion'}]

        # missing/failed base case
        if not prolog.engine.ask_one(engine_id, 'dup([], [])'):
            return [{'id': 'base_case'}]

        # target predicate seems to always be false
        if not prolog.engine.ask_truth(engine_id, 'dup([_,_,_], _)'):
            return [{'id': 'predicate_always_false'}]

        # base case works, the recursive doesn't (but it doesn't timeout)
        # this may be left as the last, most generic hint
        if not prolog.engine.ask_truth(engine_id, 'dup([qa,qb,qc], [qa,qa,qb,qb,qc,qc])'):
            return [{'id': 'recursive_case'}]


    except socket.timeout as ex:
        return [{'id': 'timeout'}]

    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    return []
