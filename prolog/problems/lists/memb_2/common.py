from operator import itemgetter
import prolog.engine
import prolog.util
import socket
from server.hints import Hint, HintPopup
import server.problems

id = 103
number = 2
visible = True
facts = None

solution = '''\
memb(X, [X|_]).
memb(X, [_|T]) :-
  memb(X, T).
'''

hint_type = {
    'eq_instead_of_equ_markup': HintPopup('eq_instead_of_equ_markup'),
    'eq_instead_of_equ': Hint('eq_instead_of_equ'),
    'predicate_always_false': Hint('predicate_always_false'),
    'base_case': Hint('base_case'),
    'recursive_case': Hint('recursive_case'),
    'timeout': Hint('timeout'),
    'final_hint': Hint('final_hint'),
}

test_cases = [
    ('memb(c, [X])',
        [{'X': 'c'}]),
    ('memb(X, [y, l, r, g, a])',
        [{'X': 'y'}, {'X': 'l'}, {'X': 'r'}, {'X': 'g'}, {'X': 'a'}]),
]

def test(code, aux_code):
    n_correct = 0
    engine_id = None
    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)
        if engine_id is not None and 'error' not in map(itemgetter(0), output):
            # Engine successfully created, and no syntax error in program.
            for query, answers in test_cases:
                if prolog.engine.check_answers(engine_id, query=query, answers=answers, timeout=1.0):
                    n_correct += 1
    except socket.timeout:
        pass
    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_cases)}}]
    if n_correct == len(test_cases):
        hints += [{'id': 'final_hint'}]
    return n_correct, len(test_cases), hints

def hint(code, aux_code):
    tokens = prolog.util.tokenize(code)

    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)

        # strict equality testing instead of simple matching
        # this is usually (but not necessarily) wrong
        targets = [prolog.util.Token('EQ', '==')]
        marks = [(t.pos, t.pos + len(t.val)) for t in tokens if t in targets]
        if marks:
            return [{'id': 'eq_instead_of_equ_markup', 'start': m[0], 'end': m[1]} for m in marks] + \
                   [{'id': 'eq_instead_of_equ'}]

        # recursion is getting bigger and bigger


        # missing/failed base case
        if not prolog.engine.ask_truthTO(engine_id, 'memb(qq, [qq,a,b,c])'):
            return [{'id': 'base_case'}]

        # target predicate seems to always be false
        if not prolog.engine.ask_truthTO(engine_id, 'memb(_, [_,_,_,_,_,_])'):
            return [{'id': 'predicate_always_false'}]

        # base case works, the recursive doesn't (but it doesn't timeout)
        # this may be left as the last, most generic hint
        if not prolog.engine.ask_truth(engine_id, 'memb(qQ, [qa,qb,qQ,qc])'):
            return [{'id': 'recursive_case'}]

        # TODO: Tim, can we include 'fail'-used as a general hint?

    except socket.timeout as ex:
        return [{'id': 'timeout'}]

    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    return []

