name = 'memb/2'
slug = 'Preveri (poišči), če je dani element v seznamu'

description = '''\
<p><code>memb(E, L)</code>: <code>E</code> je element seznama <code>L</code>.</p>
<pre>
?- memb(X, [1,2,3]).
  X = 1 ;
  X = 2 ;
  X = 3.
?- memb(1, [3,2,X]).
  X = 1.
</pre>'''

plan = ['''\
<p>Kje se lahko skriva iskani element <code>X</code>? Spomni se, da ima seznam dva dela, glavo in rep.
Torej sta možnosti dve! ;)</p>
''', '''\
<p><img src="[%@resource base_case.svg%]" /></p>
<p>Na seznam v prologu lahko gledamo kot na vrsto ljudi, ki vstopajo na avtobus. Šofer vidi samo prvega,
ostali so skriti v repu seznama. Torej je iskani element <code>X</code> lahko prvi v vrsti ali pa...</p>
''', '''\
<p>Prvi element v seznamu enostavno "preiščemo". Kako pa pridem do preostalih? Prvega odstranim in ponovim
iskanje z manjšim seznamom. Če je <code>[H|T]</code> cel seznam, je <code>T</code> seznam brez prvega elementa.
Ker je nov seznam manjši, sem tudi problem zmanjšal.</p>
''']

hint = {
    'eq_instead_of_equ': '''\
<p>Operator <code>==</code> je strožji od operatorja <code>=</code> v smislu, da je za slednjega dovolj,
da elementa lahko naredi enaka (unifikacija). Morda z uporabo <code>=</code> narediš predikat
<code>memb/2</code> delujoč tudi v kakšni drugi smeri.</p>
<p>Seveda pa lahko nalogo rešiš brez obeh omenjenih operatorjev, spomni se, da lahko unifikacijo narediš
implicitno že kar v argumentih predikata (glavi stavka).</p>
''',

    'eq_instead_of_equ_markup': '''\
<p>Morda bi bil bolj primeren operator za unifikacijo (=)?</p>
''',

    'base_case': '''\
<p><img src="[%@resource base_case.svg%]" /></p>
<p>Si pomislil na robni pogoj? Kaj je najbolj enostaven primer, ko je element v seznamu?
Do katerega elementa najlažje prideš?</p>
''',

    'recursive_case': '''\
<p>Robni primer deluje. Kaj pa rekurzivni, splošni, primer?</p>
''',

    'predicate_always_false': '''\
<p>Vse kaže, da tvoj predikat vedno vrne "false". Si mu dal pravilno ime, si se morda pri imenu zatipkal?</p>
<p>Če je ime pravilno, se morda splača preveriti tudi, če se nisi zatipkal kje drugje,
je morda kakšna pika namesto vejice ali obratno, morda kakšna spremenljivka z malo začetnico?</p>
<p>Možno je seveda tudi, da so tvoji pogoji prestrogi ali celo nemogoči (kot bi bila npr. zahteva,
da je <code>X</code> hkrati starš in sestra od <code>Y</code> ali kaj podobno zlobnega).</p>
''',

    'timeout': '''\
<p>Je morda na delu potencialno neskončna rekurzija? Kako se bo ustavila?</p>
<p>Morda pa je kriv tudi manjkajoč, neustrezen ali preprosto nekompatibilen (s splošnim primerom) robni pogoj?</p>
''',

    'final_hint': '''\
<p>Predikat <code>memb/2</code> se da uporabljati še za marsikaj drugega kot samo za preverjanje, če je
nek element v seznamu! Pravzaprav ga bomo večinoma uporabljali v "obratni" smeri kot "vrni mi nek element
<code>X</code>, ki je v seznamu <code>L</code>". V bistvu si spisal generator elementov iz seznama.</p>
<p>Poskusi prolog vprašati tole:</p>
<p><code>?- memb(Coin, [1,2,5,10,20,50,100,200]).</code></p>
<p>ali pa tole:</p>
<p><code>?- memb(Operator, [+, -, *, /]).</code></p>
<p>Znaš prolog vprašati s katerimi tremi kovanci dobim skupno vsoto 30 centov? Operator
<code>=:=</code> pomeni aritmetično primerjanje. Koliko rešitev je? ;)</p>
''',
}
