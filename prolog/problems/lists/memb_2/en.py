name = 'memb/2'
slug = 'find elements in list'

description = '''\
<p><code>memb(M, L)</code>: <code>M</code> is an element of list <code>L</code>.</p>
<pre>
?- memb(X, [1,2,3]).
  X = 1 ;
  X = 2 ;
  X = 3.
?- memb(1, [3,2,X]).
  X = 1.
</pre>'''

plan = ['''\
<p>Where can we find the searched for element <code>X</code>? Remember that the list has two parts: the head and
the tail. Therefore, there are two possibilies! ;)</p>
''', '''\
<p><img src="[%@resource base_case.svg%]" /></p>
<p>In prolog we can understand a list like a queue of people waiting for a bus. The driver only sees
the first person in the queue, the others are hidden in the list's tail. So the element <code>X</code>
can either be at the start of the queue or...</p>
''', '''\
<p>It's easy to look at ("search for") the head of the list. But how do we search the tail? Simply,
we remove the first element and repeat the search with the smaller list (tail). If <code>[H|T]</code>
is our whole list, then <code>T</code> is this same list without the first element.
Since the new list (tail) is smaller, we reduced the problem and thus facilitated the use of recursion.</p>
''']

hint = {
    'eq_instead_of_equ': '''\
<p>The operator <code>==</code> is "stricter" than operator <code>=</code> in the sense that
for the latter it is enough to be able to make the two operands equal (unification). Perhaps by using <code>=</code>
you can make the predicate <code>memb/2</code> more general (e.g. able to work with output arguments becoming inputs).</p>
<p>Of course, you can also solve the exercise without explicit use of either of these two operators, just
remember that unification is implicitly performed with the predicate's arguments (head of clause).</p>
''',

    'eq_instead_of_equ_markup': '''\
<p>Perhaps the operator for unification (=) would be better?</p>
''',

    'base_case': '''\
<p><img src="[%@resource base_case.svg%]" /></p>
<p>Did you think of a base case? What is the simplest possible case if the element is present in the list?
Which element in the list is the easiest to access?</p>
''',

    'recursive_case': '''\
<p>The base case is ok. However, what about the general recursive case?</p>
''',

    'predicate_always_false': '''\
<p>It seems your predicate is <emph>always</emph> "false". Did you give it the correct name,
or is it perhaps misspelled?</p>
<p>If the name is correct, check whether something else is misspelled, perhaps there is a full stop instead of
a comma or vice versa, or maybe you typed a variable name in lowercase?</p>
<p>It is, of course, also possible that your conditions are too restrictive, or even impossible to satisfy
(as would be, for example, the condition that an empty list <code>[]</code> is equal to a list with
exactly three elements <code>[A,B,C]</code>,
or something similarly impossible).</p>
''',

    'timeout': '''\
<p>Is there an infinite recursion at work here? How will it ever stop?</p>
<p>Or perhaps is there a missing, faulty, or simply incompatible (with the general recursive case) base case?</p>
''',

    'final_hint': '''\
<p>Predicate <code>memb/2</code> is useful for much more than just checking whether a list contains
a given element or not. Actually, most of the time, it's used in "a different direction" as
"return some element <code>X</code> from list <code>L</code>". In fact you've just written yourself
a generator of elements from the list.</p>
<p>Try the following query:</p>
<p><code>?- memb(Coin, [1,2,5,10,20,50,100,200]).</code></p>
<p>or this one:</p>
<p><code>?- memb(Operator, [+, -, *, /]).</code></p>
<p>Can you form a query to ask prolog how do we get a sum of 30 cents with exactly three coins?
The operator <code>=:=</code> is used to check for arithmetic equality. How many solutions are there? ;)</p>
''',
}
