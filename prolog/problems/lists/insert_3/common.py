from operator import itemgetter
import prolog.engine
import prolog.util
import socket
from server.hints import Hint, HintPopup
import server.problems

id = 106
number = 1
visible = True
facts = None

solution = '''\
del106(X, [X|T], T).
del106(X, [Y|T], [Y|L]) :-
  del106(X, T, L).
insert(X, List, BigList) :-
  del106(X, BigList, List).
'''

hint_type = {
    'eq_instead_of_equ_markup': HintPopup('eq_instead_of_equ_markup'),
    'eq_instead_of_equ': Hint('eq_instead_of_equ'),
    'predicate_always_false': Hint('predicate_always_false'),
    'base_case': Hint('base_case'),
    'timeout': Hint('timeout'),
    'lost_heads': Hint('lost_heads'),
    'recursive_case': Hint('recursive_case'),
    'leading_heads_all_x': Hint('leading_heads_all_x'),
    'ins_results_in_empty_list': Hint('ins_results_in_empty_list'),
    'ins_results_in_arbitrary_result': Hint('ins_results_in_arbitrary_result'),
}

test_cases = [
    ('insert(l, [], X)',
        [{'X': '[l]'}]),
    ('insert(X, [h, t, w, j], [A, B, C, D, z])',
        [{'A': 'h', 'B': 't', 'C': 'w', 'D': 'j', 'X': 'z'}]),
    ('insert(i, [A, j], [c, X, j])',
        [{'A': 'c', 'X': 'i'}]),
]

def test(code, aux_code):
    n_correct = 0
    engine_id = None
    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)
        if engine_id is not None and 'error' not in map(itemgetter(0), output):
            # Engine successfully created, and no syntax error in program.
            for query, answers in test_cases:
                if prolog.engine.check_answers(engine_id, query=query, answers=answers, timeout=1.0):
                    n_correct += 1
    except socket.timeout:
        pass
    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_cases)}}]
    return n_correct, len(test_cases), hints

def hint(code, aux_code):
    tokens = prolog.util.tokenize(code)

    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)

        # strict equality testing instead of simple matching
        # this is usually (but not necessarily) wrong
        targets = [prolog.util.Token('EQ', '==')]
        marks = [(t.pos, t.pos + len(t.val)) for t in tokens if t in targets]
        if marks:
            return [{'id': 'eq_instead_of_equ_markup', 'start': m[0], 'end': m[1]} for m in marks] + \
                   [{'id': 'eq_instead_of_equ'}]

        # recursion is getting bigger and bigger


        # inserting an element succeeds with an emtpy list result
        if prolog.engine.ask_truthTO(engine_id, 'insert(_, _, [])'):
            return [{'id': 'ins_results_in_empty_list'}]

        # inserting an element succeeds with an arbitrary result
        if prolog.engine.ask_truth(engine_id, 'insert(_, _, L), var(L)'):
            return [{'id': 'ins_results_in_arbitrary_result'}]

        # missing/failed base case
        if not prolog.engine.ask_one(engine_id, 'insert(qQ, [qa,qb,qc], [qQ,qa,qb,qc])'):
            return [{'id': 'base_case'}]

        # target predicate seems to always be false
        if not prolog.engine.ask_truth(engine_id, 'insert(_, [_,_,_,_,_,_], _)'):
            return [{'id': 'predicate_always_false'}]

        # forgot to put list head(s) back in the list when deleting an element
        if prolog.engine.ask_truth(engine_id, 'insert(q, [a,b,c,d,x,y,z], [q,x,y,z])'):
            return [{'id': 'lost_heads'}]

        # used [X|T] instead of more general [H|T] in recursive case
        if prolog.engine.ask_truth(engine_id, \
                'findall(NL, insert(q, [a,b,c], NL), [[q,a,b,c], [q,q,b,c], [q,q,q,c], [q,q,q,q]]).') and \
           not prolog.engine.ask_truth(engine_id, 'insert(q, [a,b,c,x,y,z], [a,b,c,q,x,y,z])'):
            return [{'id': 'leading_heads_all_x'}]

        # base case works, the recursive doesn't (but it doesn't timeout)
        # this may be left as the last, most generic hint
        if not prolog.engine.ask_truth(engine_id, 'insert(qQ, [qa,qb,qc], [qa,qb,qQ,qc])'):
            return [{'id': 'recursive_case'}]


    except socket.timeout as ex:
        return [{'id': 'timeout'}]

    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    return []
