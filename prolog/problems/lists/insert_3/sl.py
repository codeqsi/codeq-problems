name = 'insert/3'
slug = 'Vstavi element na poljubno mesto v seznamu'

description = '''\
<p><code>insert(X, L1, L2)</code>: seznam <code>L2</code> dobimo iz <code>L1</code> tako, da vstavimo element <code>X</code> na poljubno mesto. Predikat naj vrne vse možne rešitve, eno za drugo.</p>
<pre>
?- insert(1, [2,3], L).
  L = [1,2,3] ;
  L = [2,1,3] ;
  L = [2,3,1].
</pre>'''

plan = ['''
<p><img src="[%@resource plan.svg%]" /></p>
<p>Kam v seznam lahko vstavimo element <code>X</code>? Spomni se, da ima seznam dva dela, glavo in rep.
Torej sta možnosti dve! Tako je, na tem mestu dve, v repu pa spet lahko vstavimo ali v njegovo glavo ali v rep od repa.
In tako dalje, rekurzija na pomoč!</p>
''', '''\
<p>Kaj je najenostavnejša možnost? Vstavimo na začetek!</p>
''', '''\
<p>Kako vstavim nekam v rep? Seznam razbijem na glavo in rep, rekurzivno (problem je za en element manjši!)
vstavim v rep in ob vračanju iz rekurzije ne pozabim na prej "odtrgano" glavo.</p>
''', '''\
<p>Rekurzivni korak: če predpostavim, da je <code>NewTail</code> rep z že vstavljenim elementom <code>X</code>,
potem je <code>[H|NewTail]</code> celoten seznam z vstavljenim elementom <code>X</code>.</p>
''']

hint = {
    'eq_instead_of_equ': '''\
<p>Operator <code>==</code> je strožji od operatorja <code>=</code> v smislu, da je za slednjega dovolj,
da elementa lahko naredi enaka (unifikacija). Morda z uporabo <code>=</code> narediš predikat
<code>insert/3</code> delujoč tudi v kakšni drugi smeri. To bo kasneje znalo priti prav!</p>
<p>Seveda pa lahko nalogo rešiš brez obeh omenjenih operatorjev, spomni se, da lahko unifikacijo narediš
implicitno že kar v argumentih predikata (glavi stavka).</p>
''',

    'eq_instead_of_equ_markup': '''\
<p>Morda bi bil bolj primeren operator za unifikacijo (=)?</p>
''',

    'base_case': '''\
<p><img src="[%@resource base_case.svg%]" /></p>
<p>Si pomislil na robni pogoj? Na katero mesto v seznamu najlažje vstaviš nek element?</p>
''',

    'recursive_case': '''\
<p>Robni primer deluje. Kaj pa rekurzivni, splošni, primer?</p>
''',

    'predicate_always_false': '''\
<p>Vse kaže, da tvoj predikat vedno vrne "false". Si mu dal pravilno ime, si se morda pri imenu zatipkal?</p>
<p>Če je ime pravilno, se morda splača preveriti tudi, če se nisi zatipkal kje drugje,
je morda kakšna pika namesto vejice ali obratno, morda kakšna spremenljivka z malo začetnico?</p>
<p>Možno je seveda tudi, da so tvoji pogoji prestrogi ali celo nemogoči (kot bi bila npr. zahteva,
da je <code>X</code> hkrati starš in sestra od <code>Y</code> ali kaj podobno zlobnega).</p>
''',

    'timeout': '''\
<p>Je morda na delu potencialno neskončna rekurzija? Kako se bo ustavila?</p>
<p>Morda pa je kriv tudi manjkajoč, neustrezen ali preprosto nekompatibilen (s splošnim primerom) robni pogoj?</p>
''',

    'ins_results_in_empty_list': '''\
<p>Kako je lahko rezultat vstavljanja v seznam prazen seznam?</p>
<p>Če je to tvoj robni pogoj, ga še enkrat premisli: kaj je rezultat vstavljanja?</p>
''',

    'ins_results_in_arbitrary_result': '''\
<p>Kako je lahko rezultat vstavljanja v seznam poljuben seznam oz. karkoli?</p>
<p>Če je to tvoj robni pogoj, ga še enkrat premisli: kaj je rezultat vstavljanja?</p>
''',

    'lost_heads': '''\
<p><img src="[%@resource lost_heads.svg%]" /></p>
<p>Element je vstavljen, ampak vsi pred njim so se pa izgubili, kajne?
Si pozabil dati glavo nazaj na začetek seznama, ko se vračaš iz rekurzije?</p>
<p>Poskusi postaviti naslednje vprašanje prologu in preglej <em>vse</em> rešitve:</p>
<p><code>?- insert(q, [a,b,c,d], L).</code></p>
''',

    'leading_heads_all_x': '''\
<p><img src="[%@resource leading_heads_all_x.svg%]" /></p>
<p>Si morda pozabil (copy/paste?) in uporabil <code>[X|T]</code> namesto bolj splošnega
<code>[H|T]</code> v rekurzivnem primeru?</p>
<p>Od spodnjih dveh vprašanj prologu prvo deluje, drugo pa ne.</p>
<p><code>?- insert(d, [d,d,d,d,e,f,g], L).</code></p>
<p><code>?- insert(d, [a,b,c,d,e,f,g], L).</code></p>
''',
}
