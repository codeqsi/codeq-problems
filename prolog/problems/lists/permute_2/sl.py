name = 'permute/2'
slug = 'Generiraj permutacije elementov v seznamu'

description = '''\
<p><code>permute(L1, L2)</code>: seznam <code>L2</code> je permutacija vrstnega reda elementov v seznamu <code>L1</code>.</p>
<pre>
?- permute([1,2,3], L).
  L = [1,2,3] ;
  L = [1,3,2] ;
  L = [2,1,3] ;
  L = [2,3,1] ;
  L = [3,1,2] ;
  L = [3,2,1].
</pre>'''


plan = ['''\
<p><img src="[%@resource plan.svg%]" /></p>
<p>Poskusi rekurzirvno pripraviti eno možnost, a pusti prologu svobodo. Le-ta bo potem sam od sebe
vračal vse možne rešitve.</p>
''', '''\
<p>Možnosti je več, a ena je naslednja: kaj, če vzamem prvi element stran, problem je s tem manjši in
ga lahko prepustim rekurziji, potem pa ta element vstavim. Kaj ga pa vstavim? Vstavi ga na vse možne načine.</p>
''', '''\
<p>Če predpostavim, da rekurzija permutira rep <code>T</code> in vrne "permutirani" rep <code>PT</code> in
ob vračanju v <code>PT</code> nekam vstavim glavo <code>H</code>, potem bom dobil neko permutacijo začetnega
seznama, ki je oblike <code>[H|T]</code>.</p>
''']

hint = {
    'eq_instead_of_equ': '''\
<p>Operator <code>==</code> je strožji od operatorja <code>=</code> v smislu, da je za slednjega dovolj,
da elementa lahko naredi enaka (unifikacija).</p>
<p>Seveda pa lahko nalogo rešiš brez obeh omenjenih operatorjev, spomni se, da lahko unifikacijo narediš
implicitno že kar v argumentih predikata (glavi stavka).</p>
''',

    'eq_instead_of_equ_markup': '''\
<p>Morda bi bil bolj primeren operator za unifikacijo (=)?</p>
''',

    'base_case': '''\
<p>Si pomislil na robni pogoj? Kaj je najbolj enostaven primer? Kaj, če je seznam kar prazen?</p>
''',

    'base_case_arbitrary': '''\
<p>Kako je lahko rezultat permutiranja seznama poljuben seznam oz. karkoli?</p>
<p>Če je tvoj robni pogoj podoben <code>permute([], _)</code>,
ga še enkrat premisli: kaj je rezultat permutiranja, kaj vračaš? Robni pogoj je vedno dokončno specificirana
rešitev, tu načeloma ni neznank (<code>_</code> ali spremenljivk brez prirejenih vrednosti) v tem kar se vrača.</p>
''',

    'unsuccessful_conc_use': '''\
<p>Uporabljaš <code>conc/3</code>? Pri tej nalogi to ni najboljša ideja, morda pa pride prav kakšna druga že
rešena naloga.</p>
''',

    'forcing_result_onto_recursion': '''
<p>Ne vsiljuj rekurziji kaj naj vrne, prepusti se ji. To je tisti del, ko narediš predpostavko,
če je ta izpolnjena, potem bo tvoje pravilo delovalo za večji primer.</p>
<p>Je tvoj rekurzivni klic oblike <code>permute(T, [H|...])</code>? S tem vsiljuješ rekurziji
da mora <em>vrniti</em> tudi glavo, ki je sploh ne pozna, ker si jo ravnokar vzel stran! To moraš
narediti ti z rezultatom, ki ga rekurzija vrne. Skratka, element <code>H</code> dodaj izven rekurzivnega klica.</p>
''',

    'recursive_case': '''\
<p>Robni primer deluje. Kaj pa rekurzivni, splošni, primer?</p>
''',

    'no_insert_or_delete': '''\
<p>Robni primer deluje. Kaj pa rekurzivni, splošni, primer? Morda se ti splača uporabiti kakšno že rešeno
nalogo s seznami?</p>
''',

    'predicate_always_false': '''\
<p>Vse kaže, da tvoj predikat vedno vrne "false". Si mu dal pravilno ime, si se morda pri imenu zatipkal?</p>
<p>Če je ime pravilno, se morda splača preveriti tudi, če se nisi zatipkal kje drugje,
je morda kakšna pika namesto vejice ali obratno, morda kakšna spremenljivka z malo začetnico?</p>
<p>Možno je seveda tudi, da so tvoji pogoji prestrogi ali celo nemogoči (kot bi bila npr. zahteva,
da je <code>X</code> hkrati starš in sestra od <code>Y</code> ali kaj podobno zlobnega).</p>
''',

    'timeout': '''\
<p>Je morda na delu potencialno neskončna rekurzija? Kako se bo ustavila?</p>
<p>Morda pa je kriv tudi manjkajoč, neustrezen ali preprosto nekompatibilen (s splošnim primerom) robni pogoj?</p>
''',
}
