from operator import itemgetter
import prolog.engine
import prolog.util
import socket
from server.hints import Hint, HintPopup
import server.problems

id = 107
number = 8
visible = True
facts = None

solution = '''\
del107(X, [X|T], T).
del107(X, [Y|T], [Y|L]) :-
  del107(X, T, L).
permute([], []).
permute(L, [X|P]) :-
  del107(X, L, L1),
  permute(L1, P).
'''

hint_type = {
    'eq_instead_of_equ_markup': HintPopup('eq_instead_of_equ_markup'),
    'eq_instead_of_equ': Hint('eq_instead_of_equ'),
    'predicate_always_false': Hint('predicate_always_false'),
    'base_case': Hint('base_case'),
    'recursive_case': Hint('recursive_case'),
    'no_insert_or_delete': Hint('no_insert_or_delete'),
    'timeout': Hint('timeout'),
    'base_case_arbitrary': Hint('base_case_arbitrary'),
    'unsuccessful_conc_use': Hint('unsuccessful_conc_use'),
    'forcing_result_onto_recursion': Hint('forcing_result_onto_recursion'),
}

test_cases = [
    ('permute([], X)',
        [{'X': '[]'}]),
    ('permute([a, i, o, e], [i, X, o, e])',
        [{'X': 'a'}]),
    ('setof(L, permute([e, b, l], L), X)',
        [{'X': '[[b, e, l], [b, l, e], [e, b, l], [e, l, b], [l, b, e], [l, e, b]]'}]),
]

def test(code, aux_code):
    n_correct = 0
    engine_id = None
    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)
        if engine_id is not None and 'error' not in map(itemgetter(0), output):
            # Engine successfully created, and no syntax error in program.
            for query, answers in test_cases:
                if prolog.engine.check_answers(engine_id, query=query, answers=answers, timeout=1.0):
                    n_correct += 1
    except socket.timeout:
        pass
    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_cases)}}]
    return n_correct, len(test_cases), hints

def hint(code, aux_code):
    tokens = prolog.util.tokenize(code)

    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)

        # strict equality testing instead of simple matching
        # this is usually (but not necessarily) wrong
        targets = [prolog.util.Token('EQ', '==')]
        marks = [(t.pos, t.pos + len(t.val)) for t in tokens if t in targets]
        if marks:
            return [{'id': 'eq_instead_of_equ_markup', 'start': m[0], 'end': m[1]} for m in marks] + \
                   [{'id': 'eq_instead_of_equ'}]

        # general unsuccessful use of conc (not appropriate for this exercise)
        if prolog.util.Token('NAME', 'conc') in tokens:
            return [{'id': 'unsuccessful_conc_use'}]

        # recursion is getting bigger and bigger


        # forcing result onto recursion; case of divide(T, [H1|L1], [H2|L2])
        if not prolog.engine.ask_truthTO(engine_id, 'permute([a,b,c], _)') and \
           prolog.engine.ask_truthTO(engine_id,
           'asserta( permute([cob], [yowza,cob]) ), permute([yowza,cob], [cob]), retract( permute([cob], [yowza,cob]) )'):
            return [{'id': 'forcing_result_onto_recursion'}]

        # base case succeeds with arbitrary result
        if prolog.engine.ask_truthTO(engine_id, 'permute([], P), var(P)'):
            return [{'id': 'base_case_arbitrary'}]

        # missing/failed base case
        if not prolog.engine.ask_truth(engine_id, 'permute([], [])'):
            return [{'id': 'base_case'}]

#        # target predicate seems to always be false
#        if not prolog.engine.ask_truthTO(engine_id, 'divide([_,_,_,_,_], _, _)') and \
#           not prolog.engine.ask_truthTO(engine_id, 'divide([_,_,_,_,_,_], _, _)'):
#            return [{'id': 'predicate_always_false'}]

        # base cases work, the recursive doesn't (but it doesn't timeout)
        # this may be left as the last, most generic hint
        if not prolog.engine.ask_truth(engine_id, 'permute([qa,qb], [qb,qa])'):
            if prolog.util.Token('NAME', 'insert') in tokens or prolog.util.Token('NAME', 'del') in tokens:
                return [{'id': 'recursive_case'}]
            else:
                return [{'id': 'no_insert_or_delete'}]

    except socket.timeout as ex:
        return [{'id': 'timeout'}]

    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    return []
