name = 'last_elem/2'
slug = 'Vrni zadnji element seznama'

description = '''\
<p><code>last_elem(L, E)</code>: <code>E</code> je zadnji element seznama <code>L</code>.</p>
<pre>
?- last_elem([1,2,3], X).
  X = 3.
?- last_elem([3,2,X], 1).
  X = 1.
</pre>
'''

plan = ['''\
<p>Do prvega elementa v seznamu je enostavno priti, do zadnjega se je treba rekurzivno prebiti.</p>
''', '''\
<p>Seznam lahko razbijem na glavo in rep in iščem dalje v repu. Problem sem zmanjšal (rep je manjši
od celega seznama), torej lahko uporabim rekurzijo.</p>
''', '''\
<p>Če je <code>X</code> zadnji element repa <code>T</code>, potem je <code>X</code> tudi zadnji element
celega seznama, ki je oblike <code>[H|T]</code>.</p>
''']

hint = {
    'eq_instead_of_equ': '''\
<p>Operator <code>==</code> je strožji od operatorja <code>=</code> v smislu, da je za slednjega dovolj,
da elementa lahko naredi enaka (unifikacija).</p>
<p>Seveda pa lahko nalogo rešiš brez obeh omenjenih operatorjev, spomni se, da lahko unifikacijo narediš
implicitno že kar v argumentih predikata (glavi stavka).</p>
''',

    'eq_instead_of_equ_markup': '''\
<p>Morda bi bil bolj primeren operator za unifikacijo (=)?</p>
''',

    'base_case': '''\
<p>Si pomislil na robni pogoj? Kaj je najbolj enostaven primer? Kaj, če ima seznam samo en element?</p>
''',

    '[]_should_not_succeed': '''\
<p>Kako si lahko uspešno našel zadnji element v praznem seznamu? Verjetno potrebuješ drugačen robni pogoj.</p>
''',

    'list_returned': '''\
<p>Vračaš seznam namesto elementa.</p>
''',

    'clumsy_conc_use': '''\
<p>Uporabljaš <code>conc/3</code>? Zanimiva ideja. Ne pozabi, da mora drugi seznam, ki ga konkateniraš
biti <em>dolžine ena</em>, če hočeš doseči to kar želiš. Torej vzorec oblike <code>[X]</code>, kajne?</p>
''',

    'unsuccessful_conc_use': '''\
<p>Uporabljaš <code>conc/3</code>? Zanimiva ideja, da se rešiti tudi tako. Vendar boš moral še malo premisliti.
Ne pozabi, <code>conc/3</code> ima tri argumente, vsi so seznami. Premisli, kakšen vzorec potrebuješ...</p>
''',

    'recursive_case': '''\
<p>Robni primer deluje. Kaj pa rekurzivni, splošni, primer?</p>
''',

    'predicate_always_false': '''\
<p>Vse kaže, da tvoj predikat vedno vrne "false". Si mu dal pravilno ime, si se morda pri imenu zatipkal?</p>
<p>Če je ime pravilno, se morda splača preveriti tudi, če se nisi zatipkal kje drugje,
je morda kakšna pika namesto vejice ali obratno, morda kakšna spremenljivka z malo začetnico?</p>
<p>Možno je seveda tudi, da so tvoji pogoji prestrogi ali celo nemogoči (kot bi bila npr. zahteva,
da je <code>X</code> hkrati starš in sestra od <code>Y</code> ali kaj podobno zlobnega).</p>
''',

    'timeout': '''\
<p>Je morda na delu potencialno neskončna rekurzija? Kako se bo ustavila?</p>
<p>Morda pa je kriv tudi manjkajoč, neustrezen ali preprosto nekompatibilen (s splošnim primerom) robni pogoj?</p>
''',

    'final_hint': '''\
<p>Zanimivost: se spomniš kako smo rekli, da lahko predikat <code>conc/3</code> uporabljamo za iskanje vzorcev?
Tudi zadnji element seznama je na nek način vzorec. Kaj se zgodi, če konkateniram poljuben seznam <code>_</code>
in seznam dolžine ena (v tem vrstnem redu)? Seznam dolžine ena seveda zapišemo kot <code>[Element]</code>.</p>
<p>Poskusi prolog vprašati tole:</p>
<p><code>?- conc(_, [Element], [a,b,c,d,e,f,q]).</code></p>
<p>Znaš sedaj dobiti zadnji element s pomočjo <code>conc/3</code>? To bo že še prišlo prav. Seveda pa je dostop
do zadnjega elementa še vedno potraten, O(n). Zato, če ni važno od kje ali kam dostopati, se splača vedno delati
s prvim elementom.</p>
<p>Kaj pa naredi tole? ;)</p>
<p><code>?- conc([a,b,c], [q], L).</code></p>
''',
}
