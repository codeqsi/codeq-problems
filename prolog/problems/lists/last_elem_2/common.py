from operator import itemgetter
import prolog.engine
import prolog.util
import socket
from server.hints import Hint, HintPopup
import server.problems

id = 10000
number = 6
visible = True
facts = None

solution = '''\
last_elem([X], X).
last_elem([_|T], X) :-
  last_elem(T, X).
'''

hint_type = {
    'eq_instead_of_equ_markup': HintPopup('eq_instead_of_equ_markup'),
    'eq_instead_of_equ': Hint('eq_instead_of_equ'),
    'predicate_always_false': Hint('predicate_always_false'),
    'base_case': Hint('base_case'),
    'recursive_case': Hint('recursive_case'),
    'timeout': Hint('timeout'),
    'final_hint': Hint('final_hint'),
    '[]_should_not_succeed': Hint('[]_should_not_succeed'),
    'list_returned': Hint('list_returned'),
    'clumsy_conc_use': Hint('clumsy_conc_use'),
    'unsuccessful_conc_use': Hint('unsuccessful_conc_use'),
}

test_cases = [
    ('last_elem([X], c)',
        [{'X': 'c'}]),
    ('last_elem([y, l, r, g, a], X)',
        [{'X': 'a'}]),
]

def test(code, aux_code):
    n_correct = 0
    engine_id = None
    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)
        if engine_id is not None and 'error' not in map(itemgetter(0), output):
            # Engine successfully created, and no syntax error in program.
            for query, answers in test_cases:
                if prolog.engine.check_answers(engine_id, query=query, answers=answers, timeout=1.0):
                    n_correct += 1
    except socket.timeout:
        pass
    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    hints = [{'id': 'test_results', 'args': {'passed': n_correct, 'total': len(test_cases)}}]
    if n_correct == len(test_cases):
        tokens = prolog.util.tokenize(code)
        if prolog.util.Token('NAME', 'conc') not in tokens or prolog.util.Token('NAME', 'append') not in tokens:
            hints += [{'id': 'final_hint'}]
    return n_correct, len(test_cases), hints

def hint(code, aux_code):
    tokens = prolog.util.tokenize(code)

    try:
        engine_id, output = prolog.engine.create(code=code+aux_code, timeout=1.0)

        # strict equality testing instead of simple matching
        # this is usually (but not necessarily) wrong
        targets = [prolog.util.Token('EQ', '==')]
        marks = [(t.pos, t.pos + len(t.val)) for t in tokens if t in targets]
        if marks:
            return [{'id': 'eq_instead_of_equ_markup', 'start': m[0], 'end': m[1]} for m in marks] + \
                   [{'id': 'eq_instead_of_equ'}]

        # recursion is getting bigger and bigger

        # using conc, but clumsily: conc(_, X, L) instead of conc(_, [X], L)
        if prolog.util.Token('NAME', 'conc') in tokens and \
           prolog.engine.ask_truthTO(engine_id, 'last_elem([q,a,b,c], [q,a,b,c])'):
            return [{'id': 'clumsy_conc_use'}]

        # general unsuccessful use of conc, but not specifically detected
        # return general_conc hint, not to confuse/force the student into a non-conc solution
        if prolog.util.Token('NAME', 'conc') in tokens:
            return [{'id': 'unsuccessful_conc_use'}]

        # succeeds when asked to return the last element of an empty list
        if prolog.engine.ask_truthTO(engine_id, 'last_elem([], _)'):
            return [{'id': '[]_should_not_succeed'}]

        # returns a list, not an element as required
        if prolog.engine.ask_one(engine_id, 'last_elem([qQ], [qQ])'):  # 'last_elem([qQ], X), is_list(X)' with ask_one?
            return [{'id': 'list_returned'}]

        # missing/failed base case
        if not prolog.engine.ask_truth(engine_id, 'last_elem([qQ], qQ)'):
            return [{'id': 'base_case'}]

        # target predicate seems to always be false
        if not prolog.engine.ask_truthTO(engine_id, 'last_elem(_, _)') and \
           not prolog.engine.ask_truthTO(engine_id, 'last_elem([_,_,_,_,_,_], _)'):
            return [{'id': 'predicate_always_false'}]

        # base case works, the recursive doesn't (but it doesn't timeout)
        # this may be left as the last, most generic hint
        if not prolog.engine.ask_truth(engine_id, 'last_elem([qa,qb,qQ,qc], qc)'):
            return [{'id': 'recursive_case'}]

    except socket.timeout as ex:
        return [{'id': 'timeout'}]

    finally:
        if engine_id:
            prolog.engine.destroy(engine_id)

    return []
