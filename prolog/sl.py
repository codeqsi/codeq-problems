name = 'Prolog'
description = '''\
Uvodni tečaj prologa.
<a target="_blank" href="[%@resource intro_sl.html%]">Napotki za uporabo aplikacije.</a>
'''

hint = {
    'no_hint': '''\
<p>Namig ne obstaja.</p>
''',

    'program_already_correct': '''\
<p>Tvoj program je pravilen!</p>
''',

    'system_error': ['''\
<p><span style="color: red;">Sistemska napaka:</span> [%=message%].</p>
'''],

    'test_results': '''\
<p>Program je opravil [%=passed%] / [%=total%] testov.</p>
''',

    'syntax_error': '''\
<p>Program vsebuje sintaktične napake! Prolog je vrnil naslednja sporočila:</p>
<pre>
[%=messages%]
</pre>
''',

    'monkey_main': '''\
<p>Uspelo mi je popraviti tvoj program. Napotki so prikazani v urejevalniku.</p>
''',

    'monkey_change': '''\
<p>Popravi ta cilj.</p>
''',

    'monkey_insert': '''\
<p>Dodaj novo pravilo oziroma cilj.</p>
''',

    'monkey_remove': '''\
<p>Odstrani ta cilj.</p>
''',

    'monkey_highlight': None,  # used to highlight erroneous tokens

    'monkey_buggy_literal': [
'''\
<p>Razmisli, ali so vrednosti v označenem delu programa pravilne:</p>
<ul class="code" style="list-style: square inside; padding-left: 2em;">[%=fragments%]</ul>
''',
'''\
<p>Ali so vse vrednosti pravilnih tipov (število/seznam/struktura/…)? Ali označeni argumenti nastopajo v pravih relacijah s spremenljivkami?</p>
'''
    ],

    'monkey_buggy_variable': [
'''\
<p>Spremenljivka <code>[%=variable%]</code> v programu ne nastopa pravilno. Posebej bodi pozoren na označene dele programa:</p>
<ul class="code" style="list-style: square inside; padding-left: 2em;">[%=fragments%]</ul>
''',
'''\
<p>Ali so vsi cilji, v katerih nastopa spremenljivka <code>[%=variable%]</code>, pravilni?
Preveri, da gre za pravi predikat ali operator, in da so označeni argumenti smiselni.</p>
<p>Ali spremenljivka <code>[%=variable%]</code> povsod označuje isto vrednost?
V prologu vse pojavitve iste spremenljivke znotraj pravila zmeraj označujejo isti objekt (npr. osebo ali število).</p>
'''
    ],

    'monkey_singleton': [
'''\
<p>Označena spremenljivka <code>[%=variable%]</code> se v pravilu pojavi le na enem mestu:</p>
<ul class="code" style="list-style: square inside; padding-left: 2em;">[%=fragments%]</ul>
''',
'''\
<p>To običajno pomeni napako. Razmisli, ali bi vrednost, ki jo označuje <code>[%=variable%]</code>, morala nastopati še kje v pravilu. Preveri tudi, da v pravilu ni kakšnih tipkarskih napak.</p>
'''
    ],

    'monkey_missing': '''\
<p>Program je deloma pravilen, vendar še ni dokončan. Morda v katerem od stavkov manjka kakšen cilj, ali pa potrebuješ še eno pravilo.</p>
''',

    'monkey_unknown': [
'''\
<p>Preveri označene dele programa:</p>
<ul class="code" style="list-style: square inside; padding-left: 2em;">[%=fragments%]</ul>
''',
'''\
<p>Ta vzorec se ne pojavlja v doslej znanih rešitvah te naloge. To lahko pomeni, da je napačen, ali pa, da tvoj program implementira novo rešitev.</p>
'''
    ],

    'noncapitalised_variable': '''\
<p>Preveri kodo, označeni so deli, kjer bi morda moral uporabiti velike črke, ki označujejo spremenljivke v prologu.</p>
''',

    'noncapitalised_variable_markup': '''\
<p>Je prav, da je to pisano z malo?</p>
''',

    'fail_rule': '''\
<p>Pravila, ki nikoli ne drži, nikoli ne potrebuješ.</p>
''',
}
